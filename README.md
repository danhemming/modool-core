# modool

A Kotlin based framework for building headless, API based editors for coding languages.  A language specification 
(defined in kotlin) can be used to generate a lexer, parser and strongly typed document API that allows the user to query and edit 
code using code.  

## Motivation
Modool is particularly useful for iterative Model Driven Development.  Most code generation tools are only
useful at the start of development as they overwrite manual contributions.  Modool allows for tools that can 
perform non-destructive editing.

# Open Development

This project is developed openly, encouraging community contributions and collaboration. You're welcome to join us in shaping its evolution, reporting issues, and proposing enhancements.

## Attention: This Repository is Under Development

Please note that this repository is currently in active development and is not yet ready for production use. While significant progress has been made, there may still be bugs, incomplete features, or other issues that could affect its stability and reliability in a production environment.

We encourage you to explore, experiment, and provide feedback. However, exercise caution and refrain from using this software in critical or production systems until it reaches a stable release. Your contributions, bug reports, and suggestions are highly appreciated as we work towards making this project production-ready.

# License

modool is licensed under the Apache License 2.0. You are free to use, modify, and distribute this software according to the terms of the license. For more details, please see the LICENSE file included in the repository.


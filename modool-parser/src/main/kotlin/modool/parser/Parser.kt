package modool.parser

import modool.core.content.repository.ElementRepository
import modool.parser.rule.ParserRulable
import modool.parser.strategy.ParseStrategy
import modool.parser.strategy.StrictParseStrategy
import modool.core.content.token.TokenReader

interface Parser<out R : ParserRulable> {
    val root: R

    fun parse(
            source: TokenReader,
            elements: ElementRepository,
            strategy: ParseStrategy = StrictParseStrategy): ParseResult
}
package modool.parser

class ParseException(source: String, cause: Throwable? = null)
    : RuntimeException("Unable to parse: $source", cause)
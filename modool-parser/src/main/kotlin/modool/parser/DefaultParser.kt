package modool.parser

import modool.core.content.repository.ElementRepository
import modool.parser.rule.ParserRulable
import modool.parser.strategy.ParseStrategy
import modool.core.content.token.TokenReader

abstract class DefaultParser<out R> : Parser<R>
        where R : ParserRulable {

    override fun parse(
            source: TokenReader,
            elements: ElementRepository,
            strategy: ParseStrategy): ParseResult {
        val parseContext = ParserContext(source, elements)
        return root.parse(parseContext, strategy)
    }
}
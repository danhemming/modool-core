package modool.parser

import modool.core.meta.element.ElementMeta
import modool.core.content.ContentOrigin
import modool.core.content.repository.ElementRepository
import modool.parser.stack.*
import modool.core.content.token.TokenReader
import modool.core.content.token.TokenReference

class ParserContext(
        val source: TokenReader,
        val elementRepo: ElementRepository,
        val contentOrigin: ContentOrigin.Source = ContentOrigin.PrimarySource,
        private var stackFrame: ParsableContextStackFrame = ParseContextStackFrame(null)) {

    init {
        source.reset()
    }

    val currentStackFrame get() = stackFrame
    var currentAnchorableStackFrame: AnchorableParserContextStackFrame? = null
    var currentElementalStackFrame: ElementalParserContextStackFrame? = null
    var currentElementUpdater: ElementUpdater? = null

    val currentElementStackFrameLocked: Boolean
        get() = currentElementalStackFrame?.isLocked ?: false

    var currentIndentFrame: ParserContextIndentFrame? = null
    var delimiterRestorePoint: TokenReference? = null

    fun nearestElementalStackFrameForAssignableType(element: ElementMeta): ElementalParserContextStackFrame? {

        var testElementalStackFrame: ParsableContextStackFrame? = currentElementalStackFrame

        while (testElementalStackFrame != null) {
            if (testElementalStackFrame is ElementalParserContextStackFrame
                    && testElementalStackFrame.isAttemptingType(element)) {
                return testElementalStackFrame
            }
            testElementalStackFrame = testElementalStackFrame.parent
        }

        return null
    }

    inline fun matching(matcher: () -> ParseResult): ParseResult {
        val restorePoint = source.createReference()
        val result = try {
            matcher()
        } catch (ex: Exception) {
            restoreTo(restorePoint)
            throw ex
        }
        if (result !is ParseResult.Success) {
            restoreTo(restorePoint)
        }
        return result
    }

    inline fun matchNextText(matcher: (found: Boolean) -> ParseResult): ParseResult {
        val startPosition = source.createReference()
        val terminalPosition = if (source.moveNextTerminal()) source.createReference() else null

        val result = try {
            matcher(terminalPosition != null)
        } catch (ex: Exception) {
            restoreTo(startPosition)
            throw ex
        }

        when {
            result is ParseResult.Success -> {
                delimiterRestorePoint = null
                // Assumes matcher hasn't progressed the source
                acceptCurrent()
            }
            delimiterRestorePoint != null -> {
                // This is an edge case, sometimes a delimited match without a close can over match
                delimiterRestorePoint?.let { drp ->

                    source.moveTo(drp)
                    val restoredStartPosition = when {
                        // The restore point may be a whitespace or some other non-text content
                        source.isTerminal() -> drp
                        source.moveNextTerminal() -> source.createReference()
                        else -> {
                            restoreTo(startPosition)
                            return result
                        }
                    }

                    val restoreResult = try {
                        matcher(true)
                    } catch (ex: Exception) {
                        restoreTo(startPosition)
                        throw ex
                    }

                    if (restoreResult is ParseResult.Success) {
                        elementRepo.contractElement(drp)
                        delimiterRestorePoint = null
                        restoreTo(restoredStartPosition)
                        acceptCurrent()
                        return restoreResult
                    } else {
                        restoreTo(startPosition)
                    }
                }
            }
            else -> restoreTo(startPosition)
        }
        return result
    }

    inline fun matchNextContent(matcher: (found: Boolean) -> ParseResult): ParseResult {
        val startPosition = source.createReference()
        val contentPosition = if (source.moveNextContent()) source.createReference() else null

        val result = try {
            matcher(contentPosition != null)
        } catch (ex: Exception) {
            restoreTo(startPosition)
            throw ex
        }
        if (result is ParseResult.Success) {
            // Assumes matcher hasn't progressed source
            acceptCurrent()
        } else {
            restoreTo(startPosition)
        }
        return result
    }

    inline fun matchNextMarker(matcher: (found: Boolean) -> ParseResult): ParseResult {
        val startPosition = source.createReference()
        val contentPosition = if (source.moveNextMarker()) source.createReference() else null

        val result = try {
            matcher(contentPosition != null)
        } catch (ex: Exception) {
            restoreTo(startPosition)
            throw ex
        }
        if (result is ParseResult.Success) {
            // Assumes matcher hasn't progressed source
            acceptCurrent()
        } else {
            restoreTo(startPosition)
        }
        return result
    }

    /**
     * We try to avoid locking in elements before we are sure but in some cases a clean up is required.  This can occur
     * when sub elements are fully realised before a parent element can be locked in.
     */
    fun restoreTo(reference: TokenReference) {
        source.moveTo(elementRepo.removeElements(reference))
    }

    /**
     * Use anchors (unique constructs within the parsing context) to decide if we can now commit to an element
     */
    fun acceptCurrent() {

        var curr: ParsableContextStackFrame? = stackFrame
        var closestProgressingElemental: ElementalParserContextStackFrame? = null
        var closestChoiceAboveProgressingElement: ChoiceParserContextStackFrame? = null

        // Locate a non-committed element with a choice above it
        while (curr != null) {
            if (curr is ElementalParserContextStackFrame) {
                if (!curr.isLocked) {
                    if (closestProgressingElemental == null) {
                        closestProgressingElemental = curr
                    }
                } else {
                    break
                }
            } else if (closestProgressingElemental != null && curr is ChoiceParserContextStackFrame) {
                closestChoiceAboveProgressingElement = curr
                break
            }

            curr = curr.parent
        }

        if (closestProgressingElemental != null) {
            // If there is a choice above our non-committed element then if the token is an anchor we are able to use it
            if (closestChoiceAboveProgressingElement != null) {
                if (closestChoiceAboveProgressingElement.isAnchorInScope(source)) {
                    closestProgressingElemental.lock()
                }
            } else if (closestProgressingElemental.isLocalAnchor(source)) {
                // If there is no choice encasing the element we can just lock it in if the token is unique
                closestProgressingElemental.lock()
            }
        }
    }

    fun peekStackFrame(): ParsableContextStackFrame {
        return stackFrame
    }

    fun pushStackFrame(frame: ParsableContextStackFrame) {
        stackFrame = frame
    }

    fun popStackFrame() {
        stackFrame = stackFrame.parent!!
    }

    /**
     * Unrecoverable failures occur when we are committed to an element but have no way forward
     */
    fun unrecoverableFail(
            message: String,
            restoreTo: TokenReference? = null,
            reference: TokenReference = source.createReference()): ParseResult.Fail {
        val result = ParseResult.UnrecoverableFailure(FailureMessage(message, reference))
        restoreTo?.let { restoreTo(it) }
        return result
    }

    fun fail(
            message: String,
            restoreTo: TokenReference? = null,
            reference: TokenReference = source.createReference()): ParseResult.Fail {
        val result = fail { FailureMessage(message, reference) }
        restoreTo?.let { restoreTo(it) }
        return result
    }

    fun fail(message: String,
             innerResult: ParseResult,
             restoreTo: TokenReference? = null,
             reference: TokenReference = source.createReference()): ParseResult.Fail {

        val result = fail {
            FailureMessage(message, reference, if (innerResult is ParseResult.FailWithMessage) innerResult.failure else null)
        }
        restoreTo?.let { restoreTo(it) }
        return result
    }

    fun fail(message: String,
             failureMessages: List<FailureMessage>,
             restoreTo: TokenReference? = null,
             reference: TokenReference = source.createReference()): ParseResult.Fail {

        val result = fail { FailureMessage(message, reference, failureMessages) }
        restoreTo?.let { restoreTo(it) }
        return result
    }

    inline fun fail(errorMessage: () -> FailureMessage): ParseResult.Fail {
        return when {
            this.currentStackFrame.optional -> ParseResult.FailButOKInContext
            // ElementParserRule will create it's own frame and will therefore override any existing optional
            currentElementalStackFrame?.isLocked == true ->
                ParseResult.UnrecoverableFailure(errorMessage())
            else -> ParseResult.FailWithMessage(errorMessage())
        }
    }

    /**
     * Creates a short context that allows rules to behave in a non-fatal way if errors occur
     */
    inline fun <T> optionally(action: () -> T): T {
        val previousOptional = currentStackFrame.optional
        currentStackFrame.optional = true
        val result = action()
        currentStackFrame.optional = previousOptional
        return result
    }
}
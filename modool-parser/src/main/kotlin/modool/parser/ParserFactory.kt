package modool.parser

import modool.parser.rule.ParserRulable

interface ParserFactory {
    fun createDocumentParserRule(): ParserRulable
}
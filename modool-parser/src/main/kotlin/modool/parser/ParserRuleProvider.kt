package modool.parser

import modool.parser.rule.ParserRulable

interface ParserRuleProvider {
    fun createParserRule(): ParserRulable
}
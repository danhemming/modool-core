package modool.parser

import modool.core.content.token.TokenReference

abstract class ParseFailure

class FailureMessage(private val message: String,
                     private val sourceReference: TokenReference,
                     private val innerFailures: List<FailureMessage>)
    : ParseFailure() {

    constructor(
            message: String,
            sourceReference: TokenReference,
            innerFailure: FailureMessage? = null) : this(
            message, sourceReference, if (innerFailure == null) emptyList() else listOf(innerFailure))

    override fun toString(): String {
        // FIXME: now we don't know we have a token, we can't do toPretty .., probably need to inject message instead
        return "$message at ${sourceReference}. ${innerFailures.joinToString(separator = ",")}"
    }
}

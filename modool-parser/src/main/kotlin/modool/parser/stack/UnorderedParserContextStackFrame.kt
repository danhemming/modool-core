package modool.parser.stack

import modool.core.content.token.ConstantTerminalDescriptor
import modool.core.content.token.TokenReference
import modool.core.content.token.TokenReferenceRange
import modool.core.meta.property.PropertyMeta
import modool.parser.ParserContext

class UnorderedParserContextStackFrame(
        parent: ParsableContextStackFrame? = null,
        anchors: Collection<ConstantTerminalDescriptor>)
    : ChoiceParserContextStackFrame(parent, anchors) {

    private val viewPropertyMembers = mutableMapOf<PropertyMeta, ViewProperty>()

    fun addViewPropertyMember(property: PropertyMeta, start: TokenReference, end: TokenReference) {
        val viewProperty = viewPropertyMembers[property]
                ?: ViewProperty.Simple(property).apply { viewPropertyMembers[property] = this }
        viewProperty.addEntry(start, end)
    }

    fun addPropertyGroupViewPropertyMember(
            propertyGroup: PropertyMeta,
            property: PropertyMeta,
            start: TokenReference,
            end: TokenReference) {
        val viewProperty = viewPropertyMembers[property]
                ?: ViewProperty.Group(propertyGroup, property).apply { viewPropertyMembers[property] = this }
        viewProperty.addEntry(start, end)
    }

    fun viewPropertiesIterator(): Iterator<ViewProperty> {
        return viewPropertyMembers.values.iterator()
    }

    sealed class ViewProperty {
        private val _entries = mutableListOf<TokenReferenceRange>()

        val entries : List<TokenReferenceRange> get() = _entries

        fun addEntry(start: TokenReference, end: TokenReference) {
            _entries.add(TokenReferenceRange(start, end))
        }

        class Simple(val property: PropertyMeta) : ViewProperty()
        class Group(val propertyGroup: PropertyMeta, val property: PropertyMeta) : ViewProperty()
    }
}

inline fun <T> ParserContext.withUnorderedStackFrame(
        anchors: Collection<ConstantTerminalDescriptor>,
        action: UnorderedParserContextStackFrame.() -> T): T {

    val newStackFrame = UnorderedParserContextStackFrame(parent = peekStackFrame(), anchors = anchors)
    val previousAnchorStackFrame = currentAnchorableStackFrame
    val previousElementUpdateReceiver = currentElementUpdater
    currentAnchorableStackFrame = newStackFrame
    currentElementUpdater = newStackFrame
    pushStackFrame(newStackFrame)
    try {
        return action(newStackFrame)
    } finally {
        popStackFrame()
        currentAnchorableStackFrame = previousAnchorStackFrame
        previousElementUpdateReceiver?.addPropertySetters(newStackFrame)
        currentElementUpdater = previousElementUpdateReceiver
    }
}
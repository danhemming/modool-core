package modool.parser.stack

open class ParseContextStackFrame(
        final override val parent: ParsableContextStackFrame? = null) : ContextState,
        ParsableContextStackFrame {

    private val state = HashMap<Any, Any>()
    private var _optional = false

    /**
     * A non-recursive indicator that the current context is optional
     */
    override var optional: Boolean
        get() = _optional
        set(value) {
            _optional = value
        }

    override fun getValue(key: Any): Any? {
        val value = state[key]
        return if (value == null && parent != null) {
            parent.getValue(key)
        } else {
            value
        }
    }

    @Suppress("UNCHECKED_CAST")
    override fun <T> get(key: Any) = getValue(key) as T

    override fun set(key: Any, value: Any) {
        state[key] = value
    }
}
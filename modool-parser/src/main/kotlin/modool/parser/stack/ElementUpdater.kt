package modool.parser.stack

import modool.core.content.element.ElementPropertySetter
import modool.core.content.token.TokenReference

interface ElementUpdater {

    val matchedPositions: Sequence<TokenReference> get() = propertySetters.asSequence().map { it.contentReference }
    val propertySetters: List<ElementPropertySetter>

    fun addPropertySetter(propertySetter: ElementPropertySetter)
    fun addPropertySetters(updaters: ElementUpdater)
    fun clearPropertySetters()
}
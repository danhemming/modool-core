package modool.parser.stack

import modool.core.content.token.TokenReader

interface AnchorableParserContextStackFrame : ParsableContextStackFrame {
    fun isAnchorInScope(source: TokenReader): Boolean
}


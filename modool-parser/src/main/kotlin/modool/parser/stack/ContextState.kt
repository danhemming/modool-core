package modool.parser.stack

interface ContextState {
    operator fun <T> get(key: Any): T
    operator fun set(key: Any, value: Any)
}
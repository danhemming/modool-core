package modool.parser.stack

import modool.core.content.token.ConstantTerminalDescriptor
import modool.parser.ParseResult
import modool.parser.ParserContext

class ListParserContextStackFrame(
        parent: ParsableContextStackFrame? = null,
        anchors: Collection<ConstantTerminalDescriptor>)
    : AnchorParserContextStackFrame(parent, anchors)


/**
 * Creates a stack frame to capture the state associated with a list, basically allows error recovery to know which
 * tokens to avoid when skipping tokens
 */
inline fun ParserContext.withListStackFrame(
        anchors: Collection<ConstantTerminalDescriptor>,
        action: ListParserContextStackFrame.() -> ParseResult): ParseResult {

    val newStackFrame = ListParserContextStackFrame(parent = peekStackFrame(), anchors = anchors)
    newStackFrame.optional = currentStackFrame.optional

    val previousAnchorStackFrame = currentAnchorableStackFrame
    val previousDelimiterRestorePoint = delimiterRestorePoint
    currentAnchorableStackFrame = newStackFrame
    delimiterRestorePoint = null
    pushStackFrame(newStackFrame)
    try {
        val result = action(newStackFrame)
        if (result !is ParseResult.Success) {
            delimiterRestorePoint = previousDelimiterRestorePoint
        }
        return result
    } finally {
        popStackFrame()
        currentAnchorableStackFrame = previousAnchorStackFrame
    }
}
package modool.parser.stack

import modool.core.content.element.ElementPropertySetter
import modool.core.content.token.ConstantTerminalDescriptor
import modool.parser.ParserContext

open class ChoiceParserContextStackFrame(
        parent: ParsableContextStackFrame? = null,
        anchors: Collection<ConstantTerminalDescriptor>)
    : AnchorParserContextStackFrame(parent, anchors),
        ElementUpdater {

    private val _updates = mutableListOf<ElementPropertySetter>()

    override val propertySetters: List<ElementPropertySetter>
        get() = _updates

    override fun addPropertySetter(propertySetter: ElementPropertySetter) {
        _updates.add(propertySetter)
    }

    override fun addPropertySetters(updaters: ElementUpdater) {
        updaters.propertySetters.forEach { addPropertySetter(it) }
    }

    override fun clearPropertySetters() {
        _updates.clear()
    }
}

/**
 * Creates a stack frame to capture the state associated with a choice, a collection of unique constants is provided
 * to allow us to determine when a choice is locked-in i.e. no other choices are permitted.
 */
inline fun <T> ParserContext.withChoiceStackFrame(
        anchors: Collection<ConstantTerminalDescriptor>,
        action: ChoiceParserContextStackFrame.() -> T): T {

    val newStackFrame = ChoiceParserContextStackFrame(parent = peekStackFrame(), anchors = anchors)
    val previousAnchorStackFrame = currentAnchorableStackFrame
    val previousElementUpdateReceiver = currentElementUpdater
    currentAnchorableStackFrame = newStackFrame
    currentElementUpdater = newStackFrame
    pushStackFrame(newStackFrame)
    try {
        return action(newStackFrame)
    } finally {
        popStackFrame()
        currentAnchorableStackFrame = previousAnchorStackFrame
        previousElementUpdateReceiver?.addPropertySetters(newStackFrame)
        currentElementUpdater = previousElementUpdateReceiver
    }
}
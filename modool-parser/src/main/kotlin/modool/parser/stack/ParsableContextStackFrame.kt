package modool.parser.stack


interface ParsableContextStackFrame {
    /**
     * A non-recursive indicator that the current context is optional
     */
    val parent: ParsableContextStackFrame?
    var optional: Boolean
    fun getValue(key: Any): Any?
    @Suppress("UNCHECKED_CAST")
    operator fun <T> get(key: Any): T
    operator fun set(key: Any, value: Any)
}


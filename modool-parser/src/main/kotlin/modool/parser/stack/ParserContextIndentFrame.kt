package modool.parser.stack

import modool.core.config.SemanticIndentationStrategy
import modool.parser.ParserContext
import modool.core.content.token.TokenReader

class ParserContextIndentFrame(
        private val previousIndentationSize: Int,
        val indentationLevel: Int,
        val strategy: SemanticIndentationStrategy) {

    var startingIndentationSize: Int = -1

    fun isValid(source: TokenReader): Boolean {
        return if (source.isNewLine()) {
            val spaceCount = source.getSpaceCount()
            val tabCount = source.getTabCount()
            strategy.calculateMarginSize(spaceCount, tabCount) ==
                    calculateStartingIndentSize(spaceCount, tabCount)
        } else {
            false
        }
    }

    private fun calculateStartingIndentSize(spaceCount: Int, tabCount: Int): Int {
        if (startingIndentationSize == -1) {
            startingIndentationSize = strategy.calculateStartingIndentationSize(
                    previousIndentationSize,
                    indentationLevel,
                    spaceCount,
                    tabCount)
        }

        return startingIndentationSize
    }
}

/**
 * Creates a stack frame to capture the expectation of a certain level of indent.
 */
inline fun <T> ParserContext.withIndentFrame(
        strategy: SemanticIndentationStrategy,
        action: ParserContextIndentFrame.() -> T): T {

    // There is no need to pollute the stack frame with hierarchical indents as it is not traversed
    val newIndentFrame = ParserContextIndentFrame(
            previousIndentationSize = currentIndentFrame?.startingIndentationSize ?: 0,
            indentationLevel = (currentIndentFrame?.indentationLevel ?: 0) + 1,
            strategy = strategy)
    val previousIndentFrame = currentIndentFrame
    currentIndentFrame = newIndentFrame
    try {
        return action(newIndentFrame)
    } finally {
        currentIndentFrame = previousIndentFrame
    }
}
package modool.parser.stack

import modool.core.content.element.ElementPropertySetter
import modool.core.content.token.ConstantTerminalDescriptor
import modool.core.meta.element.ElementMeta
import modool.parser.ParseResult
import modool.parser.ParserContext
import modool.parser.rule.ParserRulable
import modool.core.content.token.TokenRangeOption
import modool.core.content.token.TokenReader
import modool.core.content.token.TokenReference

class ElementParserContextStackFrame(
        parent: ParsableContextStackFrame? = null,
        anchors: Collection<ConstantTerminalDescriptor>,
        private val rule: ParserRulable,
        private val startReference: TokenReference,
        private val element: ElementMeta)
    : AnchorParserContextStackFrame(parent, anchors),
        ElementUpdater,
        ElementalParserContextStackFrame {

    private val _updates = mutableListOf<ElementPropertySetter>()

    override val propertySetters: List<ElementPropertySetter>
        get() = _updates

    override fun isInfiniteLoop(currentRule: ParserRulable, reference: TokenReference): Boolean {
        return currentRule == this.rule && reference == this.startReference
    }

    override fun addPropertySetter(propertySetter: ElementPropertySetter) {
        _updates.add(propertySetter)
    }

    override fun addPropertySetters(updaters: ElementUpdater) {
        updaters.propertySetters.forEach { addPropertySetter(it) }
    }

    override fun clearPropertySetters() {
        _updates.clear()
    }

    override var isLocked: Boolean = false
        private set

    override fun isLocalAnchor(source: TokenReader): Boolean {
        return anchors.find { source.isTerminalMatchable(it) } != null
    }

    override fun isAttemptingType(other: ElementMeta): Boolean {
        return other.isType(element)
    }

    override fun lock() {
        isLocked = true
    }

    override fun createElement(context: ParserContext) {
        val endOfElementPosition = context.elementRepo.createElement(
                startReference,
                context.source.createReference(),
                TokenRangeOption.START_EXCLUSIVE_END_INCLUSIVE,
                context.contentOrigin,
                element,
                propertySetters)

        context.source.moveTo(endOfElementPosition)
    }
}

/**
 * Creates a new stack frame to capture the state necessary to build an element
 */
inline fun  ParserContext.withElementalStackFrame(
        rule: ParserRulable,
        anchors: Collection<ConstantTerminalDescriptor>,
        element: ElementMeta,
        action: ElementalParserContextStackFrame.() -> ParseResult): ParseResult {

    if (currentElementalStackFrame?.isInfiniteLoop(rule, source.createReference()) == true) {
        return fail(Message.CANNOT_RECURSE_WITHOUT_PROGRESS)
    }

    @Suppress("UNCHECKED_CAST")
    val newStackFrame = ElementParserContextStackFrame(
            parent = peekStackFrame(),
            anchors = anchors,
            rule = rule,
            startReference = source.createReference(),
            element = element)

    return this@withElementalStackFrame.withElementalStackFrame(newStackFrame, action)
}

object Message {
    const val CANNOT_RECURSE_WITHOUT_PROGRESS = "Recursion would cause an infinite loop"
}
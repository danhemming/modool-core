package modool.parser.stack

import modool.core.content.token.ConstantTerminalDescriptor
import modool.core.content.token.TokenReader

/**
 * Represents a context with terminals that are entirely unique in this context.
 */
abstract class AnchorParserContextStackFrame(
        parent: ParsableContextStackFrame? = null,
        protected val anchors: Collection<ConstantTerminalDescriptor>)
    : ParseContextStackFrame(parent),
        AnchorableParserContextStackFrame {

    override fun isAnchorInScope(source: TokenReader): Boolean {
        val anchor = anchors.find { source.isTerminalMatchable(it) }
        if (anchor != null) {
            return true
        }

        var current = parent
        while (current != null) {
            if (current is AnchorableParserContextStackFrame) {
                return current.isAnchorInScope(source)
            }
            current = current.parent
        }

        return false
    }
}
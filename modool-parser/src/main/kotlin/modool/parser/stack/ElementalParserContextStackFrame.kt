package modool.parser.stack

import modool.core.meta.element.ElementMeta
import modool.parser.ParseResult
import modool.parser.ParserContext
import modool.parser.rule.ParserRulable
import modool.core.content.token.TokenReader
import modool.core.content.token.TokenReference

interface ElementalParserContextStackFrame
    : AnchorableParserContextStackFrame,
        ElementUpdater {
    val isLocked: Boolean
    fun isInfiniteLoop(currentRule: ParserRulable, reference: TokenReference): Boolean
    fun isLocalAnchor(source: TokenReader): Boolean
    fun isAttemptingType(other: ElementMeta): Boolean
    fun lock()
    fun createElement(context: ParserContext)
}

/**
 * Creates a new stack frame to capture the state necessary to build an element
 */
inline fun ParserContext.withElementalStackFrame(
        newStackFrame: ElementalParserContextStackFrame,
        action: ElementalParserContextStackFrame.() -> ParseResult): ParseResult {

    newStackFrame.optional = currentStackFrame.optional
    val previousElementStackFrame = currentElementalStackFrame
    val previousAnchorStackFrame = currentAnchorableStackFrame
    val previousElementUpdateReceiver = currentElementUpdater
    currentElementalStackFrame = newStackFrame
    currentAnchorableStackFrame = newStackFrame
    currentElementUpdater = newStackFrame
    pushStackFrame(newStackFrame)
    try {
        val result = action(newStackFrame)
        if (previousElementStackFrame == null && result == ParseResult.ElementMatch) {
            elementRepo.commitRootElement(source.createReference())
        }
        return result
    } finally {
        popStackFrame()
        currentElementalStackFrame = previousElementStackFrame
        currentAnchorableStackFrame = previousAnchorStackFrame
        currentElementUpdater = previousElementUpdateReceiver
    }
}


package modool.parser.stack

import modool.core.content.element.ElementPropertySetter
import modool.core.meta.element.ElementMeta
import modool.parser.ParserContext
import modool.parser.rule.ParserRulable
import modool.core.content.token.TokenReader
import modool.core.content.token.TokenReference

class ExpressionStackFrame(
        override val parent: ParsableContextStackFrame?,
        private val expression: ElementMeta)
    : ElementalParserContextStackFrame {

    override var optional: Boolean = false

    override val isLocked: Boolean
        get() = false

    override fun isInfiniteLoop(currentRule: ParserRulable, reference: TokenReference): Boolean {
        return false
    }

    override fun isLocalAnchor(source: TokenReader): Boolean {
        return false
    }

    override fun isAttemptingType(other: ElementMeta): Boolean {
        return other.isType(expression)
    }

    override fun lock() {
        throw RuntimeException("Unable to lock sub expression stack frame")
    }

    override fun createElement(context: ParserContext) {
        throw RuntimeException("Unable to create sub expression via stack frame")
    }

    override fun isAnchorInScope(source: TokenReader): Boolean {
        return false
    }

    override fun getValue(key: Any): Any? {
        return null
    }

    override fun <T> get(key: Any): T {
        throw RuntimeException("Unable to get typed value for sub expression via stack frame")
    }

    override fun set(key: Any, value: Any) {
        return
    }

    override val propertySetters: List<ElementPropertySetter>
        get() = emptyList()

    override fun addPropertySetter(propertySetter: ElementPropertySetter) {
        throw RuntimeException("Unable to add update to sub expression via stack frame")
    }

    override fun addPropertySetters(updaters: ElementUpdater) {
        if (updaters.propertySetters.isNotEmpty()) {
            throw RuntimeException("Unable to add update to sub expression via stack frame")
        }
    }

    override fun clearPropertySetters() {
        throw RuntimeException("Unable to clear updates on sub expression via stack frame")
    }
}
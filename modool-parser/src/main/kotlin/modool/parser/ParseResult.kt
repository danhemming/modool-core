package modool.parser

import modool.core.content.token.TokenReference

sealed class ParseResult {

    object NotAttempted : ParseResult()

    object Nothing : ParseResult()

    abstract class Success : ParseResult()

    object OK : Success()

    object TokenMatch : Success()

    object TerminatorMatch : Success()

    object ElementMatch : Success()

    object PropertyContainerMatch : Success()

    abstract class Content(val reference: TokenReference): Success()

    class CompleteContent(source: TokenReference): Content(source)

    class PartialContent(source: TokenReference): Content(source)

    abstract class Fail : ParseResult()

    open class FailWithMessage(val failure: FailureMessage) : Fail()

    class UnrecoverableFailure(failure: FailureMessage) : FailWithMessage(failure)

    object ExpectedButMissing : Fail()

    object FailButOKInContext : Fail()

}
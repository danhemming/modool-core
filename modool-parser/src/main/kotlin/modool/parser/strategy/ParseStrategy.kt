package modool.parser.strategy

import modool.parser.ParseResult
import modool.parser.ParserContext
import modool.parser.rule.ParserRulable

interface ParseStrategy {
    fun parse(context: ParserContext, rule: ParserRulable): ParseResult
    fun parse(context: ParserContext, rules: List<ParserRulable>): ParseResult
}
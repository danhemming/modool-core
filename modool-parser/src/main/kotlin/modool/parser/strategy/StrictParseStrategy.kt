package modool.parser.strategy

import modool.parser.ParseResult
import modool.parser.ParserContext
import modool.parser.processor.StrictParseProcessor
import modool.parser.rule.ParserRulable

object StrictParseStrategy: ParseStrategy {

    override fun parse(context: ParserContext, rules: List<ParserRulable>): ParseResult {
        return StrictParseProcessor.parse(context, rules, this)
    }

    override fun parse(context: ParserContext, rule: ParserRulable): ParseResult {
        return StrictParseProcessor.parse(context, rule, this)
    }
}
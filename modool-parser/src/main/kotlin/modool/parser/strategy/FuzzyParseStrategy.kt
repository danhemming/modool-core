package modool.parser.strategy

import modool.parser.ParseResult
import modool.parser.ParserContext
import modool.parser.processor.FuzzyParseProcessor
import modool.parser.rule.ParserRulable

object FuzzyParseStrategy : ParseStrategy {

    override fun parse(context: ParserContext, rules: List<ParserRulable>): ParseResult {
        return FuzzyParseProcessor.parse(context, rules, this)
    }

    override fun parse(context: ParserContext, rule: ParserRulable): ParseResult {
        return FuzzyParseProcessor.parse(context, rule, this)
    }
}
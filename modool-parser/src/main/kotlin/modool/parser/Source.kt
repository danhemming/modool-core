package modool.parser

import java.util.regex.Pattern

// Use this class to store error messages etc.
class Source(val text: String) {
    val length = text.length

    fun isEOF(index: Int) = index >= length

    fun isThereContentAt(index: Int) = index in 0..(length - 1)

    fun getSelection(start: Int, end: Int) = text.substring(start, end)

    fun getCharAt(index: Int) = text[index]

    fun matchAt(index: Int, pattern: Pattern): String? {
        val matcher = pattern.matcher(text)
        matcher.region(index, length)

        return if (matcher.lookingAt()) {
            matcher.group()
        } else {
            null
        }
    }

    fun containsAt(index: Int, value: String, ignoreCase: Boolean): Boolean {
        val valueEnd = index + value.length
        if (valueEnd > length) {
            return false
        }

        return text.regionMatches(index, value, 0, value.length, ignoreCase)
    }

    fun indexOfNextBreak(index: Int): Int {
        val endOfLine = indexOfNext(index, "\n", true)
        return if (endOfLine == -1) length else endOfLine
    }

    fun indexOfNext(index: Int, value: String, ignoreCase: Boolean): Int {
        return text.indexOf(value, index, ignoreCase)
    }
}
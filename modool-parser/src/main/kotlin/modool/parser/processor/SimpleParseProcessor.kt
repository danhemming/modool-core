package modool.parser.processor

import modool.parser.ParseResult
import modool.parser.ParserContext
import modool.parser.rule.ParserRulable
import modool.parser.strategy.ParseStrategy

abstract class SimpleParseProcessor : ParseProcessor {

    override fun parse(
            context: ParserContext,
            rules: List<ParserRulable>,
            strategy: ParseStrategy): ParseResult {

        when (rules.count()) {
            0 -> return ParseResult.ExpectedButMissing
            1 -> return parse(context, rules.first(), strategy)
        }

        return context.matching {
            var isNothing = true

            rules.forEach {
                val result = it.parse(context, strategy)
                if (result is ParseResult.Fail) {
                    return@matching recover(context, result, it, rules, strategy)
                }
                if (result is ParseResult.TerminatorMatch) {
                    return@matching ParseResult.OK
                }
                if (result !is ParseResult.Nothing) {
                    isNothing = false
                }
            }

            return@matching if (isNothing) ParseResult.Nothing else ParseResult.OK
        }
    }

    override fun parse(
            context: ParserContext,
            rule: ParserRulable,
            strategy: ParseStrategy): ParseResult {

        val result = rule.parse(context, strategy)

        return if (result is ParseResult.Success) {
            result
        } else {
            recover(context, result, rule, strategy)
        }
    }

    abstract fun recover(
            context: ParserContext,
            currentResult: ParseResult,
            currentRule: ParserRulable,
            strategy: ParseStrategy): ParseResult

    abstract fun recover(
            context: ParserContext,
            currentResult: ParseResult,
            currentRule: ParserRulable,
            rules: List<ParserRulable>,
            strategy: ParseStrategy): ParseResult
}
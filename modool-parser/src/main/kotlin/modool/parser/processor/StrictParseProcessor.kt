package modool.parser.processor

import modool.parser.ParseResult
import modool.parser.ParserContext
import modool.parser.rule.ParserRulable
import modool.parser.strategy.ParseStrategy

object StrictParseProcessor : SimpleParseProcessor() {

    override fun recover(
            context: ParserContext,
            currentResult: ParseResult,
            currentRule: ParserRulable,
            strategy: ParseStrategy): ParseResult {

        return currentResult
    }

    override fun recover(
            context: ParserContext,
            currentResult: ParseResult,
            currentRule: ParserRulable,
            rules: List<ParserRulable>,
            strategy: ParseStrategy): ParseResult {

        return currentResult
    }
}
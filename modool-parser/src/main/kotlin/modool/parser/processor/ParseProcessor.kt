package modool.parser.processor

import modool.parser.ParseResult
import modool.parser.ParserContext
import modool.parser.rule.ParserRulable
import modool.parser.strategy.ParseStrategy

interface ParseProcessor {
    fun parse(context: ParserContext, rule: ParserRulable, strategy: ParseStrategy): ParseResult
    fun parse(context: ParserContext, rules: List<ParserRulable>, strategy: ParseStrategy): ParseResult
}
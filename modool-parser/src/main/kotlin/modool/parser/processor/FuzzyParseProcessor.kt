package modool.parser.processor

import modool.core.content.signifier.tag.terminal.Keyword
import modool.parser.ParseResult
import modool.parser.ParserContext
import modool.parser.rule.*
import modool.parser.rule.property.ElementPropertySetterPropertyListRule
import modool.parser.stack.AnchorableParserContextStackFrame
import modool.parser.strategy.ParseStrategy
import modool.core.content.token.TokenRangeOption

object FuzzyParseProcessor : SimpleParseProcessor() {

    override fun recover(context: ParserContext,
                         currentResult: ParseResult,
                         currentRule: ParserRulable,
                         rules: List<ParserRulable>,
                         strategy: ParseStrategy): ParseResult {

        // Attempt to use the failed rule on subsequent tokens
        var recoveredResult = recover(context, currentResult, currentRule, strategy)

        // Attempt a balanced match
        if (recoveredResult !is ParseResult.Success) {
            recoveredResult = attemptCompletionWithBalancedMatch(context, currentRule, currentResult, strategy)
        }

        val acceptableFailureCount =
                if (context.currentElementStackFrameLocked) MAXIMUM_CONSECUTIVE_LOCKED_IN_RULE_FAILURES
                else MAXIMUM_CONSECUTIVE_UNKNOWN_RULE_FAILURES

        return attemptCompletionFromNextRule(context, recoveredResult, currentRule, rules, acceptableFailureCount, strategy)
    }

    /**
     * Try to pick out a balanced matching structure e.g. (...).  If found we do then also call the property updater
     * if applicable
     */
    private fun attemptCompletionWithBalancedMatch(
            context: ParserContext,
            currentRule: ParserRulable,
            currentResult: ParseResult,
            strategy: ParseStrategy): ParseResult {

        val unwrappedRule = currentRule.unwrap() as? DelimitedParserRule ?: return currentResult

        val open = unwrappedRule.open ?: return currentResult
        val close = unwrappedRule.close ?: return currentResult

        // We have no hope of recovering if the rules can be omitted or just whitespace
        if (open is OptionalRule || close is OptionalRule ||
                open is WhitespaceParserRule || close is WhitespaceParserRule) {
            return currentResult
        }

        return context.matching {
            val startReference = context.source.createReference()

            var foundStart = false
            var lookAheadCount = 0

            while (lookAheadCount < MAXIMUM_TEXT_TOKEN_LOOK_AHEAD) {
                // Do open.parse here to avoid recursive recovery
                val result = context.optionally { open.parse(context, strategy) }

                if (result is ParseResult.Success) {
                    foundStart = true
                    break
                }

                context.source.moveNextTerminal()
                lookAheadCount++
            }

            if (!foundStart) {
                return@matching currentResult
            }

            var requiredCloseMatches = 1

            do {
                when {
                    context.optionally { open.parse(context, strategy) } is ParseResult.Success -> requiredCloseMatches++
                    context.optionally { close.parse(context, strategy) } is ParseResult.Success -> requiredCloseMatches--
                    else -> context.source.moveNextTerminal()
                }

                if (requiredCloseMatches == 0) {

                    val propertyEndReference =
                            context.elementRepo.createPropertyGroup(
                                    startReference,
                                    context.source.createReference(),
                                    TokenRangeOption.START_EXCLUSIVE_END_INCLUSIVE,
                                    context.contentOrigin)

                    context.source.moveTo(propertyEndReference)

                    if (currentRule is ElementPropertySetterPropertyListRule) {
                        return@matching currentRule.updateOnSuccess(context, propertyEndReference, ParseResult.PropertyContainerMatch)
                    } else {
                        return@matching ParseResult.PropertyContainerMatch
                    }
                }
            } while (context.source.moveNextTerminal())

            return@matching currentResult
        }
    }

    private fun attemptCompletionFromNextRule(
            context: ParserContext,
            currentResult: ParseResult,
            currentRule: ParserRulable,
            rules: List<ParserRulable>,
            acceptableFailureCount: Int,
            strategy: ParseStrategy): ParseResult {

        // We cannot ignore a missing hard keyword
        if (currentResult !is ParseResult.Success
                && currentRule is ConstantTerminalParserRule
                && currentRule.isTagApplicable(Keyword.Hard)) {
            return currentResult
        }

        val startReference = context.source.createReference()
        var consecutiveFailureCount = 1
        var subsequentParseResult = currentResult

        for (ruleIndex in rules.indexOf(currentRule) + 1 until rules.size) {
            val rule = rules[ruleIndex]
            val parseResult = rule.parse(context, strategy)
            if (parseResult is ParseResult.Fail) {
                consecutiveFailureCount++
                if (consecutiveFailureCount > acceptableFailureCount) {
                    context.restoreTo(startReference)
                    return currentResult
                }
            } else {
                consecutiveFailureCount = 0
                subsequentParseResult = parseResult
            }
        }

        return subsequentParseResult
    }

    override fun recover(
            context: ParserContext,
            currentResult: ParseResult,
            currentRule: ParserRulable,
            strategy: ParseStrategy): ParseResult {

        val actualFailureRule = currentRule.unwrap() as? LeafNodeParserRule ?: return currentResult
        val startReference = context.source.createReference()
        val anchorStackFrame = context.currentAnchorableStackFrame ?: return currentResult

        // Test the failed rule with following tokens
        val lookAheadResult = attemptFailedRuleWithLookAhead(context, actualFailureRule, anchorStackFrame, strategy)

        return if (lookAheadResult is ParseResult.Success) {
            lookAheadResult
        } else {
            context.restoreTo(startReference)
            currentResult
        }
    }

    /**
     * Attempt failed rule on subsequent tokens.  Being careful not to go past an anchor token
     */
    private fun attemptFailedRuleWithLookAhead(
            context: ParserContext,
            rule: LeafNodeParserRule,
            anchorableStackFrame: AnchorableParserContextStackFrame,
            strategy: ParseStrategy): ParseResult {

        repeat(MAXIMUM_TEXT_TOKEN_LOOK_AHEAD) {
            if (context.source.moveNextTerminal() && anchorableStackFrame.isAnchorInScope(context.source)) {
                return ParseResult.FailButOKInContext
            }

            // Do rule.parse here to avoid recursive recovery
            val result = context.optionally { rule.parse(context, strategy) }

            if (result is ParseResult.Success) {
                return result
            }
        }

        return ParseResult.FailButOKInContext
    }

    private const val MAXIMUM_TEXT_TOKEN_LOOK_AHEAD = 6
    private const val MAXIMUM_CONSECUTIVE_UNKNOWN_RULE_FAILURES = 1
    private const val MAXIMUM_CONSECUTIVE_LOCKED_IN_RULE_FAILURES = 3
}

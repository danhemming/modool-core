package modool.parser

data class SourcePosition(val index: Int, val line: Int, val column: Int)
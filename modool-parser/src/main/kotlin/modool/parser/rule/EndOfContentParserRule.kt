package modool.parser.rule

import modool.parser.ParseResult
import modool.parser.ParserContext
import modool.parser.strategy.ParseStrategy

object EndOfContentParserRule : ParserRulable {

    override val childRules = emptyList<ParserRulable>()

    override fun parse(context: ParserContext, strategy: ParseStrategy): ParseResult {
        return when {
            context.source.isAnotherTerminalAvailable() -> context.fail("Expected end of content but more text was found")
            else -> ParseResult.TokenMatch
        }
    }
}
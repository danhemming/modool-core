package modool.parser.rule

import modool.core.config.SemanticIndentationStrategy
import modool.parser.ParseResult
import modool.parser.ParserContext
import modool.parser.stack.withIndentFrame
import modool.parser.strategy.ParseStrategy

class SemanticallyIndentedUnorderedParserRule(
        private val indentationStrategy: SemanticIndentationStrategy,
        childRules: List<CardinalityParserRule>
) : UnorderedParserRule(
        open = SemanticIndentationParserRule,
        childRules = childRules,
        separator = SemanticIndentationParserRule) {

    constructor(
            strategy: SemanticIndentationStrategy,
            vararg rules: CardinalityParserRule) : this(strategy, rules.toList())

    override fun parse(context: ParserContext, strategy: ParseStrategy): ParseResult {
        return context.withIndentFrame(indentationStrategy) {
            super.parse(context, strategy)
        }
    }
}
package modool.parser.rule

import modool.parser.ParseResult
import modool.parser.ParserContext
import modool.parser.strategy.ParseStrategy
import modool.core.content.signifier.tag.Tag

class LiteralValueParserRule(
        private val tag: Tag.DynamicTerminalType
) : TerminalParserRule() {

    private val failMessage: String = "expecting ${tag::class.java.simpleName} literal"

    override fun parse(context: ParserContext, strategy: ParseStrategy): ParseResult {
        return context.matchNextText { found ->
            if (found && context.source.isTerminalTagged(tag)) {
                ParseResult.TokenMatch
            } else {
                context.fail(failMessage)
            }
        }
    }
}
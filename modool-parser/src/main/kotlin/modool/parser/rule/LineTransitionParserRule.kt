package modool.parser.rule

import modool.core.content.signifier.tag.terminal.Meta
import modool.parser.ParseResult
import modool.parser.ParserContext
import modool.parser.strategy.ParseStrategy

object LineTransitionParserRule
    : ParserRule(),
        LeafNodeParserRule,
        FormatParserRule {

    override fun parse(context: ParserContext, strategy: ParseStrategy): ParseResult {

        if (context.source.isStartOfContent() || !context.source.isAnotherTerminalAvailable()) {
            return ParseResult.TokenMatch
        }

        val startPosition = context.source.createReference()
        context.source.moveNextContent()

        if (context.source.isWhiteSpace()) {
            if (context.source.isNewLine()) {
                return ParseResult.TokenMatch
            } else {
                // Could be a leading space before a line continuation so skip it
                context.source.moveNextContent()
            }
        }

        if (context.source.isTerminalTagged(Meta.LineContinuation)) {
            context.source.moveNextContent()
            if (context.source.isNewLine()) {
                // Line continuation followed by a new line is ignored but we do consume it
                return ParseResult.Nothing
            }
        } else if (context.source.isNewLine()) {
            // New line by itself is a match
            return ParseResult.TokenMatch
        }

        context.restoreTo(startPosition)
        return context.fail("Expected line break but none was found")
    }
}
package modool.parser.rule

import modool.core.content.token.ConstantTerminalDescriptor
import modool.core.content.token.TokenReference
import modool.parser.ParseResult
import modool.parser.ParserContext
import modool.parser.strategy.ParseStrategy

/**
 * openSingle is here to capture a terminal that signifies the presence of a single item
 */
abstract class DelimitedParserRule(
        val open: LeafNodeParserRule? = null,
        private val openSingle: LeafNodeParserRule? = null,
        private val separator: LeafNodeParserRule? = null,
        val close: LeafNodeParserRule? = null,
        private val isCloseRequiredIfOpened: Boolean = true,
        private val isSubsequentItemOnlyAllowedWhenEnclosed: Boolean = false,
        isTrailingSeparatorAllowed: Boolean = false,
        private val isTrailingSeparatorMandatory: Boolean = false)
    : ParserRule() {

    private val isTrailingSeparatorAppropriate =
            separator is FormatParserRule
                    || isTrailingSeparatorAllowed
                    || isTrailingSeparatorMandatory

    private val isCloseFormatting = close is FormatParserRule

    override fun parse(context: ParserContext, strategy: ParseStrategy): ParseResult {

        val previousPosition = context.source.createReference()

        var singleItemOnly = false
        var openParseResult = parseOpenSingle(context, strategy)
        if (openParseResult is ParseResult.Success) {
            singleItemOnly = true
        } else {
            openParseResult = parseOpen(context, strategy)
            if (openParseResult is ParseResult.Fail) {
                context.restoreTo(previousPosition)
                return openParseResult
            }
        }

        var itemParseResult = context.optionally {
            parseItem(
                    context, strategy,
                    openEncountered = !singleItemOnly && openParseResult is ParseResult.Success,
                    openSingleEncountered = singleItemOnly)
        }

        if (itemParseResult is ParseResult.Fail
                && (itemParseResult !is ParseResult.FailButOKInContext || openParseResult is ParseResult.Nothing)) {
            return if (openParseResult is ParseResult.Success) {
                context.fail(ERROR_EXPECTED_ITEM, restoreTo = previousPosition)
            } else {
                context.restoreTo(previousPosition)
                itemParseResult
            }
        }

        var lastSeparatorReference: TokenReference? = null

        while (!singleItemOnly && itemParseResult is ParseResult.Success) {

            // This allows for cases where a single item may be specified in place of a collection
            if (openParseResult == ParseResult.Nothing && isSubsequentItemOnlyAllowedWhenEnclosed) {
                break
            }

            val lastItemToken = context.source.createReference()

            // Note: if there is not a delimiter for a leaf rule e.g. identifier we may just scan forever
            val separatorParseResult = context.optionally { parseSeparator(context, strategy) }

            if (separatorParseResult is ParseResult.Success) {
                lastSeparatorReference = context.source.createReference()
            }

            if (separatorParseResult is ParseResult.Fail) {
                if (isTrailingSeparatorMandatory) {
                    return context.unrecoverableFail(ERROR_EXPECTED_SEPARATOR, restoreTo = previousPosition)
                } else {
                    break
                }
            } else {
                itemParseResult = context.optionally {
                    parseItem(
                            context, strategy,
                            openEncountered = !singleItemOnly && openParseResult is ParseResult.Success,
                            openSingleEncountered = singleItemOnly)
                }

                if (itemParseResult !is ParseResult.Success) {
                    // The unexpected separator may either be part of a close rule or a subsequent rule
                    if (!isTrailingSeparatorAppropriate && separatorParseResult is ParseResult.Success) {
                        context.restoreTo(lastItemToken)
                    }
                    break
                }
            }
        }

        val closeRequired = !singleItemOnly && isCloseRequiredIfOpened && openParseResult !== ParseResult.Nothing
        val closeParseResult = parseClose(context, strategy, closeRequired)
        if (closeParseResult is ParseResult.Fail) {
            return context.unrecoverableFail(ERROR_EXPECTED_CLOSING, restoreTo = previousPosition)
        }

        // This is incorporated to support functionality like trailing lambda expressions e.g. Kotlin, Groovy
        val leadOutParseResult = parseLeadOutItem(context, strategy)
        if (leadOutParseResult is ParseResult.Fail) {
            context.restoreTo(previousPosition)
            return context.unrecoverableFail(ERROR_EXPECTED_LEAD_OUT_ITEM, restoreTo = previousPosition)
        }

        if (context.source.createReference() == previousPosition) {
            return ParseResult.Nothing
        }

        val completeParseResult = parseComplete(context, previousPosition, context.source.createReference())
        if (completeParseResult !is ParseResult.Success) {
            context.restoreTo(previousPosition)
        } else if (closeParseResult !is ParseResult.Success &&
                leadOutParseResult !is ParseResult.Success &&
                lastSeparatorReference != null) {
            // Without a close our delimited rule may over match so we set up a restore point
            context.delimiterRestorePoint = lastSeparatorReference
        }

        return completeParseResult
    }

    open fun parseOpen(context: ParserContext, strategy: ParseStrategy): ParseResult {
        if (open == null) {
            return ParseResult.Nothing
        }

        return strategy.parse(context, open)
    }

    open fun parseOpenSingle(context: ParserContext, strategy: ParseStrategy): ParseResult {
        if (openSingle == null) {
            return ParseResult.Nothing
        }

        return strategy.parse(context, openSingle)
    }

    abstract fun parseItem(
            context: ParserContext,
            strategy: ParseStrategy,
            openEncountered: Boolean,
            openSingleEncountered: Boolean): ParseResult

    open fun parseSeparator(context: ParserContext, strategy: ParseStrategy): ParseResult {
        return separator?.let { strategy.parse(context, it) } ?: ParseResult.Nothing
    }

    open fun parseClose(
            context: ParserContext,
            strategy: ParseStrategy,
            requiredToMatch: Boolean): ParseResult {

        if (close == null) {
            return ParseResult.Nothing
        }

        val previousPosition = context.source.createReference()
        val match = strategy.parse(context, close)

        return when {
            match is ParseResult.Nothing && requiredToMatch -> ParseResult.ExpectedButMissing
            match is ParseResult.Fail && !requiredToMatch -> ParseResult.Nothing
            else -> {
                if (isCloseFormatting) {
                    // Formatting i.e. whitespace is left owned.  If we don't do this then any following rules that
                    // expect whitespace as their opening won't match.
                    context.source.moveTo(previousPosition)
                }
                match
            }
        }
    }

    open fun parseLeadOutItem(context: ParserContext, strategy: ParseStrategy): ParseResult {
        return ParseResult.Nothing
    }

    open fun parseComplete(
            context: ParserContext,
            firstReference: TokenReference,
            lastReference: TokenReference): ParseResult {
        return ParseResult.PropertyContainerMatch
    }

    override fun populateAnchorTerminals(
            possibleAnchors: MutableCollection<ConstantTerminalDescriptor>,
            visitedRules: MutableSet<ParserRulable>) {

        if (!visitedRules.add(this)) {
            return
        }

        open?.populateAnchorTerminals(possibleAnchors, visitedRules)
        separator?.populateAnchorTerminals(possibleAnchors, visitedRules)
        close?.populateAnchorTerminals(possibleAnchors, visitedRules)
    }

    companion object {
        private const val ERROR_EXPECTED_ITEM: String = "Expecting a collection item but one is not found"
        private const val ERROR_EXPECTED_SEPARATOR: String = "Expecting a separator but one is not found"
        private const val ERROR_EXPECTED_CLOSING: String = "Expecting collection closing but one is not found"
        private const val ERROR_EXPECTED_LEAD_OUT_ITEM: String = "Expecting a following collection item but one is not found"
    }
}
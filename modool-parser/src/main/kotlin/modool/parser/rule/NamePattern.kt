package modool.parser.rule

import java.util.regex.Pattern

object NamePattern {
    val caseSensitiveMustStartWithALetterOrUnderscore: Pattern = Pattern.compile("""^\b[a-zA-Z_][\w]*\b""")
    val dotQualifiedCaseSensitiveMustStartWithALetterOrUnderscore: Pattern = Pattern.compile("""^(\b[a-zA-Z_][\w]*\b\.)*\b[a-zA-Z_][\w]*\b""")
}
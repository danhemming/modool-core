package modool.parser.rule

import modool.core.content.element.ElementPropertySetter
import modool.core.content.token.TokenRangeOption
import modool.core.content.token.TokenReference
import modool.parser.FailureMessage
import modool.parser.ParseResult
import modool.parser.ParserContext
import modool.parser.stack.UnorderedParserContextStackFrame
import modool.parser.stack.withUnorderedStackFrame
import modool.parser.strategy.ParseStrategy

/**
 * Manages the creation of views
 */
open class UnorderedParserRule(
        override val childRules: List<CardinalityParserRule>,
        open: LeafNodeParserRule? = null,
        separator: LeafNodeParserRule? = null,
        close: LeafNodeParserRule? = null,
        isCloseRequiredIfOpened: Boolean = true,
        isSubsequentItemOnlyAllowedWhenEnclosed: Boolean = false,
        isTrailingSeparatorAllowed: Boolean = false,
        isTrailingSeparatorMandatory: Boolean = false
) : DelimitedParserRule(
        open = open,
        separator = separator,
        close = close,
        isCloseRequiredIfOpened = isCloseRequiredIfOpened,
        isSubsequentItemOnlyAllowedWhenEnclosed = isSubsequentItemOnlyAllowedWhenEnclosed,
        isTrailingSeparatorAllowed = isTrailingSeparatorAllowed,
        isTrailingSeparatorMandatory = isTrailingSeparatorMandatory) {

    constructor(vararg rules: CardinalityParserRule) : this(rules.toList())

    private val anchors by lazy { calculateAnchors() }

    override fun parse(context: ParserContext, strategy: ParseStrategy): ParseResult {
        return context.withUnorderedStackFrame(anchors) {
            val result = super.parse(context, strategy)
            if (result is ParseResult.Success) {
                val currentElementStackFrame = context.currentElementUpdater
                        ?: return context.fail("Attempted to update an element when there is not one in scope")

                viewPropertiesIterator().forEach {
                    val reference = context.elementRepo.createView(
                            it.entries,
                            TokenRangeOption.INCLUSIVE,
                            context.contentOrigin)

                    when (it) {
                        is UnorderedParserContextStackFrame.ViewProperty.Simple ->
                            currentElementStackFrame.addPropertySetter(
                                    ElementPropertySetter.SetPropertyView(
                                            reference, context.contentOrigin, it.property))

                        is UnorderedParserContextStackFrame.ViewProperty.Group ->
                            currentElementStackFrame.addPropertySetter(
                                    ElementPropertySetter.SetPropertyGroupPropertyView(
                                            reference, context.contentOrigin, it.propertyGroup, it.property))
                    }
                }
            }
            result
        }
    }

    override fun parseItem(
            context: ParserContext,
            strategy: ParseStrategy,
            openEncountered: Boolean,
            openSingleEncountered: Boolean): ParseResult {

        return context.matching {
            childRules.forEach {
                val parseResult = strategy.parse(context, it)

                if (parseResult is ParseResult.Success) {
                    val status = it.status(context)
                    if (status is CardinalityResult.ExceededExpectation) {
                        return@matching context.fail {
                            FailureMessage(status.toExplanation(), context.source.createReference())
                        }
                    }

                    return@matching parseResult
                }
            }

            return@matching if (childRules.firstOrNull { it.status(context) is CardinalityResult.Error } == null) ParseResult.Nothing
            else ParseResult.FailButOKInContext
        }
    }

    override fun parseComplete(
            context: ParserContext,
            firstReference: TokenReference,
            lastReference: TokenReference): ParseResult {
        val rulesInError = childRules.filter { it.status(context) is CardinalityResult.Error }

        return if (rulesInError.isEmpty()) {
            ParseResult.OK
        } else context.fail {
            FailureMessage("Unsatisfied cardinality rules", context.source.createReference(),
                    rulesInError.map {
                        FailureMessage(it.status(context).toExplanation(), context.source.createReference())
                    })
        }
    }
}
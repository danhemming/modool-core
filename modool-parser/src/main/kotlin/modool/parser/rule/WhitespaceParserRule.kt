package modool.parser.rule

import modool.parser.ParseResult
import modool.parser.ParserContext
import modool.parser.strategy.ParseStrategy

object WhitespaceParserRule
    : ParserRule(),
        LeafNodeParserRule,
        FormatParserRule {

    override fun parse(context: ParserContext, strategy: ParseStrategy): ParseResult {
        return context.matchNextContent { found ->
            if (found && context.source.isWhiteSpace()) {
                ParseResult.TokenMatch
            } else {
                context.fail("Expected whitespace but none was found")
            }
        }
    }
}
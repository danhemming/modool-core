package modool.parser.rule

import modool.parser.ParseResult
import modool.parser.ParserContext
import modool.parser.strategy.ParseStrategy
import modool.core.content.signifier.tag.Tag

/**
 * Matches a tagged whitespace
 */
class TaggedWhitespaceParserRule(
        private val tag: Tag
) : ParserRule(),
        LeafNodeParserRule,
        FormatParserRule {

    override fun parse(context: ParserContext, strategy: ParseStrategy): ParseResult {
        return context.matchNextContent { found ->
            when {
                !found -> context.fail(ERROR_TAGGED_WHITESPACE_EXPECTED)
                context.source.isWhitespaceTagged(tag) -> ParseResult.TokenMatch
                else -> context.fail(ERROR_TAGGED_WHITESPACE_EXPECTED)
            }
        }
    }

    companion object {
        private const val ERROR_TAGGED_WHITESPACE_EXPECTED = "tagged whitespace expected"
    }
}
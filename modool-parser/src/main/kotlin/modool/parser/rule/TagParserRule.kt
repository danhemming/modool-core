package modool.parser.rule

import modool.parser.ParseResult
import modool.parser.ParserContext
import modool.parser.strategy.ParseStrategy
import modool.core.content.signifier.tag.Tag

/**
 * Because most lexing is not structural (contextualised) and symbols may be used with different meaning in different
 * contexts, we add additional tags in the parsing phase.  These tags may be used by other parser rules looking at
 * prior content.
 */
class TagParserRule(
        val rule: LeafNodeParserRule,
        val tags: List<Tag>)
    : ParserRule(),
        LeafNodeParserRule {

    override fun parse(context: ParserContext, strategy: ParseStrategy): ParseResult {
        val result = rule.parse(context, strategy)
        if (result is ParseResult.Success && context.source.isTerminal()) {
            val tokenReference = context.source.createReference()
            tags.forEach { context.elementRepo.tryChangeTerminalTag(tokenReference, it) }
        }
        return result
    }
}
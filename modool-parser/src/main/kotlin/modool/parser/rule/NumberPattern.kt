package modool.parser.rule

import java.util.regex.Pattern

object NumberPattern {
    val float: Pattern = Pattern.compile("""^\d+\.\d+""")
    val integer: Pattern = Pattern.compile("""^\d+""")
}
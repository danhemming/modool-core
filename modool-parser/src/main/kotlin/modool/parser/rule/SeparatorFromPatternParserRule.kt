package modool.parser.rule

import modool.parser.ParseResult
import modool.parser.ParserContext
import modool.parser.strategy.ParseStrategy
import modool.core.content.signifier.tag.Tag
import modool.core.content.token.ConstantTerminalDescriptor

/**
 * Matches a tagged terminal and changes it's type to be a separator.  This is used where the terminal was identified
 * as part of a larger pattern but now needs to be seen as a stand alone separator in an element.
 */
class SeparatorFromPatternParserRule(
        private val patternTag: Tag,
        private val descriptor: ConstantTerminalDescriptor
) : TerminalParserRule() {

    override fun parse(context: ParserContext, strategy: ParseStrategy): ParseResult {
        return context.matchNextText { found ->
            when {
                !found -> context.fail("Pattern expected")
                context.source.isTerminalTagged(patternTag) -> {
                    if (context.elementRepo.tryChangeTerminalType(context.source.createReference(), descriptor)) {
                        ParseResult.TokenMatch
                    } else {
                        context.fail("Pattern found but could not be made into a constant")
                    }
                }
                else -> context.fail("Pattern expected")
            }
        }
    }
}
package modool.parser.rule

sealed class CardinalityResult {
    abstract fun toExplanation(): String

    interface Error

    class NotMetExpectation(val description: String, val required: Int, val received: Int)
        : CardinalityResult(),
            CardinalityResult.Error {
        override fun toExplanation(): String {
            return "$description required $required but found $received"
        }
    }

    object Satisfied : CardinalityResult() {

        override fun toExplanation(): String {
            return "Cardinality satisfied"
        }
    }

    class ExceededExpectation(val description: String, val expected: Int, val received: Int)
        : CardinalityResult(),
            CardinalityResult.Error {

        override fun toExplanation(): String {
            return "$description expected $expected but found $received"
        }
    }
}
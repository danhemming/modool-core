package modool.parser.rule

import modool.parser.ParseResult
import modool.parser.ParserContext
import modool.parser.strategy.ParseStrategy

class TerminatorParserRule(rule: LeafNodeParserRule)
    : WrapperParserRule(rule),
        LeafNodeParserRule {

    override fun parse(context: ParserContext, strategy: ParseStrategy): ParseResult {
        return when (val result = super.parse(context, strategy)) {
            is ParseResult.Success -> ParseResult.TerminatorMatch
            else -> result
        }
    }
}
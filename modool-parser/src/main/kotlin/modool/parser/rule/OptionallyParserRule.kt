package modool.parser.rule

import modool.parser.ParseResult
import modool.parser.ParserContext
import modool.parser.stack.withChoiceStackFrame
import modool.parser.strategy.ParseStrategy

class OptionallyParserRule<R : ParserRulable>(rule: R)
    : WrapperParserRule(rule),
        OptionalRule {

    private val anchors by lazy { calculateAnchors() }

    override fun parse(context: ParserContext, strategy: ParseStrategy): ParseResult {

        return context.withChoiceStackFrame(anchors) {

            val parseResult = context.optionally { wrappedRule.parse(context, strategy) }

            if (parseResult !is ParseResult.Success) {
                clearPropertySetters()
            }

            if (parseResult is ParseResult.Success || parseResult is ParseResult.UnrecoverableFailure) {
                return@withChoiceStackFrame parseResult
            }

            return@withChoiceStackFrame ParseResult.Nothing
        }
    }
}
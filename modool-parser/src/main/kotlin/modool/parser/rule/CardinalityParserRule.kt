package modool.parser.rule

import modool.parser.ParseResult
import modool.parser.ParserContext
import modool.parser.strategy.ParseStrategy

sealed class CardinalityParserRule(
        val rule: ParserRulable,
        val minCount: Int?,
        val maxCount: Int?
) : ParserRule() {

    override val childRules = listOf(rule)

    override fun parse(context: ParserContext, strategy: ParseStrategy): ParseResult {

        val result = strategy.parse(context, rule)
        return if (result is ParseResult.Success) {
            incrementCount(context)
            result
        } else {
            result
        }
    }

    private fun incrementCount(context: ParserContext) {
        // Note: because this is done within a unique stack frame, our object reference will be unique
        val count = context.currentStackFrame.getValue(this) as Int? ?: 0
        context.currentStackFrame[this] = count + 1
    }

    fun status(context: ParserContext): CardinalityResult {
        // Note: because this is done within a unique stack frame, our object reference will be unique
        val count = context.currentStackFrame.getValue(this) as Int? ?: 0

        if (minCount == null && maxCount == null) {
            return CardinalityResult.Satisfied
        } else if (minCount == null) {
            return if (count <= maxCount!!) {
                CardinalityResult.Satisfied
            } else {
                CardinalityResult.ExceededExpectation(rule.javaClass.name, maxCount, count)
            }
        } else {
            return if (count >= minCount) {
                if (maxCount == null || count <= maxCount) {
                    CardinalityResult.Satisfied
                } else {
                    CardinalityResult.ExceededExpectation(rule.javaClass.name, maxCount, count)
                }
            } else {
                CardinalityResult.NotMetExpectation(rule.javaClass.name, minCount, count)
            }
        }
    }
}

class Unlimited(rule : ParserRulable) : CardinalityParserRule(rule, null, null)
class ZeroOrOne(rule : ParserRulable) : CardinalityParserRule(rule, 0, 1)
class Exactly(rule : ParserRulable, count: Int) : CardinalityParserRule(rule, count, count)
class One(rule : ParserRulable) : CardinalityParserRule(rule, 1, 1)
class OneOrMore(rule : ParserRulable) : CardinalityParserRule(rule, 1, null)
class Between(rule : ParserRulable, min: Int, max: Int) : CardinalityParserRule(rule, min, max)
class NoMoreThan(rule : ParserRulable, max: Int) : CardinalityParserRule(rule, null, max)
package modool.parser.rule

import modool.parser.ParseResult
import modool.parser.ParserContext
import modool.parser.strategy.ParseStrategy

/**
 * This shortcut can only work for terminal rules as it does not clean up partial elements
 */
class OptionalLeafNodeParserRule(val rule: LeafNodeParserRule)
    : WrapperParserRule(rule),
        LeafNodeParserRule,
        OptionalRule {

    override fun parse(context: ParserContext, strategy: ParseStrategy): ParseResult {

        val result = context.optionally { rule.parse(context, strategy) }
        return if (result is ParseResult.Success || result is ParseResult.UnrecoverableFailure) {
            result
        } else {
            ParseResult.Nothing
        }
    }
}
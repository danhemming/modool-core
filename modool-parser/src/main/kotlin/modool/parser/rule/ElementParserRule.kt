package modool.parser.rule

import modool.core.meta.element.ElementMeta
import modool.core.content.token.ConstantTerminalDescriptor
import modool.parser.ParseResult
import modool.parser.ParserContext
import modool.parser.stack.withElementalStackFrame
import modool.parser.strategy.ParseStrategy

/**
 * A sequence of contents that should emit a newly created element
 */
class ElementParserRule(
        private val element: ElementMeta,
        ruleProvider: () -> List<ParserRulable>
) : LazyCompositeParserRule<ParserRulable>(ruleProvider),
        ElementProviderParserRule {

    override fun parse(context: ParserContext, strategy: ParseStrategy): ParseResult {

        return context.withElementalStackFrame(this, anchors, element) {

            context.matching {
                val result = super.parse(context, strategy)

                if (result is ParseResult.Success) {
                    lock()
                    createElement(context)
                    ParseResult.ElementMatch
                } else {
                    context.fail("Expecting element", result)
                }
            }
        }
    }

    override fun populateAnchorTerminals(
            possibleAnchors: MutableCollection<ConstantTerminalDescriptor>,
            visitedRules: MutableSet<ParserRulable>) {
        if (!visitedRules.add(this)) {
            return
        }

        // When populating anchors we don't want to include child element anchors.
        // (these should not be necessary to lock-in this element)
        childRules.forEach {
            // We want nested terminals just not from these
            if (it !is ElementProviderParserRule && it !is ListParserRule) {
                it.populateAnchorTerminals(possibleAnchors, visitedRules)
            }
        }
    }
}


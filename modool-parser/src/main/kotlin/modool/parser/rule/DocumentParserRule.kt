package modool.parser.rule

import modool.core.meta.element.ElementMeta
import modool.parser.ParseResult
import modool.parser.ParserContext
import modool.parser.stack.withElementalStackFrame
import modool.parser.strategy.ParseStrategy

/**
 * A document rule is always valid if empty and must always be the whole content
 */
class DocumentParserRule(
        private val document: ElementMeta,
        ruleProvider: () -> List<ParserRulable>
) : LazyCompositeParserRule<ParserRulable>(
        {
            listOf(
                    PickOneParserRule(
                            CompositeParserRule(ruleProvider() + EndOfContentParserRule),
                            EndOfContentParserRule))
        }),
        ElementProviderParserRule {

    override fun parse(context: ParserContext, strategy: ParseStrategy): ParseResult {

        return context.withElementalStackFrame(this, anchors, document) {
            // There must always be a document so we lock it in
            // NB: this holds true for one document per context only
            lock()
            context.matching {
                val result = super.parse(context, strategy)

                if (result is ParseResult.Success) {
                    createElement(context)
                    ParseResult.ElementMatch
                } else {
                    context.fail("Expecting document", result)
                }
            }
        }
    }
}
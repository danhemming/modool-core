package modool.parser.rule

interface LearningRule {
    /**
     * The confidence scores are used to help with the tolerant reader functionality this allows us to understand
     * which values are valid for a given rule.
     */
    fun increaseConfidence(value: String)

    fun decreaseConfidence(value: String)
}
package modool.parser.rule

import modool.parser.ParseResult
import modool.parser.ParserContext
import modool.parser.strategy.ParseStrategy

/**
 * A sequence of rules that produces a collection of contents
 */
open class CompositeParserRule<R>(override val childRules: List<R>) : ParserRule()
        where R : ParserRulable {

    constructor(vararg rules: R) : this(rules.toList())

    override fun parse(context: ParserContext, strategy: ParseStrategy): ParseResult {
        return strategy.parse(context, childRules)
    }
}
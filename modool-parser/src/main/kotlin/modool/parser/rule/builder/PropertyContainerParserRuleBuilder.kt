package modool.parser.rule.builder

import modool.core.content.enumeration.Enumeration
import modool.core.content.token.ConstantTerminalDescriptor
import modool.core.meta.property.PropertyMeta
import modool.parser.rule.*
import modool.parser.rule.property.*

@ModoolParserBuilderDsl
open class PropertyContainerParserRuleBuilder<R>
    : RootParserRuleBuilder<R>()
        where R : ParserRulable {

    fun ElementProviderParserRule.sets(propertyGroup: PropertyMeta, property: PropertyMeta) =
            ElementPropertyGroupPropertySetterElementValueRule(this, propertyGroup, property)

    fun ElementProviderParserRule.sets(property: PropertyMeta) =
            ElementPropertySetterElementValueRule(this, property)

    fun <F> Enumeration<F>.sets(fragmentProperty: PropertyMeta, property: PropertyMeta)
            where F : ConstantTerminalDescriptor =
            PickOneParserRule(entries.map { ConstantTerminalParserRule(it).sets(fragmentProperty, property) })

    fun <F> Enumeration<F>.sets(property: PropertyMeta)
            where F : ConstantTerminalDescriptor =
            PickOneParserRule(entries.map { ConstantTerminalParserRule(it) }).sets(property)

    fun <R> PickOneParserRule<R>.sets(
            fragmentProperty: PropertyMeta,
            property: PropertyMeta)
            where R : TerminalParserRule =
            PickOneParserRule(this.childRules.map { it.sets(fragmentProperty, property) })

    fun <R> PickOneParserRule<R>.sets(property: PropertyMeta)
            where R : TerminalParserRule =
            PickOneParserRule(this.childRules.map { it.sets(property) })

    fun TerminalParserRule.sets(fragmentProperty: PropertyMeta, property: PropertyMeta) =
            ElementPropertyGroupPropertySetterTerminalValueRule(this, fragmentProperty, property)

    fun ConstantTerminalDescriptor.sets(property: PropertyMeta) =
            ConstantTerminalParserRule(this).sets(property)

    fun TerminalParserRule.sets(property: PropertyMeta) =
            ElementPropertySetterTerminalValueRule(this, property)

    fun PropertyGroupParserRule<*>.sets(property: PropertyMeta) =
            ElementPropertySetterPropertyGroupRule(this, property)

    fun ListProviderParserRule.sets(property: PropertyMeta) =
            ElementPropertySetterPropertyListRule(this, property)
}
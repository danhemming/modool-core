package modool.parser.rule.builder

import modool.parser.rule.*

@ModoolParserBuilderDsl
open class RootParserRuleBuilder<R>(
        val rules: MutableList<R> = mutableListOf()
) : ParserRuleFactory
        where R : ParserRulable {

    operator fun R.unaryPlus() {
        rules.add(this)
    }
}
package modool.parser.rule.builder

import modool.core.content.enumeration.Enumeration
import modool.core.content.token.ConstantTerminalDescriptor
import modool.core.meta.property.PropertyMeta
import modool.parser.rule.CardinalityParserRule
import modool.parser.rule.ConstantTerminalParserRule
import modool.parser.rule.ElementProviderParserRule
import modool.parser.rule.PickOneParserRule
import modool.parser.rule.property.ElementPropertyGroupViewPropertyAdderElementValueRule
import modool.parser.rule.property.ElementPropertyGroupViewPropertyAdderTerminalValueRule
import modool.parser.rule.property.ElementViewPropertyAdderElementValueRule
import modool.parser.rule.property.ElementViewPropertyAdderTerminalValueRule

class UnorderedSectionParserRuleBuilder : ElementSectionBuilderParser<CardinalityParserRule>() {

    fun ElementProviderParserRule.addsTo(property: PropertyMeta) =
            ElementViewPropertyAdderElementValueRule(this, property)

    fun ElementProviderParserRule.addsTo(propertyGroup: PropertyMeta, property: PropertyMeta) =
            ElementPropertyGroupViewPropertyAdderElementValueRule(this, propertyGroup, property)

    fun <F> Enumeration<F>.addsTo(property: PropertyMeta)
            where F : ConstantTerminalDescriptor =
            PickOneParserRule(
                    entries.map {
                        ElementViewPropertyAdderTerminalValueRule(ConstantTerminalParserRule(it), property)
                    })

    fun <F> Enumeration<F>.addsTo(propertyGroup: PropertyMeta, property: PropertyMeta)
            where F : ConstantTerminalDescriptor =
            PickOneParserRule(
                    entries.map {
                        ElementPropertyGroupViewPropertyAdderTerminalValueRule(
                                ConstantTerminalParserRule(it),
                                propertyGroup,
                                property)
                    })
}
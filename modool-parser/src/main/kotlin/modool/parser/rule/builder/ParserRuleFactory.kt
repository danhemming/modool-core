package modool.parser.rule.builder

import modool.core.content.enumeration.Enumeration
import modool.core.content.token.ConstantTerminalDescriptor
import modool.parser.rule.*

interface ParserRuleFactory {

    fun terminal(terminal: ConstantTerminalDescriptor): ConstantTerminalParserRule {
        return ConstantTerminalParserRule(terminal)
    }

    fun enum(enum: Enumeration<out ConstantTerminalDescriptor>): PickOneParserRule<ConstantTerminalParserRule> {
        return PickOneParserRule(enum.entries.map { ConstantTerminalParserRule(it) })
    }

    fun optional(terminal: ConstantTerminalDescriptor): ParserRulable {
        return optional(ConstantTerminalParserRule(terminal))
    }

    fun optional(rule: ParserRulable): ParserRulable {
        return OptionallyParserRule(rule)
    }

    fun optional(rule: LeafNodeParserRule): LeafNodeParserRule {
        return OptionalLeafNodeParserRule(rule)
    }

    companion object Default : ParserRuleFactory
}
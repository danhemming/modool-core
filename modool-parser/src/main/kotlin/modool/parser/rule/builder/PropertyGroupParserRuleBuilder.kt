package modool.parser.rule.builder

import modool.core.meta.Meta
import modool.parser.rule.ParserRulable

class PropertyGroupParserRuleBuilder<T> : ElementSectionBuilderParser<ParserRulable>()
        where T : Meta
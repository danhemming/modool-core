package modool.parser.rule.builder

import modool.core.meta.element.ElementMeta
import modool.core.content.operator.OperatorAssociativity
import modool.parser.rule.ParserRulable
import modool.parser.rule.PickOneParserRule
import modool.parser.rule.expression.ExpressionParserRule
import modool.parser.rule.expression.operator.OperableExpressionPrecedenceParserRule
import modool.parser.rule.expression.operator.PrecedentRuleGroup

class ExpressionParserRuleBuilder(private val expression: ElementMeta) {

    private val operatorPrecedentGroups = mutableListOf<PrecedentRuleGroup>()
    private val expressionTerminators = mutableListOf<ParserRulable>()
    private val precedenceGroupBuilder = OperatorPrecedenceGroupBuilder(operatorPrecedentGroups)
    private val expressionTerminatorBuilder = ExpressionTerminatorBuilder(expressionTerminators)

    fun precedence(builder: OperatorPrecedenceGroupBuilder.() -> Unit) {
        builder(precedenceGroupBuilder)
    }

    fun terminator(builder: ExpressionTerminatorBuilder.() -> Unit) {
        builder(expressionTerminatorBuilder)
    }

    fun createExpressionParserRule(): ExpressionParserRule {
        return ExpressionParserRule(operatorPrecedentGroups, PickOneParserRule(expressionTerminators), expression)
    }

    class OperatorPrecedenceGroupBuilder(
            private val operatorPrecedentGroups: MutableList<PrecedentRuleGroup>) {

        operator fun OperatorAssociativity.invoke(builder: GroupBuilder.() -> Unit) {
            val rules = mutableListOf<OperableExpressionPrecedenceParserRule>()
            val ruleGroup = PrecedentRuleGroup(this, rules)
            builder(GroupBuilder(rules))
            operatorPrecedentGroups.add(ruleGroup)
        }

        class GroupBuilder(
                private val operablePrecedenceRules: MutableList<OperableExpressionPrecedenceParserRule>) {

            operator fun OperableExpressionPrecedenceParserRule.unaryPlus() {
                operablePrecedenceRules.add(this)
            }
        }
    }

    class ExpressionTerminatorBuilder(
            private val operatorPrecedentGroups: MutableList<ParserRulable>) {

        operator fun ParserRulable.unaryPlus() {
            operatorPrecedentGroups.add(this)
        }
    }
}
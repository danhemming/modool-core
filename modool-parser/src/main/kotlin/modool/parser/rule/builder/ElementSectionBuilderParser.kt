package modool.parser.rule.builder

import modool.core.config.SemanticIndentationStrategy
import modool.core.meta.property.PropertyMeta
import modool.parser.rule.*

abstract class ElementSectionBuilderParser<R> : PropertyContainerParserRuleBuilder<R>()
        where R : ParserRulable {

    fun unorderedSemanticallyIndented(
            strategy: SemanticIndentationStrategy,
            builder: UnorderedSectionParserRuleBuilder.() -> Unit
    ): SemanticallyIndentedUnorderedParserRule {
        val srb = UnorderedSectionParserRuleBuilder()
        builder(srb)
        return SemanticallyIndentedUnorderedParserRule(strategy, srb.rules)
    }

    fun unordered(builder: UnorderedSectionParserRuleBuilder.() -> Unit): UnorderedParserRule {
        val srb = UnorderedSectionParserRuleBuilder()
        builder(srb)
        return UnorderedParserRule(srb.rules)
    }

    fun choice(
            builder: ChoiceParserRuleBuilder.() -> Unit): PickOneParserRule<ParserRulable> {
        val srb = ChoiceParserRuleBuilder()
        builder(srb)
        return PickOneParserRule(srb.rules)
    }

    fun <T> group(
            propertyGroupProperty: T,
            definition: PropertyGroupParserRuleBuilder<T>.(T) -> Unit
    ): ParserRulable
            where T : PropertyMeta {
        val builder = PropertyGroupParserRuleBuilder<T>()
        builder.definition(propertyGroupProperty)
        return PropertyGroupParserRule(builder.rules).sets(propertyGroupProperty)
    }
}
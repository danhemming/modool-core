package modool.parser.rule.builder

import modool.core.meta.Meta
import modool.core.meta.element.ElementMeta
import modool.parser.rule.*
import modool.parser.rule.expression.ExpressionParserRule

class ElementParserRuleBuilder<T> : ElementSectionBuilderParser<ParserRulable>()
        where T : Meta

@ModoolParserBuilderDsl
inline fun <T> element(
        meta: T,
        crossinline builder: ElementParserRuleBuilder<T>.(T) -> Unit
): ElementParserRule
        where T : ElementMeta {
    return ElementParserRule(meta) {
        val erb = ElementParserRuleBuilder<T>()
        builder(erb, meta)
        erb.rules
    }
}

@ModoolParserBuilderDsl
inline fun expression(
        expression: ElementMeta,
        builder: ExpressionParserRuleBuilder.() -> Unit): ExpressionParserRule {
    val erb = ExpressionParserRuleBuilder(expression)
    builder(erb)
    return erb.createExpressionParserRule()
}

@ModoolParserBuilderDsl
fun elementChoice(
        builder: RootParserRuleBuilder<ElementProviderParserRule>.() -> Unit): ElementChoiceParserRule {
    val srb = RootParserRuleBuilder<ElementProviderParserRule>()
    builder(srb)
    return ElementChoiceParserRule(srb.rules)
}

@ModoolParserBuilderDsl
inline fun <reified E> document(
        documentElement: E,
        crossinline definition: ElementParserRuleBuilder<E>.() -> Unit): DocumentParserRule
        where E : ElementMeta {
    return DocumentParserRule(documentElement) {
        val builder = ElementParserRuleBuilder<E>()
        builder.definition()
        builder.rules
    }
}
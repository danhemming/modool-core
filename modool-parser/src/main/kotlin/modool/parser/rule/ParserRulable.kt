package modool.parser.rule

import modool.core.content.token.ConstantTerminalDescriptor
import modool.parser.ParseResult
import modool.parser.ParserContext
import modool.parser.strategy.ParseStrategy

interface ParserRulable {
    fun parse(context: ParserContext, strategy: ParseStrategy): ParseResult
    val childRules: Iterable<ParserRulable>

    /**
     * Based on the child rules identify which tokens are unique and could commit a parser to a particular element
     */
    fun populateAnchorTerminals(
            possibleAnchors: MutableCollection<ConstantTerminalDescriptor>,
            visitedRules: MutableSet<ParserRulable>) {
        if (!visitedRules.add(this)) {
            return
        }
        childRules.forEach {
            it.populateAnchorTerminals(possibleAnchors, visitedRules)
        }
    }
}

fun ParserRulable.calculateAnchors(): Collection<ConstantTerminalDescriptor> {
    val result = CountSet<ConstantTerminalDescriptor>()
    populateAnchorTerminals(result, mutableSetOf())
    return result.uniqueKeys
}

fun ParserRulable.unwrap() = when (this) {
    is WrapperParserRule -> wrappedRule
    else -> this
}

private class CountSet<T> : MutableCollection<T> {

    private val values = mutableMapOf<T, Int>()

    override val size: Int get() = values.size

    override fun contains(element: T): Boolean {
        return values.containsKey(element)
    }

    override fun containsAll(elements: Collection<T>): Boolean {
        return elements.fold(true) { result, item -> result && values.containsKey(item) }
    }

    override fun isEmpty(): Boolean {
        return values.isEmpty()
    }

    override fun addAll(elements: Collection<T>): Boolean {
        return elements.fold(true) { result, item -> result && add(item) }
    }

    override fun clear() {
        values.clear()
    }

    override fun iterator(): MutableIterator<T> {
        return values.keys.iterator()
    }

    override fun remove(element: T): Boolean {
        return values.remove(element) != null
    }

    override fun removeAll(elements: Collection<T>): Boolean {
        values.clear()
        return true
    }

    override fun retainAll(elements: Collection<T>): Boolean {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun add(element: T): Boolean {
        return if (contains(element)) {
            values[element] = values[element]!! + 1
            false
        } else {
            values[element] = 1
            true
        }
    }

    val uniqueKeys: Collection<T> get() = values.filter { it.value == 1 }.map { it.key }
}
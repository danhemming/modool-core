package modool.parser.rule

import modool.core.content.token.TokenRangeOption
import modool.parser.ParseResult
import modool.parser.ParserContext
import modool.parser.strategy.ParseStrategy

/**
 * A sequence of contents that represent a fragment of an element
 */
class PropertyGroupParserRule<R>(
        rules: List<R>
) : CompositeParserRule<R>(rules)
        where R : ParserRulable {

    override fun parse(context: ParserContext, strategy: ParseStrategy): ParseResult {
        return context.matching {

            val startPosition = context.source.createReference()
            val result = super.parse(context, strategy)

            if (result is ParseResult.Success) {
                val endPosition = context.elementRepo.createPropertyGroup(
                        startPosition,
                        context.source.createReference(),
                        TokenRangeOption.START_EXCLUSIVE_END_INCLUSIVE,
                        context.contentOrigin)

                context.source.moveTo(endPosition)
                ParseResult.PropertyContainerMatch
            } else {
                context.fail("Expecting property group", result)
            }
        }
    }
}
package modool.parser.rule

import modool.parser.ParseResult
import modool.parser.ParserContext
import modool.parser.strategy.ParseStrategy

/**
 * Fails on matching a new line (optionally allows other white space)
 * Cannot rely on composition of OptionalLeafNodeParserRule and InlineWhitespaceParserRule as they allow progress when
 * a new line is found.
 */
object OptionalInlineWhitespaceParserRule
    : ParserRule(),
        LeafNodeParserRule,
        FormatParserRule {

    override fun parse(context: ParserContext, strategy: ParseStrategy): ParseResult {
        return context.matchNextContent { found ->
            if (found && context.source.isWhiteSpace()) {
                if (context.source.isNewLine()) {
                    context.fail("Expected inline whitespace but line break was found")
                } else {
                    ParseResult.TokenMatch
                }
            } else {
                ParseResult.Nothing
            }
        }
    }
}
package modool.parser.rule

import modool.parser.FailureMessage
import modool.parser.ParseResult
import modool.parser.ParserContext
import modool.parser.stack.withChoiceStackFrame
import modool.parser.strategy.ParseStrategy

open class PickOneParserRule<R : ParserRulable>(
        override val childRules: List<R>) : ParserRule() {

    constructor(vararg rules: R) : this(rules.toList())

    private val anchors by lazy { calculateAnchors() }

    override fun parse(context: ParserContext, strategy: ParseStrategy): ParseResult {

        if (childRules.isEmpty()) {
            return ParseResult.Nothing
        }

        return context.withChoiceStackFrame(anchors) {

            val errorCollector = mutableListOf<FailureMessage>()

            // Optionally is counter intuitive here but it really means: don't promote failure to a unrecoverable one
            context.optionally {
                childRules.asSequence()
                        .map {
                            it.parse(context, strategy)
                        }
                        .firstOrNull {
                            if (it is ParseResult.UnrecoverableFailure) {
                                errorCollector.clear()
                            }

                            if (it is ParseResult.FailWithMessage) {
                                errorCollector.add(it.failure)
                            }

                            if (it !is ParseResult.Success) {
                                clearPropertySetters()
                            }

                            it is ParseResult.Success || it is ParseResult.UnrecoverableFailure
                        }
                        ?: context.fail("Unable to match any content", errorCollector)
            }
        }
    }
}
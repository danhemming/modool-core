package modool.parser.rule

import modool.parser.ParseResult
import modool.parser.ParserContext
import modool.parser.strategy.ParseStrategy

/**
 * Allow a rule to pass when it matches but do not progress through the stream of data
 * This is most useful when we want to see if we are followed by a terminator
 */
class LookAheadParserRule(
        private val rule: ParserRulable
) : ParserRule() {

    override fun parse(context: ParserContext, strategy: ParseStrategy): ParseResult {
        val startReference = context.source.createReference()
        val parseResult = strategy.parse(context, rule)
        context.restoreTo(startReference)
        return parseResult
    }
}
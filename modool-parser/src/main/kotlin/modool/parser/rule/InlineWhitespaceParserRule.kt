package modool.parser.rule

import modool.parser.ParseResult
import modool.parser.ParserContext
import modool.parser.strategy.ParseStrategy

object InlineWhitespaceParserRule
    : ParserRule(),
        LeafNodeParserRule,
        FormatParserRule {

    override fun parse(context: ParserContext, strategy: ParseStrategy): ParseResult {
        return context.matchNextContent { found ->
            if (found && context.source.isWhiteSpace() && !context.source.isNewLine()) {
                ParseResult.TokenMatch
            } else {
                context.fail("Expected inline whitespace but none was found")
            }
        }
    }
}
package modool.parser.rule

abstract class WrapperParserRule(
        val wrappedRule: ParserRulable
) : ParserRulable by wrappedRule {
    override val childRules = listOf(wrappedRule)
}
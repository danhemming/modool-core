package modool.parser.rule

abstract class TerminalParserRule
    : ParserRule(),
        LeafNodeParserRule
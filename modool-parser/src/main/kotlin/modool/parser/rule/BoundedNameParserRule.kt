package modool.parser.rule

import modool.core.content.token.TokenRangeOption
import modool.core.content.token.TokenReference
import modool.parser.ParseResult
import modool.parser.ParserContext
import modool.parser.strategy.ParseStrategy

class BoundedNameParserRule(
        open: TerminalParserRule,
        private val name: ParserRulable,
        private val unboundedName: ParserRulable? = null,
        close: TerminalParserRule
) : DelimitedParserRule(
        open = if (unboundedName == null) open else OptionalLeafNodeParserRule(open),
        separator = NoProgressParserRule, // Makes sure we don't attempt to read next terminal
        close = if (unboundedName == null) close else OptionalLeafNodeParserRule(close),
        isCloseRequiredIfOpened = true),
        ListProviderParserRule {

    override fun parseItem(
            context: ParserContext,
            strategy: ParseStrategy,
            openEncountered: Boolean,
            openSingleEncountered: Boolean): ParseResult {

        return strategy.parse(context,
                if (openEncountered) name
                else unboundedName ?: return context.fail("Name expected"))
    }

    override fun parseComplete(
            context: ParserContext,
            firstReference: TokenReference,
            lastReference: TokenReference): ParseResult {

        val endPosition = context.elementRepo.createList(
                firstReference,
                lastReference,
                TokenRangeOption.START_EXCLUSIVE_END_INCLUSIVE,
                context.contentOrigin)

        context.restoreTo(endPosition)
        return ParseResult.PropertyContainerMatch
    }
}
package modool.parser.rule.property

import modool.core.meta.property.PropertyMeta
import modool.parser.ParseResult
import modool.parser.ParserContext
import modool.core.content.element.ElementPropertySetter
import modool.core.content.token.TokenReference
import modool.parser.rule.ListParserRule
import modool.parser.rule.ListProviderParserRule

class ElementPropertySetterPropertyListRule(
        rule: ListProviderParserRule,
        private val property: PropertyMeta
) : ElementPropertySetterRule(rule) {

    override fun validateContent(context: ParserContext, parseResult: ParseResult): Boolean {
        return parseResult is ParseResult.PropertyContainerMatch &&
                context.elementRepo.isPropertyAt(context.source.createReference())
    }

    override fun createPropertySetter(
            context: ParserContext,
            contentReference: TokenReference): ElementPropertySetter {

        return ElementPropertySetter.SetPropertyList(
                contentReference,
                context.contentOrigin,
                property)
    }
}
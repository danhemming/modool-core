package modool.parser.rule.property

import modool.core.meta.property.PropertyMeta
import modool.parser.ParseResult
import modool.parser.ParserContext
import modool.parser.rule.ElementProviderParserRule
import modool.core.content.token.TokenReference
import modool.parser.stack.UnorderedParserContextStackFrame

class ElementPropertyGroupViewPropertyAdderElementValueRule(
        rule: ElementProviderParserRule,
        private val propertyGroup: PropertyMeta,
        private val property: PropertyMeta
) : ElementViewPropertyAdderRule(rule) {

    override fun validateContent(context: ParserContext, parseResult: ParseResult): Boolean {
        return parseResult is ParseResult.ElementMatch
    }

    override fun addViewPropertyMember(
            context: ParserContext,
            unordered: UnorderedParserContextStackFrame,
            contentReference: TokenReference) {
        unordered.addPropertyGroupViewPropertyMember(
                propertyGroup,
                property,
                context.elementRepo.getElementStart(contentReference),
                context.elementRepo.getElementEnd(contentReference))
    }
}
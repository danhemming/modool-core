package modool.parser.rule.property

import modool.core.meta.property.PropertyMeta
import modool.parser.ParseResult
import modool.parser.ParserContext
import modool.parser.rule.LeafNodeParserRule
import modool.parser.rule.TerminalParserRule
import modool.core.content.element.ElementPropertySetter
import modool.core.content.token.TokenReference

class ElementPropertyGroupPropertySetterTerminalValueRule(
        rule: TerminalParserRule,
        private val propertyGroupProperty: PropertyMeta,
        private val property: PropertyMeta
) : ElementPropertySetterRule(rule),
        LeafNodeParserRule {

    override fun validateContent(context: ParserContext, parseResult: ParseResult): Boolean {
        return parseResult is ParseResult.TokenMatch
    }

    override fun createPropertySetter(
            context: ParserContext,
            contentReference: TokenReference): ElementPropertySetter {

        return ElementPropertySetter.SetPropertyGroupPropertyTerminalValue(
                contentReference, context.contentOrigin, propertyGroupProperty, property)
    }
}
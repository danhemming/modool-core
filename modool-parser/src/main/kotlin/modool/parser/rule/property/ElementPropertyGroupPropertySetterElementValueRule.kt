package modool.parser.rule.property

import modool.core.meta.property.PropertyMeta
import modool.parser.ParseResult
import modool.parser.ParserContext
import modool.parser.rule.ElementProviderParserRule
import modool.core.content.element.ElementPropertySetter
import modool.core.content.token.TokenReference

class ElementPropertyGroupPropertySetterElementValueRule(
        rule: ElementProviderParserRule,
        private val propertyGroupProperty: PropertyMeta,
        private val property: PropertyMeta
) : ElementPropertySetterRule(rule) {

    override fun validateContent(context: ParserContext, parseResult: ParseResult): Boolean {
        return parseResult is ParseResult.ElementMatch
    }

    override fun createPropertySetter(
            context: ParserContext,
            contentReference: TokenReference): ElementPropertySetter {

        return ElementPropertySetter.SetPropertyGroupPropertyElementValue(
                contentReference, context.contentOrigin, propertyGroupProperty, property)
    }
}
package modool.parser.rule.property

import modool.parser.ParseResult
import modool.parser.ParserContext
import modool.parser.rule.ParserRulable
import modool.parser.rule.WrapperParserRule
import modool.core.content.element.ElementPropertySetter
import modool.parser.strategy.ParseStrategy
import modool.core.content.token.TokenReference

abstract class ElementPropertySetterRule(
        private val rule: ParserRulable
) : WrapperParserRule(rule) {

    override fun parse(context: ParserContext, strategy: ParseStrategy): ParseResult {
        val parseResult = rule.parse(context, strategy)
        return if (parseResult is ParseResult.Success) {
            if (validateContent(context, parseResult)) {
                updateOnSuccess(context, context.source.createReference(), parseResult)
            } else {
                return context.fail("Unable to derive token when updating an element property")
            }

        } else {
            parseResult
        }
    }

    protected abstract fun validateContent(context: ParserContext, parseResult: ParseResult): Boolean

    fun updateOnSuccess(
            context: ParserContext,
            contentReference: TokenReference,
            result: ParseResult): ParseResult {

        val currentElementStackFrame = context.currentElementUpdater
                ?: return context.fail("Attempted to update an element when there is not one in scope")

        @Suppress("UNCHECKED_CAST")
        currentElementStackFrame.addPropertySetter(createPropertySetter(context, contentReference))

        return result
    }

    protected abstract fun createPropertySetter(
            context: ParserContext,
            contentReference: TokenReference
    ): ElementPropertySetter
}


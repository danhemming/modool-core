package modool.parser.rule.property

import modool.parser.ParseResult
import modool.parser.ParserContext
import modool.parser.rule.ParserRulable
import modool.parser.rule.WrapperParserRule
import modool.parser.strategy.ParseStrategy
import modool.core.content.token.TokenReference
import modool.parser.stack.AnchorableParserContextStackFrame
import modool.parser.stack.UnorderedParserContextStackFrame

abstract class ElementViewPropertyAdderRule(
        private val rule: ParserRulable
) : WrapperParserRule(rule) {

    override fun parse(context: ParserContext, strategy: ParseStrategy): ParseResult {
        val parseResult = rule.parse(context, strategy)
        return if (parseResult is ParseResult.Success) {
            if (validateContent(context, parseResult)) {
                addViewPropertyMember(context, context.source.createReference(), parseResult)
            } else {
                return context.fail("Unable to derive token when adding to a view")
            }
        } else {
            parseResult
        }
    }

    protected abstract fun validateContent(context: ParserContext, parseResult: ParseResult): Boolean

    private fun addViewPropertyMember(
            context: ParserContext,
            contentReference: TokenReference,
            result: ParseResult): ParseResult {

        var currentAnchorableStackFrame = context.currentAnchorableStackFrame
        while (currentAnchorableStackFrame != null && currentAnchorableStackFrame !is UnorderedParserContextStackFrame) {
            currentAnchorableStackFrame = currentAnchorableStackFrame.parent as? AnchorableParserContextStackFrame
        }

        if (currentAnchorableStackFrame !is UnorderedParserContextStackFrame) {
            return context.fail("Attempted to add to a view outside of an unordered scope")
        }

        addViewPropertyMember(context, currentAnchorableStackFrame, contentReference)
        return result
    }

    protected abstract fun addViewPropertyMember(
            context: ParserContext,
            unordered: UnorderedParserContextStackFrame,
            contentReference: TokenReference)
}


package modool.parser.rule.property

import modool.core.meta.property.PropertyMeta
import modool.parser.ParseResult
import modool.parser.ParserContext
import modool.core.content.token.TokenReference
import modool.parser.rule.TerminalParserRule
import modool.parser.stack.UnorderedParserContextStackFrame

class ElementPropertyGroupViewPropertyAdderTerminalValueRule(
        rule: TerminalParserRule,
        private val propertyGroup: PropertyMeta,
        private val property: PropertyMeta
) : ElementViewPropertyAdderRule(rule) {

    override fun validateContent(context: ParserContext, parseResult: ParseResult): Boolean {
        return parseResult is ParseResult.TokenMatch
    }

    override fun addViewPropertyMember(
            context: ParserContext,
            unordered: UnorderedParserContextStackFrame,
            contentReference: TokenReference) {
        unordered.addPropertyGroupViewPropertyMember(propertyGroup, property, contentReference, contentReference)
    }
}
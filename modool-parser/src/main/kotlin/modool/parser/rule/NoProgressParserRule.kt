package modool.parser.rule

import modool.parser.ParseResult
import modool.parser.ParserContext
import modool.parser.strategy.ParseStrategy

object NoProgressParserRule
    : ParserRule(),
        LeafNodeParserRule {

    override fun parse(context: ParserContext, strategy: ParseStrategy): ParseResult {
        return ParseResult.FailButOKInContext
    }
}
package modool.parser.rule

/**
 * A repeater is a simple whitespace separated list of rules
 */
class RepeaterParserRule(item: ParserRulable) :
        ListParserRule(
                open = null,
                item = item,
                separator = WhitespaceParserRule,
                close = null)
package modool.parser.rule

import modool.parser.ParseResult
import modool.parser.ParserContext
import modool.parser.strategy.ParseStrategy
import modool.core.content.signifier.tag.terminal.Contextual

object SemanticIndentationParserRule
    : ParserRule(),
        LeafNodeParserRule {

    private const val EXPECTING_FRAME = "Expecting indentation frame but none available"
    private const val EXPECTING_INDENT = "Expecting end of line followed by indented content"
    private const val INCORRECT_INDENT = "Incorrect level of indentation encountered"

    override fun parse(context: ParserContext, strategy: ParseStrategy): ParseResult {

        val indentFrame = context.currentIndentFrame ?: return context.fail(EXPECTING_FRAME)

        if (context.source.isMostRecentTerminalTagged(Contextual.EscapeSemanticIndentation)) {
            return ParseResult.OK
        }

        return context.matchNextContent { found ->
            if (found && context.source.isWhiteSpace() && indentFrame.isValid(context.source)) {
                ParseResult.TokenMatch
            } else if (found && context.source.isLeftMargin()) {
                context.fail(INCORRECT_INDENT)
            } else {
                context.fail(EXPECTING_INDENT)
            }
        }
    }
}
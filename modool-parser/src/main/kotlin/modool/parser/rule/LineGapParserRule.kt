package modool.parser.rule

import modool.parser.ParseResult
import modool.parser.ParserContext
import modool.parser.strategy.ParseStrategy

/**
 * Matches new lines that represent a gap between content.
 * Also matches at the end of content i.e. no more content is a sufficient gap.
 */
class LineGapParserRule(val size: Int)
    : ParserRule(),
        LeafNodeParserRule,
        FormatParserRule {

    override fun parse(context: ParserContext, strategy: ParseStrategy): ParseResult {
        return context.matchNextContent { found ->
            // Note: not found means end of content and our gap criteria is considered met
            if (!found || (found && context.source.isNewLine(size  + 1))) {
                ParseResult.TokenMatch
            } else {
                context.fail("Expected line gap of size: $size but none was found")
            }
        }
    }
}
package modool.parser.rule

import modool.core.config.SemanticIndentationStrategy
import modool.parser.ParseResult
import modool.parser.ParserContext
import modool.parser.stack.withIndentFrame
import modool.parser.strategy.ParseStrategy

class SemanticallyIndentedListParserRule(
        private val indentationStrategy: SemanticIndentationStrategy,
        item: ParserRulable)
    : ListParserRule(
        open = SemanticIndentationParserRule,
        item = item,
        separator = SemanticIndentationParserRule) {

    override fun parse(context: ParserContext, strategy: ParseStrategy): ParseResult {
        return context.withIndentFrame(indentationStrategy) {
            super.parse(context, strategy)
        }
    }
}
package modool.parser.rule

import modool.core.content.signifier.tag.Tag
import modool.core.content.token.ConstantTerminalDescriptor
import modool.parser.ParseResult
import modool.parser.ParserContext
import modool.parser.strategy.ParseStrategy

class ConstantTerminalParserRule(
        private val descriptor: ConstantTerminalDescriptor)
    : TerminalParserRule() {

    private val unmatchedErrorMessage = "Failed to match ${descriptor.get()}"

    override fun parse(context: ParserContext, strategy: ParseStrategy): ParseResult {
        return context.matchNextText { found ->
            when {
                !found -> context.fail(unmatchedErrorMessage)
                context.elementRepo.tryChangeTerminalType(context.source.createReference(), descriptor) -> ParseResult.TokenMatch
                else -> context.fail(unmatchedErrorMessage)
            }
        }
    }

    override fun populateAnchorTerminals(
            possibleAnchors: MutableCollection<ConstantTerminalDescriptor>,
            visitedRules: MutableSet<ParserRulable>) {
        if (!visitedRules.add(this)) {
            return
        }
        possibleAnchors.add(descriptor)
    }

    fun isTagApplicable(tag: Tag): Boolean {
        return descriptor.isTaggedType(tag)
    }
}
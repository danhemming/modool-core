package modool.parser.rule

import modool.parser.ParseResult
import modool.parser.ParserContext
import modool.parser.strategy.ParseStrategy
import modool.core.content.signifier.tag.Tag

class NameParserRule(private val nameTag: Tag)
    : TerminalParserRule() {

    override fun parse(context: ParserContext, strategy: ParseStrategy): ParseResult {
        return context.matchNextText { found ->
            when {
                !found -> context.fail("Name expected")
                context.source.isTerminalTagged(nameTag) -> ParseResult.TokenMatch
                context.source.isTerminalSoftKeyword() -> {
                    // Sometimes a soft keyword can also be an identifier in some languages
                    context.elementRepo.tryChangeTerminalTag(context.source.createReference(), nameTag)
                    ParseResult.TokenMatch
                }
                else -> context.fail("Name expected")
            }
        }
    }
}

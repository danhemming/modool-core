package modool.parser.rule

import modool.parser.ParseResult
import modool.parser.ParserContext
import modool.parser.strategy.ParseStrategy

abstract class LazyCompositeParserRule<R>(rulesProvider: () -> List<R>) : ParserRule()
        where R : ParserRulable {

    override val childRules: List<R> by lazy(rulesProvider)
    protected val anchors by lazy { calculateAnchors() }

    override fun parse(context: ParserContext, strategy: ParseStrategy): ParseResult {
        return strategy.parse(context, childRules)
    }
}
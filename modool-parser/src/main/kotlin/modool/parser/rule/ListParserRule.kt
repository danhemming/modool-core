package modool.parser.rule

import modool.core.content.token.TokenRangeOption
import modool.core.content.token.TokenReference
import modool.parser.ParseResult
import modool.parser.ParserContext
import modool.parser.stack.withListStackFrame
import modool.parser.strategy.ParseStrategy

open class ListParserRule(
        open: LeafNodeParserRule? = null,
        openSingle: LeafNodeParserRule? = null,
        private val item: ParserRulable,
        separator: LeafNodeParserRule? = null,
        close: LeafNodeParserRule? = null,
        private val leadOutItem: ParserRulable? = null,
        isTrailingSeparatorAllowed: Boolean = false,
        isTrailingSeparatorMandatory: Boolean = false,
        isCloseRequiredIfOpened: Boolean = true,
        isSubsequentItemOnlyAllowedWhenEnclosed: Boolean = false)
    : DelimitedParserRule(
        open = open,
        openSingle = openSingle,
        separator = separator,
        close = close,
        isCloseRequiredIfOpened = isCloseRequiredIfOpened,
        isSubsequentItemOnlyAllowedWhenEnclosed = isSubsequentItemOnlyAllowedWhenEnclosed,
        isTrailingSeparatorAllowed = isTrailingSeparatorAllowed,
        isTrailingSeparatorMandatory = isTrailingSeparatorMandatory),
        ListProviderParserRule {

    private val anchors by lazy { calculateAnchors() }
    override val childRules = listOfNotNull(open, item, separator, close)

    override fun parseItem(
            context: ParserContext,
            strategy: ParseStrategy,
            openEncountered: Boolean,
            openSingleEncountered: Boolean): ParseResult {

        return context.withListStackFrame(anchors) {
            strategy.parse(context, item)
        }
    }

    override fun parseLeadOutItem(context: ParserContext, strategy: ParseStrategy): ParseResult {
        return leadOutItem?.let { leadOut ->
            context.withListStackFrame(anchors) { strategy.parse(context, leadOut) }
        } ?: super.parseLeadOutItem(context, strategy)
    }

    override fun parseComplete(
            context: ParserContext,
            firstReference: TokenReference,
            lastReference: TokenReference): ParseResult {

        val endPosition = context.elementRepo.createList(
                firstReference,
                lastReference,
                TokenRangeOption.START_EXCLUSIVE_END_INCLUSIVE,
                context.contentOrigin)

        context.restoreTo(endPosition)
        return ParseResult.PropertyContainerMatch
    }
}
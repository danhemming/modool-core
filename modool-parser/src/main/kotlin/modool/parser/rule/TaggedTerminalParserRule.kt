package modool.parser.rule

import modool.parser.ParseResult
import modool.parser.ParserContext
import modool.parser.strategy.ParseStrategy
import modool.core.content.signifier.tag.Tag

/**
 * Matches a tagged terminal
 */
class TaggedTerminalParserRule(
        private val tag: Tag
) : TerminalParserRule() {

    override fun parse(context: ParserContext, strategy: ParseStrategy): ParseResult {
        return context.matchNextText { found ->
            when {
                !found -> context.fail(ERROR_TAGGED_TERMINAL_EXPECTED)
                context.source.isTerminalTagged(tag) -> ParseResult.TokenMatch
                else -> context.fail(ERROR_TAGGED_TERMINAL_EXPECTED)
            }
        }
    }

    companion object {
        private const val ERROR_TAGGED_TERMINAL_EXPECTED = "tagged terminal expected"
    }
}
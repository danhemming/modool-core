package modool.parser.rule

import modool.parser.ParseResult
import modool.parser.ParserContext
import modool.parser.strategy.ParseStrategy
import modool.core.content.signifier.tag.Tag

/**
 * Matches a tagged marker
 */
class TaggedMarkerParserRule(
        private val tag: Tag
) : TerminalParserRule() {

    override fun parse(context: ParserContext, strategy: ParseStrategy): ParseResult {
        return context.matchNextMarker { found ->
            when {
                !found -> context.fail(ERROR_TAGGED_MARKER_EXPECTED)
                context.source.isMarkerTagged(tag) -> ParseResult.TokenMatch
                else -> context.fail(ERROR_TAGGED_MARKER_EXPECTED)
            }
        }
    }

    companion object {
        private const val ERROR_TAGGED_MARKER_EXPECTED = "tagged marker expected"
    }
}
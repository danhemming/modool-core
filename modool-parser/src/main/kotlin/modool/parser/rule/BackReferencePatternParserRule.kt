package modool.parser.rule

import modool.parser.ParseResult
import modool.parser.ParserContext
import modool.parser.strategy.ParseStrategy
import modool.core.content.signifier.tag.Tag
import modool.core.content.token.peek

/**
 * Matches a tagged terminal and accesses the elemental context to determine previously matched terminals of the same
 * pattern to do a value equality check.
 * This is used when we expect a repeat of a pattern with the exact same value.
 *
 * Note: This may need improving if we ever have a circumstance that two properties test for the same pattern and we
 * need to distinguish them when matching. In that case the implementation will need to match on property name or some
 * additional tag on the first value
 */
class BackReferencePatternParserRule(
        private val patternTag: Tag
) : TerminalParserRule() {

    override fun parse(context: ParserContext, strategy: ParseStrategy): ParseResult {
        return context.matchNextText { found ->
            when {
                !found -> context.fail("Pattern expected")

                context.source.isTerminalTagged(patternTag) -> {
                    val sourcePosition = context.source.createReference()
                    context.source.peek {
                        val earlierMatchingPosition =
                                context.currentElementUpdater?.matchedPositions?.firstOrNull {

                                    context.source.moveTo(it)
                                    context.source.isTerminalTagged(patternTag) &&
                                            context.source.isTerminalEqual(sourcePosition)
                                }

                        if (earlierMatchingPosition == null) {
                            context.fail("Pattern found but value does not match previously encountered value")
                        } else {
                            ParseResult.TokenMatch
                        }
                    }
                }

                else -> context.fail("Pattern expected")
            }
        }
    }
}
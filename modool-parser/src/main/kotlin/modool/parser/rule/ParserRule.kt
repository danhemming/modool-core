package modool.parser.rule

abstract class ParserRule : ParserRulable {
    override val childRules: Iterable<ParserRulable> = emptyList()
}
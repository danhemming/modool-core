package modool.parser.rule.expression.operator.condition

enum class ExpressionConditionResult {
    ALLOW,
    DISALLOW,
    NOT_APPLICABLE,
    NOT_APPLICABLE_DEFAULT_TO_DISALLOW
}
package modool.parser.rule.expression.operator

import modool.core.meta.element.ElementMeta
import modool.core.meta.property.PropertyMeta
import modool.core.content.token.TokenRangeOption
import modool.parser.ParseResult
import modool.parser.ParserContext
import modool.parser.rule.TerminalParserRule
import modool.parser.rule.expression.operator.condition.ExpressionCondition
import modool.core.content.element.ElementPropertySetter
import modool.parser.strategy.ParseStrategy
import modool.core.content.token.TokenReference

open class TernaryOperatorExpressionPrecedenceParserRule(
        override val expression: ElementMeta,
        private val firstExpressionProperty: PropertyMeta,
        private val secondExpressionProperty: PropertyMeta,
        private val thirdExpressionProperty: PropertyMeta,
        private val firstOperatorRule: TerminalParserRule,
        private val secondOperatorRule: TerminalParserRule,
        override val isOverlapped: Boolean = false,
        conditions: List<ExpressionCondition> = emptyList()
) : OperatorExpressionPrecedenceParserRule(isOverlapped, conditions),
        OpenOperableExpressionPrecedenceParserRule,
        CloseOperableExpressionPrecedenceParserRule {

    override fun parseOpen(context: ParserContext, strategy: ParseStrategy): ParseResult {
        return strategy.parse(context, firstOperatorRule)
    }

    override fun parseClose(context: ParserContext, strategy: ParseStrategy): ParseResult {
        return strategy.parse(context, secondOperatorRule)
    }

    fun createObject(
            context: ParserContext,
            firstExpressionReference: TokenReference,
            secondExpressionReference: TokenReference,
            thirdExpressionReference: TokenReference): TokenReference {

        return context.elementRepo.createElement(
                context.elementRepo.getElementStart(firstExpressionReference),
                context.elementRepo.getElementEnd(thirdExpressionReference),
                TokenRangeOption.INCLUSIVE,
                context.contentOrigin,
                expression,
                listOf(
                        ElementPropertySetter.SetPropertyElementValue(
                                firstExpressionReference, context.contentOrigin, firstExpressionProperty),
                        ElementPropertySetter.SetPropertyElementValue(
                                secondExpressionReference, context.contentOrigin, secondExpressionProperty),
                        ElementPropertySetter.SetPropertyElementValue(
                                thirdExpressionReference, context.contentOrigin, thirdExpressionProperty)))
    }
}
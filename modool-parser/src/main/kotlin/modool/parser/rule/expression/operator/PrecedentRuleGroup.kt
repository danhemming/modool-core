package modool.parser.rule.expression.operator

import modool.core.content.operator.OperatorAssociativity

class PrecedentRuleGroup(
        val associativity: OperatorAssociativity,
        val operablePrecedenceRules: List<OperableExpressionPrecedenceParserRule>)
package modool.parser.rule.expression.operator

import modool.parser.ParseResult
import modool.parser.ParserContext
import modool.parser.strategy.ParseStrategy

interface OpenOperableExpressionPrecedenceParserRule : OperableExpressionPrecedenceParserRule {

    fun parseOpen(context: ParserContext, strategy: ParseStrategy): ParseResult {
        return ParseResult.Nothing
    }
}
package modool.parser.rule.expression.operator.condition

import modool.parser.ParserContext

abstract class ExpressionCondition {
    abstract fun evaluate(context: ParserContext): ExpressionConditionResult
}
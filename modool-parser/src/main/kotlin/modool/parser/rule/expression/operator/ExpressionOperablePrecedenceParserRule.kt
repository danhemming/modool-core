package modool.parser.rule.expression.operator

import modool.core.meta.element.ElementMeta
import modool.parser.ParseResult
import modool.parser.ParserContext
import modool.parser.rule.ElementProviderParserRule
import modool.parser.rule.expression.operator.condition.ExpressionCondition
import modool.parser.strategy.ParseStrategy

/**
 * This is used to wrap an expression to allow it participate precedence logic
 */
class ExpressionOperablePrecedenceParserRule(
        override val expression: ElementMeta,
        private val expressionRule: ElementProviderParserRule,
        override val isOverlapped: Boolean = false,
        conditions: List<ExpressionCondition> = emptyList()
) : OperatorExpressionPrecedenceParserRule(isOverlapped, conditions),
        OpenOperableExpressionPrecedenceParserRule {

    override fun parseOpen(context: ParserContext, strategy: ParseStrategy): ParseResult {
        return expressionRule.parse(context, strategy)
    }
}
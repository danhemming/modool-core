package modool.parser.rule.expression.operator

import modool.core.meta.element.ElementMeta
import modool.core.meta.property.PropertyMeta
import modool.core.content.token.TokenRangeOption
import modool.parser.ParseResult
import modool.parser.ParserContext
import modool.parser.rule.TerminalParserRule
import modool.parser.rule.expression.operator.condition.ExpressionCondition
import modool.core.content.element.ElementPropertySetter
import modool.parser.strategy.ParseStrategy
import modool.core.content.token.TokenReference

class UnaryPrefixOperatorExpressionPrecedenceParserRule(
        override val expression: ElementMeta,
        private val expressionProperty: PropertyMeta,
        private val operatorRule: TerminalParserRule,
        override val isOverlapped: Boolean = false,
        conditions: List<ExpressionCondition> = emptyList()
) : OperatorExpressionPrecedenceParserRule(isOverlapped, conditions),
        OpenOperableExpressionPrecedenceParserRule {

    override fun parseOpen(context: ParserContext, strategy: ParseStrategy): ParseResult {
        return strategy.parse(context, operatorRule)
    }

    fun createObject(
            context: ParserContext,
            openReference: TokenReference,
            expressionReference: TokenReference): TokenReference {

        return context.elementRepo.createElement(
                openReference,
                context.elementRepo.getElementEnd(expressionReference),
                TokenRangeOption.INCLUSIVE,
                context.contentOrigin,
                expression,
                listOf(
                        ElementPropertySetter.SetPropertyElementValue(
                                expressionReference, context.contentOrigin, expressionProperty)))
    }
}
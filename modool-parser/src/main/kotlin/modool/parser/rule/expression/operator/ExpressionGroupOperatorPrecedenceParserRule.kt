package modool.parser.rule.expression.operator

import modool.core.meta.element.ElementMeta
import modool.core.meta.property.PropertyMeta
import modool.core.content.token.TokenRangeOption
import modool.parser.ParseResult
import modool.parser.ParserContext
import modool.parser.rule.TerminalParserRule
import modool.parser.rule.expression.operator.condition.ExpressionCondition
import modool.core.content.element.ElementPropertySetter
import modool.parser.strategy.ParseStrategy
import modool.core.content.token.TokenReference

/**
 * This is basically for the purpose of a bracketed expression, some languages treat it as an operator in their specification
 * and it is given a precedence so we will honour this.
 */
class ExpressionGroupOperatorPrecedenceParserRule(
        override val expression: ElementMeta,
        private val expressionProperty: PropertyMeta,
        private val openOperatorRule: TerminalParserRule,
        private val closeOperatorRule: TerminalParserRule,
        override val isOverlapped: Boolean = false,
        conditions: List<ExpressionCondition> = emptyList()
) : OperatorExpressionPrecedenceParserRule(isOverlapped, conditions),
        GroupingOperatorPrecedenceParserRule,
        ContextBoundaryOperatorPrecedenceParserRule {

    override fun parseOpen(context: ParserContext, strategy: ParseStrategy): ParseResult {
        return strategy.parse(context, openOperatorRule)
    }

    override fun parseClose(context: ParserContext, strategy: ParseStrategy): ParseResult {
        return strategy.parse(context, closeOperatorRule)
    }

    fun createObject(
            context: ParserContext,
            openReference: TokenReference,
            expressionReference: TokenReference,
            closeReference: TokenReference): TokenReference {

        return context.elementRepo.createElement(
                openReference,
                closeReference,
                TokenRangeOption.INCLUSIVE,
                context.contentOrigin,
                expression,
                listOf(
                        ElementPropertySetter.SetPropertyElementValue(
                                expressionReference, context.contentOrigin, expressionProperty)))
    }
}
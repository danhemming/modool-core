package modool.parser.rule.expression.operator

/**
 * A marker interface that determines if the operator introduces a new scope i.e. is not effected by the conditions
 * applied to other previous rules.
 */
interface ContextBoundaryOperatorPrecedenceParserRule
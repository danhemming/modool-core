package modool.parser.rule.expression.operator

import modool.core.meta.element.ElementMeta
import modool.parser.ParserContext

interface OperableExpressionPrecedenceParserRule {
    /** Represents if this rule can be confused with another rule **/
    val isOverlapped: Boolean
    val isContextDependant: Boolean
    val expression: ElementMeta
    fun isApplicable(context: ParserContext): Boolean
}
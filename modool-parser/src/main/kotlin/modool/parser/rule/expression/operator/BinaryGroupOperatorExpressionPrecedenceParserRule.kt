package modool.parser.rule.expression.operator

import modool.core.meta.element.ElementMeta
import modool.core.meta.property.PropertyMeta
import modool.core.content.token.TokenRangeOption
import modool.parser.ParseResult
import modool.parser.ParserContext
import modool.parser.rule.TerminalParserRule
import modool.parser.rule.expression.operator.condition.ExpressionCondition
import modool.core.content.element.ElementPropertySetter
import modool.parser.strategy.ParseStrategy
import modool.core.content.token.TokenReference

/**
 * This is used for enclosing / paired operators
 * The most common example is an array indexer e.g. blah[index]
 */
class BinaryGroupOperatorExpressionPrecedenceParserRule(
        override val expression: ElementMeta,
        private val lhsProperty: PropertyMeta,
        private val rhsProperty: PropertyMeta,
        openRule: TerminalParserRule,
        private val closeRule: TerminalParserRule,
        override val isOverlapped: Boolean = false,
        conditions: List<ExpressionCondition> = emptyList()
) : BinaryOperatorExpressionPrecedenceParserRule(
        expression,
        lhsProperty,
        rhsProperty,
        openRule,
        isOverlapped,
        conditions),
        GroupingOperatorPrecedenceParserRule {

    override fun parseClose(context: ParserContext, strategy: ParseStrategy): ParseResult {
        return strategy.parse(context, closeRule)
    }

    fun createObject(
            context: ParserContext,
            lhsReference: TokenReference,
            rhsReference: TokenReference,
            closePosition: TokenReference
    ): TokenReference {

        return context.elementRepo.createElement(
                context.elementRepo.getElementStart(lhsReference),
                closePosition,
                TokenRangeOption.INCLUSIVE,
                context.contentOrigin,
                expression,
                listOf(
                        ElementPropertySetter.SetPropertyElementValue(lhsReference, context.contentOrigin, lhsProperty),
                        ElementPropertySetter.SetPropertyElementValue(rhsReference, context.contentOrigin, rhsProperty)))
    }
}
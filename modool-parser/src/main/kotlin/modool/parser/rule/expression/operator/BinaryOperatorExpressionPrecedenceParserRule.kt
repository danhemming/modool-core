package modool.parser.rule.expression.operator

import modool.core.meta.element.ElementMeta
import modool.core.meta.property.PropertyMeta
import modool.core.content.token.TokenRangeOption
import modool.parser.ParseResult
import modool.parser.ParserContext
import modool.parser.rule.TerminalParserRule
import modool.parser.rule.expression.operator.condition.ExpressionCondition
import modool.core.content.element.ElementPropertySetter
import modool.parser.strategy.ParseStrategy
import modool.core.content.token.TokenReference

open class BinaryOperatorExpressionPrecedenceParserRule(
        override val expression: ElementMeta,
        private val lhsProperty: PropertyMeta,
        private val rhsProperty: PropertyMeta,
        private val operatorRule: TerminalParserRule,
        override val isOverlapped: Boolean = false,
        conditions: List<ExpressionCondition> = emptyList()
) : OperatorExpressionPrecedenceParserRule(isOverlapped, conditions),
        OpenOperableExpressionPrecedenceParserRule {

    override fun parseOpen(context: ParserContext, strategy: ParseStrategy): ParseResult {
        return strategy.parse(context, operatorRule)
    }

    fun createObject(
            context: ParserContext,
            lhsReference: TokenReference,
            rhsReference: TokenReference): TokenReference {

        return context.elementRepo.createElement(
                context.elementRepo.getElementStart(lhsReference),
                context.elementRepo.getElementEnd(rhsReference),
                TokenRangeOption.INCLUSIVE,
                context.contentOrigin,
                expression,
                listOf(
                        ElementPropertySetter.SetPropertyElementValue(lhsReference, context.contentOrigin, lhsProperty),
                        ElementPropertySetter.SetPropertyElementValue(rhsReference, context.contentOrigin, rhsProperty)))
    }
}
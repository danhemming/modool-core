package modool.parser.rule.expression.operator

interface GroupingOperatorPrecedenceParserRule :
        OpenOperableExpressionPrecedenceParserRule,
        CloseOperableExpressionPrecedenceParserRule
package modool.parser.rule.expression.operator.condition

import modool.core.meta.element.ElementMeta
import modool.parser.ParserContext

class ExpressionAllowCondition(
        private val categoryType: ElementMeta,
        private val elementType: ElementMeta
) : ExpressionCondition() {

    override fun evaluate(context: ParserContext): ExpressionConditionResult {
        val nearestExpressionFrame = context.nearestElementalStackFrameForAssignableType(categoryType)
                ?: return ExpressionConditionResult.NOT_APPLICABLE_DEFAULT_TO_DISALLOW

        return if (nearestExpressionFrame.isAttemptingType(elementType)) {
            ExpressionConditionResult.ALLOW
        } else {
            ExpressionConditionResult.NOT_APPLICABLE_DEFAULT_TO_DISALLOW
        }
    }
}
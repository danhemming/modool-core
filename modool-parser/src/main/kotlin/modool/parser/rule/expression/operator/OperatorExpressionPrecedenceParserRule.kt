package modool.parser.rule.expression.operator

import modool.parser.ParserContext
import modool.parser.rule.expression.operator.condition.ExpressionCondition
import modool.parser.rule.expression.operator.condition.ExpressionConditionResult

abstract class OperatorExpressionPrecedenceParserRule(
        override val isOverlapped: Boolean,
        private val conditions: List<ExpressionCondition>
) : OperableExpressionPrecedenceParserRule {

    override val isContextDependant: Boolean = conditions.isNotEmpty()

    override fun isApplicable(context: ParserContext): Boolean {
        var default = true
        conditions.forEach {
            when (it.evaluate(context)) {
                ExpressionConditionResult.ALLOW -> return true
                ExpressionConditionResult.DISALLOW -> return false
                ExpressionConditionResult.NOT_APPLICABLE -> {}
                ExpressionConditionResult.NOT_APPLICABLE_DEFAULT_TO_DISALLOW -> default = false
            }
        }
        return default
    }
}
package modool.parser.rule.expression

import modool.core.meta.element.ElementMeta
import modool.core.content.operator.OperatorAssociativity
import modool.parser.ParseResult
import modool.parser.ParserContext
import modool.parser.rule.ElementProviderParserRule
import modool.parser.rule.EndOfContentParserRule
import modool.parser.rule.ParserRulable
import modool.parser.rule.ParserRule
import modool.parser.rule.expression.operator.*
import modool.parser.stack.ExpressionStackFrame
import modool.parser.stack.withElementalStackFrame
import modool.parser.strategy.ParseStrategy
import modool.core.content.token.TokenReference
import modool.core.content.token.peek
import java.util.*
import kotlin.math.min

/**
 * Parser rule that recursively extracts expressions
 *
 * Supports recovery for ambiguous expression grammars where operators of different precedence have overlapping opening
 * syntax e.g. Java group and cast i.e. (expr) vs. (type)expr.  A grammar is considered ambiguous if any expression type
 * has the same opening symbol as another expression type.
 *
 * Supports allow & disallow conditions for each rule based on the context
 * e.g. Javascript function call vs. sequence i.e. call(expr, expr) vs. expr, expr. Here the comma should be resolved to
 * the higher precedence function call.
 *
 * Note: does not currently support conditions on n-ary expressions because there is no stack frame for them
 */
class ExpressionParserRule(
        private val precedentGroups: List<PrecedentRuleGroup>,
        private val terminatorRule: ParserRulable,
        private val expression: ElementMeta
) : ParserRule(),
        ElementProviderParserRule {

    private val ruleAssociativityLookup = mutableMapOf<OperableExpressionPrecedenceParserRule, OperatorAssociativity>()
    private val openableRules = mutableListOf<OpenOperableExpressionPrecedenceParserRule>()
    private val closableOnlyRules = mutableListOf<CloseOperableExpressionPrecedenceParserRule>()
    private var isContextDependant = false

    init {
        precedentGroups.forEach { group ->
            group.operablePrecedenceRules.forEach { rule ->
                if (rule !is OpenOperableExpressionPrecedenceParserRule
                        && rule is CloseOperableExpressionPrecedenceParserRule) {
                    closableOnlyRules.add(rule)
                } else if (rule is OpenOperableExpressionPrecedenceParserRule) {
                    openableRules.add(rule)
                }

                isContextDependant = isContextDependant || rule.isContextDependant
                ruleAssociativityLookup[rule] = group.associativity
            }
        }
    }

    override fun parse(context: ParserContext, strategy: ParseStrategy): ParseResult {

        val parts = mutableListOf<Expression.Content>()
        val startReference = context.source.createReference()
        var recoverAfterRule: OperableExpressionPrecedenceParserRule? = null
        var recoveryPartCount = -1
        val openClosableOperators = ArrayDeque<Expression.OpenOperator>()
        var isRecoverySupported = false

        while (true) {
            val currentPartCount = parts.count()
            val currentPart = if (currentPartCount == 0) null else parts.last()
            // Need to make sure a higher precedence unary +/- does not consume our binary operator
            val allowUnaryPrefixOperator = currentPart !is Expression.Element && currentPart !is Expression.CloseOperator
            // A binary operator (even a group one e.g. prop[index] needs some leading expression ... could be postfix operator too)
            val allowBinaryOrTernaryOperator = currentPart is Expression.Element || currentPart is Expression.CloseOperator

            if (currentPart is Expression.Element) {
                val postfixPart = tryParseClosableOnlyOperator(context, strategy)
                if (postfixPart is Expression.CloseOperator) {
                    parts.add(postfixPart)
                }
            }

            if (allowBinaryOrTernaryOperator) {
                openClosableOperators.peek()?.let {
                    val closePart = tryParseCloseOperator(context, strategy, it)
                    if (closePart is Expression.CloseOperator) {
                        parts.add(closePart)
                        openClosableOperators.pop()
                        isRecoverySupported = isRecoverySupported || closePart.operablePrecedenceRule.isOverlapped
                    }
                }
            }

            val nextPart = tryParseOpen(
                    context, strategy,
                    allowUnaryPrefixOperator, allowBinaryOrTernaryOperator,
                    recoverAfterRule)

            if (nextPart is Expression.OpenOperator) {
                if (nextPart.operablePrecedenceRule is GroupingOperatorPrecedenceParserRule) {
                    @Suppress("UNCHECKED_CAST")
                    val subExpressionResult = tryParseGroupedSubExpressionAndPopulateParts(
                            context, strategy,
                            parts,
                            nextPart,
                            nextPart.operablePrecedenceRule)
                    if (subExpressionResult !is ParseResult.Success
                            && (isRecoverySupported || nextPart.operablePrecedenceRule.isOverlapped)) {
                        isRecoverySupported = true
                        nextPart.rollback(context)
                        recoverAfterRule = nextPart.operablePrecedenceRule
                        continue
                    }
                    isRecoverySupported = isRecoverySupported || nextPart.operablePrecedenceRule.isOverlapped
                } else {
                    if (nextPart.operablePrecedenceRule is CloseOperableExpressionPrecedenceParserRule) {
                        openClosableOperators.push(nextPart)
                        isRecoverySupported = isRecoverySupported || nextPart.operablePrecedenceRule.isOverlapped
                    }
                    parts.add(nextPart)
                }
            } else if (nextPart is Expression.ElementFromRule) {
                parts.add(nextPart)
                isRecoverySupported = isRecoverySupported || nextPart.operablePrecedenceRule.isOverlapped
            } else if (nextPart is Expression.Element) {
                parts.add(nextPart)
            }

            val atTerminator = tryParseTerminator(context, strategy)
            val atEndOfContent = !atTerminator && tryParseEndOfContent(context, strategy)
            val progressNotMade = currentPartCount == parts.count()
            val expressionsInSequence = currentPart is Expression.Element && nextPart is Expression.Element
            val cannotProgress = atTerminator || atEndOfContent || progressNotMade || expressionsInSequence
            val recoveryHasFailed = cannotProgress && recoveryPartCount == parts.count()

            recoverAfterRule = null
            recoveryPartCount = -1

            if (cannotProgress) {
                val result = if (recoveryHasFailed) {
                    ParseResult.FailButOKInContext
                } else {
                    buildExpressionTree(
                            context,
                            parts,
                            startOfContentReference = startReference,
                            terminalEncountered = atTerminator,
                            requireRollbackRecovery = isRecoverySupported)
                }
                if (result is ParseResult.Fail && isRecoverySupported) {
                    val partToRecover = parts.asSequence().filterIsInstance<Expression.ContentRulable>().lastOrNull()
                    partToRecover?.let {
                        recoverAfterRule = partToRecover.operablePrecedenceRule
                        partToRecover.rollback(context)
                        @Suppress("ControlFlowWithEmptyBody")
                        while (parts.removeAt(parts.count() - 1) != partToRecover);
                        recoveryPartCount = parts.count()
                    }

                    if (recoverAfterRule != null) {
                        continue
                    }
                }
                return result
            }
        }
    }

    private fun tryParseGroupedSubExpressionAndPopulateParts(
            context: ParserContext,
            strategy: ParseStrategy,
            parts: MutableList<Expression.Content>,
            openOperatorPart: Expression.OpenOperator,
            groupRule: GroupingOperatorPrecedenceParserRule): ParseResult {

        // Only use the special expression stack frame if we have rules to check, otherwise save the object creation
        val subExpressionParseResult =
                if (isContextDependant
                        && openOperatorPart.operablePrecedenceRule is ContextBoundaryOperatorPrecedenceParserRule) {
                    context.withElementalStackFrame(ExpressionStackFrame(context.peekStackFrame(), groupRule.expression)) {
                        parse(context, strategy)
                    }
                } else {
                    parse(context, strategy)
                }

        val subExpressionPosition = if (subExpressionParseResult is ParseResult.ElementMatch
                && context.elementRepo.isElementAt(context.source.createReference(), expression)) {
            context.source.createReference()
        } else {
            return subExpressionParseResult
        }

        val rollbackPosition = context.source.createReference()
        val closeResult = groupRule.parseClose(context, strategy)
        if (closeResult is ParseResult.TokenMatch) {
            parts.add(openOperatorPart)
            val openOperatorPartIndex = parts.count() - 1
            parts.add(Expression.Element(subExpressionPosition))
            parts.add(Expression.CloseOperator(
                    rollbackPosition,
                    context.source.createPriorWhitespaceReference(),
                    context.source.createReference(),
                    openOperatorPart.operablePrecedenceRule))

            // NOTE: We treat this operator as having the highest precedence and always having LTR associativity
            val reduceParseResult = reducePartsForGroupOperatorAt(context, parts, openOperatorPart, openOperatorPartIndex)
            if (reduceParseResult !is ParseResult.Success) {
                @Suppress("ControlFlowWithEmptyBody")
                while (parts.isNotEmpty() && parts.removeAt(parts.count() - 1) != openOperatorPart);
            }
            return reduceParseResult
        }

        return closeResult
    }

    private fun tryParseOpen(
            context: ParserContext,
            strategy: ParseStrategy,
            allowUnaryPrefixOperator: Boolean,
            allowBinaryOrTernaryOperator: Boolean,
            recoveryRule: OperableExpressionPrecedenceParserRule?
    ): Expression {

        var continueAfterRuleFound = recoveryRule == null
        val rollbackToken = context.source.createReference()

        return context.optionally {

            for (operatorRule in openableRules) {

                if (!continueAfterRuleFound) {
                    continueAfterRuleFound = recoveryRule == operatorRule
                    continue
                }

                if (!allowUnaryPrefixOperator && operatorRule is UnaryPrefixOperatorExpressionPrecedenceParserRule) {
                    continue
                }

                if (!allowBinaryOrTernaryOperator
                        && (operatorRule is BinaryOperatorExpressionPrecedenceParserRule
                                || operatorRule is TernaryOperatorExpressionPrecedenceParserRule)) {
                    continue
                }

                if (!operatorRule.isApplicable(context)) {
                    continue
                }

                val openResult = operatorRule.parseOpen(context, strategy)

                if (operatorRule is ExpressionOperablePrecedenceParserRule) {
                    if (openResult is ParseResult.ElementMatch) {
                        return Expression.ElementFromRule(context.source.createReference(), operatorRule)
                    } else if (openResult is ParseResult.TokenMatch) {
                        // Matches an "operator" that is part of the expression but not the expression itself
                        return Expression.None
                    }
                } else if (openResult is ParseResult.TokenMatch) {
                    return Expression.OpenOperator(
                            rollbackToken,
                            context.source.createPriorWhitespaceReference(),
                            context.source.createReference(),
                            operatorRule)
                }
            }

            Expression.None
        }
    }

    private fun tryParseClosableOnlyOperator(
            context: ParserContext,
            strategy: ParseStrategy): Expression {

        return context.optionally {
            val rollbackPosition = context.source.createReference()

            for (unaryPostfixRule in closableOnlyRules) {
                if (!unaryPostfixRule.isApplicable(context)) {
                    continue
                }
                val closeResult = unaryPostfixRule.parseClose(context, strategy)
                if (closeResult is ParseResult.TokenMatch) {
                    return Expression.CloseOperator(
                            rollbackPosition,
                            context.source.createPriorWhitespaceReference(),
                            context.source.createReference(),
                            unaryPostfixRule)
                } else if (closeResult is ParseResult.Success) {
                    return Expression.None
                }
            }

            Expression.None
        }
    }

    private fun tryParseCloseOperator(
            context: ParserContext,
            strategy: ParseStrategy,
            openOperatorPart: Expression.OpenOperator): Expression {

        @Suppress("UNCHECKED_CAST")
        val openPrecedenceRule = openOperatorPart.operablePrecedenceRule as? CloseOperableExpressionPrecedenceParserRule
                ?: return Expression.None

        return context.optionally {
            val rollbackPosition = context.source.createReference()
            val closeResult = openPrecedenceRule.parseClose(context, strategy)
            if (closeResult is ParseResult.TokenMatch) {
                return Expression.CloseOperator(
                        rollbackPosition,
                        context.source.createPriorWhitespaceReference(),
                        context.source.createReference(),
                        openOperatorPart.operablePrecedenceRule)
            }

            Expression.None
        }
    }

    private fun tryParseTerminator(context: ParserContext, strategy: ParseStrategy): Boolean {
        return context.source.peek { strategy.parse(context, terminatorRule) } is ParseResult.Success
    }

    private fun tryParseEndOfContent(context: ParserContext, strategy: ParseStrategy): Boolean {
        return strategy.parse(context, EndOfContentParserRule) is ParseResult.Success
    }

    private fun buildExpressionTree(
            context: ParserContext,
            parts: MutableList<Expression.Content>,
            startOfContentReference: TokenReference,
            terminalEncountered: Boolean,
            requireRollbackRecovery: Boolean): ParseResult {

        when (parts.count()) {
            0 -> return if (terminalEncountered) ParseResult.Nothing else context.fail("Failed to parse expression")
            1 -> return calculateSinglePartParseResult(context, parts)
        }

        if (!validateAssociativity(parts)) {
            return context.fail("Multiple operators with no associativity were used in sequence")
        }

        val reductionParts = if (requireRollbackRecovery) parts.toMutableList() else parts
        val snapshot =
                if (requireRollbackRecovery) context.elementRepo.createSnapshot(startOfContentReference)
                else null

        precedentGroups.forEach { group ->

            when (group.associativity) {
                OperatorAssociativity.NONE,
                OperatorAssociativity.LEFT_TO_RIGHT -> {
                    var partIndex = 0
                    while (partIndex < reductionParts.count()) {
                        val part = reductionParts[partIndex]
                        if (part is Expression.Operator && part.operablePrecedenceRule in group.operablePrecedenceRules) {
                            val reduceResult = reducePartsForNaryOperatorAt(context, reductionParts, part, partIndex, group.associativity)
                            if (reduceResult is ParseResult.Fail) {
                                return reduceResult
                            }
                            // Continue scanning from the same point
                        } else {
                            partIndex++
                        }
                    }
                }

                OperatorAssociativity.RIGHT_TO_LEFT -> {
                    var partIndex = reductionParts.count() - 1
                    while (partIndex >= 0) {
                        val part = reductionParts[partIndex]
                        if (part is Expression.Operator && part.operablePrecedenceRule in group.operablePrecedenceRules) {
                            val reduceResult = reducePartsForNaryOperatorAt(context, reductionParts, part, partIndex, group.associativity)
                            if (reduceResult is ParseResult.Fail) {
                                return reduceResult
                            }
                            partIndex = min(partIndex - 1, reductionParts.count() - 2)
                        } else {
                            partIndex--
                        }
                    }
                }
            }

            if (reductionParts.count() == 1) {
                return calculateSinglePartParseResult(context, reductionParts)
            }
        }

        return if (reductionParts.count() == 1) {
            calculateSinglePartParseResult(context, reductionParts)
        } else {
            if (requireRollbackRecovery && snapshot != null) {
                // Undo the effect of reduction on the token chain i.e. remove binary expression elements etc.
                context.elementRepo.restoreSnapshot(snapshot)
            }
            context.fail("Failed to parse expression")
        }
    }

    private fun calculateSinglePartParseResult(
            context: ParserContext,
            parts: Iterable<Expression.Content>): ParseResult {
        val result = parts.first()
        return if (result is Expression.Element) {
            // This should take care of the terminator
            context.elementRepo.expandElement(result.reference, context.source.createReference())
            ParseResult.ElementMatch
        } else {
            context.fail("Failed to parse expression")
        }
    }

    private fun reducePartsForNaryOperatorAt(
            context: ParserContext,
            parts: MutableList<Expression.Content>,
            operator: Expression.Operator,
            operatorIndex: Int,
            operatorAssociativity: OperatorAssociativity): ParseResult {

        @Suppress("UNCHECKED_CAST")
        return when (operator.operablePrecedenceRule) {

            is BinaryOperatorExpressionPrecedenceParserRule ->
                buildBinaryExpression(context, operator, operatorIndex, parts)

            is UnaryPrefixOperatorExpressionPrecedenceParserRule ->
                buildUnaryPrefixExpression(context, operator, operatorIndex, parts)

            is UnaryPostfixOperatorExpressionPrecedenceParserRule ->
                buildUnaryPostfixExpression(context, operator, operatorIndex, parts)

            is TernaryOperatorExpressionPrecedenceParserRule ->
                buildTernaryExpression(context, operator, operatorIndex, operatorAssociativity, parts)

            else -> context.fail("Unexpected operator type")
        }
    }

    @Suppress("UNCHECKED_CAST")
    private fun buildTernaryExpression(
            context: ParserContext,
            operator: Expression.Operator,
            operatorIndex: Int,
            operatorAssociativity: OperatorAssociativity,
            parts: MutableList<Expression.Content>): ParseResult {

        val ternaryRange = if (operatorAssociativity == OperatorAssociativity.RIGHT_TO_LEFT) {
            operatorIndex - 3..operatorIndex + 1
        } else {
            operatorIndex - 1..operatorIndex + 3
        }

        val ternaryParts = extractParts(parts, ternaryRange)
                ?: return context.fail("Failed to detect content for ternary operator")

        val firstExpression = ternaryParts[TERNARY_EXPRESSION_FIRST].reference
        val secondExpression = ternaryParts[TERNARY_EXPRESSION_SECOND].reference
        val thirdExpression = ternaryParts[TERNARY_EXPRESSION_THIRD].reference
        val firstOperatorRule = operator.operablePrecedenceRule as TernaryOperatorExpressionPrecedenceParserRule
        val ternaryExpressionReference = firstOperatorRule.createObject(
                context,
                firstExpression,
                secondExpression,
                thirdExpression)
        context.source.moveTo(ternaryExpressionReference)

        parts.add(ternaryRange.first, Expression.ElementFromRule(ternaryExpressionReference, firstOperatorRule))
        return ParseResult.ElementMatch
    }

    @Suppress("UNCHECKED_CAST")
    private fun buildBinaryExpression(
            context: ParserContext,
            operator: Expression.Operator,
            operatorIndex: Int,
            parts: MutableList<Expression.Content>): ParseResult {

        val binaryRange = operatorIndex - 1..operatorIndex + 1
        val binaryParts = extractParts(parts, binaryRange)
                ?: return context.fail("Failed to detect content for binary operator")

        val lhs = (binaryParts[BINARY_EXPRESSION_LHS] as? Expression.Element)?.reference
                ?: return context.fail("Failed to detect expression prior to binary operator")

        val rhs = (binaryParts[BINARY_EXPRESSION_RHS] as? Expression.Element)?.reference
                ?: return context.fail("Failed to detect expression following binary operator")

        val opRule = operator.operablePrecedenceRule as BinaryOperatorExpressionPrecedenceParserRule
        val binaryExpressionReference = opRule.createObject(context, lhs, rhs)
        context.source.moveTo(binaryExpressionReference)

        parts.add(binaryRange.first, Expression.ElementFromRule(binaryExpressionReference, opRule))
        return ParseResult.ElementMatch
    }

    @Suppress("UNCHECKED_CAST")
    private fun buildUnaryPrefixExpression(
            context: ParserContext,
            operator: Expression.Operator,
            operatorIndex: Int,
            parts: MutableList<Expression.Content>): ParseResult {

        val unaryRange = operatorIndex..operatorIndex + 1

        val unaryParts = extractParts(parts, unaryRange)
                ?: return context.fail("Failed to detect content for unary prefix operator")

        val expr = (unaryParts[1] as? Expression.Element)?.reference
                ?: return context.fail("Failed to detect expression for unary prefix operator")

        val opRule = operator.operablePrecedenceRule as UnaryPrefixOperatorExpressionPrecedenceParserRule
        val unaryExpressionReference = opRule.createObject(context, operator.openReference, expr)
        context.source.moveTo(unaryExpressionReference)

        parts.add(unaryRange.first, Expression.ElementFromRule(unaryExpressionReference, opRule))
        return ParseResult.ElementMatch
    }

    @Suppress("UNCHECKED_CAST")
    private fun buildUnaryPostfixExpression(
            context: ParserContext,
            operator: Expression.Operator,
            operatorIndex: Int,
            parts: MutableList<Expression.Content>): ParseResult {

        val unaryRange = operatorIndex - 1..operatorIndex

        val unaryParts = extractParts(parts, unaryRange)
                ?: return context.fail("Failed to detect content for unary postfix operator")

        val expr = (unaryParts[0] as? Expression.Element)?.reference
                ?: return context.fail("Failed to detect expression for unary postfix operator")

        val opRule = operator.operablePrecedenceRule as UnaryPostfixOperatorExpressionPrecedenceParserRule
        val unaryExpressionReference = opRule.createObject(context, expr, operator.reference)
        context.source.moveTo(unaryExpressionReference)

        parts.add(unaryRange.first, Expression.ElementFromRule(unaryExpressionReference, opRule))
        return ParseResult.ElementMatch
    }

    private fun reducePartsForGroupOperatorAt(
            context: ParserContext,
            parts: MutableList<Expression.Content>,
            operator: Expression.Operator,
            operatorIndex: Int): ParseResult {

        @Suppress("UNCHECKED_CAST")
        return when (operator.operablePrecedenceRule) {
            is BinaryGroupOperatorExpressionPrecedenceParserRule ->
                buildBinaryGroupedExpression(context, operator, operatorIndex, parts)

            is ExpressionGroupOperatorPrecedenceParserRule ->
                buildExpressionGroup(context, operator, operatorIndex, parts)

            else -> context.fail("Unexpected operator type")
        }
    }

    @Suppress("UNCHECKED_CAST")
    private fun buildBinaryGroupedExpression(
            context: ParserContext,
            operator: Expression.Operator,
            openOperatorIndex: Int,
            parts: MutableList<Expression.Content>): ParseResult {

        val binaryBoundedRange = openOperatorIndex - 1..openOperatorIndex + 2
        val binaryBoundedParts = extractParts(parts, binaryBoundedRange)
                ?: return context.fail("Failed to detect content for binary operator")

        val lhs = (binaryBoundedParts[BINARY_EXPRESSION_GROUP_LHS] as? Expression.Element)?.reference
                ?: return context.fail("Failed to detect expression prior to binary operator")

        if (binaryBoundedParts[BINARY_EXPRESSION_GROUP_OPEN] !is Expression.OpenOperator) {
            return context.fail("Failed to detect open operator")
        }

        val rhs = (binaryBoundedParts[BINARY_EXPRESSION_GROUP_RHS] as? Expression.Element)?.reference
                ?: return context.fail("Failed to detect expression following binary operator")

        val closeOperator = (binaryBoundedParts[BINARY_EXPRESSION_GROUP_CLOSE] as? Expression.CloseOperator)
                ?: return context.fail("Failed to detect close operator")

        val opRule = operator.operablePrecedenceRule as BinaryGroupOperatorExpressionPrecedenceParserRule
        val boundedBinaryExpression = opRule.createObject(context, lhs, rhs, closeOperator.reference)
        context.source.moveTo(boundedBinaryExpression)

        parts.add(binaryBoundedRange.first, Expression.ElementFromRule(boundedBinaryExpression, opRule))
        return ParseResult.ElementMatch
    }

    @Suppress("UNCHECKED_CAST")
    private fun buildExpressionGroup(
            context: ParserContext,
            operator: Expression.Operator,
            operatorIndex: Int,
            parts: MutableList<Expression.Content>): ParseResult {

        val boundedRange = operatorIndex..operatorIndex + 2
        val boundedParts = extractParts(parts, boundedRange)
                ?: return context.fail("Failed to detect content for grouped expression")

        val openOp = (boundedParts[EXPRESSION_GROUP_OPEN] as? Expression.Operator)
                ?: return context.fail("Failed to detect opening operator for grouped expression")

        val expr = (boundedParts[EXPRESSION_GROUP_EXPRESSION] as? Expression.Element)?.reference
                ?: return context.fail("Failed to detect expression for grouped expression")

        val closeOp = (boundedParts[EXPRESSION_GROUP_CLOSE] as? Expression.Operator)
                ?: return context.fail("Failed to detect closing operator for grouped expression")

        val opRule = operator.operablePrecedenceRule as ExpressionGroupOperatorPrecedenceParserRule
        val boundedExpressionReference = opRule.createObject(context, openOp.openReference, expr, closeOp.reference)
        context.source.moveTo(boundedExpressionReference)

        parts.add(boundedRange.first, Expression.ElementFromRule(boundedExpressionReference, opRule))
        return ParseResult.ElementMatch
    }

    private fun validateAssociativity(parts: Iterable<Expression.Content>): Boolean {
        var previousOperatorIsNonAssociative = false

        for (part in parts) {
            if (part !is Expression.Operator) {
                continue
            }

            val currentOperatorIsNonAssociative = ruleAssociativityLookup[part.operablePrecedenceRule] == OperatorAssociativity.NONE
            if (currentOperatorIsNonAssociative && previousOperatorIsNonAssociative) {
                return false
            }
            previousOperatorIsNonAssociative = currentOperatorIsNonAssociative
        }

        return true
    }

    private fun extractParts(parts: MutableList<Expression.Content>, range: IntRange): Array<Expression.Content>? {
        if (parts.count() == 0 || range.first < 0 || range.last > parts.count() - 1) {
            return null
        }

        val extraction = Array(range.count()) { parts[it + range.first] }
        if (range.count() == parts.count()) {
            parts.clear()
        } else {
            // This is an interesting approach to get around the fact that removeRange is protected
            parts.subList(range.first, range.last + 1).clear()
        }

        return extraction
    }

    private sealed class Expression {

        object None : Expression()

        interface Content {
            val reference: TokenReference
            fun rollback(context: ParserContext)
        }

        interface Rulable {
            val operablePrecedenceRule: OperableExpressionPrecedenceParserRule
        }

        interface ContentRulable : Content, Rulable

        open class Element(override val reference: TokenReference)
            : Expression(),
                Content {

            override fun rollback(context: ParserContext) {
                context.source.moveTo(
                        context.elementRepo.removeElements(
                                context.elementRepo.getElementStart(reference),
                                context.elementRepo.getElementEnd(reference)))
            }
        }

        class ElementFromRule(
                reference: TokenReference,
                override val operablePrecedenceRule: OperableExpressionPrecedenceParserRule
        ) : Element(reference),
                ContentRulable

        abstract class Operator(
                private val rollbackReference: TokenReference,
                val openReference: TokenReference,
                override val reference: TokenReference,
                override val operablePrecedenceRule: OperableExpressionPrecedenceParserRule
        ) : Expression(),
                Content,
                Rulable {

            override fun rollback(context: ParserContext) {
                context.source.moveTo(context.elementRepo.removeElements(rollbackReference, reference))
            }
        }

        class OpenOperator(
                rollbackReference: TokenReference,
                openReference: TokenReference,
                closeReference: TokenReference,
                operablePrecedenceRule: OperableExpressionPrecedenceParserRule
        ) : Operator(rollbackReference, openReference, closeReference, operablePrecedenceRule)

        class CloseOperator(
                rollbackReference: TokenReference,
                openReference: TokenReference,
                closeReference: TokenReference,
                operablePrecedenceRule: OperableExpressionPrecedenceParserRule
        ) : Operator(rollbackReference, openReference, closeReference, operablePrecedenceRule)
    }

    private companion object {
        const val BINARY_EXPRESSION_LHS = 0
        const val BINARY_EXPRESSION_RHS = 2
        const val EXPRESSION_GROUP_OPEN = 0
        const val EXPRESSION_GROUP_EXPRESSION = 1
        const val EXPRESSION_GROUP_CLOSE = 2
        const val BINARY_EXPRESSION_GROUP_LHS = 0
        const val BINARY_EXPRESSION_GROUP_OPEN = 1
        const val BINARY_EXPRESSION_GROUP_RHS = 2
        const val BINARY_EXPRESSION_GROUP_CLOSE = 3
        const val TERNARY_EXPRESSION_FIRST = 0
        const val TERNARY_EXPRESSION_SECOND = 2
        const val TERNARY_EXPRESSION_THIRD = 4
    }
}


package modool.parser.rule

class ElementChoiceParserRule(
        rules: List<ElementProviderParserRule>
) : PickOneParserRule<ElementProviderParserRule>(rules),
        ElementProviderParserRule {

    constructor(vararg rules: ElementProviderParserRule) : this(rules.toList())
}
rootProject.name = "modool"
rootProject.buildFileName = "build.gradle.kts"

File("./").walkTopDown().forEach {
    if (it.isFile && it.name.endsWith(".gradle.kts") && it.parent != ".") {
        val projectName = it.parentFile.name

        var currentDirectory = it.parentFile
        var gradleProjectPath = ":"

        while (currentDirectory.name != ".") {
            gradleProjectPath = ":" + currentDirectory.name + gradleProjectPath
            currentDirectory = currentDirectory.parentFile
        }

        if (gradleProjectPath.endsWith(":")) {
            gradleProjectPath = gradleProjectPath.substring(0, gradleProjectPath.length - 1)
        }

        println("INFO: discovered '$projectName' at '$currentDirectory'.")
        include(gradleProjectPath)

        if (!setBuildFileForProject(rootProject.children, gradleProjectPath, it.name, projectName)) {
            throw Exception("Unable to set build file for project: '$gradleProjectPath'")
        }
    }
}

fun setBuildFileForProject(children: Collection<ProjectDescriptor>, path: String, fileName: String, projectName: String): Boolean {
    for (child in children) {
        if (child.path == path) {
            child.buildFileName = fileName
            child.name = projectName
            return true
        } else if (setBuildFileForProject(child.children, path, fileName, projectName)) {
            return true
        }
    }

    return false
}

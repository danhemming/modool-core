package modool.specification.validation

import modool.core.message.CategorisedMessage
import modool.core.message.MessageLevel
import org.dh.specification.Specifiable

class SpecificationValidationMessage(
        val specification: Specifiable,
        text: String,
        level: MessageLevel
) : CategorisedMessage(text, level)
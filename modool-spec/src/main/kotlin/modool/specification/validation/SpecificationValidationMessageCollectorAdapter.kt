package modool.specification.validation

import modool.core.message.MessageLevel
import modool.core.message.collector.MessageCollector
import org.dh.specification.Specifiable
import org.dh.specification.validation.SpecificationValidationMessageCollector

class SpecificationValidationMessageCollectorAdapter(
    private val collector: MessageCollector<SpecificationValidationMessage>
) : SpecificationValidationMessageCollector {

    override fun error(source: Specifiable, message: String) {
        collector.report(SpecificationValidationMessage(source, message, MessageLevel.ERROR))
    }

    override fun warning(source: Specifiable, message: String) {
        collector.report(SpecificationValidationMessage(source, message, MessageLevel.WARNING))
    }
}
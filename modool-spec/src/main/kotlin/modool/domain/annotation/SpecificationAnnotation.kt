package modool.domain.annotation

import org.dh.domain.graph.annotation.DomainGraphAnnotation
import org.dh.specification.Specifiable

class SpecificationAnnotation(specifiable: Specifiable): DomainGraphAnnotation
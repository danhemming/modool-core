package modool.test.dom

import modool.dom.node.DomElementBacked

fun DomElementBacked<*>.assertRendersTo(expectedText: String) {
    val actualText = backing.toFormattedString()
    if (actualText != expectedText) {
        throw AssertionError("Expecting ->$expectedText<- but got ->$actualText<-")
    }
}
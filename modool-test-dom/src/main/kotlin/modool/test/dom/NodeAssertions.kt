package modool.test.dom

import modool.dom.node.DomNode
import modool.dom.token.DomToken


fun DomNode.assertJoinToStart(value: String, sep: String = ",") {
    val result = StringBuilder()
    result.append(this.toFormattedString())
    var current = this as? DomToken

    while (current?.prior != null) {
        if (current.prior is DomNode) {
            val node = current.prior as DomNode
            result.append(sep)
            result.append(node.toFormattedString())
        }
        current = current.prior as DomToken
    }

    if (result.toString() != value) {
        throw AssertionError("Values are not equal expected: <$value> actual: <$result>")
    }
}

fun DomNode.assertJoinToEnd(value: String, sep: String = ",") {
    val result = StringBuilder()
    result.append(this.toFormattedString())
    var current = this as? DomToken

    while (current?.next != null) {
        if (current.next is DomNode) {
            val node = current.next as DomNode
            result.append(sep)
            result.append(node.toFormattedString())
        }
        current = current.next
    }

    if (result.toString() != value) {
        throw AssertionError("Values are not equal expected: <$value> actual: <$result>")
    }
}
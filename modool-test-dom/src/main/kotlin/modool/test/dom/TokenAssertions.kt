package modool.test.dom

import modool.core.content.token.TokenReader
import modool.core.content.signifier.tag.Tag
import modool.dom.node.text.DomTerminalNode
import modool.dom.token.DomToken

fun TokenReader.assertStartOfContent(): TokenReader {
    if (!isStartOfContent()) {
        throw AssertionError("Expected start of content but not found")
    }
    return this
}

fun TokenReader.assertTerminal(value: String): TokenReader {
    if (!isTerminalEqual(value)) {
        throw AssertionError("Expected terminal with value $value but not found")
    }
    return this
}

fun TokenReader.assertTerminal(value: String, tag: Tag): TokenReader {
    if (!isTerminalTagged(value, tag)) {
        when {
            isTerminalEqual(value) -> {
                throw AssertionError("Expected $value found but tag ${tag::class.java.simpleName} NOT found")
            }
            isTerminalTagged(tag) -> {
                throw AssertionError("Expected $value NOT found but ${tag::class.java.simpleName} tag found")
            }
            else -> {
                throw AssertionError("Expected $value NOT found and tag ${tag::class.java.simpleName} NOT found")
            }
        }
    }
    return this
}

fun TokenReader.assertMarker(tag: Tag): TokenReader {
    if (!isMarkerTagged(tag)) {
        throw AssertionError("Expected marker with tag ${tag::class.java.simpleName} NOT found")
    }
    return this
}

fun TokenReader.assertUnknown(): TokenReader {
    if (!isUnknown()) {
        throw AssertionError("Expected unknown terminal but not found")
    }
    return this
}

fun TokenReader.assertUnknown(value: String): TokenReader {
    if (!isUnknown(value)) {
        throw AssertionError("Expected unknown terminal with value $value but not found")
    }
    return this
}

fun TokenReader.assertWhitespace(): TokenReader {
    if (!isWhiteSpace()) {
        throw AssertionError("Expected whitespace but not found")
    }
    return this
}

fun TokenReader.assertSpace(count: Int): TokenReader {
    if (!isSpace(count)) {
        throw AssertionError("Expected $count spaces but not found")
    }
    return this
}

fun TokenReader.assertNewLine(count: Int): TokenReader {
    if (!isNewLine(count)) {
        throw AssertionError("Expected $count new lines but not found")
    }
    return this
}

fun TokenReader.assertNext(): TokenReader {
    if (!moveNext()) {
        throw AssertionError("Expected a token but non found")
    }
    return this
}

fun TokenReader.assertNextContent(): TokenReader {
    if (!moveNextContent()) {
        throw AssertionError("Expected a token but non found")
    }
    return this
}

fun TokenReader.assertStartOfComment(): TokenReader {
    if (!isStartOfComment()) {
        throw AssertionError("Expected start of comment but not found")
    }
    return this
}

fun TokenReader.assertEndOfComment(): TokenReader {
    if (!isEndOfComment()) {
        throw AssertionError("Expected end of comment but not found")
    }
    return this
}


fun TokenReader.assertEndOfContent(): TokenReader {
    if (!isEndOfContent()) {
        throw AssertionError("Expected end of content but not found")
    }
    return this
}

fun Iterable<DomToken>.assertTokenTextContent(vararg contents: String): Iterable<DomToken> {
    if (contents.isEmpty()) {
        return this
    }

    var matches = 0

    asSequence().mapNotNull { if (it is DomTerminalNode) it else null }
            .zip(contents.asSequence())
            .forEach {
                val text = (it.first).get()
                if (text != it.second) {
                    throw AssertionError("Expected ${it.second} but got $text")
                }
                matches++
            }

    if (matches != contents.size) {
        throw AssertionError("Not all contents were matched")
    }

    return this
}
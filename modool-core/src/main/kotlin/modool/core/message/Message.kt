package modool.core.message

/**
 * With conformed message formats we can have somewhat reusable reporting mechanisms
 */

abstract class Message(val text: String) {

    override fun toString(): String {
        return text
    }
}
package modool.core.message

abstract class CategorisedMessage(
        text: String,
        val level: MessageLevel
) : Message(text)
package modool.core.message

enum class MessageLevel {
    INFO,
    WARNING,
    ERROR,
    EXCEPTION
}
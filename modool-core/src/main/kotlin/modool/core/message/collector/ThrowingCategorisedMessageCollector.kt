package modool.core.message.collector

import modool.core.message.CategorisedMessage
import modool.core.message.MessageLevel
import java.lang.RuntimeException

class ThrowingCategorisedMessageCollector : CategorisedMessageCollector {
    private var isErrorEncountered = false
    private var isWarningEncountered = false

    override fun clear() {
        isErrorEncountered = false
        isWarningEncountered = false
    }

    override fun hasErrors(): Boolean {
        return isErrorEncountered
    }

    override fun hasWarnings(): Boolean {
        return isWarningEncountered
    }

    override fun report(message: CategorisedMessage) {
        if (message.level == MessageLevel.ERROR) {
            isErrorEncountered = true
        }

        if (message.level == MessageLevel.ERROR || message.level == MessageLevel.EXCEPTION) {
            throw RuntimeException(message.toString())
        }

        if (message.level == MessageLevel.WARNING) {
            isWarningEncountered = true
        }
    }
}
package modool.core.message.collector

import modool.core.message.CategorisedMessage

interface CategorisedMessageCollector : MessageCollector<CategorisedMessage> {
    fun hasErrors(): Boolean
    fun hasWarnings(): Boolean
}
package modool.core.message.collector

import modool.core.message.CategorisedMessage
import modool.core.message.MessageLevel
import java.lang.RuntimeException

class CapturingCategorisedMessageCollector : CategorisedMessageCollector {
    private val messages = mutableListOf<CategorisedMessage>()
    private var isErrorEncountered = false
    private var isWarningEncountered = false

    override fun report(message: CategorisedMessage) {
        if (message.level == MessageLevel.ERROR) {
            isErrorEncountered = true
        }

        if (message.level == MessageLevel.WARNING) {
            isWarningEncountered = true
        }

        messages.add(message)
    }

    override fun hasErrors(): Boolean {
        return isErrorEncountered
    }

    override fun hasWarnings(): Boolean {
        return isWarningEncountered
    }

    fun toException(): Exception {
        val content = StringBuilder()
        messages.mapNotNull {
            when (it.level) {
                MessageLevel.ERROR -> content.appendln("ERROR: $it")
                MessageLevel.WARNING -> content.appendln("WARNING: $it")
                else -> null
            }
        }
        return RuntimeException(content.toString())
    }

    override fun clear() {
        messages.clear()
        isErrorEncountered = false
        isWarningEncountered = false
    }
}
package modool.core.message.collector

import modool.core.message.Message

interface MessageCollector<in T : Message> {
    fun report(message: T)
    fun clear()
}
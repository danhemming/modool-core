package modool.core.message

interface HierarchicalMessage<T : Message> {
    val innerMessage: T?
}
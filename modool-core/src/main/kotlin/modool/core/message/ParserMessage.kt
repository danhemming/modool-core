package modool.core.message

import modool.core.content.token.TokenReference

class ParserMessage(
        text: String,
        level: MessageLevel,
        val terminalReference: TokenReference
) : CategorisedMessage(text, level)
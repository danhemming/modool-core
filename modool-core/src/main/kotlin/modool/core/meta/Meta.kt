package modool.core.meta

interface Meta {
    val name: String
    val description: String
}
package modool.core.meta

interface Describeable<T>
        where T : Meta {
    val meta: T
}
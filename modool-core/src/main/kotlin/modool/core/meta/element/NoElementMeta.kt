package modool.core.meta.element

object NoElementMeta : ElementMeta {
    override val name = "[Not provided]"
    override val description = "[Not provided]"
}
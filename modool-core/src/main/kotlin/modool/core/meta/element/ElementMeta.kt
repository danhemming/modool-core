package modool.core.meta.element

import modool.core.meta.Meta

// Should include things like ...
// language
// introduced in version

interface ElementMeta : Meta {
    val pluralName: String  get() = name + "s"

    fun isType(other: ElementMeta): Boolean {
        return other == this
    }
}
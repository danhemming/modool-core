package modool.core.meta.property

object NoPropertyMeta : PropertyMeta {
    override val name = "[Not provided]"
    override val description = "[Not provided]"
}
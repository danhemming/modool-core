package modool.core.name

enum class NameConvention {
    UPPER_CASE {
        override fun apply(builder: NameBuilder): String = builder.toUpperCase()
    },
    LOWER_CASE {
        override fun apply(builder: NameBuilder): String = builder.toLowerCase()
    },
    PASCAL_CASE {
        override fun apply(builder: NameBuilder): String = builder.toPascalCase()
    },
    LOWER_CAMEL_CASE {
        override fun apply(builder: NameBuilder): String = builder.toLowerCamelCase()
    },
    UPPER_CAMEL_CASE {
        override fun apply(builder: NameBuilder): String = builder.toUpperCamelCase()
    },
    SNAKE_CASE {
        override fun apply(builder: NameBuilder): String = builder.toSnakeCase()
    },
    SCREAMING_SNAKE_CASE {
        override fun apply(builder: NameBuilder): String = builder.toScreamingSnakeCase()
    },
    KEBAB_CASE {
        override fun apply(builder: NameBuilder): String = builder.toKebabCase()
    };

    fun apply(name: String): String {
        return apply(NameBuilder.fromEncodedWithSeparators(name))
    }

    fun apply(vararg names: String): String {
        return apply(NameBuilder.fromEncodedWithSeparators(*names))
    }

    protected abstract fun apply(builder: NameBuilder): String
}
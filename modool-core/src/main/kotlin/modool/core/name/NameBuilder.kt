package modool.core.name

class NameBuilder private constructor() {

    private constructor(nameBuilder: NameBuilder) : this() {
        parts.addAll(nameBuilder.parts)
    }

    private val parts = mutableListOf<String>()

    fun add(part: String, splitOnCapitals: Boolean = true, splitOnSeparators: Boolean = true): NameBuilder {
        parts.addAll(
                split(
                        value = replaceInvalidJvmNameCharactersWithDescription(part, ignoreSeparators = splitOnSeparators),
                        onCapitals = splitOnCapitals,
                        onSeparators = splitOnSeparators))
        return this
    }

    fun countOfParts() = parts.count()

    fun removeLastPart(): String {
        return removePart(countOfParts() - 1)
    }

    fun removePart(index: Int): String {
        return parts.removeAt(index)
    }

    fun toPascalCase(): String {
        return toUpperCamelCase()
    }

    fun toLowerCase(): String {
        return parts.joinToString(separator = "") { it.toLowerCase() }
    }

    fun toUpperCase(): String {
        return parts.joinToString(separator = "") { it.toUpperCase() }
    }

    fun toLowerCamelCase(): String {
        return when (parts.count()) {
            0 -> ""
            1 -> parts[0].toLowerCase()
            else -> {
                val lowerCaseRequired = isScreaming()
                parts[0].toLowerCase() + parts.drop(1).joinToString(separator = "") {
                    if (lowerCaseRequired) it.toLowerCase().capitalize()
                    else it.capitalize()
                }
            }
        }
    }

    fun toUpperCamelCase(): String {
        return when (parts.count()) {
            0 -> ""
            else -> {
                val lowerCaseRequired = isScreaming()
                parts.joinToString(separator = "") {
                    if (lowerCaseRequired) it.toLowerCase().capitalize()
                    else it.capitalize()
                }
            }
        }
    }

    private fun isScreaming(): Boolean {
        if (parts.count() <= 1) return false

        parts.forEach { part ->
            part.forEach { char ->
                if (char.isLowerCase()) return false
            }
        }

        return true
    }

    fun toScreamingSnakeCase(): String {
        return when (parts.count()) {
            0 -> ""
            1 -> parts[0].toUpperCase()
            else -> parts.joinToString(separator = "_") { it.toUpperCase() }
        }
    }

    fun toSnakeCase(): String {
        return when (parts.count()) {
            0 -> ""
            1 -> parts[0].toLowerCase()
            else -> parts.joinToString(separator = "_") { it.toLowerCase() }
        }
    }

    fun toKebabCase(): String {
        return when (parts.count()) {
            0 -> ""
            1 -> parts[0].toLowerCase()
            else -> parts.joinToString(separator = "-") { it.toLowerCase() }
        }
    }

    fun toDotSeparated(): String {
        return when (parts.count()) {
            0 -> ""
            1 -> parts[0]
            else -> parts.joinToString(separator = ".")
        }
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is NameBuilder) return false

        if (countOfParts() != other.countOfParts()) {
            return false
        }
        parts.zip(other.parts).forEach { if (it.first != it.second) return false }
        return true
    }

    override fun hashCode(): Int {
        return parts.sumBy { it.hashCode() }
    }

    companion object {

        fun create() = NameBuilder()

        fun from(nameBuilder: NameBuilder) = NameBuilder(nameBuilder)

        fun from(value: String) = NameBuilder().apply {
            if (!value.isBlank()) add(value, splitOnCapitals = true, splitOnSeparators = false)
        }

        fun from(vararg values: String?) = NameBuilder().apply {
            values.forEach { if (!it.isNullOrBlank()) add(it, splitOnCapitals = true, splitOnSeparators = false) }
        }

        fun fromEncodedWithSeparators(value: String) = NameBuilder().apply {
            if (!value.isBlank()) add(value, splitOnCapitals = true, splitOnSeparators = true)
        }

        fun fromEncodedWithSeparators(vararg values: String?) = NameBuilder().apply {
            values.forEach { if (!it.isNullOrBlank()) add(it, splitOnCapitals = true, splitOnSeparators = true) }
        }

        /**
         * Replace invalid name symbols with a descriptive word.
         */
        fun replaceInvalidJvmNameCharactersWithDescription(
                value: String,
                ignoreSeparators: Boolean = true): String {

            val safeName = StringBuilder(value.length)
            value.forEach {
                if (!ignoreSeparators) {
                    when (it) {
                        '\\' -> safeName.append("BackSlash")
                        '.' -> safeName.append("Dot")
                        ':' -> safeName.append("Colon")
                        '-' -> safeName.append("Minus")
                        ' ' -> return@forEach
                    }
                }

                when (it) {
                    '*' -> safeName.append("Star")
                    '+' -> safeName.append("Plus")
                    '/' -> safeName.append("Slash")
                    '@' -> safeName.append("At")
                    '(' -> safeName.append("OpenParen")
                    ')' -> safeName.append("CloseParen")
                    '[' -> safeName.append("OpenSquareBracket")
                    ']' -> safeName.append("CloseSquareBracket")
                    '{' -> safeName.append("OpenBrace")
                    '}' -> safeName.append("CloseBrace")
                    '<' -> safeName.append("OpenAngleBracket")
                    '>' -> safeName.append("CloseAngleBracket")
                    ',' -> safeName.append("Comma")
                    '#' -> safeName.append("Hash")
                    '&' -> safeName.append("Ampersand")
                    '$' -> safeName.append("Dollar")
                    '%' -> safeName.append("Percent")
                    '=' -> safeName.append("Equal")
                    ';' -> safeName.append("SemiColon")
                    else -> safeName.append(it)
                }
            }
            return safeName.toString()
        }

        fun splitOnCapitalsAndSeparators(value: String): List<String> {
            return split(value, onCapitals = true, onSeparators = true)
        }

        fun split(value: String, onCapitals: Boolean, onSeparators: Boolean): List<String> {

            if (!onCapitals && !onSeparators) {
                return listOf(value)
            }

            if (!onCapitals && onSeparators) {
                return splitOnSeparators(value)
            }

            val results = mutableListOf<String>()
            val current = StringBuilder(15)
            var currentIsUpperCase = false
            var previousIsUpperCase = true

            for (char in value) {
                currentIsUpperCase = char.isUpperCase()
                // Guard against acronyms e.g. TLA as we want to preserve them
                if (currentIsUpperCase && !previousIsUpperCase) {
                    if (onSeparators) {
                        results.addAll(splitOnSeparators(current.toString()))
                    } else {
                        results.add(current.toString())
                    }
                    current.clear()
                }

                current.append(char)
                previousIsUpperCase = currentIsUpperCase
            }

            if (current.count() > 0) {
                if (onSeparators) {
                    results.addAll(splitOnSeparators(current.toString()))
                } else {
                    results.add(current.toString())
                }
            }

            return results
        }

        fun splitOnSeparators(value: String): List<String> {
            return value.split(' ', '.', '\\', ':', '_', '-').filter { it.isNotEmpty() }
        }
    }


}
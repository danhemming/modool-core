package modool.core.config

sealed class SemanticIndentationStrategy(val size: Int, val tabCharacterCount: Int) {

    fun calculateMarginSize(
            numberOfSpacesOnCurrentLine: Int,
            numberOfTabsOnCurrentLine: Int): Int {
        return numberOfSpacesOnCurrentLine + (tabCharacterCount * numberOfTabsOnCurrentLine)
    }

    abstract fun calculateStartingIndentationSize(
            previousIndentationSize: Int,
            currentIndentationLevel: Int,
            numberOfSpacesOnCurrentLine: Int,
            numberOfTabsOnCurrentLine: Int): Int

    class AtLeast(size: Int, tabCharacterCount: Int) : SemanticIndentationStrategy(size, tabCharacterCount) {

        override fun calculateStartingIndentationSize(
                previousIndentationSize: Int,
                currentIndentationLevel: Int,
                numberOfSpacesOnCurrentLine: Int,
                numberOfTabsOnCurrentLine: Int): Int {

            val calculatedSize = calculateMarginSize(numberOfSpacesOnCurrentLine, numberOfTabsOnCurrentLine)
            return if (calculatedSize >= previousIndentationSize + size) calculatedSize else -1
        }
    }

    class Exactly(size: Int, tabCharacterCount: Int) : SemanticIndentationStrategy(size, tabCharacterCount) {
        override fun calculateStartingIndentationSize(
                previousIndentationSize: Int,
                currentIndentationLevel: Int,
                numberOfSpacesOnCurrentLine: Int,
                numberOfTabsOnCurrentLine: Int): Int {

            val calculatedSize = calculateMarginSize(numberOfSpacesOnCurrentLine, numberOfTabsOnCurrentLine)
            return if (calculatedSize == currentIndentationLevel * size) calculatedSize
            else -1
        }
    }
}
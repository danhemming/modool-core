package modool.core.content.repository

import modool.core.content.ContentOrigin
import modool.core.content.element.ElementPropertySetter
import modool.core.meta.element.*
import modool.core.meta.property.PropertyMeta
import modool.core.content.token.TokenRangeOption
import modool.core.content.signifier.tag.Tag
import modool.core.content.token.ConstantTerminalDescriptor
import modool.core.content.token.TokenReference
import modool.core.content.token.TokenReferenceRange

interface ElementRepository {

    fun isElementAt(reference: TokenReference, element: ElementMeta): Boolean
    fun isPropertyAt(reference: TokenReference): Boolean
    fun getElementStart(reference: TokenReference): TokenReference
    fun getElementEnd(reference: TokenReference): TokenReference
    fun <T : ElementRepositoryProviderView> createProviderView(viewType: Class<T>): T

    fun tryChangeTerminalType(reference: TokenReference, descriptor: ConstantTerminalDescriptor): Boolean
    fun tryChangeTerminalTag(reference: TokenReference, tag: Tag): Boolean

    /**
     * Returns the token representing the end of the list (may be different to the one passed)
     */
    fun createList(
            from: TokenReference,
            to: TokenReference,
            tokenRangeOption: TokenRangeOption,
            contentOrigin: ContentOrigin): TokenReference

    /**
     * Returns a token representing the view.
     * Given the distributed nature of the content the returned token may be some arbitrary token within the content.
     * The repository implementation must therefore maintain an internal mapping to facilitate the setting of the
     * property.
     */
    fun createView(
            members: List<TokenReferenceRange>,
            tokenRangeOption: TokenRangeOption,
            contentOrigin: ContentOrigin): TokenReference

    /**
     * Returns the token representing the end of the property group (may be different to the one passed)
     */
    fun createPropertyGroup(
            from: TokenReference,
            to: TokenReference,
            tokenRangeOption: TokenRangeOption,
            contentOrigin: ContentOrigin): TokenReference

    /**
     * Returns the token representing the end of the element (may be different to the one passed)
     */
    fun createElement(
            from: TokenReference,
            to: TokenReference,
            tokenRangeOption: TokenRangeOption,
            contentOrigin: ContentOrigin,
            element: ElementMeta,
            propertySetters: List<ElementPropertySetter>): TokenReference

    fun expandElement(reference: TokenReference, to: TokenReference)

    fun contractElement(referenceTo: TokenReference)

    fun removeElements(from: TokenReference, to: TokenReference? = null): TokenReference

    fun setTerminalValue(
            parentReference: TokenReference,
            valueReference: TokenReference,
            contentOrigin: ContentOrigin,
            valueProperty: PropertyMeta)

    fun setElementValue(
            parentReference: TokenReference,
            elementValueReference: TokenReference,
            contentOrigin: ContentOrigin,
            elementValueProperty: PropertyMeta)

    fun setList(
            parentReference: TokenReference,
            listReference: TokenReference,
            contentOrigin: ContentOrigin,
            listProperty: PropertyMeta)

    fun setView(
            parentReference: TokenReference,
            viewReference: TokenReference,
            contentOrigin: ContentOrigin,
            viewProperty: PropertyMeta)

    fun setPropertyGroup(
            parentReference: TokenReference,
            propertyGroupReference: TokenReference,
            contentOrigin: ContentOrigin,
            propertyGroupProperty: PropertyMeta)

    fun setPropertyGroupTerminalValue(
            propertyGroupReference: TokenReference,
            valueReference: TokenReference,
            contentOrigin: ContentOrigin,
            groupProperty: PropertyMeta,
            valueProperty: PropertyMeta)

    fun setPropertyGroupElementValue(
            propertyGroupReference: TokenReference,
            elementValueReference: TokenReference,
            contentOrigin: ContentOrigin,
            groupProperty: PropertyMeta,
            elementValueProperty: PropertyMeta)

    fun setPropertyGroupView(
            propertyGroupReference: TokenReference,
            viewReference: TokenReference,
            contentOrigin: ContentOrigin,
            groupProperty: PropertyMeta,
            viewProperty: PropertyMeta)

    fun createSnapshot(from: TokenReference, to: TokenReference? = null): ElementRepositorySnapshot

    fun restoreSnapshot(snapshot: ElementRepositorySnapshot)

    /**
     * Called once for each root element that has successfully completed.
     * Notes:
     * - Represents the completion of any work in progress so clear down any internal state
     */
    fun commitRootElement(reference: TokenReference)

    /**
     * Remove any internal state and prepare for reuse
     */
    fun clear()
}


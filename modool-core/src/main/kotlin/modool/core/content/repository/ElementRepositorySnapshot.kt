package modool.core.content.repository

interface ElementRepositorySnapshot {
   object None : ElementRepositorySnapshot
}
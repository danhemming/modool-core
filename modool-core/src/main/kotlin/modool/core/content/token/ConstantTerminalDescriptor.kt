package modool.core.content.token

import modool.core.content.ReadableValue
import modool.core.content.signifier.tag.Tag
import modool.core.content.signifier.tag.Tag.ConstantTerminalType

/**
 * Note: taggedX properties are named this way to avoid any naming conflicts with generated overloads
 */
open class ConstantTerminalDescriptor(
    protected val value: String,
    protected val taggedType: ConstantTerminalType,
    val isCaseSensitive: Boolean,
    protected val taggedCategories: List<Tag.Categorical> = emptyList()
) : ReadableValue<String> {

    override fun get() = value

    fun isValueEqual(value: String): Boolean {
        return this.value.equals(value, ignoreCase = !isCaseSensitive)
    }

    fun isValueEqualAt(index: Int, value: Char): Boolean {
        return index < this.value.length
                && this.value[index].equals(value, ignoreCase = !isCaseSensitive)
    }

    fun isTaggedType(tag: Tag): Boolean {
        return taggedType.isA(tag)
    }
}
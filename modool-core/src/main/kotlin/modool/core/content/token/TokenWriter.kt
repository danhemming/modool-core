package modool.core.content.token

import modool.core.content.signifier.tag.Tag

interface TokenWriter {
    val isAccepting: Boolean
    fun writeStart()
    fun isAcceptable(descriptor: ConstantTerminalDescriptor): Boolean
    fun isEmpty(): Boolean
    fun createRestorePoint(): TokenWriterRestorePoint
    fun updateRestorePoint(restorePoint: TokenWriterRestorePoint)
    fun restore(restorePoint: TokenWriterRestorePoint)
    /**
     * Factory provides us additional tags
     */
    fun writeFactoryTerminal(index: Int, count: Int, descriptor: ConstantTerminalDescriptor, tags: List<Tag>)
    fun writeUnknownTerminal(index: Int, count: Int)
    fun writeTerminal(index: Int, count: Int, tags: List<Tag>)
    fun writeComment(body: () -> Unit)
    fun writeMarker(tags: List<Tag>, isDeferred: Boolean)
    fun overwriteWhitespace(whitespace: WhitespaceContent, tags: List<Tag>)
    fun copyFrom(writer: TokenWriter)
    fun writeEnd()
    fun clear()
    fun createWhitespaceBuilder(): WhitespaceContent
    fun createComponentWriter(): TokenWriter

    /**
     * Creates a component writer that deep copies tokens passed from other TokenWriters i.e. copy value, not reference.
     * This is only of importance where the token structure of the underlying implementation is part of a data structure
     * that is not immutable e.g. a linked list.
     */
    fun createValueComponentWriter(): TokenWriter
}
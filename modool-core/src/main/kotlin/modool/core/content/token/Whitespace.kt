package modool.core.content.token

enum class Whitespace(
        val encoding: Byte,
        val outputRepresentation: Char,
        val description: String,
        val isLineBreak: Boolean,
        val isComplete: Boolean) {

    NON_WHITE_SPACE(0, '?', "[UNKNOWN]", isLineBreak = false, isComplete = true),
    SPACE(1, WhitespaceCharacter.SPACE, "[SPACE]", isLineBreak = false, isComplete = true),
    TAB(2, WhitespaceCharacter.TAB, "[TAB]", isLineBreak = false, isComplete = true),
    CARRIAGE_RETURN(3, WhitespaceCharacter.CARRIAGE_RETURN, "[CR]", isLineBreak = false, isComplete = false),
    CARRIAGE_RETURN_STAND_ALONE(4, WhitespaceCharacter.CARRIAGE_RETURN, "[CR]", isLineBreak = true, isComplete = true),
    LINE_FEED(5, WhitespaceCharacter.LINE_FEED, "[LF]", isLineBreak = true, isComplete = true),
    LINE_SEPARATOR(6, WhitespaceCharacter.LINE_SEPARATOR, "[LINE SEP]", isLineBreak = true, isComplete = true),
    PARAGRAPH_SEPARATOR(7, WhitespaceCharacter.PARAGRAPH_SEPARATOR, "[PARA SEP]", isLineBreak = true, isComplete = true);

    companion object {
        private val lookup = arrayOf(
                NON_WHITE_SPACE,
                SPACE,
                TAB,
                CARRIAGE_RETURN,
                CARRIAGE_RETURN_STAND_ALONE,
                LINE_FEED,
                LINE_SEPARATOR,
                PARAGRAPH_SEPARATOR)

        fun from(encoding: Int): Whitespace {
            return lookup[encoding]
        }

        fun from(value: Char, nextValue: Char?): Whitespace {
            return when (value) {
                WhitespaceCharacter.SPACE -> SPACE
                WhitespaceCharacter.LINE_FEED -> LINE_FEED
                WhitespaceCharacter.TAB -> TAB
                WhitespaceCharacter.CARRIAGE_RETURN ->
                    if (nextValue == WhitespaceCharacter.LINE_FEED) CARRIAGE_RETURN else CARRIAGE_RETURN_STAND_ALONE
                WhitespaceCharacter.LINE_SEPARATOR -> LINE_SEPARATOR
                WhitespaceCharacter.PARAGRAPH_SEPARATOR -> PARAGRAPH_SEPARATOR
                else -> NON_WHITE_SPACE
            }
        }

        fun isUnicodeWhitespace(value: Char): Boolean {
            return when (value) {
                WhitespaceCharacter.SPACE,
                WhitespaceCharacter.LINE_FEED,
                WhitespaceCharacter.TAB,
                WhitespaceCharacter.CARRIAGE_RETURN,
                WhitespaceCharacter.LINE_SEPARATOR,
                WhitespaceCharacter.PARAGRAPH_SEPARATOR -> true
                else -> false
            }
        }

        fun isAsciiWhitespace(value: Char): Boolean {
            return when (value) {
                WhitespaceCharacter.SPACE,
                WhitespaceCharacter.LINE_FEED,
                WhitespaceCharacter.TAB,
                WhitespaceCharacter.CARRIAGE_RETURN -> true
                else -> false
            }
        }

        fun isInlineWhitespace(value: Char): Boolean {
            return when (value) {
                WhitespaceCharacter.SPACE,
                WhitespaceCharacter.TAB -> true
                else -> false
            }
        }

        /**
         * Determines if the passed character is a leading indicator of an end of line (so includes CR).
         */
        fun isUnicodeEndOfLineIndicator(value: Char): Boolean {
            return when (value) {
                WhitespaceCharacter.LINE_FEED,
                WhitespaceCharacter.LINE_SEPARATOR,
                WhitespaceCharacter.CARRIAGE_RETURN,
                WhitespaceCharacter.PARAGRAPH_SEPARATOR -> true
                else -> false
            }
        }

        fun isAsciiEndOfLineIndicator(value: Char): Boolean {
            return when (value) {
                WhitespaceCharacter.LINE_FEED,
                WhitespaceCharacter.CARRIAGE_RETURN -> true
                else -> false
            }
        }
    }
}
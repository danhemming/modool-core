package modool.core.content.token

enum class TokenRangeOption {
    START_INCLUSIVE_END_EXCLUSIVE,
    START_EXCLUSIVE_END_INCLUSIVE,
    INCLUSIVE,
    EXCLUSIVE
}
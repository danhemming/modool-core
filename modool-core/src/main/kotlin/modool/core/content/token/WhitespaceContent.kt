package modool.core.content.token

import it.unimi.dsi.fastutil.bytes.ByteArrayList

interface WhitespaceContent {
    val spaceCount: Int
    val lineCount: Int
    val tabCount: Int
    val isLeftMargin get() = lineCount > 0 && (spaceCount > 0 || tabCount > 0)
    val isNewLine get() = lineCount > 0
    val marginX: Int
    val marginY: Int get() = lineCount
    val renderLength: Int

    fun save(): WhitespaceContent
    fun add(encoding: Whitespace)
    fun asByteArrayList(): ByteArrayList

    companion object {
        // TODO: the margin size of a TAB needs to be configurable
        const val TAB_SIZE = 4
    }
}
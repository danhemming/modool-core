package modool.core.content.token

import modool.core.content.signifier.tag.Tag

interface TokenReader {
    fun reset()
    fun moveNext(): Boolean
    fun moveNextContent(): Boolean
    fun moveNextTerminal(): Boolean {
        while (!isEndOfContent() && moveNextContent()) {
            if (isTerminal()) {
                return true
            }
            if (isMarker()) {
                return false
            }
        }
        return false
    }
    fun moveNextMarker(): Boolean {
        while (!isEndOfContent() && moveNextContent()) {
            if (isMarker()) {
                return true
            }
            if (isTerminal()) {
                return false
            }
        }
        return false
    }
    fun moveTo(reference: TokenReference)
    fun moveToEnd(): Boolean {
        while (!isEndOfContent()) { moveNext() }
        return true
    }
    fun isStartOfContent(): Boolean
    fun isEndOfContent(): Boolean
    fun isMoreContentAvailable(): Boolean {
        return peek { moveNextContent() && !isEndOfContent() }
    }
    fun isAnotherTerminalAvailable(): Boolean {
        return peek { moveNextTerminal() }
    }
    fun isTerminal(): Boolean
    fun isTerminalEqual(value: String): Boolean
    fun isTerminalEqual(reference: TokenReference): Boolean
    fun isTerminalTagged(value: String, tag: Tag): Boolean
    fun isTerminalTagged(tag: Tag): Boolean
    fun isTerminalSoftKeyword(): Boolean
    fun isTerminalMatchable(descriptor: ConstantTerminalDescriptor): Boolean
    fun isMostRecentTerminalTagged(tag: Tag): Boolean
    fun isMarker(): Boolean
    fun isMarkerTagged(tag: Tag): Boolean
    fun isUnknown(): Boolean
    fun isUnknown(value: String): Boolean
    fun isWhiteSpace(): Boolean
    fun isWhitespaceTagged(tag: Tag): Boolean
    fun isNewLine(): Boolean
    fun isNewLine(count: Int): Boolean
    fun isSpace(): Boolean
    fun isSpace(count: Int): Boolean
    fun isLeftMargin(): Boolean
    fun isStartOfComment(): Boolean
    fun isEndOfComment(): Boolean
    fun getSpaceCount(): Int
    fun getTabCount(): Int
    fun createReference(): TokenReference
    fun createPriorWhitespaceReference(): TokenReference

    /**
     * Gives us an escape hatch to get at the underlying implementation
     */
    fun <T: TokenReaderProviderView> createProviderView(viewType: Class<T>): T
}

inline fun <T> TokenReader.peek(moveAround: () -> T): T {
    val startReference = createReference()
    return try {
        moveAround()
    } finally {
        moveTo(startReference)
    }
}
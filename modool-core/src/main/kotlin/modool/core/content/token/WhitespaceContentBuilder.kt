package modool.core.content.token

import it.unimi.dsi.fastutil.bytes.ByteArrayList

class WhitespaceContentBuilder(
        private val contents: ByteArrayList,
        private var numberOfSpacesOnCurrentLine: Int,
        private var numberOfTabsOnCurrentLine: Int,
        private var numberOfNewLines: Int
) : WhitespaceContent {

    override val lineCount: Int get() = numberOfNewLines
    override val spaceCount: Int get() = numberOfSpacesOnCurrentLine
    override val tabCount: Int get() = numberOfTabsOnCurrentLine
    override val marginX: Int get() = numberOfSpacesOnCurrentLine + (numberOfTabsOnCurrentLine * WhitespaceContent.TAB_SIZE)
    override val renderLength: Int get() = contents.count()

    constructor() : this(ByteArrayList(3), 0, 0, 0)

    override fun add(encoding: Whitespace) {
        contents.add(encoding.encoding)

        when {
            encoding.isLineBreak -> {
                numberOfSpacesOnCurrentLine = 0
                numberOfTabsOnCurrentLine = 0
                numberOfNewLines++
            }
            encoding == Whitespace.SPACE ->
                numberOfSpacesOnCurrentLine++
            encoding == Whitespace.TAB ->
                numberOfTabsOnCurrentLine++
            else -> return
        }
    }

    override fun save(): WhitespaceContent {
        return WhitespaceContentBuilder(
                contents.clone(),
                numberOfSpacesOnCurrentLine,
                numberOfTabsOnCurrentLine,
                numberOfNewLines)
    }

    override fun asByteArrayList(): ByteArrayList {
        return contents
    }
}
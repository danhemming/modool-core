package modool.core.content.token

interface TokenWriterRestorePoint {
    fun clear()
}
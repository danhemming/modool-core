package modool.core.content.token

interface TokenReaderProvider {
    fun createTokenReader(): TokenReader
}
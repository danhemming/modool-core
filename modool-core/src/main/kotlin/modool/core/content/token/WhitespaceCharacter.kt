package modool.core.content.token

object WhitespaceCharacter {
    const val SPACE = ' '
    const val LINE_FEED = '\n'
    const val TAB = '\t'
    const val CARRIAGE_RETURN = '\r'
    const val LINE_SEPARATOR = '\u2028'
    const val PARAGRAPH_SEPARATOR = '\u2029'
}
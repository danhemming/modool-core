package modool.core.content.token

interface TokenReference {
    object None : TokenReference
}
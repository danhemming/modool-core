package modool.core.content.token

data class TokenReferenceRange(val start: TokenReference, val end: TokenReference)
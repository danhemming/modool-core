package modool.core.content

sealed class ContentOrigin {
    abstract class Source : ContentOrigin()
    object PrimarySource : Source()
    object SecondarySource : Source()
    object API: ContentOrigin()

    fun isFromSource() = this == PrimarySource || this == SecondarySource
    fun isFormattable() = this == API || this == SecondarySource
}
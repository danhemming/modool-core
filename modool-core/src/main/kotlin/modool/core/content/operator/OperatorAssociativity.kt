package modool.core.content.operator

enum class OperatorAssociativity {
    NONE,
    LEFT_TO_RIGHT,
    RIGHT_TO_LEFT
}
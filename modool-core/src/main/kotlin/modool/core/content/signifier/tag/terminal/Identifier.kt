package modool.core.content.signifier.tag.terminal

import modool.core.content.signifier.tag.Tag

object Identifier : Tag.DynamicTerminalType()
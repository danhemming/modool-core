package modool.core.content.signifier.tag

import modool.core.content.Copyable

class TagBag private constructor(private val tags: MutableList<Tag>) : Copyable<TagBag> {

    constructor() : this(tags = mutableListOf())

    fun add(tag: Tag): Boolean {
        if (tag is Tag.Categorical) {
            tags.find { it is Tag.Categorical && it.isInSameCategoryAs(tag) }?.let { remove(it) }
        }
        return tags.add(tag)
    }

    fun clear() {
        tags.clear()
    }

    fun remove(tag: Tag): Boolean {
        return tags.remove(tag)
    }

    fun contains(category: TagCategory): Boolean {
        return tags.find { it is Tag.Categorical && it.isInCategory(category) } != null
    }

    fun contains(tag: Tag): Boolean {
        return tags.contains(tag)
    }

    fun isEmpty(): Boolean {
        return tags.isEmpty()
    }

    override fun copy(): TagBag {
        return TagBag(tags.toMutableList())
    }
}
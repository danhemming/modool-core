package modool.core.content.signifier.trait

import modool.core.content.signifier.Signifier

/**
 * A trait is a deterministic attribute (based on some function of an elements content).
 */
abstract class Trait(
        private val inherited: List<Trait>,
        private val children: List<Trait>
) : Signifier {

    constructor(
            vararg inherited: Trait,
            children: List<Trait> = emptyList()) : this(inherited.toList(), children)

    constructor(inherited: Trait, children: List<Trait> = emptyList()) : this(listOf(inherited), children)

    override fun isA(signifier: Signifier): Boolean {
        return this == signifier || inherited.firstOrNull { it.isA(signifier) } != null
    }
}
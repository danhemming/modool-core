package modool.core.content.signifier

interface Signifiable {
    fun signifies(signifier: Signifier): Boolean = false
}
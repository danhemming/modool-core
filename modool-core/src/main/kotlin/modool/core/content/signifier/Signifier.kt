package modool.core.content.signifier

interface Signifier {
    fun isA(signifier: Signifier): Boolean
}
package modool.core.content.signifier.tag.terminal

import modool.core.content.signifier.tag.TagCategory

object Literal : TagCategory(inherits = Terminal)
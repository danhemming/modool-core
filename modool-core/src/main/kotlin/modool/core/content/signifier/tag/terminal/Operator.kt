package modool.core.content.signifier.tag.terminal

import modool.core.content.signifier.tag.Tag
import modool.core.content.signifier.tag.TagCategory

object Operator : TagCategory(inherits = Terminal) {

    object Binary : Tag.ConstantTerminalType(category = Operator)

    object UnaryPrefix : Tag.ConstantTerminalType(category = Operator)

    object UnaryPostfix : Tag.ConstantTerminalType(category = Operator)

    object Modifier : Tag.Categorical()
}
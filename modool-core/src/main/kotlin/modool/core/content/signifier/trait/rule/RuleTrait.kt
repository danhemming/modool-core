package modool.core.content.signifier.trait.rule

import modool.core.content.signifier.trait.Trait

abstract class RuleTrait(
    val description: String
) : Trait(emptyList(), emptyList())
package modool.core.content.signifier.trait.feature

abstract class VariationFeatureTrait(
    name: String,
    description: String,
    val parent: FeatureTrait
) : FeatureTrait(name, description, parent.group) {

    constructor(name: String, parent: FeatureTrait) : this(name, parent.description, parent)
}
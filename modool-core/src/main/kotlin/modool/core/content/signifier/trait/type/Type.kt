package modool.core.content.signifier.trait.type

import modool.core.content.signifier.trait.Trait
import modool.core.content.signifier.trait.component.Named

object Type : Trait(Named)
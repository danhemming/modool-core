package modool.core.content.signifier.trait.feature

import modool.core.content.signifier.trait.Trait

abstract class FeatureGroupTrait(
        val name: String
) : Trait(emptyList(), emptyList())
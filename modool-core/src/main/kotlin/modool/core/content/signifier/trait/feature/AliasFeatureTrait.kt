package modool.core.content.signifier.trait.feature

abstract class AliasFeatureTrait(
    name: String,
    description: String,
    val source: FeatureTrait
) : FeatureTrait(name, description, source.group) {

    constructor(name: String, source: FeatureTrait) : this(name, source.description, source)
}
package modool.core.content.signifier.tag.terminal

import modool.core.content.signifier.tag.Tag
import modool.core.content.signifier.tag.TagCategory

/**
 * These represent JVM literals i.e. those literals that can be determined through parsing i.e by decoration (suffix etc)
 * TODO we need to include binary, hex literals etc which have a type of Integer
 */
object JvmLiteral : TagCategory(inherits = Literal) {
    object Untyped : Tag.DynamicTerminalType(category = JvmLiteral)
    object Integer : Tag.DynamicTerminalType(category = JvmLiteral)
    object Long : Tag.DynamicTerminalType(category = JvmLiteral)
    object Float : Tag.DynamicTerminalType(category = JvmLiteral)
    object Double : Tag.DynamicTerminalType(category = JvmLiteral)
    object String : Tag.DynamicTerminalType(category = JvmLiteral)
    object Char : Tag.DynamicTerminalType(category = JvmLiteral)
    object Boolean : Tag.DynamicTerminalType(category = JvmLiteral)
}

package modool.core.content.signifier.trait.type

import modool.core.content.signifier.trait.Trait

object PseudoType : Trait(Type) {
    object None : Trait(PseudoType), PseudoTypeTrait
    object Undefined : Trait(PseudoType), PseudoTypeTrait
}
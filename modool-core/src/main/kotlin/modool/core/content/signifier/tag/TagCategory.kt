package modool.core.content.signifier.tag

abstract class TagCategory(private val inherits: TagCategory? = null) {

    fun isA(category: TagCategory): Boolean {
        return category == this || (inherits != null && inherits.isA(category))
    }
}
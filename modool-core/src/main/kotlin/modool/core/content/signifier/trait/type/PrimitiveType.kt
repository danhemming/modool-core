package modool.core.content.signifier.trait.type

import modool.core.content.signifier.trait.Trait

object PrimitiveType : Trait(Type) {
    object Array : Trait(PrimitiveType), PrimitiveTypeTrait
    object Boolean : Trait(PrimitiveType), PrimitiveTypeTrait
    object Bit : Trait(Boolean, Number), PrimitiveTypeTrait
    object Byte : Trait(Number), PrimitiveTypeTrait
    object Char : Trait(PrimitiveType), PrimitiveTypeTrait
    object Float32 : Trait(Number), PrimitiveTypeTrait
    object Float64 : Trait(Number), PrimitiveTypeTrait
    object Int16 : Trait(Number), PrimitiveTypeTrait
    object Int32 : Trait(Number), PrimitiveTypeTrait
    object Int64 : Trait(Number), PrimitiveTypeTrait
    object Number : Trait(PrimitiveType), PrimitiveTypeTrait
    object String : Trait(Type), PrimitiveTypeTrait
    object Symbol : Trait(Type), PrimitiveTypeTrait
}

package modool.core.content.signifier.trait.feature

import modool.core.content.signifier.trait.Trait

abstract class FeatureTrait(
    val name: String,
    val description: String,
    val group: FeatureGroupTrait
) : Trait(group, emptyList()) {

    constructor(name: String, group: FeatureGroupTrait) : this(name, name, group)
}


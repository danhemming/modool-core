package modool.core.content.signifier.tag

import modool.core.content.signifier.Signifier
import modool.core.content.signifier.tag.terminal.Terminal
import modool.core.content.token.ConstantTerminalDescriptor

/**
 * A tag is a non-deterministic signifier.  Some external contextual knowledge is required for a tag to be applied.
 */
sealed class Tag : Signifier {

    override fun isA(signifier: Signifier): Boolean {
        return signifier == this
    }

    /** A System tag is for platform purposes e.g. setting the type of a token */
    abstract class Categorical constructor(private val category: TagCategory? = null) : Tag() {

        fun isInSameCategoryAs(tag: Categorical): Boolean {
            return tag.category?.let { isInCategory(tag.category) } ?: false
        }

        fun isInCategory(queryCategory: TagCategory): Boolean {
            return category != null && category.isA(queryCategory)
        }
    }

    abstract class ConstantTerminalType constructor(
            category: TagCategory = Terminal
    ) : Tag.TerminalType(category) {

        open operator fun invoke(value: String, vararg tags: Categorical): ConstantTerminalDescriptor {
            return ConstantTerminalDescriptor(
                value,
                isCaseSensitive = true,
                taggedType = this,
                taggedCategories = tags.toList())
        }

        open operator fun invoke(
            value: String,
            isCaseSensitive: Boolean,
            vararg tags: Categorical
        ): ConstantTerminalDescriptor {
            return ConstantTerminalDescriptor(
                value,
                isCaseSensitive = isCaseSensitive,
                taggedType = this,
                taggedCategories = tags.toList())
        }
    }

    abstract class TerminalType constructor(category: TagCategory = Terminal) : Categorical(category)

    abstract class DynamicTerminalType constructor(category: TagCategory = Terminal) : TerminalType(category)

    /** A User tag is a straight forward signifier that relies on direct equivalence */
    abstract class User : Tag()
}
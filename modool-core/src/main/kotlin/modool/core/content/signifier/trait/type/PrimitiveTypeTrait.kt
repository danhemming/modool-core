package modool.core.content.signifier.trait.type

/**
 * A marker interface for the convenience of defining specifications
 */
interface PrimitiveTypeTrait
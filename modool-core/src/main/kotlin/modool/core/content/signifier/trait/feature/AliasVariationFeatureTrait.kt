package modool.core.content.signifier.trait.feature

abstract class AliasVariationFeatureTrait(
    name: String,
    description: String,
    val source: VariationFeatureTrait
) : VariationFeatureTrait(name, description, source.parent) {

    constructor(name: String, source: VariationFeatureTrait) : this(name, source.description, source)
}
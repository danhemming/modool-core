package modool.core.content.signifier.trait.type

import modool.core.content.signifier.trait.Trait

object ComplexType : Trait(Type, listOf(Type))
package modool.core.content.signifier.trait.component

import modool.core.content.signifier.trait.Trait

object Procedure : Trait(Named, Invokable, children = listOf(Parameter))
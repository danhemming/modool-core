package modool.core.content.signifier.tag.terminal

import modool.core.content.signifier.tag.Tag
import modool.core.content.signifier.tag.TagCategory

object LiteralDecorator : TagCategory(inherits = Terminal) {
    object Open : Tag.DynamicTerminalType(category = LiteralDecorator)
    object Close : Tag.DynamicTerminalType(category = LiteralDecorator)
}
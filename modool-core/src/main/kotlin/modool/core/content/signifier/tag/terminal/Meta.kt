package modool.core.content.signifier.tag.terminal

import modool.core.content.signifier.tag.Tag
import modool.core.content.signifier.tag.TagCategory

object Meta : TagCategory(inherits = Terminal) {
    object Comment : Tag.DynamicTerminalType(category = Meta)
    object LineContinuation : Tag.DynamicTerminalType(category = Meta)
}

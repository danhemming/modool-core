package modool.core.content.signifier.tag

import modool.core.content.signifier.Signifiable

interface Taggable : Signifiable {
    fun tag(tag: Tag): Boolean
    fun untag(tag: Tag): Boolean
    fun signifies(category: TagCategory): Boolean = false
}
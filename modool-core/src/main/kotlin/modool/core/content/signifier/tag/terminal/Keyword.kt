package modool.core.content.signifier.tag.terminal

import modool.core.content.signifier.Signifier
import modool.core.content.signifier.tag.Tag
import modool.core.content.signifier.tag.TagCategory

object Keyword : TagCategory(inherits = Terminal) {

    object Hard : Tag.ConstantTerminalType(category = Keyword)

    object Soft : Tag.ConstantTerminalType(category = Keyword)

    object Modifier : Tag.Categorical()
}
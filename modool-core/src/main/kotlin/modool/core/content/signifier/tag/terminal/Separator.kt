package modool.core.content.signifier.tag.terminal

import modool.core.content.signifier.tag.Tag
import modool.core.content.signifier.tag.TagCategory

object Separator : TagCategory(inherits = Terminal) {
    object Symbol : Tag.ConstantTerminalType(category = Separator)
}
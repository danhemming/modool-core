package modool.core.content

enum class SourceProcessingLevel {
    NONE,
    TOKENISE,
    PARSE
}
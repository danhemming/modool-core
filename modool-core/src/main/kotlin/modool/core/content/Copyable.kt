package modool.core.content

/**
 * Implemented by instantiable classes to provide a deep copy of itself
 */
interface Copyable<T> {
    fun copy(): T
}

inline fun <reified T> T.tryCopyOrNull(): T? {
    if (this == null) return null
    return tryCopy()
}

inline fun <reified T> T.tryCopy(): T {
    return if (this is Copyable<*>) this.copy() as T else error("Attempted to copy ${T::class.java} but it is not copyable")
}
package modool.core.content.element

import modool.core.meta.property.PropertyMeta
import modool.core.content.ContentOrigin
import modool.core.content.repository.ElementRepository
import modool.core.content.token.TokenReference
import modool.core.content.token.TokenReferenceRange

sealed class ElementPropertySetter(
        val contentReference: TokenReference,
        val contentOrigin: ContentOrigin) {

    abstract fun apply(repo: ElementRepository, propertyContainerReference: TokenReference)


    class SetPropertyTerminalValue(
            contentReference: TokenReference,
            contentOrigin: ContentOrigin,
            private val property: PropertyMeta
    ) : ElementPropertySetter(contentReference, contentOrigin) {

        override fun apply(repo: ElementRepository, propertyContainerReference: TokenReference) {
            repo.setTerminalValue(propertyContainerReference, contentReference, contentOrigin, property)
        }
    }

    class SetPropertyElementValue(
            contentReference: TokenReference,
            contentOrigin: ContentOrigin,
            private val property: PropertyMeta
    ) : ElementPropertySetter(contentReference, contentOrigin) {

        override fun apply(repo: ElementRepository, propertyContainerReference: TokenReference) {
            repo.setElementValue(propertyContainerReference, contentReference, contentOrigin, property)
        }
    }

    class SetPropertyView(
            contentReference: TokenReference,
            contentOrigin: ContentOrigin,
            private val property: PropertyMeta
    ) : ElementPropertySetter(contentReference, contentOrigin) {

        override fun apply(repo: ElementRepository, propertyContainerReference: TokenReference) {
            repo.setView(propertyContainerReference, contentReference, contentOrigin, property)
        }
    }

    class SetPropertyList(
            contentReference: TokenReference,
            contentOrigin: ContentOrigin,
            private val property: PropertyMeta
    ) : ElementPropertySetter(contentReference, contentOrigin) {

        override fun apply(repo: ElementRepository, propertyContainerReference: TokenReference) {
            repo.setList(propertyContainerReference, contentReference, contentOrigin, property)
        }
    }

    class SetPropertyGroup(
            contentReference: TokenReference,
            contentOrigin: ContentOrigin,
            private val property: PropertyMeta
    ) : ElementPropertySetter(contentReference, contentOrigin) {

        override fun apply(repo: ElementRepository, propertyContainerReference: TokenReference) {
            repo.setPropertyGroup(propertyContainerReference, contentReference, contentOrigin, property)
        }
    }

    class SetPropertyGroupPropertyTerminalValue(
            contentReference: TokenReference,
            contentOrigin: ContentOrigin,
            private val propertyGroupProperty: PropertyMeta,
            private val property: PropertyMeta
    ) : ElementPropertySetter(contentReference, contentOrigin) {

        override fun apply(repo: ElementRepository, propertyContainerReference: TokenReference) {
            repo.setPropertyGroupTerminalValue(
                    propertyContainerReference, contentReference, contentOrigin, propertyGroupProperty, property)
        }
    }

    class SetPropertyGroupPropertyElementValue(
            contentReference: TokenReference,
            contentOrigin: ContentOrigin,
            private val propertyGroupProperty: PropertyMeta,
            private val property: PropertyMeta
    ) : ElementPropertySetter(contentReference, contentOrigin) {

        override fun apply(repo: ElementRepository, propertyContainerReference: TokenReference) {
            repo.setPropertyGroupElementValue(
                    propertyContainerReference, contentReference, contentOrigin, propertyGroupProperty, property)
        }
    }

    class SetPropertyGroupPropertyView(
            contentReference: TokenReference,
            contentOrigin: ContentOrigin,
            private val propertyGroupProperty: PropertyMeta,
            private val property: PropertyMeta
    ) : ElementPropertySetter(contentReference, contentOrigin) {

        override fun apply(repo: ElementRepository, propertyContainerReference: TokenReference) {
            repo.setPropertyGroupView(
                    propertyContainerReference, contentReference, contentOrigin, propertyGroupProperty, property)
        }
    }
}
package modool.core.content.enumeration

import modool.core.content.token.ConstantTerminalDescriptor

interface Enumeration<T : ConstantTerminalDescriptor> {
    val entries: List<T>
}
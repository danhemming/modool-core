package modool.core.content

interface WritableValue<T>: ReadableValue<T> {
    fun set(value: T)
}
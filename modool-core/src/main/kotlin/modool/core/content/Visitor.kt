package modool.core.content

interface Visitor<T, U> {
    fun visit(context: T, member: U)
}
package modool.core.content

interface ReadableValue<out T> {
    fun get(): T
}
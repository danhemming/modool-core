package modool.dom.solution.vfs

import modool.dom.node.DomDocument
import modool.dom.node.text.StreamRenderer
import modool.dom.token.DomToken
import modool.dom.token.findNext
import modool.parser.ParseResult
import java.io.InputStream
import java.io.OutputStream
import java.nio.charset.Charset

class VfsFile(
        fileSystem: Vfs,
        override val parent: VfsDirectory?,
        name: String,
        val charset: Charset = Charsets.UTF_8
) : VfsObject(fileSystem, parent, name) {

    private var parseResult: ParseResult = ParseResult.NotAttempted

    var content: DomDocument<*>? = null
        private set

    fun isParsed(): Boolean {
        return parseResult !is ParseResult.NotAttempted
    }

    @JvmName("setParsedContent")
    fun setContent(parseResult: ParseResult) {
        this.parseResult = parseResult
        this.content = ((parseResult as? ParseResult.Content)?.reference as? DomToken)?.findNext()
    }

    @JvmName("setDocumentContent")
    fun <T : DomDocument<*>> setContent(document: T) {
        this.parseResult = ParseResult.NotAttempted
        this.content = document
    }

    fun createInputStream(): InputStream {
        return fileSystem.createInputStream(this)
    }

    fun createOutputStream(): OutputStream {
        return fileSystem.createOutputStream(this)
    }

    override fun save() {
        content?.let { doc ->
            doc.refreshFormatting()

            createOutputStream().writer(charset).use {
                val streamRenderer = StreamRenderer(it)
                doc.render(streamRenderer)
            }
        }
    }
}
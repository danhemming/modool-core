package modool.dom.solution.vfs

import modool.dom.solution.vfs.VfsFile

interface VfsEvents {
    fun directoryCreated(directory: VfsDirectory) {}
    fun directoryDiscovered(directory: VfsDirectory) {}
    fun directoryDeleted(directory: VfsDirectory) {}
    fun fileCreated(file: VfsFile) {}
    fun fileDiscovered(file: VfsFile) {}
    fun fileWritten(file: VfsFile) {}
    fun fileDeleted(file: VfsFile) {}
}
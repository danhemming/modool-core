package modool.dom.solution.vfs

import java.io.OutputStream

class NotifyingOutputStream(
        private val source: OutputStream,
        private val changedAndClosedHandler: () -> Unit
) : OutputStream() {

    private var changed = false

    override fun write(p0: Int) {
        source.write(p0)
        changed = true
    }

    override fun write(b: ByteArray?) {
        source.write(b)
        changed = true
    }

    override fun write(b: ByteArray?, off: Int, len: Int) {
        source.write(b, off, len)
        changed = true
    }

    override fun flush() {
        source.flush()
    }

    override fun close() {
        source.close()
        if (changed) changedAndClosedHandler()
    }
}
package modool.dom.solution.vfs

abstract class VfsObject(
        protected val fileSystem: Vfs,
        open val parent: VfsObject?,
        val name: String
) {

    open fun delete() {
        fileSystem.deleteObject(this)
    }

    abstract fun save()

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is VfsObject) return false

        if (parent != other.parent) return false
        if (name != other.name) return false

        return true
    }

    override fun hashCode(): Int {
        var result = parent?.hashCode() ?: 0
        result = 31 * result + name.hashCode()
        return result
    }
}
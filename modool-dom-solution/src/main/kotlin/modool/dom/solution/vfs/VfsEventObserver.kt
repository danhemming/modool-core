package modool.dom.solution.vfs

import modool.dom.solution.vfs.VfsFile

/**
 * Dispatcher for all Subscribers
 */
class VfsEventObserver
    : VfsEvents,
        MutableList<VfsEvents> by mutableListOf() {

    override fun directoryCreated(directory: VfsDirectory) {
        forEach { it.directoryCreated(directory) }
    }

    override fun directoryDiscovered(directory: VfsDirectory) {
        forEach { it.directoryDiscovered(directory) }
    }

    override fun directoryDeleted(directory: VfsDirectory) {
        forEach { it.directoryDeleted(directory) }
    }

    override fun fileCreated(file: VfsFile) {
        forEach { it.fileCreated(file) }
    }

    override fun fileDiscovered(file: VfsFile) {
        forEach { it.fileDiscovered(file) }
    }

    override fun fileWritten(file: VfsFile) {
        forEach { it.fileWritten(file) }
    }

    override fun fileDeleted(file: VfsFile) {
        forEach { it.fileDeleted(file) }
    }

}


package modool.dom.solution.vfs

import modool.dom.solution.vfs.VfsFile
import java.io.InputStream
import java.io.OutputStream
import java.nio.file.FileSystems
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.StandardOpenOption

/**
 * A native io (java.nio) implementation of our virtual file system.  This allows us to reuse a lot of existing adapters
 * path must be achieved through FileSystem.getPath
 */
class NativeVfs(
        observer: VfsEventObserver,
        private val path: Path
) : Vfs(observer) {

    override fun listObjects(parent: VfsDirectory): List<VfsObject> {
        val objects = mutableListOf<VfsObject>()
        Files.walk(path, 1).use {
            it.forEach { path ->
                if (Files.isDirectory(path)) {
                    if (path != this.path) {
                        val directory = VfsDirectory(NativeVfs(events, path), parent, path.fileName.toString())
                        objects.add(directory)
                        events.directoryDiscovered(directory)
                    }
                } else if (Files.isRegularFile(path)) {
                    val file = VfsFile(NativeVfs(events, path), parent, path.fileName.toString())
                    events.fileDiscovered(file)
                    objects.add(file)
                }
            }
        }
        return objects
    }

    override fun createFile(parent: VfsDirectory, name: String): VfsFile {
        val filePath = path.resolve(name)
        Files.createFile(filePath)
        val file = VfsFile(NativeVfs(events, filePath), parent, name)
        events.fileDiscovered(file)
        events.fileCreated(file)
        return file
    }

    override fun createDirectory(parent: VfsDirectory, name: String): VfsDirectory {
        val directoryPath = path.resolve(name)
        // FIXME can still fail, not thread safe
        if (!Files.exists(directoryPath)) Files.createDirectory(directoryPath)
        val directory = VfsDirectory(NativeVfs(events, directoryPath), parent, name)
        events.directoryDiscovered(directory)
        events.directoryCreated(directory)
        return directory
    }

    override fun createInputStream(file: VfsFile): InputStream {
        if (Files.isRegularFile(path)) {
            return Files.newInputStream(path)
        } else {
            throw RuntimeException("Attempted to open an InputStream for $path but it is not a file")
        }
    }

    override fun createOutputStream(file: VfsFile): OutputStream {
        return NotifyingOutputStream(Files.newOutputStream(path, StandardOpenOption.WRITE)) {
            events.fileWritten(file)
        }
    }

    override fun deleteObject(obj: VfsObject) {
        Files.delete(path)
        when (obj) {
            is VfsFile -> events.fileDeleted(obj)
            is VfsDirectory -> events.directoryDeleted(obj)
        }
    }

    companion object {
        fun default(path: String) = NativeVfs(VfsEventObserver(), Path.of(path))
        fun currentPath() = NativeVfs(VfsEventObserver(), FileSystems.getDefault().getPath(".").toAbsolutePath())
    }
}
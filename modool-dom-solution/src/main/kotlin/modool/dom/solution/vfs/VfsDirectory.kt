package modool.dom.solution.vfs

class VfsDirectory(
        fileSystem: Vfs,
        override val parent: VfsDirectory?,
        name: String
) : VfsObject(fileSystem, parent, name) {

    private val _contents by lazy { fileSystem.listObjects(this).toMutableSet() }
    internal val events = fileSystem.events

    val contents: Set<VfsObject> get() = _contents
    val files: List<VfsFile> get() = _contents.filterIsInstance(VfsFile::class.java)
    val directories: List<VfsDirectory> get() = _contents.filterIsInstance(VfsDirectory::class.java)

    fun cd(vararg names: String, createIfNotPresent: Boolean = true): VfsDirectory {
        val existingDirectory = _contents
                .filterIsInstance(VfsDirectory::class.java)
                .firstOrNull { it.name == names.first() }
        
        if (existingDirectory != null) {
            return drillDirectory(existingDirectory, names, createIfNotPresent)
        } else if (!createIfNotPresent) {
            throw RuntimeException("directory $name does not exist")
        }
        
        val directory = fileSystem.createDirectory(this, names.first())
        _contents.add(directory)

        return drillDirectory(directory, names, createIfNotPresent)
    }

    private fun drillDirectory(
            firstDirectory: VfsDirectory,
            names: Array<out String>,
            createIfNotPresent: Boolean = true): VfsDirectory {
        return if (names.count() == 1) {
            firstDirectory
        } else {
            var currentDirectory = firstDirectory
            names.drop(1).forEach {
                currentDirectory = currentDirectory.cd(it, createIfNotPresent = createIfNotPresent)
            }
            currentDirectory
        }
    }

    fun open(name: String, createIfNotPresents: Boolean = true): VfsFile {
        val existingFile = _contents
                .filterIsInstance(VfsFile::class.java)
                .firstOrNull { it.name == name }

        if (existingFile != null) {
            return existingFile
        } else if (!createIfNotPresents) {
            throw RuntimeException("file $name does not exist")
        }
        
        val file = fileSystem.createFile(this, name)
        // Accessing contents the first time causes the collection to load (and we just created the file) ...
        val listedFile = files.firstOrNull { it == file }
        return if (listedFile != null) listedFile else {
            _contents.add(file)
            file
        }
    }

    override fun save() {
        files.forEach { it.save() }
        directories.forEach { it.save() }
    }

    override fun delete() {
        files.forEach { it.delete() }
        directories.forEach { it.delete() }
        _contents.clear()
        super.delete()
    }
}
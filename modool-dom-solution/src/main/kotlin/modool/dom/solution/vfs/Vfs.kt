package modool.dom.solution.vfs

import java.io.InputStream
import java.io.OutputStream

abstract class Vfs(internal val events: VfsEventObserver) {
    abstract fun listObjects(parent: VfsDirectory): List<VfsObject>
    abstract fun createFile(parent: VfsDirectory, name: String): VfsFile
    abstract fun createDirectory(parent: VfsDirectory, name: String): VfsDirectory
    abstract fun createInputStream(file: VfsFile): InputStream
    abstract fun createOutputStream(file: VfsFile): OutputStream
    abstract fun deleteObject(obj: VfsObject)
}



package modool.dom.solution.project

import modool.dom.node.DomDocument

interface DomProjectEvents {
    fun documentLoaded(document: DomDocument<*>) {}
    fun documentSaved(document: DomDocument<*>) {}
    fun documentDeleted(document: DomDocument<*>) {}
}
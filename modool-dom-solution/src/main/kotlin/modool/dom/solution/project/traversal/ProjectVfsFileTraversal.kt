package modool.dom.solution.project.traversal

import modool.dom.DomContext
import modool.dom.node.DomDocument
import modool.dom.solution.project.DomProject
import modool.dom.solution.project.DomProjectContextBinding
import modool.dom.solution.vfs.VfsFile

class ProjectVfsFileTraversal(
        private val project: DomProject,
        private var current: Sequence<VfsFile>
) : ProjectVfsFileModelSelectorTraversal,
        ProjectVfsFileFilterTraversal<ProjectVfsFileTraversal> {

    override fun listFiles(name: String): ProjectVfsFileTraversal {
        current = current.filter { it.name == name }
        return this
    }

    override fun listFiles(predicate: (VfsFile) -> Boolean): ProjectVfsFileTraversal {
        current = current.filter(predicate)
        return this
    }

    override fun <D : DomContext, M : DomProjectContextBinding<D>> listFiles(binding: M): ProjectVfsModelFileTraversal {
        return ProjectVfsModelFileTraversal(project, current.filter { binding.isFileSupported(it) }, binding)
    }

    override fun <D : DomContext, M : DomProjectContextBinding<D>> asDocumentSequence(binding: M): Sequence<DomDocument<*>> {
        return current.mapNotNull { it.content ?: project.loadFile(binding, it)?.content }
    }

    fun asSequence(): Sequence<VfsFile> {
        return current
    }
}
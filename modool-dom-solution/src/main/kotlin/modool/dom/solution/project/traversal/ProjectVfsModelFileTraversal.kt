package modool.dom.solution.project.traversal

import modool.dom.node.DomDocument
import modool.dom.solution.project.DomProject
import modool.dom.solution.project.DomProjectContextBinding
import modool.dom.solution.vfs.VfsFile

class ProjectVfsModelFileTraversal(
        private val project: DomProject,
        private var current: Sequence<VfsFile>,
        private var binding: DomProjectContextBinding<*>
) : ProjectVfsFileModelSelectedTraversal,
        ProjectVfsFileFilterTraversal<ProjectVfsModelFileTraversal> {

    override fun listFiles(name: String): ProjectVfsModelFileTraversal {
        current = current.filter { it.name == name }
        return this
    }

    override fun listFiles(predicate: (VfsFile) -> Boolean): ProjectVfsModelFileTraversal {
        current = current.filter(predicate)
        return this
    }

    override fun asDocumentSequence(): Sequence<DomDocument<*>> {
        return current.mapNotNull { it.content ?: project.loadFile(binding, it)?.content }
    }

    fun asSequence(): Sequence<VfsFile> {
        return current
    }
}
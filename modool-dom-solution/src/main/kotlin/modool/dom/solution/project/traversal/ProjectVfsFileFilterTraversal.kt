package modool.dom.solution.project.traversal

import modool.dom.solution.vfs.VfsFile

interface ProjectVfsFileFilterTraversal<T> {
    fun listFiles(name: String): T
    fun listFiles(predicate: (VfsFile) -> Boolean): T
}
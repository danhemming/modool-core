package modool.dom.solution.project

import modool.dom.DomContext
import modool.dom.solution.vfs.VfsFile

abstract class DomProjectContextBinding<D>(val context: D) where D: DomContext {
    abstract fun isFileSupported(file: VfsFile): Boolean
}
package modool.dom.solution.project

import modool.dom.DomContext
import modool.dom.solution.project.traversal.ProjectVfsDirectoryTraversal
import modool.dom.solution.vfs.VfsDirectory
import modool.dom.solution.vfs.VfsEventSubscriber
import modool.dom.solution.vfs.VfsFile

class DomProject(
        val directory: VfsDirectory,
        var lazyLoading: Boolean = false) {

    val events = DomProjectEventsObserver()

    private val contextBindings = mutableListOf<DomProjectContextBinding<*>>()
    private val fileSystemEventSubscriber = object : VfsEventSubscriber {
        override fun fileDiscovered(file: VfsFile) {
            contextBindings.forEach {
                if (it.isFileSupported(file)) {
                    if (!lazyLoading && events.isNotEmpty()) {
                        loadFile(it.context, file)
                    }
                }
            }
        }
    }

    init {
        directory.events.add(fileSystemEventSubscriber)
    }

    fun addContextBinding(binding: DomProjectContextBinding<*>) {
        contextBindings.add(binding)
    }

    fun traversal(): ProjectVfsDirectoryTraversal {
        return ProjectVfsDirectoryTraversal(this, listOf(directory).asSequence())
    }

    fun <T> loadFile(binding: DomProjectContextBinding<T>, file: VfsFile): VfsFile? where T : DomContext {
        return loadFile(binding.context, file)
    }

    fun <T> loadFile(domContext: T, file: VfsFile): VfsFile
            where T : DomContext {

        if (!file.isParsed()) {
            file.createInputStream().use {
                file.setContent(
                        domContext.parse(
                                domContext.lexer.createDocumentLexerRules(),
                                domContext.parser.createDocumentParserRule(),
                                source = it))
            }
        }
        return file
    }

    fun save() {
        directory.save()
    }

    fun delete() {
        directory.delete()
    }
}
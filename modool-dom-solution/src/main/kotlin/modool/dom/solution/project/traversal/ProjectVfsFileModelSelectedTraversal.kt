package modool.dom.solution.project.traversal

import modool.dom.node.DomDocument

interface ProjectVfsFileModelSelectedTraversal {
    fun asDocumentSequence(): Sequence<DomDocument<*>>
}
package modool.dom.solution.project.traversal

import modool.dom.DomContext
import modool.dom.node.DomDocument
import modool.dom.solution.project.DomProject
import modool.dom.solution.project.DomProjectContextBinding
import modool.dom.solution.vfs.VfsDirectory
import modool.dom.solution.vfs.VfsFile

class ProjectVfsDirectoryTraversal(
        private val project: DomProject,
        private var current: Sequence<VfsDirectory>
) : ProjectVfsFileFilterTraversal<ProjectVfsFileTraversal>,
        ProjectVfsFileModelSelectorTraversal {

    fun listDirectories(name: String): ProjectVfsDirectoryTraversal {
        current = current.map { cd -> cd.directories.filter { it.name == name } }.flatten()
        return this
    }

    fun listDirectories(predicate: (VfsDirectory) -> Boolean): ProjectVfsDirectoryTraversal {
        current = current.map { cd -> cd.directories.filter(predicate) }.flatten()
        return this
    }

    override fun listFiles(name: String): ProjectVfsFileTraversal {
        return ProjectVfsFileTraversal(project, current.map { cd -> cd.files.filter { it.name == name } }.flatten())
    }

    override fun listFiles(predicate: (VfsFile) -> Boolean): ProjectVfsFileTraversal {
        return ProjectVfsFileTraversal(project, current.map { cd -> cd.files.filter(predicate) }.flatten())
    }

    override fun <D : DomContext, M : DomProjectContextBinding<D>> listFiles(binding: M): ProjectVfsModelFileTraversal {
        return ProjectVfsModelFileTraversal(project, current.flatMap { cd ->
            cd.files.asSequence().filter { binding.isFileSupported(it) }
        }, binding)
    }

    fun asSequence(): Sequence<VfsDirectory> {
        return current
    }

    override fun <D : DomContext, M : DomProjectContextBinding<D>> asDocumentSequence(binding: M): Sequence<DomDocument<*>> {
        return listFiles(binding).asDocumentSequence()
    }
}

package modool.dom.solution.project.traversal

import modool.dom.DomContext
import modool.dom.node.DomDocument
import modool.dom.solution.project.DomProjectContextBinding

interface ProjectVfsFileModelSelectorTraversal {
    fun <D, M> listFiles(binding: M): ProjectVfsModelFileTraversal where D : DomContext, M : DomProjectContextBinding<D>
    fun <D, M> asDocumentSequence(binding: M): Sequence<DomDocument<*>> where D: DomContext, M : DomProjectContextBinding<D>
}
package modool.dom.solution.project

import modool.dom.DomContext
import modool.dom.DomModel
import modool.dom.solution.vfs.VfsFile

class DomProjectContextModelBinding<D>(
        context: D,
        private val model: DomModel<D>
) : DomProjectContextBinding<D>(context)
        where D: DomContext {

    override fun isFileSupported(file: VfsFile): Boolean {
        return model.supportedFileExtensions.find { file.name.endsWith(it) } != null
    }
}
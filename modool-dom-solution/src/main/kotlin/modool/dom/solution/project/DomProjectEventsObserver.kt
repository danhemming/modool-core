package modool.dom.solution.project

import modool.dom.node.DomDocument

class DomProjectEventsObserver
    : DomProjectEvents,
        MutableList<DomProjectEventsSubscriber> by mutableListOf() {

    override fun documentLoaded(document: DomDocument<*>) {
        forEach { it.documentLoaded(document) }
    }

    override fun documentSaved(document: DomDocument<*>) {
        forEach { it.documentSaved(document) }
    }

    override fun documentDeleted(document: DomDocument<*>) {
        forEach { it.documentDeleted(document) }
    }
}
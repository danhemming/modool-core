package modool.dom.solution.project

import modool.dom.DomContext
import modool.dom.solution.vfs.VfsFile

class DomProjectContextFileExtensionBinding<D>(
        context: D,
        private val extensions: List<String>
) : DomProjectContextBinding<D>(context)
        where D: DomContext {

    override fun isFileSupported(file: VfsFile): Boolean {
        return extensions.find { file.name.endsWith(it) } != null
    }
}
package modool.dom.solution

import modool.dom.solution.project.DomProject
import modool.dom.solution.vfs.NativeVfs
import modool.dom.solution.vfs.VfsDirectory

class DomSolution(private val directory: VfsDirectory = VfsDirectory(NativeVfs.currentPath(), null, "root")) {
    private val _projects = mutableMapOf<String, DomProject>()
    private val projects: Iterable<DomProject> get() = _projects.values

    /**
     * Create a project, if no directory is provided we will assume that the project lives in the solution directory
     * TODO: project directory may exist on disk so create directory will not work
     */
    fun project(name: String, directory: VfsDirectory = this.directory.cd(name)): DomProject {
        return _projects.getOrElse(name) {
            val project = DomProject(directory)
            _projects[name] = project
            project
        }
    }

    fun save() {
        projects.forEach { it.save() }
    }

    fun delete() {
        directory.delete()
    }
}
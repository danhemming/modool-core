
dependencies {
	compile(project(":modool-core"))    
	compile(project(":modool-dom"))
	compile("it.unimi.dsi:fastutil:8.2.1")
    compile("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.0.0")
    compile("org.jetbrains.kotlin:kotlin-reflect:1.3.0")
}

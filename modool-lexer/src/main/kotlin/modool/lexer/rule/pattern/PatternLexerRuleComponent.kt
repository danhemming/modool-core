package modool.lexer.rule.pattern

import modool.lexer.rule.LexerRulable

interface PatternLexerRuleComponent : LexerRulable

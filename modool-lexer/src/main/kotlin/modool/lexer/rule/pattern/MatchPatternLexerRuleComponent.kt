package modool.lexer.rule.pattern

import modool.lexer.LexerContext
import modool.lexer.variable.LexerMatchVariable
import modool.lexer.rule.LexerRule
import modool.lexer.rule.state.VariableLexerRulable

class MatchPatternLexerRuleComponent(
    private val variableName: String
) : VariableLexerRulable(),
    LexerRule,
    PatternLexerRuleComponent {

    private var variable: LexerMatchVariable? = null

    override fun variable(context: LexerContext): LexerMatchVariable {
        if (variable == null) {
            variable = context.variables.getOrCreateMatch(variableName)
        }
        return variable!!
    }
}
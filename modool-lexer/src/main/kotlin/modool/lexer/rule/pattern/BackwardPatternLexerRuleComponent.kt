package modool.lexer.rule.pattern

import modool.lexer.rule.contextual.BackwardLexerRulable

class BackwardPatternLexerRuleComponent(
        rules: List<PatternLexerRuleComponent>
): BackwardLexerRulable(rules),
        PatternLexerRuleComponent
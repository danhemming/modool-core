package modool.lexer.rule.pattern

import modool.lexer.rule.state.EvalValidateBeforeLexerRulable
import modool.lexer.rule.state.operator.LexerOperator

class EvalValidateBeforePatternLexerRuleComponent(
        operator: LexerOperator,
        rule: PatternLexerRuleComponent
) : EvalValidateBeforeLexerRulable(
        operator,
        rule
), PatternLexerRuleComponent
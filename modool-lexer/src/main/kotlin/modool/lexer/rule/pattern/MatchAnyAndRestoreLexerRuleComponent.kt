package modool.lexer.rule.pattern

import modool.lexer.rule.state.MatchAnyAndRestoreLexerRulable

class MatchAnyAndRestoreLexerRuleComponent(
        variableName: String
) : MatchAnyAndRestoreLexerRulable(variableName),
        PatternLexerRuleComponent
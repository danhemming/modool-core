package modool.lexer.rule.pattern

import modool.lexer.LexerMatch
import modool.lexer.rule.state.EvalEachValidateAfterLexerRulable
import modool.lexer.rule.state.operator.LexerOperator

class EvalEachValidateAfterPatternLexerRuleComponent<T>(
        rule: T,
        operator: LexerOperator
) : EvalEachValidateAfterLexerRulable<T>(rule, operator),
        PatternLexerRuleComponent
        where T : PatternLexerRuleComponent,
              T : Iterable<LexerMatch>
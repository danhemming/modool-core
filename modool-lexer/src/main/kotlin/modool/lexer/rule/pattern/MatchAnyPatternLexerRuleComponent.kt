package modool.lexer.rule.pattern

import modool.lexer.rule.state.MatchAnyLexerRulable

class MatchAnyPatternLexerRuleComponent(
        variableName: String
) : MatchAnyLexerRulable(variableName),
        PatternLexerRuleComponent
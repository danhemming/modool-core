package modool.lexer.rule.pattern

import modool.lexer.rule.state.MatchAndAbandonLexerRulable

class MatchAndAbandonPatternLexerRuleComponent(
        variableName: String
) : MatchAndAbandonLexerRulable(variableName),
        PatternLexerRuleComponent
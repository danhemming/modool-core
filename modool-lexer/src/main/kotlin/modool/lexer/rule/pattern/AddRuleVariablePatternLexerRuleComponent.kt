package modool.lexer.rule.pattern

import modool.lexer.rule.state.AddRuleVariableLexerRulable

class AddRuleVariablePatternLexerRuleComponent(
    variableName: String,
    rule: PatternLexerRuleComponent
) : AddRuleVariableLexerRulable<PatternLexerRuleComponent>(variableName, rule),
    PatternLexerRuleComponent
package modool.lexer.rule.pattern

import modool.lexer.rule.ComponentLexerRulable

/**
 * Encapsulates a collection of pattern rule components so that they act (match) as one unit.
 */
class PatternGroupPatternLexerRuleComponent(
        override val rules: List<PatternLexerRuleComponent>
) : ComponentLexerRulable<PatternLexerRuleComponent>(),
        PatternLexerRuleComponent
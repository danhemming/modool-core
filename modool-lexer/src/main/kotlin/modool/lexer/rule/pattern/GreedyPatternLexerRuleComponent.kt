package modool.lexer.rule.pattern

import modool.lexer.rule.selfish.GreedyLexerRulable

class GreedyPatternLexerRuleComponent(rule: PatternLexerRuleComponent)
    : PatternLexerRuleComponent,
        GreedyLexerRulable<PatternLexerRuleComponent>(rule)
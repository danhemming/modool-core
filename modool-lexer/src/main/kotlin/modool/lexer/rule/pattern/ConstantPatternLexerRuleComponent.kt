package modool.lexer.rule.pattern

import modool.lexer.rule.constant.ConstantLexerRulable

class ConstantPatternLexerRuleComponent(
    value: String,
    isCaseSensitive: Boolean
) : ConstantLexerRulable(value, isCaseSensitive),
        PatternLexerRuleComponent
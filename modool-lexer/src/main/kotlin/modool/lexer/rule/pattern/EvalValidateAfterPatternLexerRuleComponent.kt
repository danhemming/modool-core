package modool.lexer.rule.pattern

import modool.lexer.rule.state.EvalValidateAfterLexerRulable
import modool.lexer.rule.state.operator.LexerOperator

class EvalValidateAfterPatternLexerRuleComponent(
        rule: PatternLexerRuleComponent,
        operator: LexerOperator
) : EvalValidateAfterLexerRulable(
        rule,
        operator
), PatternLexerRuleComponent
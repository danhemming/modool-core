package modool.lexer.rule.pattern

import modool.lexer.rule.boundary.StartLexerRulable

object StartPatternLexerRuleComponent
    : StartLexerRulable(),
        PatternLexerRuleComponent
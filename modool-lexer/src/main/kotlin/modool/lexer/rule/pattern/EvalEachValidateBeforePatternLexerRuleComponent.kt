package modool.lexer.rule.pattern

import modool.lexer.LexerMatch
import modool.lexer.rule.state.EvalEachValidateBeforeLexerRulable
import modool.lexer.rule.state.operator.LexerOperator

class EvalEachValidateBeforePatternLexerRuleComponent<T>(
        operator: LexerOperator,
        rule: T
) : EvalEachValidateBeforeLexerRulable<T>(operator, rule),
        PatternLexerRuleComponent
        where T : PatternLexerRuleComponent,
              T : Iterable<LexerMatch>
package modool.lexer.rule.pattern

import modool.core.content.token.TokenWriter
import modool.lexer.LexerContext
import modool.core.content.signifier.tag.Tag
import modool.lexer.rule.ComponentLexerRulable
import modool.lexer.rule.LexerRule

/**
 * A pattern is a terminal emitting rule.
 */
class PatternLexerRule(
        override val rules: List<PatternLexerRuleComponent>,
        private val tags: List<Tag>
) : ComponentLexerRulable<PatternLexerRuleComponent>(),
        LexerRule {

    override fun commit(context: LexerContext, content: TokenWriter) {
        if (content.isAccepting && matchIndex >- 1) {
            rules.forEach { it.commit(context, content) }
            content.writeTerminal(matchIndex, matchLength, tags)
        }
    }
}
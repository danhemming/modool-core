package modool.lexer.rule.pattern

import modool.lexer.rule.state.MatchAndClearLexerRulable

class MatchAndClearPatternLexerRuleComponent(
        variableName: String
) : MatchAndClearLexerRulable(variableName),
        PatternLexerRuleComponent
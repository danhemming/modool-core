package modool.lexer.rule.pattern

import modool.lexer.rule.boundary.EndLexerRulable

object EndPatternLexerRuleComponent
    : EndLexerRulable(),
        PatternLexerRuleComponent
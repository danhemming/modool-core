package modool.lexer.rule.pattern

import modool.lexer.rule.selfish.YieldLexerRulable

class YieldPatternLexerRuleComponent(
    variableName: String,
    rule: PatternLexerRuleComponent
) : YieldLexerRulable<PatternLexerRuleComponent>(variableName, rule),
        PatternLexerRuleComponent
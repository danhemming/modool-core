package modool.lexer.rule.pattern

import modool.lexer.LexerContext
import modool.lexer.rule.repeat.RepeaterLexerRulable

class RepeaterPatternLexerRuleComponent(
        component: PatternLexerRuleComponent,
        minOccurs: Int = -1,
        maxOccurs: Int = -1)
    : RepeaterLexerRulable<PatternLexerRuleComponent>(component, minOccurs, maxOccurs),
        PatternLexerRuleComponent {

    override fun afterOccurrence(context: LexerContext) {
        super.afterOccurrence(context)
        rule.reset(context)
    }
}
package modool.lexer.rule.pattern

import modool.lexer.rule.option.ChooseLexerRulable

class ChoosePatternLexerRuleComponent<T>(components: List<T>)
    : ChooseLexerRulable<T>(components),
        PatternLexerRuleComponent
        where T : PatternLexerRuleComponent

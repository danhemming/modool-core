package modool.lexer.rule.pattern

import modool.lexer.rule.selfish.EagerLexerRulable

class EagerPatternLexerRuleComponent(rule: PatternLexerRuleComponent)
    : PatternLexerRuleComponent,
        EagerLexerRulable<PatternLexerRuleComponent>(rule)
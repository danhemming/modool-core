package modool.lexer.rule.whitespace

import modool.core.content.token.Whitespace

/**
 * Used to capture non-semantic whitespace
 */
open class UnicodeWhitespaceLexerRule : WhitespaceLexerRule() {

    override fun isWhiteSpace(value: Char): Boolean {
        return Whitespace.isUnicodeWhitespace(value)
    }
}


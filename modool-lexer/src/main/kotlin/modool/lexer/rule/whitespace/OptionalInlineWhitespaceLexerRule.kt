package modool.lexer.rule.whitespace

import modool.core.content.token.Whitespace
import modool.lexer.LexerContext

/**
 * This exists as a convenience to support context free lexers.  This rule is interspersed throughout components to
 * capture the whitespace between tokens.
 */
class OptionalInlineWhitespaceLexerRule
    : WhitespaceLexerRule() {

    override fun isStartable(context: LexerContext): Boolean {
        return false
    }

    override fun isSkipable(context: LexerContext): Boolean {
        return true
    }

    override fun isSatisfied(context: LexerContext): Boolean {
        return true
    }

    override fun isWhiteSpace(value: Char): Boolean {
        return Whitespace.isInlineWhitespace(value)
    }
}
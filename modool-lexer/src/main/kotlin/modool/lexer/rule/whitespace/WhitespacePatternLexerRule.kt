package modool.lexer.rule.whitespace

import modool.core.content.token.TokenWriter
import modool.core.content.token.Whitespace
import modool.lexer.LexerContext
import modool.core.content.signifier.tag.Tag
import modool.lexer.rule.ComponentLexerRule
import modool.lexer.rule.pattern.PatternLexerRuleComponent

/**
 * Creates whitespace that is tagged and processed in parsing.
 */
class WhitespacePatternLexerRule(
        rules: List<PatternLexerRuleComponent>,
        private val tags: List<Tag>
) : WhitespaceLexerRule() {

    private val component = ComponentLexerRule(rules)

    override fun reset(context: LexerContext) {
        super.reset(context)
        component.reset(context)
    }

    override fun isSkipable(context: LexerContext): Boolean {
        return component.isSkipable(context)
    }

    override fun isStartable(context: LexerContext): Boolean {
        return super.isStartable(context) && component.isStartable(context)
    }

    override fun isAcceptable(context: LexerContext): Boolean {
        contextualize(context)
        return (isStartOfContentEncountered || super.isAcceptable(context))
                && component.isAcceptable(context)
    }

    override fun isWhiteSpace(value: Char): Boolean {
        // Unicode is the greediest match so we use that as a guard in case the component matches non-whitespace
        return Whitespace.isUnicodeWhitespace(value)
    }

    override fun consume(context: LexerContext) {
        super.consume(context)
        component.consume(context)
    }

    override fun moveNext(context: LexerContext) {
        super.moveNext(context)
        component.moveNext(context)
    }

    override fun stash(context: LexerContext) {
        super.stash(context)
        component.stash(context)
    }

    override fun isSatisfied(context: LexerContext): Boolean {
        return component.isSatisfied(context)
    }

    override fun isContinuable(context: LexerContext): Boolean {
        return component.isContinuable(context)
    }

    override fun complete(context: LexerContext) {
        super.complete(context)
        component.complete(context)
    }

    override fun commit(context: LexerContext, content: TokenWriter) {
        if (content.isAccepting) {
            whitespaces?.let {
                content.overwriteWhitespace(it, tags)
            }
        }
    }
}
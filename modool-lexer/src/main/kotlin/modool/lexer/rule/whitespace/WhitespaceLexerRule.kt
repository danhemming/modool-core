package modool.lexer.rule.whitespace

import modool.core.content.token.TokenWriter
import modool.core.content.token.Whitespace
import modool.core.content.token.WhitespaceContent
import modool.lexer.LexerContext
import modool.lexer.LexerMatch
import modool.lexer.rule.LexerRule

/**
 * Due to the requirement that all contiguous whitespace is represented by one token, care is taken to look behind and
 * capture previous whitespace.
 * It is unusual for there to be sequential whitespace capturing rules in a grammar but it can happen in context
 * sensitive grammars e.g. AsciiDoc.
 * Notes:
 * - Regular whitespace is not considered meaningful (semantic) content by default (isYielding() = true)
 * - Is NOT a LookAround rule (can be achieved using whitespace character patterns).  LookAround does not work well for
 * those whitespace rules which are satisfied by edge conditions.
 */
abstract class WhitespaceLexerRule
    : LexerRule {

    private var contextualized = false
    protected var whitespaces: WhitespaceContent? = null
        private set
    protected var isStartOfContentEncountered = false
        private set

    val currentMargin: Int
        get() =
            if (whitespaces?.isLeftMargin == true || isStartOfContentEncountered) whitespaces?.marginX ?: 0 else 0

    protected val lineCount: Int get() = whitespaces?.lineCount ?: 0

    override var matchIndex = LexerMatch.NO_MATCH_INDEX
        protected set

    override var matchLength = 0
        protected set

    override fun reset(context: LexerContext) {
        whitespaces = null
        isStartOfContentEncountered = false
        contextualized = false
        matchIndex = LexerMatch.NO_MATCH_INDEX
        matchLength = 0
    }

    /**
     * Populate previous whitespaces (due to sequential lexer rules) or start of content may exist.
     * The whitespaces are ultimately used to replace the prior output whitespace token (should it exist)
     */
    protected fun contextualize(context: LexerContext) {
        if (contextualized) {
            return
        }

        if (context.source.currentIndex == 0) {
            isStartOfContentEncountered = true
            contextualized = true
            return
        }

        var priorWhitespaceStartIndex = -1
        for (index in context.source.currentIndex - 1 downTo 0) {
            val priorValue = context.source.readCharAt(index) ?: break
            if (isWhiteSpace(priorValue)) {
                priorWhitespaceStartIndex = index
                if (context.source.currentIndex == 0) {
                    isStartOfContentEncountered = true
                }
            } else {
                break
            }
        }

        if (priorWhitespaceStartIndex > -1) {

            if (whitespaces == null) {
                whitespaces = context.tokens.createWhitespaceBuilder()
            }
            for (index in priorWhitespaceStartIndex until context.source.currentIndex) {
                whitespaces?.add(Whitespace.from(context.source.readCharAt(index)!!, context.source.readCharAt(index + 1)))
            }
        }

        contextualized = true
    }

    fun createRestorePoint(): WhitespaceContent? {
        return whitespaces?.save()
    }

    fun restore(restorePoint: WhitespaceContent?) {
        whitespaces = restorePoint
    }

    override fun isStartable(context: LexerContext): Boolean {
        return context.source.readChar()?.let { isWhiteSpace(it) } ?: false
    }

    override fun isAcceptable(context: LexerContext): Boolean {
        return context.source.readChar()?.let { isWhiteSpace(it) } ?: false
    }

    abstract fun isWhiteSpace(value: Char): Boolean

    override fun moveNext(context: LexerContext) {
        matchLength ++
        if (matchLength == 1) {
            matchIndex = context.source.currentIndex
        }
    }

    override fun stash(context: LexerContext) {
        contextualize(context)
        if (whitespaces == null) {
            whitespaces = context.tokens.createWhitespaceBuilder()
        }
        if (context.source.isAtStart) {
            isStartOfContentEncountered = true
        }
        whitespaces?.add(Whitespace.from(context.source.readChar()!!, context.source.readCharAhead()))
    }

    fun getContentLength(): Int {
        return whitespaces?.renderLength ?: 0
    }

    fun isLineCountExceeded(lineCount: Int): Boolean {
        return whitespaces?.let { (lineCount > -1 && it.lineCount >= lineCount) } ?: false
    }

    override fun isYielding(context: LexerContext): Boolean {
        return true
    }

    override fun isMatchingContent(context: LexerContext): Boolean {
        return whitespaces != null
    }

    override fun isSatisfied(context: LexerContext): Boolean {
        return whitespaces != null
    }

    override fun commit(context: LexerContext, content: TokenWriter) {
        if (content.isAccepting) {
            whitespaces?.let { content.overwriteWhitespace(it, emptyList()) }
        }
    }
}
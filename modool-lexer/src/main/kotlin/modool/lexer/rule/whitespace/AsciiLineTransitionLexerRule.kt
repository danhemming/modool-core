package modool.lexer.rule.whitespace

import modool.lexer.LexerContext

/**
 * This exists as a convenience to support context free lexers.  This rule is used in line oriented grammars to
 * delimit elements.
 * This differs from the newline character rule because it is satisfied at the edges of content
 */
class AsciiLineTransitionLexerRule
    : AsciiWhitespaceLexerRule() {

    override fun isSatisfied(context: LexerContext): Boolean {
        // contextualize here to ensure isStartOfContentEncountered is discovered
        contextualize(context)
        return isStartOfContentEncountered
                || !context.source.isMoreContentAvailable
                || whitespaces?.isNewLine ?: false
    }
}
package modool.lexer.rule.whitespace

import modool.lexer.LexerContext

/**
 * Mirrors LineGapParserRule and is mostly for convenience in context sensitive grammars
 * We are tolerant of a missing closing line gap BUT not of an opening one.  This is because an empty opening gap is
 * confused with matching anything!
 */
class AsciiLineGapLexerRule(val size: Int) : AsciiWhitespaceLexerRule() {

    override fun isSatisfied(context: LexerContext): Boolean {
        // currentMargin check is required in edge case of consuming prior whitespace and current value is space
        return !context.source.isMoreContentAvailable
                || ((whitespaces?.lineCount ?: 0) == size + 1 && currentMargin == 0)
    }
}
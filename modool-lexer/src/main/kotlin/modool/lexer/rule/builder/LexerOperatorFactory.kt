package modool.lexer.rule.builder

import modool.core.content.signifier.tag.Tag
import modool.lexer.LexerMatch
import modool.lexer.rule.LexerRulable
import modool.lexer.rule.state.operator.*
import modool.lexer.rule.state.operator.variable.ClearVariableLexerOperator
import modool.lexer.rule.state.operator.variable.match.IterateMatchesLexerOperator
import modool.lexer.rule.state.operator.variable.MissingVariableLexerOperator
import modool.lexer.rule.state.operator.variable.bool.AvailableBooleanVariableLexerOperator
import modool.lexer.rule.state.operator.variable.bool.EqualsBooleanVariableLexerOperator
import modool.lexer.rule.state.operator.variable.bool.SetBooleanVariableLexerOperator
import modool.lexer.rule.state.operator.variable.integer.*
import modool.lexer.rule.state.operator.variable.match.*
import modool.lexer.rule.state.operator.variable.rule.MatchableRuleVariableLexerOperator

@LexerDsl
interface LexerOperatorFactory {

    fun <T> eval(source: T, action: LexerOperator)
            where T : LexerOperator,
                  T : LexerMatch =
        EvalLexerOperator(action, source)

    fun <T> evalEach(source: T, action: LexerOperator)
            where T : LexerOperator,
                  T : Iterable<LexerMatch> =
        EvalEachLexerOperator(action, source)

    fun any(vararg operators: LexerOperator) =
        AnyLexerStateOperator(operators.toList())

    fun all(vararg operators: LexerOperator) =
        AllLexerStateOperator(operators.toList())

    fun first(vararg operators: LexerOperator) =
        FirstLexerStateOperator(operators.toList())

    fun not(operator: LexerOperator) =
        NotLexerStateOperator(operator)

    fun optional(operator: LexerOperator) =
        OptionalLexerStateOperator(operator)

    fun precedes(condition: LexerRulable) =
        PrecedesLexerOperator(condition)

    fun matches(condition: LexerRulable) =
        MatchesLexerOperator(condition)

    fun follows(condition: LexerRulable) =
        FollowsLexerOperator(condition)

    fun insertMarker(vararg tags: Tag) =
        InsertMarkerLexerOperator(tags.toList())

    fun insertDeferredMarker(vararg tags: Tag) =
        InsertDeferredMarkerLexerOperator(tags.toList())

    // common variable operators

    fun missing(variableName: String) =
        MissingVariableLexerOperator(variableName)

    fun clear(variableName: String) =
        ClearVariableLexerOperator(variableName)

    // int variable operators

    fun availableInt(variableName: String) =
        AvailableIntegerVariableLexerOperator(variableName)

    fun setInt(variableName: String, value: Int) =
        SetIntegerVariableLexerOperator(
            variableName,
            value
        )

    fun incrementInt(variableName: String, value: Int) =
        IncrementIntegerVariableLexerOperator(
            variableName,
            value
        )

    fun decrementInt(variableName: String, value: Int) =
        DecrementIntegerVariableLexerOperator(
            variableName,
            value
        )

    fun equalsInt(variableName: String, value: Int) =
        EqualsIntegerVariableLexerOperator(
            variableName,
            value
        )

    // boolean variable operators

    fun availableBoolean(variableName: String) =
        AvailableBooleanVariableLexerOperator(variableName)

    fun setBoolean(variableName: String, value: Boolean) =
        SetBooleanVariableLexerOperator(
            variableName,
            value
        )

    fun equalsBoolean(variableName: String, value: Boolean) =
        EqualsBooleanVariableLexerOperator(
            variableName,
            value
        )

    // single-match variable operators

    fun matched(variableName: String) =
        MatchedLexerOperator(
            variableName
        )

    fun setMatch(variableName: String) =
        SetMatchVariableLexerOperator(
            variableName
        )

    fun expandMatch(variableName: String) =
        ExpandMatchLexerOperator(
            variableName
        )

    fun equalsMatch(variableName: String, condition: LexerRulable) =
        EqualsMatchLexerOperator(
            variableName,
            condition
        )

    fun followsMatch(variableName: String) =
        FollowsMatchLexerOperator(variableName)

    fun followsMatchThen(variableName: String, condition: LexerRulable) =
        FollowsMatchThenLexerOperator(
            variableName,
            condition
        )

    fun followsMatchThenInlineWhitespace(variableName: String) =
        FollowsMatchThenInlineWhitespaceLexerOperator(
            variableName
        )

    fun followsMatchThenUnicodeWhitespace(variableName: String) =
        FollowsMatchThenUnicodeWhitespaceLexerOperator(
            variableName
        )

    // Multi-match variable operators

    fun addMatch(variableName: String) =
        AddMatchLexerOperator(variableName)

    fun abandonMatch(variableName: String) =
        AbandonMatchLexerOperator(
            variableName
        )

    fun restoreMatch(variableName: String, condition: LexerRulable) =
        RestoreMatchLexerOperator(
            variableName,
            condition
        )

    fun iterateMatches(variableName: String) =
        IterateMatchesLexerOperator(variableName)

    fun findMatch(variableName: String, condition: LexerRulable) =
        FindMatchLexerOperator(
            variableName,
            condition
        )

    fun filterMatches(variableName: String, condition: LexerRulable) =
        FilterMatchesLexerOperator(
            variableName,
            condition
        )

    // Rule variable operators (doesn't make sense to create with an operator i.e. conditionally due to it's static nature)

    fun matchable(variableName: String) =
        MatchableRuleVariableLexerOperator(variableName)

    companion object Default : LexerOperatorFactory
}
package modool.lexer.rule.builder

import modool.core.content.enumeration.Enumeration
import modool.core.content.signifier.tag.Tag
import modool.core.content.token.ConstantTerminalDescriptor
import modool.lexer.rule.ComponentLexerRule
import modool.lexer.rule.LexerRule
import modool.lexer.rule.constant.ConstantIndex
import modool.lexer.rule.constant.ConstantIndexLexerRule
import modool.lexer.rule.constant.ConstantTerminalFactoryLexerRule
import modool.lexer.rule.pattern.PatternLexerRule
import modool.lexer.rule.whitespace.WhitespacePatternLexerRule

class LexerRuleCollectionFactory private constructor() : LexerRuleFactory {

    private val rules = mutableListOf<LexerRule>()

    fun component(body: LexerRuleCollectionFactory.() -> Unit): ComponentLexerRule<LexerRule> {
        return ComponentLexerRule(createList(body))
    }

    fun pattern(
            vararg tags: Tag,
            body: LexerPatternRuleComponentCollectionFactory.() -> Unit
    ): PatternLexerRule {
        return PatternLexerRule(LexerPatternRuleComponentCollectionFactory.createList(body), tags.toList())
    }

    fun whitespace(
            vararg tags: Tag,
            body: LexerPatternRuleComponentCollectionFactory.() -> Unit): WhitespacePatternLexerRule {
        return WhitespacePatternLexerRule(LexerPatternRuleComponentCollectionFactory.createList(body), tags.toList())
    }

    operator fun List<LexerRule>.unaryPlus() {
        forEach { rules.add(it) }
    }

    operator fun Enumeration<*>.unaryPlus() {
        entries.forEach { rules.add(ConstantTerminalFactoryLexerRule(it)) }
    }

    @JvmName("terminals")
    operator fun Sequence<ConstantTerminalDescriptor>.unaryPlus() {
        forEach { rules.add(ConstantTerminalFactoryLexerRule(it)) }
    }

    @JvmName("terminal")
    operator fun ConstantTerminalDescriptor.unaryPlus() {
        rules.add(ConstantTerminalFactoryLexerRule(this))
    }

    @JvmName("constantIndex")
    operator fun ConstantIndex.unaryPlus() {
        rules.add(ConstantIndexLexerRule(this))
    }

    operator fun LexerRule.unaryPlus() {
        rules.add(this)
    }

    fun toList(): List<LexerRule> {
        return rules
    }

    companion object {
        fun create(body: LexerRuleCollectionFactory.() -> Unit): LexerRuleCollectionFactory {
            val ruleList = LexerRuleCollectionFactory()
            body(ruleList)
            return ruleList
        }

        fun createList(body: LexerRuleCollectionFactory.() -> Unit): List<LexerRule> {
            val builder = LexerRuleCollectionFactory()
            body(builder)
            return builder.toList()
        }
    }
}
package modool.lexer.rule.builder

import modool.lexer.rule.pattern.PatternGroupPatternLexerRuleComponent
import modool.lexer.rule.pattern.PatternLexerRuleComponent

class LexerPatternRuleComponentCollectionFactory private constructor(): LexerPatternRuleComponentFactory {

    private val components = mutableListOf<PatternLexerRuleComponent>()

    fun group(body: LexerPatternRuleComponentCollectionFactory.() -> Unit) =
            PatternGroupPatternLexerRuleComponent(createList(body))

    operator fun PatternLexerRuleComponent.unaryPlus() {
        components.add(this)
    }

    fun toList(): List<PatternLexerRuleComponent> {
        return components
    }

    companion object {
        fun create(body: LexerPatternRuleComponentCollectionFactory.() -> Unit): LexerPatternRuleComponentCollectionFactory {
            val builder = LexerPatternRuleComponentCollectionFactory()
            body(builder)
            return builder
        }

        fun createList(body: LexerPatternRuleComponentCollectionFactory.() -> Unit): List<PatternLexerRuleComponent> {
            val builder = LexerPatternRuleComponentCollectionFactory()
            body(builder)
            return builder.toList()
        }
    }
}
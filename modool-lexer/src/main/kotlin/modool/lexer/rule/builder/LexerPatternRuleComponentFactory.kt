package modool.lexer.rule.builder

import modool.lexer.LexerMatch
import modool.lexer.rule.character.*
import modool.lexer.rule.option.ExceptPatternLexerRuleComponent
import modool.lexer.rule.pattern.*
import modool.lexer.rule.state.operator.LexerOperator

interface LexerPatternRuleComponentFactory : LexerOperatorFactory {

    fun start() = StartPatternLexerRuleComponent

    fun end() = EndPatternLexerRuleComponent

    fun anyChar() = AnyCharacterPatternLexerRuleComponent()

    fun asciiLowerLetterChar() =
        AsciiLowerCaseLetterCharacterPatternLexerRuleComponent()

    fun asciiUpperLetterChar() =
        AsciiUpperCaseLetterCharacterPatternLexerRuleComponent()

    fun asciiLetterOrDigitChar() =
        AsciiLetterOrDigitCharacterPatternLexerRuleComponent()

    fun asciiLetterChar() =
        AsciiLetterCharacterPatternLexerRuleComponent()

    fun unicodeLetterChar() =
        UnicodeLetterCharacterPatternLexerRuleComponent()

    fun digit() =
        AsciiDigitPatternLexerRuleComponent()

    fun asciiWhitespaceChar(tabMatchCount: Int = 1) =
        AsciiWhitespaceCharacterPatternLexerRuleComponent(tabMatchCount)

    fun asciiNewLineChar() =
        AsciiNewLineCharacterPatternLexerRuleComponent()

    fun unicodeWhitespaceChar(tabMatchCount: Int = 1) =
        UnicodeWhitespaceCharacterPatternLexerRuleComponent(tabMatchCount)

    fun unicodeNewLineChar() =
        UnicodeNewLineCharacterPatternLexerRuleComponent()

    fun inlineWhitespaceChar(tabMatchCount: Int = 1) =
        InlineWhitespaceCharacterPatternLexerRuleComponent(tabMatchCount)

    fun char(value: Char) =
        SpecificCharacterPatternLexerRuleComponent(value)

    fun charRange(startValue: Char, endValue: Char) =
        SpecificCharacterRangePatternLexerRuleComponent(startValue, endValue)

    fun escape(charRule: CharacterPatternLexerRuleComponent, value: Char) =
        EscapeCharacterPatternLexerRuleComponent(value, charRule)

    fun greedy(component: PatternLexerRuleComponent) =
        GreedyPatternLexerRuleComponent(component)

    fun eager(component: PatternLexerRuleComponent) =
        EagerPatternLexerRuleComponent(component)

    fun optional(component: PatternLexerRuleComponent) =
        RepeaterPatternLexerRuleComponent(component, minOccurs = 0, maxOccurs = 1)

    fun unlimited(component: PatternLexerRuleComponent) =
        RepeaterPatternLexerRuleComponent(component, minOccurs = -1, maxOccurs = -1)

    fun exactly(count: Int, component: PatternLexerRuleComponent) =
        RepeaterPatternLexerRuleComponent(component, minOccurs = count, maxOccurs = count)

    fun atLeast(count: Int, component: PatternLexerRuleComponent) =
        RepeaterPatternLexerRuleComponent(component, minOccurs = count, maxOccurs = -1)

    fun atMost(count: Int, component: PatternLexerRuleComponent) =
        RepeaterPatternLexerRuleComponent(component, minOccurs = -1, maxOccurs = count)

    fun group(vararg components: PatternLexerRuleComponent) =
        PatternGroupPatternLexerRuleComponent(components.toList())

    fun choose(vararg components: PatternLexerRuleComponent) =
        ChoosePatternLexerRuleComponent(components.toList())

    fun except(vararg components: PatternLexerRuleComponent) =
        ExceptPatternLexerRuleComponent(components.toList())

    fun precedes(rule: PatternLexerRuleComponent, conditionRule: PatternLexerRuleComponent) =
        eval(rule, precedes(conditionRule))

    fun follows(conditionRule: PatternLexerRuleComponent, rule: PatternLexerRuleComponent) =
        eval(follows(conditionRule), rule)

    fun backwards(vararg rules: PatternLexerRuleComponent) =
        BackwardPatternLexerRuleComponent(rules.toList())

    fun constant(value: String, isCaseSensitive: Boolean = true) =
        ConstantPatternLexerRuleComponent(value, isCaseSensitive)

    fun eval(beforeOperator: LexerOperator, rule: PatternLexerRuleComponent, afterOperator: LexerOperator) =
        eval(eval(beforeOperator, rule), afterOperator)

    fun eval(operator: LexerOperator, rule: PatternLexerRuleComponent) =
        EvalValidateBeforePatternLexerRuleComponent(operator, rule)

    fun eval(rule: PatternLexerRuleComponent, operator: LexerOperator) =
        EvalValidateAfterPatternLexerRuleComponent(rule, operator)

    fun <T> evalEach(operator: LexerOperator, rule: T)
            where T : PatternLexerRuleComponent,
                  T : Iterable<LexerMatch> =
        EvalEachValidateBeforePatternLexerRuleComponent(operator, rule)

    fun <T> evalEach(rule: T, operator: LexerOperator)
            where T : PatternLexerRuleComponent,
                  T : Iterable<LexerMatch> =
        EvalEachValidateAfterPatternLexerRuleComponent(rule, operator)

    // variable operations

    fun missing(variableName: String, rule: PatternLexerRuleComponent) =
        eval(missing(variableName), rule)

    fun clear(variableName: String, rule: PatternLexerRuleComponent) =
        eval(clear(variableName), rule)

    // Single match operations

    fun matched(variableName: String, rule: PatternLexerRuleComponent) =
        eval(matched(variableName), rule)
    
    fun setMatch(variableName: String, rule: PatternLexerRuleComponent) =
        eval(setMatch(variableName), rule)

    fun expandMatch(variableName: String, rule: PatternLexerRuleComponent) =
        eval(expandMatch(variableName), rule)

    fun match(variableName: String) =
        MatchPatternLexerRuleComponent(variableName)

    fun matchAndClear(variableName: String) =
        MatchAndClearPatternLexerRuleComponent(variableName)

    // multi-match operations

    fun addMatch(variableName: String, rule: PatternLexerRuleComponent) =
        eval(addMatch(variableName), rule)

    fun matchAny(variableName: String) =
        MatchAnyPatternLexerRuleComponent(variableName)

    fun matchAndAbandon(variableName: String) =
        MatchAndAbandonPatternLexerRuleComponent(variableName)

    fun matchAnyAndRestore(variableName: String) =
        MatchAnyAndRestoreLexerRuleComponent(variableName)

    // rule operations

    fun addRule(variableName: String, rule: PatternLexerRuleComponent) =
        AddRuleVariablePatternLexerRuleComponent(variableName, rule)

    fun yieldTo(variableName: String, rule: PatternLexerRuleComponent) =
        YieldPatternLexerRuleComponent(variableName, rule)

    companion object Default : LexerPatternRuleComponentFactory
}
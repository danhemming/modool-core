package modool.lexer.rule.builder

import modool.core.content.signifier.tag.Tag
import modool.core.content.token.ConstantTerminalDescriptor
import modool.lexer.LexerMatch
import modool.lexer.ObservableLexerRuleTracker
import modool.lexer.rule.ComponentLexerRule
import modool.lexer.rule.LexerRule
import modool.lexer.rule.boundary.EndLexerRule
import modool.lexer.rule.boundary.StartLexerRule
import modool.lexer.rule.bounded.*
import modool.lexer.rule.constant.ConstantLexerRule
import modool.lexer.rule.constant.ConstantTerminalFactoryLexerRule
import modool.lexer.rule.constant.ConstantTerminalLexerRule
import modool.lexer.rule.contextual.BackwardLexerRule
import modool.lexer.rule.option.ChooseLexerRule
import modool.lexer.rule.option.ExceptLexerRule
import modool.lexer.rule.pattern.PatternLexerRule
import modool.lexer.rule.pattern.PatternLexerRuleComponent
import modool.lexer.rule.repeat.RepeaterLexerRule
import modool.lexer.rule.scope.ScopeLexerRule
import modool.lexer.rule.scope.TerminatedScopeLexerRule
import modool.lexer.rule.selfish.EagerLexerRule
import modool.lexer.rule.selfish.GreedyLexerRule
import modool.lexer.rule.selfish.YieldLexerRule
import modool.lexer.rule.state.*
import modool.lexer.rule.state.operator.LexerOperator
import modool.lexer.rule.unknown.UnknownLexerRule
import modool.lexer.rule.whitespace.*
import modool.lexer.strategy.LexerCommitStrategy
import modool.lexer.strategy.LexerRecoveryStrategy

interface LexerRuleFactory : LexerOperatorFactory {

    fun start() = StartLexerRule

    fun end() = EndLexerRule

    fun component(rules: List<LexerRule>): ComponentLexerRule<LexerRule> {
        return ComponentLexerRule(rules)
    }

    fun component(vararg rules: LexerRule): ComponentLexerRule<LexerRule> {
        return ComponentLexerRule(rules.toList())
    }

    fun pattern(tags: List<Tag>, rules: List<PatternLexerRuleComponent>): PatternLexerRule {
        return PatternLexerRule(rules, tags)
    }

    fun scope(
        unknownRule: UnknownLexerRule,
        rule: LexerRule
    ): ScopeLexerRule {
        return scope(
            unknownRule,
            listOf(rule)
        )
    }

    fun scope(
        unknownRule: UnknownLexerRule,
        rules: List<LexerRule>
    ): ScopeLexerRule {
        return scope(
            LexerRecoveryStrategy.Default,
            LexerCommitStrategy.Default,
            unknownRule,
            rules
        )
    }

    fun <T : LexerRule> scope(
        recoveryStrategy: LexerRecoveryStrategy,
        commitStrategy: LexerCommitStrategy,
        unknownRule: UnknownLexerRule,
        rules: List<T>
    ): ScopeLexerRule {
        return ScopeLexerRule(
            unknownRule,
            ObservableLexerRuleTracker(
                rules.toList(),
                recoveryStrategy
            ),
            recoveryStrategy,
            commitStrategy
        )
    }

    fun scope(
        unknownRule: UnknownLexerRule,
        rule: LexerRule,
        terminatorRule: LexerRule
    ): TerminatedScopeLexerRule {
        return scope(
            unknownRule,
            listOf(rule),
            terminatorRule
        )
    }

    fun <T : LexerRule> scope(
        unknownRule: UnknownLexerRule,
        rules: List<T>,
        terminatorRule: LexerRule
    ): TerminatedScopeLexerRule {
        return scope(
            LexerRecoveryStrategy.Default,
            LexerCommitStrategy.Default,
            unknownRule,
            rules,
            terminatorRule
        )
    }

    fun scope(
        recoveryStrategy: LexerRecoveryStrategy,
        commitStrategy: LexerCommitStrategy,
        unknownRule: UnknownLexerRule,
        rules: List<LexerRule>,
        terminatorRule: LexerRule
    ): TerminatedScopeLexerRule {
        return TerminatedScopeLexerRule(
            unknownRule,
            ObservableLexerRuleTracker(rules, recoveryStrategy),
            terminatorRule,
            recoveryStrategy,
            commitStrategy
        )
    }

    fun captureUntil(
        tags: List<Tag>,
        until: Array<Char>,
        escape: Char? = null
    ) = CaptureUntilLexerRuleComponent(until, escape, tags)

    fun captureFromUntil(
        tags: List<Tag>,
        from: LexerRule,
        until: LexerRule,
        escape: Char? = null
    ) = CaptureFromUntilLexerRuleComponent(from, until, escape, tags)

    fun captureFromTo(
        tags: List<Tag>,
        from: LexerRule,
        to: LexerRule,
        escape: Char? = null
    ) = CaptureFromToLexerRuleComponent(from, to, escape, tags)

    fun eval(beforeOperator: LexerOperator, rule: LexerRule, afterOperator: LexerOperator) =
        eval(eval(beforeOperator, rule), afterOperator)

    fun eval(operator: LexerOperator, rule: LexerRule) =
        EvalValidateBeforeLexerRule(operator, rule)

    fun eval(rule: LexerRule, operator: LexerOperator) =
        EvalValidateAfterLexerRule(rule, operator)

    fun <T> evalEach(operator: LexerOperator, rule: T)
            where T : LexerRule,
                  T : Iterable<LexerMatch> =
        EvalEachValidateBeforeLexerRule(operator, rule)

    fun <T> evalEach(rule: T, operator: LexerOperator)
            where T : LexerRule,
                  T : Iterable<LexerMatch> =
        EvalEachValidateAfterLexerRule(rule, operator)

    // variable operations
    
    fun missing(variableName: String, rule: LexerRule) =
        eval(missing(variableName), rule)

    fun clear(variableName: String, rule: LexerRule) =
        eval(clear(variableName), rule)

    // Single match operations

    fun matched(variableName: String, rule: LexerRule) =
        eval(matched(variableName), rule)

    fun setMatch(variableName: String, rule: LexerRule) =
        eval(setMatch(variableName), rule)

    fun expandMatch(variableName: String, rule: LexerRule) =
        eval(expandMatch(variableName), rule)

    fun match(variableName: String, vararg tags: Tag) =
        MatchLexerRule(variableName, tags.toList())

    fun matchAndClear(variableName: String, vararg tags: Tag) =
        MatchAndClearLexerRule(variableName, tags.toList())

    // multi-match operations

    fun addMatch(variableName: String, rule: LexerRule) =
        eval(addMatch(variableName), rule)

    fun matchAny(variableName: String, vararg tags: Tag) =
        MatchAnyLexerRule(variableName, tags.toList())

    fun matchAndAbandon(variableName: String, vararg tags: Tag) =
        MatchAndAbandonLexerRule(variableName, tags.toList())

    fun matchAnyAndRestore(variableName: String, vararg tags: Tag) =
        MatchAnyAndRestoreLexerRule(variableName, tags.toList())

    // rule operations

    fun addRule(variableName: String, rule: LexerRule) =
        AddRuleVariableLexerRule(variableName, rule)

    fun yieldTo(variableName: String, rule: LexerRule) =
        YieldLexerRule(variableName, rule)

    // general operations
    
    fun precedes(rule: LexerRule, conditionRule: LexerRule) =
        eval(rule, precedes(conditionRule))

    fun follows(conditionRule: LexerRule, rule: LexerRule) =
        eval(follows(conditionRule), rule)

    fun backwards(vararg rules: LexerRule) =
        BackwardLexerRule(rules.toList())

    fun startsWith(condition: LexerRule, rule: LexerRule) =
        StartsWithLexerRule(condition, rule)

    fun endsWith(rule: LexerRule, condition: LexerRule) =
        EndsWithLexerRule(condition, rule)

    fun singleLineComment(start: ConstantLexerRule) =
        SingleLineCommentLexerRule(start)

    fun multiLineComment(start: ConstantLexerRule, end: ConstantLexerRule) =
        MultiLineCommentLexerRule(start, end)

    fun unicodeWhitespace() =
        UnicodeWhitespaceLexerRule()

    fun optionalUnicodeWhitespace() =
        OptionalUnicodeWhitespaceLexerRule()

    fun unicodeLineTransition() =
        UnicodeLineTransitionLexerRule()

    fun unicodeLineGap(size: Int) =
        UnicodeLineGapLexerRule(size)

    fun asciiWhitespace() =
        AsciiWhitespaceLexerRule()

    fun optionalAsciiWhitespace() =
        OptionalAsciiWhitespaceLexerRule()

    fun asciiLineTransition() =
        AsciiLineTransitionLexerRule()

    fun asciiLineGap(size: Int) =
        AsciiLineGapLexerRule(size)

    fun optionalInlineWhitespace() =
        OptionalInlineWhitespaceLexerRule()

    fun whitespace(tags: List<Tag>, rules: List<PatternLexerRuleComponent>): WhitespacePatternLexerRule {
        return WhitespacePatternLexerRule(rules, tags)
    }

    fun choose(vararg components: LexerRule) =
        ChooseLexerRule(components.toList())

    fun except(vararg components: LexerRule) =
        ExceptLexerRule(components.toList())

    fun greedy(component: LexerRule) =
        GreedyLexerRule(component)

    fun eager(component: LexerRule) =
        EagerLexerRule(component)

    fun terminated(component: LexerRule, terminator: LexerRule) =
        TerminatedLexerRule(component, terminator)

    fun optional(component: LexerRule) =
        RepeaterLexerRule(component, minOccurs = 0, maxOccurs = 1)

    fun unlimited(component: LexerRule) =
        RepeaterLexerRule(component, minOccurs = -1, maxOccurs = -1)

    fun exactly(count: Int, component: LexerRule) =
        RepeaterLexerRule(component, minOccurs = count, maxOccurs = count)

    fun atLeast(count: Int, component: LexerRule) =
        RepeaterLexerRule(component, minOccurs = count, maxOccurs = -1)

    fun atMost(count: Int, component: LexerRule) =
        RepeaterLexerRule(component, minOccurs = -1, maxOccurs = count)

    fun constant(value: String, isCaseSensitive: Boolean = true): ConstantLexerRule {
        return ConstantLexerRule(value, isCaseSensitive)
    }

    fun terminal(value: String, vararg tags: Tag): ConstantLexerRule {
        return ConstantTerminalLexerRule(value, isCaseSensitive = true, tags = tags.toList())
    }

    fun terminal(value: String, isCaseSensitive: Boolean, vararg tags: Tag): ConstantLexerRule {
        return ConstantTerminalLexerRule(value, isCaseSensitive, tags.toList())
    }

    fun terminal(
        constantTerminalDescriptor: ConstantTerminalDescriptor,
        adHocTags: List<Tag> = emptyList()
    ): ConstantTerminalFactoryLexerRule {
        return ConstantTerminalFactoryLexerRule(constantTerminalDescriptor, adHocTags)
    }

    companion object Default : LexerRuleFactory
}
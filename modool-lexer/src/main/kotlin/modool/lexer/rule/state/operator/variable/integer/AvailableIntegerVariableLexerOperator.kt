package modool.lexer.rule.state.operator.variable.integer

import modool.lexer.LexerContext

class AvailableIntegerVariableLexerOperator(
        variableName: String
) : IntegerVariableLexerOperator(variableName) {

    override fun validate(context: LexerContext): Boolean {
        return variable?.isPopulated == true
    }
}
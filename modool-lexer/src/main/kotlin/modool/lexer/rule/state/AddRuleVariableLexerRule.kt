package modool.lexer.rule.state

import modool.lexer.rule.LexerRule

class AddRuleVariableLexerRule(
    variableName: String,
    rule: LexerRule
) : AddRuleVariableLexerRulable<LexerRule>(variableName, rule),
    LexerRule
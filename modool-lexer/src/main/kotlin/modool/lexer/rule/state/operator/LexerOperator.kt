package modool.lexer.rule.state.operator

import modool.core.content.token.TokenWriter
import modool.lexer.LexerContext

abstract class LexerOperator {
    open fun reset(context: LexerContext) {
        return
    }

    open fun validate(context: LexerContext): Boolean {
        return false
    }

    open fun execute(context: LexerContext, content: TokenWriter, matchIndex: Int, matchLength: Int) {
        return
    }
}
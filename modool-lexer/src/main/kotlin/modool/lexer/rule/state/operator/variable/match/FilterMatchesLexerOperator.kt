package modool.lexer.rule.state.operator.variable.match

import modool.lexer.LexerContext
import modool.lexer.LexerMatch
import modool.lexer.rule.LexerRulable

class FilterMatchesLexerOperator(
    variableName: String,
    private val condition: LexerRulable
) : MatchVariableLexerOperator(variableName),
    Iterable<LexerMatch> {

    private var matches = emptyList<LexerMatch>()

    override fun reset(context: LexerContext) {
        super.reset(context)
        matches = emptyList()
    }

    override fun validate(context: LexerContext): Boolean {
        variable?.let { variable ->
            if (!variable.isPopulated) {
                return false
            }
            matches = variable.filter { condition.isMatching(context, it) }
        }
        return matches.isEmpty()
    }

    override fun iterator(): Iterator<LexerMatch> {
        return matches.iterator()
    }
}
package modool.lexer.rule.state.operator.variable.match

import modool.core.content.token.TokenWriter
import modool.lexer.LexerContext

class AbandonMatchLexerOperator(
    variableName: String
) : MatchVariableLexerOperator(variableName) {

    override fun validate(context: LexerContext): Boolean {
        return variable?.isPopulated ?: false
    }

    override fun execute(context: LexerContext, content: TokenWriter, matchIndex: Int, matchLength: Int) {
        variable?.abandon()
    }
}
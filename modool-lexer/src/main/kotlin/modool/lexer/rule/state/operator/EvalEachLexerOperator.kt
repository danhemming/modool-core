package modool.lexer.rule.state.operator

import modool.core.content.token.TokenWriter
import modool.lexer.LexerContext
import modool.lexer.LexerMatch

class EvalEachLexerOperator<T>(
        private val action: LexerOperator,
        private val source: T
) : LexerOperator()
        where T : LexerOperator,
              T : Iterable<LexerMatch> {

    override fun reset(context: LexerContext) {
        super.reset(context)
        action.reset(context)
        source.reset(context)
    }

    override fun validate(context: LexerContext): Boolean {
        return source.validate(context) && action.validate(context)
    }

    override fun execute(context: LexerContext, content: TokenWriter, matchIndex: Int, matchLength: Int) {
        // Note: we deliberately use the values from source and not the ones from the parent match
        source.forEach { match ->
            action.execute(context, content, match.matchIndex, match.matchLength)
        }
    }
}
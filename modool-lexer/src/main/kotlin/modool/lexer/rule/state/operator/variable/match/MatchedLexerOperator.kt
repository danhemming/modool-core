package modool.lexer.rule.state.operator.variable.match

import modool.lexer.LexerContext

class MatchedLexerOperator(
    variableName: String
) : MatchVariableLexerOperator(variableName) {

    override fun validate(context: LexerContext): Boolean {
        return variable?.isPopulated == true
    }
}
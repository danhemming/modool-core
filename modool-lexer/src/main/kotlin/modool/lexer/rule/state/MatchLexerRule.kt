package modool.lexer.rule.state

import modool.core.content.signifier.tag.Tag
import modool.core.content.token.TokenWriter
import modool.lexer.LexerContext
import modool.lexer.LexerMatch
import modool.lexer.variable.LexerMatchVariable
import modool.lexer.rule.LexerRule

class MatchLexerRule(
    private val variableName: String,
    private val tags: List<Tag>
) : VariableLexerRulable(),
    LexerRule {

    private var variable: LexerMatchVariable? = null

    override fun variable(context: LexerContext): LexerMatchVariable {
        if (variable == null) {
            variable = context.variables.getOrCreateMatch(variableName)
        }
        return variable!!
    }

    override fun commit(context: LexerContext, content: TokenWriter) {
        if (content.isAccepting && matchIndex != LexerMatch.NO_MATCH_INDEX && matchLength > 0) {
            content.writeTerminal(matchIndex, matchLength, tags)
        }
    }
}
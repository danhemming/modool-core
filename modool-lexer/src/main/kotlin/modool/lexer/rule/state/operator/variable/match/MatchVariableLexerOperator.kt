package modool.lexer.rule.state.operator.variable.match

import modool.lexer.LexerContext
import modool.lexer.LexerMatch
import modool.lexer.variable.LexerMatchVariable
import modool.lexer.rule.state.operator.LexerOperator

abstract class MatchVariableLexerOperator(
    private val variableName: String
) : LexerOperator(),
    LexerMatch {

    protected var variable: LexerMatchVariable? = null

    override val matchIndex: Int
        get() = variable!!.matchIndex

    override val matchLength: Int
        get() = variable!!.matchLength

    override val matchCount: Int
        get() = variable!!.matchCount

    override fun reset(context: LexerContext) {
        super.reset(context)
        if (variable == null) {
            variable = context.variables.getOrCreateMatch(variableName)
        }
    }
}
package modool.lexer.rule.state

import modool.lexer.rule.LexerRule
import modool.lexer.rule.state.operator.LexerOperator

class EvalValidateAfterLexerRule(
        rule: LexerRule,
        operator: LexerOperator
) : EvalValidateAfterLexerRulable(
        rule,
        operator
), LexerRule
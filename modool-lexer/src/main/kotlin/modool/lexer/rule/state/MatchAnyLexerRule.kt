package modool.lexer.rule.state

import modool.core.content.token.TokenWriter
import modool.lexer.LexerContext
import modool.core.content.signifier.tag.Tag
import modool.lexer.LexerMatch
import modool.lexer.rule.LexerRule

class MatchAnyLexerRule(
        variableName: String,
        private val tags: List<Tag>
) : MatchAnyLexerRulable(variableName),
        LexerRule {

    override fun commit(context: LexerContext, content: TokenWriter) {
        if (content.isAccepting && matchIndex != LexerMatch.NO_MATCH_INDEX && matchLength > 0) {
            content.writeTerminal(matchIndex, matchLength, tags)
        }
    }
}
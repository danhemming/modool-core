package modool.lexer.rule.state.operator.variable

import modool.lexer.LexerContext

class MissingVariableLexerOperator(
    variableName: String
) : VariableLexerOperator(variableName) {

    override fun validate(context: LexerContext): Boolean {
        return variable?.isPopulated != true
    }
}
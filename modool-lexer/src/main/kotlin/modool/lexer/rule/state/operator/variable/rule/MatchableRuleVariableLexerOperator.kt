package modool.lexer.rule.state.operator.variable.rule

import modool.lexer.LexerContext
import modool.lexer.rule.LexerRulable

class MatchableRuleVariableLexerOperator(
    variableName: String
) : RuleVariableLexerOperator(variableName) {

    override fun validate(context: LexerContext): Boolean {
        return variable?.isStartable(context) ?: false
    }
}
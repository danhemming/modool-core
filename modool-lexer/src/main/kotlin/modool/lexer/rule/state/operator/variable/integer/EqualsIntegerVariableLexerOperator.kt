package modool.lexer.rule.state.operator.variable.integer

import modool.lexer.LexerContext

class EqualsIntegerVariableLexerOperator(
        variableName: String,
        private val value: Int
) : IntegerVariableLexerOperator(variableName) {

    override fun validate(context: LexerContext): Boolean {
        return variable?.isEqual(value) ?: false
    }
}
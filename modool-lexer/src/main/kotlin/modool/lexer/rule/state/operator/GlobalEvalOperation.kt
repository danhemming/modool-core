package modool.lexer.rule.state.operator

import modool.core.content.token.TokenWriter
import modool.lexer.LexerContext
import modool.lexer.LexerInitialization
import modool.lexer.LexerMatch

class GlobalEvalOperation(
        private val before: LexerOperator?,
        private val after: LexerOperator?
) : LexerInitialization {

    override fun reset(context: LexerContext) {
        super.reset(context)
        before?.reset(context)
        after?.reset(context)
    }

    fun isValid(context: LexerContext, match: LexerMatch): Boolean {
        return (before == null
                || context.lookAround { context.source.moveTo(match.matchIndex) && before.validate(context) })
                && (after == null
                || context.lookAround { context.source.moveTo(match.matchIndex + match.matchLength - 1) && after.validate(context) })
    }

    fun executeBefore(context: LexerContext, content: TokenWriter, matchIndex: Int, matchLength: Int) {
        before?.execute(context, content, matchIndex, matchLength)
    }

    fun executeAfter(context: LexerContext, content: TokenWriter, matchIndex: Int, matchLength: Int) {
        after?.execute(context, content, matchIndex, matchLength)
    }
}
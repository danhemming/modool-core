package modool.lexer.rule.state.operator

import modool.lexer.LexerContext

class NotLexerStateOperator(
        private val operator: LexerOperator
) : LexerOperator() {

    override fun reset(context: LexerContext) {
        operator.reset(context)
    }

    override fun validate(context: LexerContext): Boolean {
        return !operator.validate(context)
    }
}
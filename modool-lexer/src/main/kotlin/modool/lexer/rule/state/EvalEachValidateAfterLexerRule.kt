package modool.lexer.rule.state

import modool.lexer.LexerMatch
import modool.lexer.rule.LexerRule
import modool.lexer.rule.state.operator.LexerOperator

class EvalEachValidateAfterLexerRule<T>(
        rule: T,
        operator: LexerOperator
) : EvalEachValidateAfterLexerRulable<T>(rule, operator),
        LexerRule
        where T : LexerRule,
              T : Iterable<LexerMatch>
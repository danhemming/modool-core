package modool.lexer.rule.state

import modool.lexer.*
import modool.lexer.rule.LexerRulable
import modool.lexer.variable.LexerMatchVariable
import modool.lexer.variable.isEqual
import modool.lexer.variable.isEqualAt

/**
 * Searches all the values within a stack variable for a match at the current content.
 */
abstract class MatchAnyLexerRulable(
        private val variableName: String)
    : LexerRulable {

    private var variable: LexerMatchVariable? = null
    protected var matchValues: List<LexerMatch> = emptyList()

    final override var matchIndex = LexerMatch.NO_MATCH_INDEX
        protected set

    final override var matchLength = 0
        protected set

    override fun isMatchBefore(context: LexerContext): Boolean {
        return variable(context).let { variable ->
            variable.isPopulated &&
                    variable.find { entry ->
                        context.source.readStringBehind(entry.matchLength)
                                ?.let { value -> entry.isEqual(context.source, value) }
                                ?: false
                    } != null
        }
    }

    override fun isMatch(context: LexerContext): Boolean {
        return variable(context).let { variable ->
            variable.isPopulated &&
                    variable.find { entry ->
                        context.source.readString(entry.matchLength)
                                ?.let { value -> entry.isEqual(context.source, value) }
                                ?: false
                    } != null
        }
    }

    override fun isMatchAfter(context: LexerContext): Boolean {
        return variable(context).let { variable ->
            variable.isPopulated &&
                    variable.find { entry ->
                        context.source.readStringAhead(entry.matchLength)
                                ?.let { value -> entry.isEqual(context.source, value) }
                                ?: false
                    } != null
        }
    }

    override fun isMatchAt(context: LexerContext, index: Int): Boolean {
        return variable(context).let { variable ->
            variable.isPopulated &&
                    variable.find { entry ->
                        context.source.readStringAt(index, entry.matchLength)
                                ?.let { value -> entry.isEqual(context.source, value) }
                                ?: false
                    } != null
        }
    }

    override fun isStartable(context: LexerContext): Boolean {
        return variable(context).let { variable ->
            variable.isPopulated && context.source.readChar()?.let {
                variable.find { entry -> entry.isEqualAt(context.source, 0, it) } != null
            } ?: false
        }
    }

    override fun reset(context: LexerContext) {
        matchValues = emptyList()
        matchIndex = LexerMatch.NO_MATCH_INDEX
        matchLength = 0
    }

    override fun isAcceptable(context: LexerContext): Boolean {
        val variable = variable(context)

        if (!variable.isPopulated) {
            return false
        }

        val testIndex = if (matchIndex == LexerMatch.NO_MATCH_INDEX) 0 else context.source.currentIndex - matchIndex
        if (testIndex < 0 || testIndex >= variable.matchLength) {
            return false
        }

        if (testIndex == 0 && matchValues.isEmpty()) {
            matchValues = variable.toList()
        }

        return context.source.readChar()
                ?.let { matchValues.find { entry -> entry.isEqualAt(context.source, testIndex, it) } != null }
                ?: false
    }

    protected fun variable(context: LexerContext): LexerMatchVariable {
        if (variable == null) {
            variable = context.variables.getOrCreateMatch(variableName)
        }
        return variable!!
    }

    override fun isMatchingContent(context: LexerContext): Boolean {
        return matchIndex != LexerMatch.NO_MATCH_INDEX
    }

    override fun moveNext(context: LexerContext) {
        if (matchLength == 0 && matchValues.isEmpty()) {
            matchValues = variable(context).toList()
        }
        matchValues = matchValues.filter { entry ->
            entry.isEqualAt(context.source, matchLength, context.source.readChar()!!)
        }
        matchLength++
        if (matchLength == 1) {
            matchIndex = context.source.currentIndex
        }
    }

    override fun isSatisfied(context: LexerContext): Boolean {
        return matchValues.isNotEmpty()
                && matchValues.find { entry -> entry.matchLength > 0 && entry.matchLength == matchLength } != null
    }
}
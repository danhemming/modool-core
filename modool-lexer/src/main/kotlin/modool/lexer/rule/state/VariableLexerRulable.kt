package modool.lexer.rule.state

import modool.lexer.LexerContext
import modool.lexer.LexerMatch
import modool.lexer.variable.LexerMatchVariable
import modool.lexer.rule.LexerRulable

/**
 * Reads previously stored variable and matches against current content
 */
abstract class VariableLexerRulable
    : LexerRulable {

    final override var matchIndex = LexerMatch.NO_MATCH_INDEX
        private set

    final override var matchLength = 0
        private set

    override fun isMatchBefore(context: LexerContext): Boolean {
        return variable(context).let { variable ->
            variable.isPopulated &&
                    context.source.readStringBehind(variable.matchLength)?.let { value ->
                        variable.isEqual(value)
                    } ?: false
        }
    }

    override fun isMatch(context: LexerContext): Boolean {
        return variable(context).let { variable ->
            variable.isPopulated &&
                    context.source.readString(variable.matchLength)?.let { value ->
                        variable.isEqual(value)
                    } ?: false
        }
    }

    override fun isMatchAfter(context: LexerContext): Boolean {
        return variable(context).let { variable ->
            variable.isPopulated &&
                    context.source.readStringAhead(variable.matchLength)?.let { value ->
                        variable.isEqual(value)
                    } ?: false
        }
    }

    override fun isMatchAt(context: LexerContext, index: Int): Boolean {
        return variable(context).let { variable ->
            variable.isPopulated &&
                    context.source.readStringAt(index, variable.matchLength)?.let { value ->
                        variable.isEqual(value)
                    } ?: false
        }
    }

    override fun isStartable(context: LexerContext): Boolean {
        return variable(context).let { variable ->
            variable.isPopulated && context.source.readChar()?.let { variable.isEqualAt(0, it) } ?: false
        }
    }

    override fun reset(context: LexerContext) {
        matchIndex = LexerMatch.NO_MATCH_INDEX
        matchLength = 0
    }

    override fun isAcceptable(context: LexerContext): Boolean {
        val variable = variable(context)

        if (!variable.isPopulated) {
            return false
        }

        val testIndex = if (matchIndex == LexerMatch.NO_MATCH_INDEX) 0 else context.source.currentIndex - matchIndex
        if (testIndex < 0 || testIndex >= variable.matchLength) {
            return false
        }

        return context.source.readChar()
                ?.let { variable.isEqualAt(testIndex, it) }
                ?: false
    }

    override fun isMatchingContent(context: LexerContext): Boolean {
        return matchIndex != LexerMatch.NO_MATCH_INDEX
    }

    override fun moveNext(context: LexerContext) {
        matchLength++
        if (matchLength == 1) {
            matchIndex = context.source.currentIndex
        }
    }

    override fun isSatisfied(context: LexerContext): Boolean {
        return variable(context).let { variable -> variable.matchLength > 0 && matchLength == variable.matchLength }
    }

    protected abstract fun variable(context: LexerContext): LexerMatchVariable
}
package modool.lexer.rule.state.operator

import modool.core.content.token.TokenWriter
import modool.lexer.LexerContext

class OptionalLexerStateOperator(
        private val operator: LexerOperator
) : LexerOperator() {

    private var isMatched = false

    override fun reset(context: LexerContext) {
        operator.reset(context)
        isMatched = false
    }

    override fun validate(context: LexerContext): Boolean {
        isMatched = operator.validate(context)
        return true
    }

    override fun execute(context: LexerContext, content: TokenWriter, matchIndex: Int, matchLength: Int) {
        if (isMatched) {
            operator.execute(context, content, matchIndex, matchLength)
        }
    }
}
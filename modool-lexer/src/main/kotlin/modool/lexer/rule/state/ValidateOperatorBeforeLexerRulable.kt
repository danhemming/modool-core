package modool.lexer.rule.state

import modool.lexer.LexerContext
import modool.lexer.LexerMatch
import modool.lexer.rule.LexerRulable
import modool.lexer.rule.WrappedLexerRulable
import modool.lexer.rule.state.operator.LexerOperator

abstract class ValidateOperatorBeforeLexerRulable<T>(
    protected val operator: LexerOperator,
    rule: T
) : WrappedLexerRulable.LocalConsume<T>(rule)
        where T : LexerRulable {

    override fun reset(context: LexerContext) {
        operator.reset(context)
        super.reset(context)
    }

    override fun isAcceptable(context: LexerContext): Boolean {
        return if (matchIndex == LexerMatch.NO_MATCH_INDEX) {
            isValid(context) && super.isAcceptable(context)
        } else {
            super.isAcceptable(context)
        }
    }

    override fun isSatisfied(context: LexerContext): Boolean {
        return (matchIndex != LexerMatch.NO_MATCH_INDEX || isValid(context)) && super.isSatisfied(context)
    }

    private fun isValid(context: LexerContext): Boolean {
        operator.reset(context)
        return operator.validate(context)
    }
}
package modool.lexer.rule.state

import modool.lexer.LexerMatch
import modool.lexer.rule.LexerRule
import modool.lexer.rule.state.operator.LexerOperator

class EvalEachValidateBeforeLexerRule<T>(
        operator: LexerOperator,
        rule: T
) : EvalEachValidateBeforeLexerRulable<T>(operator, rule),
        LexerRule
        where T : LexerRule,
              T : Iterable<LexerMatch>
package modool.lexer.rule.state

import modool.lexer.LexerContext
import modool.lexer.rule.LexerRulable
import modool.lexer.rule.WrappedLexerRulable
import modool.lexer.variable.LexerRuleVariable

/**
 * Allows the wrapped rule to be accessed by a variable name.
 * This is primarily done to support precedence i.e. yield functionality.
 */
abstract class AddRuleVariableLexerRulable<T : LexerRulable>(
    private val variableName: String,
    private val rule: T
) : WrappedLexerRulable.DelegatedConsume<T>(rule) {

    private var variable: LexerRuleVariable? = null

    override fun reset(context: LexerContext) {
        super.reset(context)
        if (variable == null) {
            variable = context.variables.getOrCreateRule(variableName)
            variable?.add(rule)
        }
    }
}
package modool.lexer.rule.state

import modool.lexer.rule.LexerRule
import modool.lexer.rule.state.operator.LexerOperator

class EvalValidateBeforeLexerRule(
        operator: LexerOperator,
        rule: LexerRule
) : EvalValidateBeforeLexerRulable(
        operator,
        rule
), LexerRule
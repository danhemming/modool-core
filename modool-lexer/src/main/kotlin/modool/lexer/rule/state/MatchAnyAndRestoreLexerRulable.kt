package modool.lexer.rule.state

import modool.core.content.token.TokenWriter
import modool.lexer.LexerContext
import modool.lexer.LexerMatch

abstract class MatchAnyAndRestoreLexerRulable(
        variableName: String
) : MatchAnyLexerRulable(variableName),
        Iterable<LexerMatch> {

    private var removedMatches = emptyList<LexerMatch>()

    override fun reset(context: LexerContext) {
        super.reset(context)
        removedMatches = emptyList()
    }

    override fun commit(context: LexerContext, content: TokenWriter) {
        removedMatches = variable(context).restore(matchValues.first())
    }

    override fun iterator(): Iterator<LexerMatch> {
        return removedMatches.iterator()
    }
}
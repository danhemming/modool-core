package modool.lexer.rule.state.operator.variable.match

import modool.lexer.LexerContext
import modool.lexer.rule.LexerRulable

class EqualsMatchLexerOperator(
    variableName: String,
    private val condition: LexerRulable
) : MatchVariableLexerOperator(variableName) {

    override fun validate(context: LexerContext): Boolean {
        return variable?.let { it.isPopulated && condition.isMatching(context, it) } ?: false
    }
}
package modool.lexer.rule.state.operator.variable.integer

import modool.lexer.*
import modool.lexer.rule.state.operator.LexerOperator
import modool.lexer.variable.LexerIntegerVariable

abstract class IntegerVariableLexerOperator(
        private val variableName: String
) : LexerOperator() {

    protected var variable: LexerIntegerVariable? = null

    override fun reset(context: LexerContext) {
        super.reset(context)
        if (variable == null) {
            variable = context.variables.getOrCreateInteger(variableName)
        }
    }
}
package modool.lexer.rule.state.operator

import modool.lexer.LexerContext
import modool.lexer.rule.LexerRulable

class PrecedesLexerOperator(
        private val condition: LexerRulable
) : LexerOperator() {

    override fun validate(context: LexerContext): Boolean {
        return condition.isMatchAfter(context)
    }
}
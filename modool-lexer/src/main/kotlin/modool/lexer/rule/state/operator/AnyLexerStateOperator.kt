package modool.lexer.rule.state.operator

import modool.core.content.token.TokenWriter
import modool.lexer.LexerContext

class AnyLexerStateOperator(
        private val operators: List<LexerOperator>
) : LexerOperator() {

    private var isMatched = false
    private val matches = Array(operators.size) { false }

    override fun reset(context: LexerContext) {
        operators.forEach { it.reset(context) }
        isMatched = false
        matches.fill(false)
    }

    override fun validate(context: LexerContext): Boolean {
        operators.forEachIndexed { operatorIndex, operator ->
            if (operator.validate(context)) {
                matches[operatorIndex] = true
                isMatched = true
            } else {
                matches[operatorIndex] = false
            }
        }
        return isMatched
    }

    override fun execute(context: LexerContext, content: TokenWriter, matchIndex: Int, matchLength: Int) {
        if (!isMatched) {
            return
        }
        operators.forEachIndexed { operatorIndex, operator ->
            if (matches[operatorIndex]) {
                operator.execute(context, content, matchIndex, matchLength)
            }
        }
    }
}
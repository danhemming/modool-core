package modool.lexer.rule.state.operator.variable

import modool.lexer.LexerContext
import modool.lexer.variable.LexerVariable
import modool.lexer.rule.state.operator.LexerOperator

abstract class VariableLexerOperator(
        private val variableName: String
) : LexerOperator() {

    protected var variable: LexerVariable? = null

    override fun reset(context: LexerContext) {
        super.reset(context)
        if (variable == null) {
            variable = context.variables.get(variableName)
        }
    }
}
package modool.lexer.rule.state.operator.variable.integer

import modool.core.content.token.TokenWriter
import modool.lexer.LexerContext
import modool.lexer.rule.state.operator.variable.integer.IntegerVariableLexerOperator

class SetIntegerVariableLexerOperator(
        variableName: String,
        private val value: Int
) : IntegerVariableLexerOperator(variableName) {

    override fun validate(context: LexerContext): Boolean {
        return true
    }

    override fun execute(context: LexerContext, content: TokenWriter, matchIndex: Int, matchLength: Int) {
        variable?.set(value)
    }
}
package modool.lexer.rule.state.operator.variable.match

import modool.core.content.token.Whitespace

class FollowsMatchThenUnicodeWhitespaceLexerOperator(
    variableName: String
) : FollowsMatchSimpleTestLexerOperator(variableName) {

    override fun isValid(value: Char): Boolean {
        return Whitespace.isUnicodeWhitespace(value)
    }
}
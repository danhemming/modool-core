package modool.lexer.rule.state

import modool.core.content.token.TokenWriter
import modool.lexer.LexerContext
import modool.lexer.LexerMatch
import modool.lexer.rule.LexerRulable
import modool.lexer.rule.state.operator.LexerOperator

/**
 * Mechanism to iterate over matches produced by another rule.
 */
abstract class EvalEachValidateAfterLexerRulable<T>(
        private val rule: T,
        operator: LexerOperator
) : ValidateOperatorAfterLexerRulable<T>(rule, operator)
        where T : LexerRulable,
              T : Iterable<LexerMatch> {

    override fun commit(context: LexerContext, content: TokenWriter) {
        super.commit(context, content)
        rule.forEach { match ->
            operator.execute(context, content, match.matchIndex, match.matchLength)
        }
    }
}
package modool.lexer.rule.state

import modool.lexer.LexerContext
import modool.lexer.rule.LexerRulable
import modool.lexer.rule.WrappedLexerRulable
import modool.lexer.rule.state.operator.LexerOperator

/**
 * Notes:
 * - Stops accepting content as soon as the operator matches (standard non greedy operation)
 */
abstract class ValidateOperatorAfterLexerRulable<T>(
    rule: T,
    protected val operator: LexerOperator
) : WrappedLexerRulable.LocalConsume<T>(rule)
        where T : LexerRulable {

    private var isSatisfied = false

    override fun reset(context: LexerContext) {
        isSatisfied = false
        operator.reset(context)
        super.reset(context)
    }

    override fun isAcceptable(context: LexerContext): Boolean {
        return !isSatisfied && super.isAcceptable(context)
    }

    override fun isContinuable(context: LexerContext): Boolean {
        return !isSatisfied && super.isContinuable(context)
    }

    override fun consume(context: LexerContext) {
        if (context.source.isContent) {
            moveNext(context)
            stash(context)
        }
    }

    override fun stash(context: LexerContext) {
        super.stash(context)
        if (!isSatisfied && super.isSatisfied(context)) {
            isSatisfied = operator.validate(context)
        }
    }

    override fun isSatisfied(context: LexerContext): Boolean {
        return isSatisfied
    }
}
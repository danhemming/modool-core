package modool.lexer.rule.state.operator.variable.bool

import modool.lexer.LexerContext

class AvailableBooleanVariableLexerOperator(
        variableName: String
) : BooleanVariableLexerOperator(variableName) {

    override fun validate(context: LexerContext): Boolean {
        return variable?.isPopulated == true
    }
}
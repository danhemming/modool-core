package modool.lexer.rule.state.operator.variable.match

import modool.lexer.LexerContext
import modool.lexer.LexerMatch

class IterateMatchesLexerOperator(
        variableName: String
) : MatchVariableLexerOperator(variableName),
        Iterable<LexerMatch> {

    override fun validate(context: LexerContext): Boolean {
        return variable?.isPopulated ?: false
    }

    override fun iterator(): Iterator<LexerMatch> {
        return variable?.iterator() ?: emptyList<LexerMatch>().iterator()
    }
}
package modool.lexer.rule.state.operator.variable.match

import modool.core.content.token.TokenWriter
import modool.lexer.LexerContext
import modool.lexer.LexerMatch
import modool.lexer.rule.LexerRulable

class RestoreMatchLexerOperator(
    variableName: String,
    private val condition: LexerRulable
) : MatchVariableLexerOperator(variableName),
    Iterable<LexerMatch> {

    private var match: LexerMatch? = null
    private var removedMatches = emptyList<LexerMatch>()

    override fun reset(context: LexerContext) {
        super.reset(context)
        match = null
        removedMatches = emptyList()
    }

    override fun validate(context: LexerContext): Boolean {
        variable?.let { variable ->
            if (!variable.isPopulated) {
                return false
            }
            variable.forEach {
                if (condition.isMatching(context, it)) {
                    match = it
                    return true
                }
            }
        }
        return false
    }

    override fun execute(context: LexerContext, content: TokenWriter, matchIndex: Int, matchLength: Int) {
        match?.let {
            removedMatches = variable?.restore(it) ?: emptyList()
        }
    }

    override fun iterator(): Iterator<LexerMatch> {
        return removedMatches.iterator()
    }
}
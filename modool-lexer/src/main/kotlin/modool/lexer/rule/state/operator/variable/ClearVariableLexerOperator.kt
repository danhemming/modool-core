package modool.lexer.rule.state.operator.variable

import modool.core.content.token.TokenWriter
import modool.lexer.LexerContext

/**
 * Always valid to clear a variable
 */
class ClearVariableLexerOperator(
        variableName: String
) : VariableLexerOperator(variableName) {

    override fun validate(context: LexerContext): Boolean {
        return true
    }

    override fun execute(context: LexerContext, content: TokenWriter, matchIndex: Int, matchLength: Int) {
        variable?.clear()
    }
}
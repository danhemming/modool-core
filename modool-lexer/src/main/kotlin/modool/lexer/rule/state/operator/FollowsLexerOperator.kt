package modool.lexer.rule.state.operator

import modool.lexer.LexerContext
import modool.lexer.rule.LexerRulable

class FollowsLexerOperator(
        private val condition: LexerRulable
) : LexerOperator() {

    override fun validate(context: LexerContext): Boolean {
        return condition.isMatchBefore(context)
    }
}
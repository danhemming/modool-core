package modool.lexer.rule.state

import modool.core.content.token.TokenWriter
import modool.lexer.LexerContext
import modool.lexer.LexerMatch
import modool.lexer.rule.LexerRulable
import modool.lexer.rule.state.operator.LexerOperator

/**
 * Mechanism to iterate over matches produced by another rule.
 * Note:
 * - Conditional i.e. the operator has to be valid before the rule can match
 */
abstract class EvalEachValidateBeforeLexerRulable<T>(
        operator: LexerOperator,
        private val rule: T
) : ValidateOperatorBeforeLexerRulable<T>(operator, rule)
        where T : LexerRulable,
              T : Iterable<LexerMatch> {

    override fun commit(context: LexerContext, content: TokenWriter) {
        super.commit(context, content)
        rule.forEach { match ->
            operator.execute(context, content, match.matchIndex, match.matchLength)
        }
    }
}
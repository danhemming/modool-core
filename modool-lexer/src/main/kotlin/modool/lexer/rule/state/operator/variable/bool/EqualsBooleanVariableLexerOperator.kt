package modool.lexer.rule.state.operator.variable.bool

import modool.lexer.LexerContext
import modool.lexer.rule.state.operator.variable.bool.BooleanVariableLexerOperator

class EqualsBooleanVariableLexerOperator(
        variableName: String,
        private val value: Boolean
) : BooleanVariableLexerOperator(variableName) {

    override fun validate(context: LexerContext): Boolean {
        return variable?.isEqual(value) ?: false
    }
}
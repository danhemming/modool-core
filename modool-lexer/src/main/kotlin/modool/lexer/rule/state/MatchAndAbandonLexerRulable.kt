package modool.lexer.rule.state

import modool.core.content.token.TokenWriter
import modool.lexer.LexerContext
import modool.lexer.variable.LexerMatchVariable

abstract class MatchAndAbandonLexerRulable(
        private val variableName: String
) : VariableLexerRulable() {

    private var variable: LexerMatchVariable? = null

    override fun variable(context: LexerContext): LexerMatchVariable {
        if (variable == null) {
            variable = context.variables.getOrCreateMatch(variableName)
        }
        return variable!!
    }

    override fun commit(context: LexerContext, content: TokenWriter) {
        variable(context).abandon()
    }
}
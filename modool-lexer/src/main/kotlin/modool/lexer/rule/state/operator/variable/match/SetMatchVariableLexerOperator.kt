package modool.lexer.rule.state.operator.variable.match

import modool.core.content.token.TokenWriter
import modool.lexer.LexerContext

class SetMatchVariableLexerOperator(
    variableName: String
) : MatchVariableLexerOperator(variableName) {

    override fun validate(context: LexerContext): Boolean {
        return true
    }

    override fun execute(context: LexerContext, content: TokenWriter, matchIndex: Int, matchLength: Int) {
        variable?.set(matchIndex, matchLength)
    }
}
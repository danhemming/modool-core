package modool.lexer.rule.state.operator.variable.match

import modool.lexer.LexerContext

abstract class FollowsMatchSimpleTestLexerOperator(
    variableName: String
) : MatchVariableLexerOperator(variableName) {

    override fun validate(context: LexerContext): Boolean {
        val variable = this.variable ?: return false

        if (!variable.isPopulated) {
            return false
        }

        val startIndex = variable.matchIndex + variable.matchLength
        val endIndex = context.source.currentIndex - 1

        if (endIndex <= startIndex) {
            return true
        }
        context.lookAround {
            context.source.moveTo(startIndex)
            do {
                if (!isValid(context.source.readChar() ?: return false)) {
                    return false
                }
            } while (context.source.currentIndex < endIndex && context.source.moveForward())
            return true
        }
    }

    abstract fun isValid(value: Char): Boolean
}
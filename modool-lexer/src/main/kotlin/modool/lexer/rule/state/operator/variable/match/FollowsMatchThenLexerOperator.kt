package modool.lexer.rule.state.operator.variable.match

import modool.lexer.LexerContext
import modool.lexer.rule.LexerRulable

class FollowsMatchThenLexerOperator(
    variableName: String,
    private val skipRule: LexerRulable
) : MatchVariableLexerOperator(variableName) {

    override fun validate(context: LexerContext): Boolean {
        return variable?.let {
            it.isPopulated
                    && skipRule.isMatchInRange(context, it.matchIndex + it.matchLength, context.source.currentIndex - 1)
        } ?: false
    }
}
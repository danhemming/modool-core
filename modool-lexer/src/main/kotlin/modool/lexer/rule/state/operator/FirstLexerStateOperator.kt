package modool.lexer.rule.state.operator

import modool.core.content.token.TokenWriter
import modool.lexer.LexerContext

class FirstLexerStateOperator(
        private val operators: List<LexerOperator>
) : LexerOperator() {

    private var matchingOperatorIndex = -1

    override fun reset(context: LexerContext) {
        operators.forEach { it.reset(context) }
        matchingOperatorIndex = -1
    }

    override fun validate(context: LexerContext): Boolean {
        operators.forEachIndexed { index, command ->
            if (command.validate(context)) {
                matchingOperatorIndex = index
                return true
            }
        }
        return false
    }

    override fun execute(context: LexerContext, content: TokenWriter, matchIndex: Int, matchLength: Int) {
        if (matchingOperatorIndex > -1) {
            operators[matchingOperatorIndex].execute(context, content, matchIndex, matchLength)
        }
    }
}
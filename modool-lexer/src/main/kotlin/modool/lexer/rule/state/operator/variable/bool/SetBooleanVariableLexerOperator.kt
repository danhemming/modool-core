package modool.lexer.rule.state.operator.variable.bool

import modool.core.content.token.TokenWriter
import modool.lexer.LexerContext
import modool.lexer.rule.state.operator.variable.bool.BooleanVariableLexerOperator

class SetBooleanVariableLexerOperator(
        variableName: String,
        private val value: Boolean
) : BooleanVariableLexerOperator(variableName) {

    override fun validate(context: LexerContext): Boolean {
        return true
    }

    override fun execute(context: LexerContext, content: TokenWriter, matchIndex: Int, matchLength: Int) {
        variable?.set(value)
    }
}
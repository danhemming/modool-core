package modool.lexer.rule.state.operator

import modool.core.content.token.TokenWriter
import modool.lexer.LexerContext

class AllLexerStateOperator(
        private val operators: List<LexerOperator>
) : LexerOperator() {

    override fun reset(context: LexerContext) {
        operators.forEach { it.reset(context) }
    }

    override fun validate(context: LexerContext): Boolean {
        operators.forEach {
            if (!it.validate(context)) {
                return false
            }
        }
        return true
    }

    override fun execute(context: LexerContext, content: TokenWriter, matchIndex: Int, matchLength: Int) {
        operators.forEach {
            it.execute(context, content, matchIndex, matchLength)
        }
    }
}
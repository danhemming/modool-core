package modool.lexer.rule.state.operator.variable.bool

import modool.lexer.variable.LexerBooleanVariable
import modool.lexer.LexerContext
import modool.lexer.rule.state.operator.LexerOperator

abstract class BooleanVariableLexerOperator(
        private val variableName: String
) : LexerOperator() {

    protected var variable: LexerBooleanVariable? = null

    override fun reset(context: LexerContext) {
        super.reset(context)
        if (variable == null) {
            variable = context.variables.getOrCreateBoolean(variableName)
        }
    }
}
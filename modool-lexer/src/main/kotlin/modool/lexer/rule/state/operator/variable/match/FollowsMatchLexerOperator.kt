package modool.lexer.rule.state.operator.variable.match

import modool.lexer.LexerContext

class FollowsMatchLexerOperator(
    variableName: String
) : MatchVariableLexerOperator(variableName) {

    override fun validate(context: LexerContext): Boolean {
        return variable?.let { it.matchIndex + it.matchLength == context.source.currentIndex } ?: false
    }
}
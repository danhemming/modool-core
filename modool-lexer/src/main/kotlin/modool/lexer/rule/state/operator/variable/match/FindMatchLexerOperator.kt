package modool.lexer.rule.state.operator.variable.match

import modool.lexer.LexerContext
import modool.lexer.rule.LexerRulable

class FindMatchLexerOperator(
    variableName: String,
    private val condition: LexerRulable
) : MatchVariableLexerOperator(variableName) {

    override fun validate(context: LexerContext): Boolean {
        return variable?.let { variable ->
            variable.isPopulated
                    && variable.find { condition.isMatching(context, it) } != null
        } ?: false
    }
}
package modool.lexer.rule.state.operator.variable.rule

import modool.lexer.LexerContext
import modool.lexer.rule.state.operator.LexerOperator
import modool.lexer.variable.LexerRuleVariable

abstract class RuleVariableLexerOperator(
    private val variableName: String
) : LexerOperator() {

    protected var variable: LexerRuleVariable? = null

    override fun reset(context: LexerContext) {
        super.reset(context)
        if (variable == null) {
            variable = context.variables.getOrCreateRule(variableName)
        }
    }
}
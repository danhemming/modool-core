package modool.lexer.rule.state

import modool.core.content.token.TokenWriter
import modool.lexer.LexerContext
import modool.lexer.rule.LexerRulable
import modool.lexer.rule.state.operator.LexerOperator

/**
 * Mechanism to allow (composable) conditional execution of a rule.
 * Operator provides both a guard mechanism (pre rule matching) and a state mutation mechanism (post rule matching).
 */
abstract class EvalValidateBeforeLexerRulable(
        operator: LexerOperator,
        rule: LexerRulable
) : ValidateOperatorBeforeLexerRulable<LexerRulable>(operator, rule) {

    override fun commit(context: LexerContext, content: TokenWriter) {
        super.commit(context, content)
        operator.execute(context, content, matchIndex, matchLength)
    }
}
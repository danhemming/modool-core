package modool.lexer.rule.state.operator

import modool.core.content.signifier.tag.Tag
import modool.core.content.token.TokenWriter
import modool.lexer.LexerContext

class InsertDeferredMarkerLexerOperator(
        private val tags: List<Tag>
) : LexerOperator() {

    override fun validate(context: LexerContext): Boolean {
        return true
    }

    override fun execute(context: LexerContext, content: TokenWriter, matchIndex: Int, matchLength: Int) {
        content.writeMarker(tags,  isDeferred = true)
    }
}
package modool.lexer.rule.state.operator.variable.integer

import modool.core.content.token.TokenWriter
import modool.lexer.LexerContext

class DecrementIntegerVariableLexerOperator(
        variableName: String,
        private val value: Int
) : IntegerVariableLexerOperator(variableName) {

    override fun validate(context: LexerContext): Boolean {
        return true
    }

    override fun execute(context: LexerContext, content: TokenWriter, matchIndex: Int, matchLength: Int) {
        variable?.decrement(value)
    }
}
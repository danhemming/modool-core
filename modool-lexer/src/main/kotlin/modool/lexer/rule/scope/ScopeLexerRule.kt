package modool.lexer.rule.scope

import modool.core.content.token.TokenWriter
import modool.lexer.*
import modool.lexer.rule.LexerRulable
import modool.lexer.rule.LexerRule
import modool.lexer.rule.unknown.UnknownLexerRule
import modool.lexer.strategy.LexerCommitStrategy
import modool.lexer.strategy.LexerRecoveryStrategy

/**
 * Allows for recursive processing by creating a new set of rules to process nested content.
 * Recursive processing should be the domain of the parser but unfortunately some languages are not complete i.e. those
 * that support free text.  The lexer/parser for these languages e.g. Markdown/Asciidoc is barely more than a bunch
 * of regular expressions.
 *
 * Notes:
 * Standard pattern for usage:
 *
 * Scope(
 *      Choice(
 *          Component(Whitespace, Open, Scope),
 *          Component(Whitespace, Open, Scope))
 *
 * Any scope that can be nested MUST have an unique opening sequence!! i.e.
 * Scope(
 *      Choice(
 *          Component(Whitespace, Open, Scope),
 *          Component(->Scope<-)) WRONG!
 *
 * - Inner scope without opening guard is PROBLEMATIC as it always matches!!
 * - Component must have an initial whitespace rule otherwise an Unknown rule will begin matching first
 */
open class ScopeLexerRule(
        private val unknownRule: UnknownLexerRule,
        private val tracker: LexerRuleTracker,
        recoveryStrategy: LexerRecoveryStrategy,
        commitStrategy: LexerCommitStrategy
) : TrackingLexerRuleProcessor(
        unknownRule,
        tracker,
        recoveryStrategy,
        commitStrategy
), LexerRule {

    private lateinit var intermediateContent: TokenWriter
    private lateinit var variables: LexerScopeState

    final override var matchIndex = LexerMatch.NO_MATCH_INDEX
        private set

    final override var matchLength = 0
        private set

    override fun reset(context: LexerContext) {
        super<TrackingLexerRuleProcessor>.reset(context)
        intermediateContent = context.tokens.createComponentWriter()
        variables = context.createScopeState()
        matchIndex = LexerMatch.NO_MATCH_INDEX
        matchLength = 0
    }

    override fun getCurrentTokenWriter(context: LexerContext): TokenWriter {
        return intermediateContent
    }

    override fun isStartable(context: LexerContext): Boolean {
        // We would prefer not to match unknown so left out of check
        return scoped(context) { tracker.isStartable(context) }
    }

    override fun isAcceptable(context: LexerContext): Boolean {
        return scoped(context) { tracker.isAcceptable(context) }
                || (unknownRule.isProducerOfFirstClassContent(context) && unknownRule.isAcceptable(context))
    }

    override fun isYielding(context: LexerContext): Boolean {
        return !rulesHaveMatched && tracker.isYielding(context)
    }

    override fun isMatchingContent(context: LexerContext): Boolean {
        // i.e. do we have anything to commit
        return !intermediateContent.isEmpty() || tracker.isMatchingContent(context)
    }

    override fun isSkipable(context: LexerContext): Boolean {
        return !rulesAreMatching
    }

    override fun moveNext(context: LexerContext) {
        matchLength ++
        if (matchLength == 1) {
            matchIndex = context.source.currentIndex
        }
        scoped(context) {
            super<TrackingLexerRuleProcessor>.moveNext(context)
        }
    }

    override fun isSatisfied(context: LexerContext): Boolean {
        return !rulesAreMatching
                || !intermediateContent.isEmpty()
                // This basically means is there something we can convert to unknown text
                || (rulesAreMatching && tracker.isCurrentMatching())
    }

    override fun complete(context: LexerContext) {
        val satisfiedRule = tracker.getFirstMatchingSatisfiedRuleOrNull(context)
        if (satisfiedRule == null) {
            unknownRule.commit(context, intermediateContent)
            unknownRule.reset(context)
        } else {
            commitUnknownContentPriorToRule(context)
            satisfiedRule.complete(context)
        }
    }

    override fun commit(context: LexerContext, content: TokenWriter) {
        content.copyFrom(intermediateContent)
        tracker.getFirstMatchingSatisfiedRuleOrNull(context)?.commit(context, content)
    }

    private inline fun <T> scoped(context: LexerContext, action: () -> T): T {
        context.applyScopeState(variables)
        try {
            return action()
        } finally {
            context.restoreParentScopeState(variables)
        }
    }
}
package modool.lexer.rule.scope

import modool.core.content.token.TokenWriter
import modool.lexer.LexerContext
import modool.lexer.LexerRuleTracker
import modool.lexer.rule.LexerRule
import modool.lexer.rule.unknown.UnknownLexerRule
import modool.lexer.strategy.LexerCommitStrategy
import modool.lexer.strategy.LexerRecoveryStrategy

/**
 * A scope that constantly scans for a terminator rule.
 * Control does not pass to a terminator rule immediately, instead a terminator must match entirely before the main
 * rules stop matching.
 */
class TerminatedScopeLexerRule(
        private val unknownRule: UnknownLexerRule,
        private val tracker: LexerRuleTracker,
        private val terminatorRule: LexerRule,
        recoveryStrategy: LexerRecoveryStrategy,
        commitStrategy: LexerCommitStrategy
) : ScopeLexerRule(
        unknownRule,
        tracker,
        recoveryStrategy,
        commitStrategy
), LexerRule {

    private lateinit var terminatedContent: TokenWriter
    private var isTerminated = false
    private var isSatisfied = false
    private var terminatorStartIndex = -1
    private var terminatorEndIndex = -1

    override fun reset(context: LexerContext) {
        super<ScopeLexerRule>.reset(context)
        terminatedContent = context.tokens.createValueComponentWriter()
        resetTerminator(context)
    }

    private fun resetTerminator(context: LexerContext) {
        isTerminated = false
        isSatisfied = false
        terminatorStartIndex = -1
        terminatorEndIndex = -1
        terminatorRule.reset(context)
        terminatedContent.clear()
    }

    override fun isStartable(context: LexerContext): Boolean {
        return terminatorRule.isStartable(context) || super<ScopeLexerRule>.isStartable(context)
    }

    override fun isAcceptable(context: LexerContext): Boolean {
        return !isTerminated && (terminatorRule.isAcceptable(context) || super<ScopeLexerRule>.isAcceptable(context))
    }

    override fun isMatchingContent(context: LexerContext): Boolean {
        // i.e. do we have anything to commit
        return terminatorRule.isMatchingContent(context) || super<ScopeLexerRule>.isMatchingContent(context)
    }

    override fun isSkipable(context: LexerContext): Boolean {
        return terminatorRule.isSkipable(context) && super<ScopeLexerRule>.isSkipable(context)
    }

    override fun isSatisfied(context: LexerContext): Boolean {
        return isSatisfied
    }

    override fun moveNext(context: LexerContext) {

        if (terminatorRule.isAcceptable(context)) {
            if (terminatorStartIndex == -1) {
                // Capture what we have up until now then we can use them later.
                terminatedContent.copyFrom(getCurrentTokenWriter(context))
                // Give any current rule an opportunity to complete (even if it may continue to capture)
                val firstSatisfiedRule = tracker.getFirstMatchingSatisfiedRuleOrNull(context)
                if (firstSatisfiedRule == null) {
                    unknownRule.commit(context, terminatedContent)
                } else {
                    firstSatisfiedRule.commit(context, terminatedContent)
                }
                terminatorRule.consume(context)
                isTerminated = terminatorRule.isSatisfied(context)
                isSatisfied = isTerminated
                terminatorStartIndex = context.source.currentIndex
                terminatorEndIndex = terminatorStartIndex
            } else if (context.source.currentIndex > terminatorEndIndex) {
                // This guard is here because the super.moveNext() may call moveNext() on completion of a rule and then
                // the same character would be processed twice
                terminatorRule.consume(context)
                isTerminated = terminatorRule.isSatisfied(context)
                isSatisfied = isTerminated
                terminatorEndIndex = context.source.currentIndex
            }
        } else if (!isTerminated && terminatorStartIndex > -1) {
            resetTerminator(context)
        }

        if (!isTerminated && !context.source.isMoreContentAvailable && terminatorRule.isSatisfied(context)) {
            isSatisfied = true
        }

        super<ScopeLexerRule>.moveNext(context)
    }

    override fun tryToCommitARule(context: LexerContext): Boolean {
        val isSuccessful = super.tryToCommitARule(context)
        // The terminator has started matching and is willing to yield (probably a space) because we have a new match.
        if (isSuccessful && terminatorStartIndex > -1 && terminatorRule.isYielding(context)) {
            resetTerminator(context)
        }
        return isSuccessful
    }

    override fun tryToRecoverAndCommitARule(context: LexerContext): Boolean {
        if (isTerminated
                || (terminatorRule.isMatchingContent(context) && !terminatorRule.isYielding(context))) {
            // Nested rules may have failed but we are matching the terminator so don't allow rewind
            return false
        }

        val isRecovered = super.tryToRecoverAndCommitARule(context)
        if (isRecovered && terminatorStartIndex > -1 && terminatorStartIndex >= context.source.currentIndex) {
            resetTerminator(context)
        }

        return isRecovered
    }

    override fun isYielding(context: LexerContext): Boolean {
        return (terminatorStartIndex == -1 || terminatorRule.isYielding(context)) &&
                super<ScopeLexerRule>.isYielding(context)
    }

    override fun complete(context: LexerContext) {
        if (isTerminated) {
            terminatorRule.complete(context)
        } else {
            super<ScopeLexerRule>.complete(context)
        }
    }

    override fun commit(context: LexerContext, content: TokenWriter) {
        if (isTerminated) {
            content.copyFrom(terminatedContent)
            terminatorRule.commit(context, content)
        } else {
            super<ScopeLexerRule>.commit(context, content)
        }
    }
}
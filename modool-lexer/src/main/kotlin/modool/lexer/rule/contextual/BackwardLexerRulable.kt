package modool.lexer.rule.contextual

import modool.lexer.LexerContext
import modool.lexer.LexerMatch
import modool.lexer.rule.ComponentLexerRule
import modool.lexer.rule.LexerRulable

/**
 * Matches a sequence of rules backwards from the current index.  This is designed as a companion rule to "precedes" to
 * allow for backward matching of composite rules.
 *
 * Notes:
 * - This does not match content and is only provided for look behind purposes
 */
abstract class BackwardLexerRulable(
        rules: List<LexerRulable>
) : LexerRulable {

    private val component = ComponentLexerRule(rules)

    override val matchIndex: Int get() = LexerMatch.NO_MATCH_INDEX
    override val matchLength: Int get() = 0
    override val matchCount: Int get() = 0

    override fun isMatchBefore(context: LexerContext): Boolean {
        context.lookAround {
            component.reset(context)
            if (!context.source.moveBack()) {
                return component.isSatisfied(context)
            } else if (component.isSatisfied(context)) {
                return true
            }
            do {
                if (component.isAcceptable(context)) {
                    component.consume(context)
                } else {
                    return false
                }
                if (component.isSatisfied(context)) {
                    return true
                }
            } while (context.source.moveBack())
            return false
        }
    }
}
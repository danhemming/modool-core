package modool.lexer.rule.contextual

import modool.lexer.rule.LexerRule

class BackwardLexerRule(
        rules: List<LexerRule>
): BackwardLexerRulable(rules),
        LexerRule
package modool.lexer.rule.debug

import modool.lexer.LexerContext
import modool.lexer.rule.LexerRule

class DebugLexerRule(
        identifier: String,
        target: LexerRule,
        filter: ((LexerContext, String) -> Boolean)? = null)
    : DebugLexerRulable<LexerRule>(identifier, target, filter),
        LexerRule


package modool.lexer.rule.debug

import modool.core.content.token.TokenWriter
import modool.lexer.LexerContext
import modool.lexer.rule.LexerRulable

abstract class DebugLexerRulable<T>(
        private val identifier: String,
        private val target: T,
        private val filter: ((LexerContext, String) -> Boolean)?)
    : LexerRulable
        where T : LexerRulable {

    override val matchIndex: Int get() = target.matchIndex
    override val matchLength: Int get() = target.matchLength
    override val matchCount: Int get() = target.matchCount

    override fun reset(context: LexerContext) {
        context.debug(identifier, "reset", filter) {
            target.reset(context)
        }
    }

    override fun isStartable(context: LexerContext): Boolean {
        return context.debug(identifier, "isStartable", filter) {
            target.isStartable(context)
        }
    }

    override fun isAcceptable(context: LexerContext): Boolean {
        return context.debug(identifier, "isAcceptable", filter) {
            target.isAcceptable(context)
        }
    }

    override fun isMatchingContent(context: LexerContext): Boolean {
        return context.debug(identifier, "isMatchingContent", filter) {
            target.isMatchingContent(context)
        }
    }

    override fun isContinuable(context: LexerContext): Boolean {
        return context.debug(identifier, "isContinuable", filter) {
            target.isContinuable(context)
        }
    }

    override fun isSkipable(context: LexerContext): Boolean {
        return context.debug(identifier, "isSkipable", filter) {
            target.isSkipable(context)
        }
    }

    override fun isSatisfied(context: LexerContext): Boolean {
        return context.debug(identifier, "isSatisfied", filter) {
            target.isSatisfied(context)
        }
    }

    override fun consume(context: LexerContext) {
        context.debug(identifier, "consume", filter) {
            target.consume(context)
        }
    }

    override fun moveNext(context: LexerContext) {
        context.debug(identifier, "moveNext", filter) {
            target.moveNext(context)
        }
    }

    override fun stash(context: LexerContext) {
        context.debug(identifier, "stash", filter) {
            target.stash(context)
        }
    }

    override fun complete(context: LexerContext) {
        context.debug(identifier, "complete", filter) {
            target.complete(context)
        }
    }

    override fun commit(context: LexerContext, content: TokenWriter) {
        context.debug(identifier, "commit", filter) {
            target.commit(context, content)
        }
    }
}
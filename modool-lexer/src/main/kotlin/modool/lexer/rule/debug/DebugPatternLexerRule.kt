package modool.lexer.rule.debug

import modool.lexer.LexerContext
import modool.lexer.rule.pattern.PatternLexerRuleComponent

class DebugPatternLexerRule(
        identifier: String,
        target: PatternLexerRuleComponent,
        filter: ((LexerContext, String) -> Boolean)? = null)
    : DebugLexerRulable<PatternLexerRuleComponent>(identifier, target, filter),
        PatternLexerRuleComponent
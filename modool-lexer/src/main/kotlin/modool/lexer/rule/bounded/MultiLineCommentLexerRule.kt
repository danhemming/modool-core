package modool.lexer.rule.bounded

import modool.lexer.LexerContext
import modool.core.content.token.TokenWriter
import modool.lexer.rule.constant.ConstantLexerRule
import modool.core.content.signifier.tag.terminal.Meta

class MultiLineCommentLexerRule(start: ConstantLexerRule, end: ConstantLexerRule)
    : CaptureFromToLexerRuleComponent(
        start,
        end,
        escape = null,
        tags = listOf(Meta.Comment)) {

    override fun commit(context: LexerContext, content: TokenWriter) {
        // TODO we should be able to start filtering based on the comment start token
        if (content.isAccepting) {
            content.writeComment {
                super.commit(context, content)
            }
        }
    }
}
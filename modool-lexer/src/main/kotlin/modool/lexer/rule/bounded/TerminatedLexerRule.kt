package modool.lexer.rule.bounded

import modool.core.content.token.TokenWriter
import modool.lexer.LexerContext
import modool.lexer.LexerMatch
import modool.lexer.rule.LexerRule

/**
 * A rule that constantly scans for a terminator rule.
 * Control does not pass to a terminator rule immediately, instead a terminator must match entirely before the main
 * rule stop matching.
 */
class TerminatedLexerRule(
        private val rule: LexerRule,
        private val terminatorRule: LexerRule
) : LexerRule {

    private lateinit var terminatedContent: TokenWriter
    private var isTerminating = false
    private var isTerminated = false
    private var isSatisfied = false

    override var matchIndex = LexerMatch.NO_MATCH_INDEX
        private set

    override var matchLength = 0
        private set

    override fun reset(context: LexerContext) {
        terminatedContent = context.tokens.createValueComponentWriter()
        rule.reset(context)
        resetTerminator(context)
    }

    private fun resetTerminator(context: LexerContext) {
        isTerminating = false
        isTerminated = false
        isSatisfied = false
        terminatorRule.reset(context)
        terminatedContent.clear()
    }

    override fun isStartable(context: LexerContext): Boolean {
        return terminatorRule.isStartable(context) || rule.isStartable(context)
    }

    override fun isAcceptable(context: LexerContext): Boolean {
        return !isTerminated && (terminatorRule.isAcceptable(context) || rule.isAcceptable(context))
    }

    override fun isMatchingContent(context: LexerContext): Boolean {
        // i.e. do we have anything to commit
        return terminatorRule.isMatchingContent(context) || rule.isMatchingContent(context)
    }

    override fun isSkipable(context: LexerContext): Boolean {
        return terminatorRule.isSkipable(context) && rule.isSkipable(context)
    }

    override fun isSatisfied(context: LexerContext): Boolean {
        return isSatisfied
    }

    override fun moveNext(context: LexerContext) {

        matchLength++
        if (matchLength == 1) {
            matchIndex = context.source.currentIndex
        }

        if (terminatorRule.isAcceptable(context)) {
            // Capture the content prior to the (potentially) matching terminator
            if (!isTerminating && rule.isSatisfied(context)) {
                rule.commit(context, terminatedContent)
                isTerminating = true
            }
            terminatorRule.moveNext(context)
        } else if (isTerminating && !isTerminated) {
            resetTerminator(context)
        }

        if (!isTerminated && !context.source.isMoreContentAvailable && terminatorRule.isSatisfied(context)) {
            isSatisfied = true
        }

        if (rule.isAcceptable(context)) {
            rule.moveNext(context)
        }
    }

    override fun stash(context: LexerContext) {
        if (isTerminating) {
            terminatorRule.stash(context)
            isTerminated = terminatorRule.isSatisfied(context)
            isSatisfied = isTerminated
        }
        if (rule.isAcceptable(context)) {
            rule.stash(context)
        }
    }

    override fun isYielding(context: LexerContext): Boolean {
        return (!isTerminating || terminatorRule.isYielding(context)) &&
                rule.isYielding(context)
    }

    override fun complete(context: LexerContext) {
        if (isTerminated) {
            terminatorRule.complete(context)
        } else {
            rule.complete(context)
        }
    }

    override fun commit(context: LexerContext, content: TokenWriter) {
        if (isTerminated) {
            content.copyFrom(terminatedContent)
            terminatorRule.commit(context, content)
        } else {
            rule.commit(context, content)
        }
    }
}
package modool.lexer.rule.bounded

import modool.core.content.token.TokenWriter
import modool.lexer.LexerContext
import modool.lexer.rule.LexerRule

/**
 * Adds a non-capturing closing guard to a rule, the rule can only match when the condition is matched in parallel at
 * the end of the match.
 * Notes:
 * 1. This works like the terminated rule BUT the closing content is not separate content.
 * 2. Like the terminated rule it is satisfied as soon as the condition is met.
 */
class EndsWithLexerRule(
        private val conditionRule: LexerRule,
        private val rule: LexerRule
) : LexerRule {

    override val matchIndex get() = rule.matchIndex
    override val matchLength get() = rule.matchLength

    private var isConditionSatisfied = false

    override fun reset(context: LexerContext) {
        conditionRule.reset(context)
        rule.reset(context)
        isConditionSatisfied = false
    }

    override fun isStartable(context: LexerContext): Boolean {
        return rule.isStartable(context)
    }

    override fun isAcceptable(context: LexerContext): Boolean {
        return !isConditionSatisfied && rule.isAcceptable(context)
    }

    override fun isMatchingContent(context: LexerContext): Boolean {
        return rule.isMatchingContent(context)
    }

    override fun isSkipable(context: LexerContext): Boolean {
        return rule.isSkipable(context)
    }

    override fun isSatisfied(context: LexerContext): Boolean {
        return (isConditionSatisfied || conditionRule.isSatisfied(context)) && rule.isSatisfied(context)
    }

    override fun moveNext(context: LexerContext) {
        if (conditionRule.isAcceptable(context)) {
            conditionRule.moveNext(context)
        } else {
            conditionRule.reset(context)
        }
        rule.moveNext(context)
    }

    override fun stash(context: LexerContext) {
        if (!isConditionSatisfied && conditionRule.isAcceptable(context)) {
            conditionRule.stash(context)
        }
        isConditionSatisfied = isConditionSatisfied || conditionRule.isSatisfied(context)
        rule.stash(context)
    }

    override fun isYielding(context: LexerContext): Boolean {
        return rule.isYielding(context)
    }

    override fun complete(context: LexerContext) {
        rule.complete(context)
    }

    override fun commit(context: LexerContext, content: TokenWriter) {
        rule.commit(context, content)
    }
}
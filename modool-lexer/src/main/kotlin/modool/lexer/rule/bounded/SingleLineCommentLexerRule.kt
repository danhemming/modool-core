package modool.lexer.rule.bounded

import modool.lexer.LexerContext
import modool.core.content.token.TokenWriter
import modool.lexer.rule.constant.ConstantLexerRule
import modool.core.content.signifier.tag.terminal.Meta
import modool.lexer.rule.whitespace.UnicodeLineTransitionLexerRule

class SingleLineCommentLexerRule(
        start: ConstantLexerRule
) : CaptureFromUntilLexerRuleComponent(
        start,
        UnicodeLineTransitionLexerRule(),
        escape = null,
        tags = listOf(Meta.Comment)) {

    private var isEoF: Boolean = false

    override fun reset(context: LexerContext) {
        super.reset(context)
        isEoF = false
    }

    override fun stash(context: LexerContext) {
        super.stash(context)
        isEoF = ! context.source.isMoreContentAvailable
    }

    override fun isSatisfied(context: LexerContext): Boolean {
        return isEoF || super.isSatisfied(context)
    }

    override fun commit(context: LexerContext, content: TokenWriter) {
        if (content.isAccepting) {
            content.writeComment {
                super.commit(context, content)
            }
        }
    }
}
package modool.lexer.rule.bounded

import modool.core.content.token.TokenWriter
import modool.lexer.LexerContext
import modool.lexer.rule.LexerRule

/**
 * Adds a non-capturing opening guard to a rule, the rule can only match when the condition is matched in parallel at
 * the start of the match.
 */
class StartsWithLexerRule(
        private val conditionRule: LexerRule,
        private val rule: LexerRule
) : LexerRule {

    private var isConditionSatisfied = false

    override val matchIndex get() = rule.matchIndex
    override val matchLength get() = rule.matchLength

    override fun reset(context: LexerContext) {
        conditionRule.reset(context)
        rule.reset(context)
        isConditionSatisfied = false
    }

    override fun isStartable(context: LexerContext): Boolean {
        return rule.isStartable(context) || conditionRule.isStartable(context)
    }

    override fun isAcceptable(context: LexerContext): Boolean {
        return (isConditionSatisfied || conditionRule.isSatisfied(context) || conditionRule.isAcceptable(context))
                && rule.isAcceptable(context)
    }

    override fun isMatchingContent(context: LexerContext): Boolean {
        return rule.isMatchingContent(context)
    }

    override fun isSkipable(context: LexerContext): Boolean {
        return rule.isSkipable(context)
    }

    override fun isSatisfied(context: LexerContext): Boolean {
        return (isConditionSatisfied || conditionRule.isSatisfied(context)) && rule.isSatisfied(context)
    }

    override fun moveNext(context: LexerContext) {
        if (!isConditionSatisfied && conditionRule.isAcceptable(context)) {
            conditionRule.moveNext(context)
        }
        rule.moveNext(context)
    }

    override fun stash(context: LexerContext) {
        if (!isConditionSatisfied && conditionRule.isAcceptable(context)) {
            conditionRule.stash(context)
        }
        isConditionSatisfied = isConditionSatisfied || conditionRule.isSatisfied(context)
        rule.stash(context)
    }

    override fun isYielding(context: LexerContext): Boolean {
        return rule.isYielding(context)
    }

    override fun complete(context: LexerContext) {
        rule.complete(context)
    }

    override fun commit(context: LexerContext, content: TokenWriter) {
        rule.commit(context, content)
    }
}
package modool.lexer.rule.bounded

import modool.core.content.signifier.tag.Tag
import modool.core.content.token.TokenWriter
import modool.lexer.LexerContext
import modool.lexer.LexerMatch
import modool.lexer.rule.LexerRulable
import modool.lexer.rule.LexerRule
import modool.lexer.rule.bounded.CaptureFromToLexerRuleComponent.State.*

/**
 * Useful for multi-line comments and other bounded content
 * Inclusive
 */
open class CaptureFromToLexerRuleComponent(
        private val start: LexerRulable,
        private val end: LexerRulable,
        private val escape: Char? = null,
        private val tags: List<Tag>) : LexerRule {

    enum class State {
        RESET,
        MATCHING_START,
        READY_FOR_CONTENT,
        MATCHING_CONTENT,
        MATCHING_ESCAPE,
        READY_TO_END_EMPTY,
        READY_TO_END_CONTENT,
        MATCHING_END,
        COMPLETE
    }

    private var state = RESET
    private var contentMatchIndex = LexerMatch.NO_MATCH_INDEX
    private var contentMatchLength = 0

    override val matchIndex get() = start.matchIndex
    override val matchLength get() = start.matchLength + contentMatchLength + end.matchLength

    override fun reset(context: LexerContext) {
        this.state = RESET
        contentMatchIndex = LexerMatch.NO_MATCH_INDEX
        contentMatchLength = 0
        start.reset(context)
        end.reset(context)
    }

    override fun isStartable(context: LexerContext): Boolean {
        return start.isStartable(context)
    }

    override fun isAcceptable(context: LexerContext): Boolean {
        return when (state) {
            RESET, MATCHING_START -> start.isAcceptable(context)
            MATCHING_END -> end.isAcceptable(context)
            COMPLETE -> false
            else -> true
        }
    }

    override fun isMatchingContent(context: LexerContext): Boolean {
        return state != RESET
    }

    override fun isSatisfied(context: LexerContext): Boolean {
        return state == COMPLETE
    }

    override fun moveNext(context: LexerContext) {
        state = when (state) {
            RESET -> {
                start.moveNext(context)
                if (start.isSatisfied(context)) {
                    READY_FOR_CONTENT
                } else {
                    MATCHING_START
                }
            }
            MATCHING_START -> {
                start.moveNext(context)
                if (start.isSatisfied(context)) {
                    start.complete(context)
                    if (end.isMatchAfter(context)) {
                        READY_TO_END_EMPTY
                    } else {
                        READY_FOR_CONTENT
                    }
                } else {
                    MATCHING_START
                }
            }
            READY_FOR_CONTENT,
            MATCHING_CONTENT -> when {
                isEscape(context) -> MATCHING_ESCAPE
                end.isMatchAfter(context) -> READY_TO_END_CONTENT
                else -> MATCHING_CONTENT
            }
            MATCHING_ESCAPE -> when {
                end.isMatchAfter(context) -> READY_TO_END_CONTENT
                else -> MATCHING_CONTENT
            }
            READY_TO_END_EMPTY,
            READY_TO_END_CONTENT,
            MATCHING_END -> {
                end.moveNext(context)
                if (end.isSatisfied(context)) {
                    end.complete(context)
                    COMPLETE
                } else {
                    MATCHING_END
                }
            }
            COMPLETE -> throw RuntimeException("Cannot progress past a complete state")
        }
    }

    private fun isEscape(context: LexerContext): Boolean {
        return escape != null && escape == context.source.readChar()
    }

    override fun stash(context: LexerContext) {
        when (state) {
            MATCHING_START -> start.stash(context)
            MATCHING_CONTENT,
            MATCHING_ESCAPE,
            READY_TO_END_CONTENT -> {
                contentMatchLength++
                if (contentMatchLength == 1) {
                    contentMatchIndex = context.source.currentIndex
                }
            }
            MATCHING_END -> end.stash(context)
            else -> return
        }
    }

    override fun commit(context: LexerContext, content: TokenWriter) {
        start.commit(context, content)
        if (content.isAccepting && contentMatchIndex != LexerMatch.NO_MATCH_INDEX) {
            content.writeTerminal(contentMatchIndex, contentMatchLength, tags)
        }
        end.commit(context, content)
    }
}
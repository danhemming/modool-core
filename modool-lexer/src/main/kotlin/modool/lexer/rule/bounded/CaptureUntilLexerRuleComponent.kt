package modool.lexer.rule.bounded

import modool.lexer.LexerContext
import modool.core.content.token.TokenWriter
import modool.lexer.rule.LexerRule
import modool.lexer.rule.bounded.CaptureUntilLexerRuleComponent.State.*
import modool.core.content.signifier.tag.Tag
import modool.lexer.LexerMatch

/**
 * Useful for single line comments, not inclusive i.e. does not attempt to capture the terminator
 */
class CaptureUntilLexerRuleComponent(
        private val terminators: Array<Char>,
        private val escape: Char? = null,
        private val tags: List<Tag>
) : LexerRule {

    enum class State {
        RESET,
        MATCHING_CONTENT,
        MATCHING_ESCAPE,
        COMPLETE
    }

    private var state = RESET

    override var matchIndex = LexerMatch.NO_MATCH_INDEX
        private set

    override var matchLength = 0
        private set

    override fun reset(context: LexerContext) {
        this.state = RESET
        matchIndex = LexerMatch.NO_MATCH_INDEX
        matchLength = 0
    }

    override fun isStartable(context: LexerContext): Boolean {
        return true
    }

    override fun isAcceptable(context: LexerContext): Boolean {
        return state != COMPLETE
    }

    override fun isMatchingContent(context: LexerContext): Boolean {
        return state != RESET
    }

    override fun isSatisfied(context: LexerContext): Boolean {
        return state == COMPLETE
    }

    override fun moveNext(context: LexerContext) {
        state = when (state) {
            RESET -> MATCHING_CONTENT
            MATCHING_ESCAPE -> MATCHING_CONTENT
            MATCHING_CONTENT -> when {
                isEscape(context) -> MATCHING_ESCAPE
                isTerminatorNext(context) -> COMPLETE
                else -> MATCHING_CONTENT
            }
            COMPLETE -> throw RuntimeException("Cannot progress past a complete state")
        }
    }

    private fun isEscape(context: LexerContext): Boolean {
        return escape != null && escape == context.source.readChar()
    }

    private fun isTerminatorNext(context: LexerContext): Boolean {
        val next = context.source.readCharAhead()
        return terminators.find { it == next } != null
    }

    override fun stash(context: LexerContext) {
        when (state) {
            MATCHING_ESCAPE,
            MATCHING_CONTENT,
            COMPLETE -> {
                matchLength++
                if (matchLength == 1) {
                    matchIndex = context.source.currentIndex
                }
            }
            else -> return
        }
    }

    override fun commit(context: LexerContext, content: TokenWriter) {
        content.writeTerminal(matchIndex, matchLength, tags)
    }
}
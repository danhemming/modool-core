package modool.lexer.rule.bounded

import modool.core.content.signifier.tag.Tag
import modool.core.content.token.TokenWriter
import modool.lexer.LexerContext
import modool.lexer.LexerMatch
import modool.lexer.rule.LexerRulable
import modool.lexer.rule.LexerRule
import modool.lexer.rule.bounded.CaptureFromUntilLexerRuleComponent.State.*

/**
 * Not inclusive i.e. does not attempt to capture the terminator
 */
open class CaptureFromUntilLexerRuleComponent(
        private val start: LexerRulable,
        private val terminator: LexerRulable,
        private val escape: Char? = null,
        private val tags: List<Tag>) : LexerRule {

    enum class State {
        RESET,
        MATCHING_START,
        READY_FOR_CONTENT,
        MATCHING_CONTENT,
        MATCHING_ESCAPE,
        COMPLETE_EMPTY,
        COMPLETE_CONTENT
    }

    private var state = RESET
    private var contentMatchIndex = LexerMatch.NO_MATCH_INDEX
    private var contentMatchLength = 0

    override val matchIndex get() = start.matchIndex
    override val matchLength get() = start.matchLength + contentMatchLength

    override fun reset(context: LexerContext) {
        this.state = RESET
        contentMatchIndex = LexerMatch.NO_MATCH_INDEX
        contentMatchLength = 0
        start.reset(context)
    }

    override fun isStartable(context: LexerContext): Boolean {
        return start.isStartable(context)
    }

    override fun isAcceptable(context: LexerContext): Boolean {
        return when(state) {
            RESET,
            MATCHING_START -> start.isAcceptable(context)
            COMPLETE_CONTENT -> false
            else -> true
        }
    }

    override fun isMatchingContent(context: LexerContext): Boolean {
        return state != RESET
    }

    override fun isSatisfied(context: LexerContext): Boolean {
        return state == COMPLETE_CONTENT
    }

    override fun moveNext(context: LexerContext) {
        state = when(state) {
            RESET -> {
                start.moveNext(context)
                if (start.isSatisfied(context)) {
                    start.complete(context)
                    if (terminator.isMatchAfter(context)) {
                        COMPLETE_EMPTY
                    } else {
                        READY_FOR_CONTENT
                    }
                } else {
                    MATCHING_START
                }
            }
            MATCHING_START -> {
                start.moveNext(context)
                if (start.isSatisfied(context)) {
                    start.complete(context)
                    if (terminator.isMatchAfter(context)) {
                        COMPLETE_EMPTY
                    } else {
                        READY_FOR_CONTENT
                    }
                } else {
                    MATCHING_START
                }
            }
            READY_FOR_CONTENT -> {
                if (terminator.isMatchAfter(context)) {
                    COMPLETE_EMPTY
                } else {
                    MATCHING_CONTENT
                }
            }
            MATCHING_CONTENT -> when {
                isEscape(context) -> MATCHING_ESCAPE
                terminator.isMatchAfter(context) -> COMPLETE_CONTENT
                else -> MATCHING_CONTENT
            }
            MATCHING_ESCAPE -> MATCHING_CONTENT
            COMPLETE_EMPTY,
            COMPLETE_CONTENT -> throw RuntimeException("Cannot progress past a complete state")
        }
    }

    private fun isEscape(context: LexerContext): Boolean {
        return escape != null && escape == context.source.readChar()
    }

    override fun stash(context: LexerContext) {
        when (state) {
            MATCHING_START -> start.stash(context)
            MATCHING_CONTENT,
            MATCHING_ESCAPE,
            COMPLETE_CONTENT -> {
                contentMatchLength++
                if (contentMatchLength == 1) {
                    contentMatchIndex = context.source.currentIndex
                }
            }
            else -> return
        }
    }

    override fun commit(context: LexerContext, content: TokenWriter) {
        start.commit(context, content)
        if (content.isAccepting && contentMatchIndex != LexerMatch.NO_MATCH_INDEX) {
            content.writeTerminal(contentMatchIndex, contentMatchLength, tags)
        }
    }
}
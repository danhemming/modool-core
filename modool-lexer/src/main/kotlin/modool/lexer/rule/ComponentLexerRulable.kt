package modool.lexer.rule

import modool.core.content.token.TokenWriter
import modool.lexer.CachingLexerRulable
import modool.lexer.LexerContext
import modool.lexer.LexerMatch

abstract class ComponentLexerRulable<T>
    : CachingLexerRulable()
        where T : LexerRulable {

    abstract val rules: List<T>
    private var current: T? = null
    private var next: T? = null
    private var nextIndex = -1

    override var matchIndex = LexerMatch.NO_MATCH_INDEX
        protected set

    override var matchLength = 0
        protected set

    final override fun reset(context: LexerContext) {
        super.reset(context)
        rules.forEach { it.reset(context) }
        current = if (rules.isEmpty()) null else rules[0]
        next = if (rules.size < 2) null else rules[1]
        nextIndex = 1
        matchIndex = LexerMatch.NO_MATCH_INDEX
        matchLength = 0
    }

    override fun isSkipable(context: LexerContext): Boolean {
        rules.forEach {
            if (!it.isSkipable(context)) {
                return false
            }
        }
        return true
    }

    override fun isStartable(context: LexerContext): Boolean {
        rules.forEach {
            if (it.isStartable(context)) {
                return true
            }
            // Will only work if the rule logic relies on context not state!
            if (!it.isSkipable(context)) {
                return false
            }
        }
        return false
    }

    override fun isAcceptable(context: LexerContext): Boolean {
        return isAcceptableWithCache(context) {
            (current?.isSatisfied(context) == true && isNextAcceptable(context))
                    || current?.isAcceptable(context) == true
        }
    }

    private fun isNextAcceptable(context: LexerContext): Boolean {
        var nextTest = next ?: return false
        var nextTestIndex = nextIndex

        if (nextTest.isAcceptable(context)) {
            return true
        }

        var startSkipIndex = -1
        var endSkipIndex = -1

        while (nextTest.isSkipable(context)) {
            nextTest = if (nextTestIndex < rules.size - 1) {
                rules[++nextTestIndex]
            } else {
                break
            }

            if (startSkipIndex == -1) {
                startSkipIndex = nextTestIndex
                endSkipIndex = nextTestIndex
            } else {
                endSkipIndex = nextTestIndex
            }
        }

        if (startSkipIndex > -1) {
            // The first non-skipable rule
            if (nextTest.isAcceptable(context)) {
                return true
            }

            for (skipIndex in startSkipIndex..endSkipIndex) {
                val skip = rules[skipIndex]
                if (skip.isAcceptable(context)) {
                    return true
                }
            }
        }

        return false
    }

    final override fun isContinuable(context: LexerContext): Boolean {
        return current?.isContinuable(context) == true || next != null
    }

    override fun isYielding(context: LexerContext): Boolean {
        // Check up to and including the current rule
        rules.forEach {
            if (!it.isYielding(context)) {
                return false
            }
            if (it == current) {
                return true
            }
        }
        return true
    }

    override fun isMatchingContent(context: LexerContext): Boolean {
        // Check up to and including the current rule
        rules.forEach {
            if (it.isMatchingContent(context)) {
                return true
            }
            if (it == current) {
                return false
            }
        }
        return false
    }

    override fun isSatisfied(context: LexerContext): Boolean {
        return isSatisfiedWithCache(context) {
            current?.isSatisfied(context) == true && isRemainingSatisfied(context)
        }
    }

    private fun isRemainingSatisfied(context: LexerContext): Boolean {
        var testIndex = nextIndex

        while (testIndex < rules.size) {
            val test = rules[testIndex]
            if (test.isSkipable(context) || test.isSatisfied(context)) {
                testIndex++
                continue
            } else {
                return false
            }
        }

        return true
    }

    final override fun moveNext(context: LexerContext) {
        matchLength++
        if (matchLength == 1) {
            matchIndex = context.source.currentIndex
        }

        if (current?.isSatisfied(context) == true) {
            attemptToProgressThroughRules(context)
        }

        // Progress
        current?.moveNext(context)
        resetSatisfiedCache()
    }

    private fun attemptToProgressThroughRules(context: LexerContext) {
        val oldNext = next
        val oldNextIndex = nextIndex
        var startSkipIndex = -1
        var endSkipIndex = -1
        // This whole process is basically a look ahead
        while (next?.isSkipable(context) == true) {
            if (startSkipIndex == -1) {
                startSkipIndex = nextIndex
                endSkipIndex = nextIndex
            } else {
                endSkipIndex = nextIndex
            }
            progressNextRule()
        }

        // We always prefer to move forward to the next rule over staying. So allow the next rule to consume context
        if (next?.isAcceptable(context) == true) {
            progressCurrentToNextRule(context)
        } else {
            next = oldNext
            nextIndex = oldNextIndex

            if (startSkipIndex > -1) {
                // After skipping we may have a next that;
                // 1. Is not set
                // 2. Is not acceptable
                // so we see if any (first) of the skip rules are acceptable
                for (skipIndex in startSkipIndex..endSkipIndex) {
                    val skip = rules[skipIndex]
                    if (skip.isAcceptable(context)) {
                        next = skip
                        nextIndex = skipIndex
                        progressCurrentToNextRule(context)
                        break
                    }
                }
            }
        }
    }

    private fun progressCurrentToNextRule(context: LexerContext) {
        current?.complete(context)
        current = next
        progressNextRule()
    }

    private fun progressNextRule() {
        next = if (nextIndex < rules.size - 1) {
            rules[++nextIndex]
        } else {
            null
        }
        resetSatisfiedCache()
    }

    final override fun stash(context: LexerContext) {
        current?.stash(context)
        resetSatisfiedCache()
    }

    override fun complete(context: LexerContext) {
        current?.complete(context)
    }

    override fun commit(context: LexerContext, content: TokenWriter) {
        rules.forEach { it.commit(context, content) }
    }
}
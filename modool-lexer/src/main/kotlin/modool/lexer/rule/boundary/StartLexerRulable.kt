package modool.lexer.rule.boundary

import modool.lexer.LexerContext
import modool.lexer.rule.LexerRulable

/**
 * Start of content check.
 * Notes:
 * - Never matches content
 * - Can be used as a guard in look around rules or a component/pattern
 */
abstract class StartLexerRulable
    : LexerRulable {

    override val matchIndex: Int get() = -1
    override val matchLength: Int get() = 0
    override val matchCount: Int get() = 0

    override fun isMatchBefore(context: LexerContext): Boolean {
        return context.source.currentIndex == 0
    }

    override fun isAcceptable(context: LexerContext): Boolean {
        return context.source.isAtStart
    }

    override fun isSatisfied(context: LexerContext): Boolean {
        return context.source.isAtStart
    }
}
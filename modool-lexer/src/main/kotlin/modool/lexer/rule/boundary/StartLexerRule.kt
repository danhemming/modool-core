package modool.lexer.rule.boundary

import modool.lexer.rule.LexerRule

object StartLexerRule
    : StartLexerRulable(),
        LexerRule
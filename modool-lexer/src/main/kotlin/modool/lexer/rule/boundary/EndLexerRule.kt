package modool.lexer.rule.boundary

import modool.lexer.rule.LexerRule

object EndLexerRule
    : EndLexerRulable(),
        LexerRule
package modool.lexer.rule.boundary

import modool.lexer.LexerContext
import modool.lexer.rule.LexerRulable

/**
 * End of content check.
 * Notes:
 * - Never matches content
 * - Can be used as a guard in look around rules or a component/pattern
 */
abstract class EndLexerRulable
    : LexerRulable {

    override val matchIndex: Int get() = -1
    override val matchLength: Int get() = 0
    override val matchCount: Int get() = 0

    override fun isAcceptable(context: LexerContext): Boolean {
        return context.source.isAtEnd
    }

    override fun isSatisfied(context: LexerContext): Boolean {
        return context.source.isAtEnd
    }

    override fun isMatchAfter(context: LexerContext): Boolean {
        return !context.source.isMoreContentAvailable
    }
}
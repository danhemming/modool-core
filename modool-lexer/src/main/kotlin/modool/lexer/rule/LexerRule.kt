package modool.lexer.rule

/**
 * A lexer rule is one that can appear in the root of a builder, i.e. a root rule, not a component
 */
interface LexerRule : LexerRulable
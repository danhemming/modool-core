package modool.lexer.rule

import modool.core.content.token.TokenWriter
import modool.lexer.LexerContext
import modool.lexer.rule.LexerRulable

sealed class WrappedLexerRulable<T : LexerRulable>(
    private val rule: T
) : LexerRulable {

    override val matchIndex: Int
        get() = rule.matchIndex

    override val matchLength: Int
        get() = rule.matchLength

    override val matchCount: Int
        get() = rule.matchCount

    override fun reset(context: LexerContext) {
        rule.reset(context)
        super.reset(context)
    }

    override fun isSkipable(context: LexerContext): Boolean {
        return rule.isSkipable(context)
    }

    override fun isStartable(context: LexerContext): Boolean {
        return rule.isStartable(context)
    }

    override fun isAcceptable(context: LexerContext): Boolean {
        return rule.isAcceptable(context)
    }

    override fun isContinuable(context: LexerContext): Boolean {
        return rule.isContinuable(context)
    }

    override fun moveNext(context: LexerContext) {
        rule.moveNext(context)
    }

    override fun isMatchingContent(context: LexerContext): Boolean {
        return rule.isMatchingContent(context)
    }

    override fun isYielding(context: LexerContext): Boolean {
        return rule.isYielding(context)
    }

    override fun isSatisfied(context: LexerContext): Boolean {
        return rule.isSatisfied(context)
    }

    override fun stash(context: LexerContext) {
        rule.stash(context)
    }

    override fun complete(context: LexerContext) {
        rule.complete(context)
    }

    override fun commit(context: LexerContext, content: TokenWriter) {
        rule.commit(context, content)
    }

    abstract class DelegatedConsume<T : LexerRulable>(private val rule: T) : WrappedLexerRulable<T>(rule) {

        override fun consume(context: LexerContext) {
            rule.consume(context)
        }
    }

    abstract class LocalConsume<T : LexerRulable>(rule: T) : WrappedLexerRulable<T>(rule)

}
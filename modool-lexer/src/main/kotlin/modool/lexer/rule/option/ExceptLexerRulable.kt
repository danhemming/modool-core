package modool.lexer.rule.option

import modool.lexer.CachingLexerRulable
import modool.lexer.LexerContext
import modool.lexer.LexerMatch
import modool.lexer.rule.LexerRulable

/**
 * Matches content except that identified in the passed rules
 * Notes:
 * - Implicitly includes start and end to list of exclusions as they are not content
 */
abstract class ExceptLexerRulable<T>(protected val rules: List<T>)
    : CachingLexerRulable()
        where T : LexerRulable {

    override var matchIndex = LexerMatch.NO_MATCH_INDEX
        protected set

    override var matchLength = 0
        protected set

    final override fun isMatchBefore(context: LexerContext): Boolean {
        return rules.firstOrNull { it.isMatchBefore(context) } == null
    }

    final override fun isMatchAfter(context: LexerContext): Boolean {
        return rules.firstOrNull { it.isMatchAfter(context) } == null
    }

    final override fun isMatch(context: LexerContext): Boolean {
        return rules.firstOrNull { it.isMatch(context) } == null
    }

    final override fun isStartable(context: LexerContext): Boolean {
        return rules.firstOrNull { it.isStartable(context) } == null
    }

    final override fun reset(context: LexerContext) {
        super.reset(context)
        matchIndex = LexerMatch.NO_MATCH_INDEX
        matchLength = 0
        rules.forEach { it.reset(context) }
    }

    final override fun isAcceptable(context: LexerContext): Boolean {
        return isAcceptableWithCache(context) {
            context.source.isContent && rules.firstOrNull { it.isMatch(context) } == null
        }
    }

    final override fun stash(context: LexerContext) {
        matchLength++
        if (matchLength == 1) {
            matchIndex = context.source.currentIndex
        }
        resetSatisfiedCache()
    }

    final override fun isMatchingContent(context: LexerContext): Boolean {
        return matchLength > 0
    }

    final override fun isYielding(context: LexerContext): Boolean {
        return false
    }

    final override fun isSatisfied(context: LexerContext): Boolean {
        return isSatisfiedWithCache(context) {
            isMatchingContent(context)
        }
    }
}
package modool.lexer.rule.option

import modool.lexer.rule.pattern.PatternLexerRuleComponent

class ExceptPatternLexerRuleComponent<T>(components: List<T>)
    : ExceptLexerRulable<T>(components),
        PatternLexerRuleComponent
        where T : PatternLexerRuleComponent
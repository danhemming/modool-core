package modool.lexer.rule.option

import modool.lexer.rule.LexerRule

class ChooseLexerRule(rules: List<LexerRule>)
    : ChooseLexerRulable<LexerRule>(rules), LexerRule
package modool.lexer.rule.option

import modool.core.content.token.TokenWriter
import modool.lexer.LexerContext
import modool.lexer.LexerMatch
import modool.lexer.rule.LexerRule

class ExceptLexerRule<T>(components: List<T>)
    : ExceptLexerRulable<T>(components)
        where T : LexerRule {

    override fun commit(context: LexerContext, content: TokenWriter) {
        if (matchIndex != LexerMatch.NO_MATCH_INDEX) {
            content.writeTerminal(matchIndex, matchLength, emptyList())
        }
    }
}
package modool.lexer.rule.option

import modool.core.content.token.TokenWriter
import modool.lexer.CachingLexerRulable
import modool.lexer.LexerContext
import modool.lexer.LexerMatch
import modool.lexer.rule.LexerRulable

abstract class ChooseLexerRulable<T>(
        protected val rules: List<T>
) : CachingLexerRulable()
        where T : LexerRulable {

    private val matchedRules = Array(rules.size) { true }
    private val satisfiedRules = Array(rules.size) { true }

    override var matchIndex = LexerMatch.NO_MATCH_INDEX
        protected set

    override var matchLength = 0
        protected set

    override var matchCount = 0
        protected set

    final override fun isMatchBefore(context: LexerContext): Boolean {
        return rules.firstOrNull { it.isMatchBefore(context) } != null
    }

    final override fun isMatch(context: LexerContext): Boolean {
        return rules.firstOrNull { it.isMatch(context) } != null
    }

    final override fun isMatchAfter(context: LexerContext): Boolean {
        return rules.firstOrNull { it.isMatchAfter(context) } != null
    }

    final override fun isStartable(context: LexerContext): Boolean {
        for (counter in 0 until rules.count()) {
            if (rules[counter].isStartable(context)) {
                return true
            }
        }
        return false
    }

    final override fun reset(context: LexerContext) {
        super.reset(context)
        matchedRules.fill(true)
        satisfiedRules.find { false }
        rules.forEach { it.reset(context) }
        matchIndex = LexerMatch.NO_MATCH_INDEX
        matchLength = 0
        matchCount = 0
    }

    final override fun isAcceptable(context: LexerContext): Boolean {
        return isAcceptableWithCache(context) {
            firstMatchingComponentOrNull { it.isAcceptable(context) } != null
        }
    }

    final override fun isContinuable(context: LexerContext): Boolean {
        return firstMatchingComponentOrNull { it.isContinuable(context) } != null
    }

    final override fun moveNext(context: LexerContext) {
        matchLength++
        if (matchLength == 1) {
            matchIndex = context.source.currentIndex
            matchCount = 1
        }

        filterMatchingAndProgress(context)
    }

    final override fun stash(context: LexerContext) {
        forEachMatchingComponent { it.stash(context) }
        resetSatisfiedCache()
    }

    final override fun isMatchingContent(context: LexerContext): Boolean {
        return firstMatchingComponentOrNull { it.isMatchingContent(context) } != null
    }

    final override fun isYielding(context: LexerContext): Boolean {
        return firstMatchingComponentOrNull { !it.isYielding(context) } == null
    }

    final override fun isSatisfied(context: LexerContext): Boolean {
        return isSatisfiedWithCache(context) {
            firstMatchingComponentOrNull { it.isSatisfied(context) } != null
        }
    }

    final override fun complete(context: LexerContext) {
        firstMatchingComponentOrNull { it.isSatisfied(context) }?.let {
            it.complete(context)
            matchCount = it.matchCount
        }
    }

    override fun commit(context: LexerContext, content: TokenWriter) {
        firstMatchingComponentOrNull { it.isSatisfied(context) }?.commit(context, content)
    }

    /**
     * To help address over matching scopes we remove those rules that are satisfied but not matching content - when we
     * have another rule that is satisfied.
     */
    private fun filterMatchingAndProgress(context: LexerContext) {
        satisfiedRules.fill(false)
        var satisfiedCount = 0
        var satisfiedUnyieldingCount = 0
        var matchCount = 0
        var unsatisfiedRulesRefuseToYield = false

        for (counter in 0 until rules.count()) {
            val component = rules[counter]
            if (matchedRules[counter]) {
                matchCount++
                if (component.isAcceptable(context)) {
                    component.moveNext(context)
                    if (component.isSatisfied(context)) {
                        satisfiedCount++
                        if (!component.isYielding(context)) {
                            satisfiedUnyieldingCount++
                            satisfiedRules[counter] = true
                        }
                    } else if (!component.isYielding(context)) {
                        unsatisfiedRulesRefuseToYield = true
                    }
                } else {
                    matchedRules[counter] = false
                }
            }
        }

        if (!unsatisfiedRulesRefuseToYield
                && satisfiedUnyieldingCount > 0
                && (matchCount > satisfiedCount || satisfiedUnyieldingCount < satisfiedCount)) {
            for (counter in 0 until rules.count()) {
                if (!satisfiedRules[counter]) {
                    matchedRules[counter] = false
                }
            }
        }
    }

    private inline fun forEachMatchingComponent(action: (T) -> Unit) {
        for (counter in 0 until rules.count()) {
            if (matchedRules[counter]) {
                action(rules[counter])
            }
        }
    }

    protected fun isNotMatching(): Boolean {
        for (counter in 0 until rules.count()) {
            if (matchedRules[counter]) {
                return false
            }
        }
        return true
    }

    private inline fun firstMatchingComponentOrNull(predicate: (T) -> Boolean): T? {
        for (counter in 0 until rules.count()) {
            if (matchedRules[counter] && predicate(rules[counter])) {
                return rules[counter]
            }
        }

        return null
    }
}
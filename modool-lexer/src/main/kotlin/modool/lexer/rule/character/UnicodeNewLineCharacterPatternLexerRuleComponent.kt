package modool.lexer.rule.character

import modool.core.content.token.Whitespace

class UnicodeNewLineCharacterPatternLexerRuleComponent
    : WhitespaceCharacterPatternLexerRuleComponent() {

    override fun isValueAcceptable(value: Char?): Boolean {
        return (value != null && Whitespace.isUnicodeEndOfLineIndicator(value))
    }
}
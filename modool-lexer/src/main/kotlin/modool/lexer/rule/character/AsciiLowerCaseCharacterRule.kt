package modool.lexer.rule.character

class AsciiLowerCaseLetterCharacterPatternLexerRuleComponent : CharacterPatternLexerRuleComponent() {

    override fun isValueAcceptable(value: Char?): Boolean {
        return value in 'a'..'z'
    }
}

package modool.lexer.rule.character

class AnyCharacterPatternLexerRuleComponent : CharacterPatternLexerRuleComponent() {

    override fun isValueAcceptable(value: Char?): Boolean {
        return true
    }
}
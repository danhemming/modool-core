package modool.lexer.rule.character

class AsciiUpperCaseLetterCharacterPatternLexerRuleComponent : CharacterPatternLexerRuleComponent() {

    override fun isValueAcceptable(value: Char?): Boolean {
        return value in 'A'..'Z'
    }
}
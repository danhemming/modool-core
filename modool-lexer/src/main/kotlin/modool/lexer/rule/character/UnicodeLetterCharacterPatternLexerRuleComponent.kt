package modool.lexer.rule.character

class UnicodeLetterCharacterPatternLexerRuleComponent : CharacterPatternLexerRuleComponent() {

    override fun isValueAcceptable(value: Char?): Boolean {
        return value != null && Character.isLetter(value)
    }
}
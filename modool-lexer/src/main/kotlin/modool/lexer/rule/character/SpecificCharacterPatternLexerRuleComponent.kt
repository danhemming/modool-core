package modool.lexer.rule.character

class SpecificCharacterPatternLexerRuleComponent(private val char: Char) : CharacterPatternLexerRuleComponent() {

    override fun isValueAcceptable(value: Char?): Boolean {
        return value == char
    }
}
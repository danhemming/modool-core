package modool.lexer.rule.character

import modool.core.content.token.Whitespace

class InlineWhitespaceCharacterPatternLexerRuleComponent(
    tabMatchCount: Int
) : WhitespaceTabbedCharacterPatternLexerRuleComponent(tabMatchCount) {

    override fun isValueAcceptable(value: Char?): Boolean {
        return (value != null && Whitespace.isInlineWhitespace(value))
    }
}


package modool.lexer.rule.character

import modool.core.content.token.Whitespace

class AsciiNewLineCharacterPatternLexerRuleComponent
    : WhitespaceCharacterPatternLexerRuleComponent(){

    override fun isValueAcceptable(value: Char?): Boolean {
        return value != null && Whitespace.isAsciiEndOfLineIndicator(value)
    }
}
package modool.lexer.rule.character

import modool.lexer.LexerContext
import modool.core.content.token.TokenWriter
import modool.lexer.rule.pattern.PatternLexerRuleComponent

/**
 * The purpose of this class is to not progress to the escapedRule if the escapeChar matches
 */
class EscapeCharacterPatternLexerRuleComponent(
        private val escapeChar: Char,
        private val escapedRule: CharacterPatternLexerRuleComponent)
    : PatternLexerRuleComponent {

    override val matchIndex get() = escapedRule.matchIndex
    override val matchLength get() = escapedRule.matchLength

    override fun reset(context: LexerContext) {
        escapedRule.reset(context)
    }

    override fun isStartable(context: LexerContext): Boolean {
        return isAcceptable(context)
    }

    override fun isAcceptable(context: LexerContext): Boolean {
        return context.source.readCharBehind() != escapeChar && escapedRule.isAcceptable(context)
    }

    override fun isContinuable(context: LexerContext): Boolean {
        return escapedRule.isContinuable(context)
    }

    override fun stash(context: LexerContext) {
        escapedRule.stash(context)
    }

    override fun moveNext(context: LexerContext) {
        escapedRule.moveNext(context)
    }

    override fun isMatchingContent(context: LexerContext): Boolean {
        return escapedRule.isMatchingContent(context)
    }

    override fun isSatisfied(context: LexerContext): Boolean {
        return escapedRule.isSatisfied(context)
    }

    override fun complete(context: LexerContext) {
        escapedRule.complete(context)
    }

    override fun commit(context: LexerContext, content: TokenWriter) {
        escapedRule.commit(context, content)
    }
}
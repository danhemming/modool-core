package modool.lexer.rule.character

class AsciiLetterCharacterPatternLexerRuleComponent : CharacterPatternLexerRuleComponent() {

    override fun isValueAcceptable(value: Char?): Boolean {
        return value in 'A'..'Z' || value in 'a'..'z'
    }
}
package modool.lexer.rule.character

import modool.core.content.token.Whitespace

class UnicodeWhitespaceCharacterPatternLexerRuleComponent(
    tabMatchCount: Int
) : WhitespaceTabbedCharacterPatternLexerRuleComponent(tabMatchCount) {

    override fun isValueAcceptable(value: Char?): Boolean {
        return value != null && Whitespace.isUnicodeWhitespace(value)
    }
}
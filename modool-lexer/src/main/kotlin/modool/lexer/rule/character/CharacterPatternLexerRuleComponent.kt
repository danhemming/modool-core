package modool.lexer.rule.character

import modool.lexer.LexerContext
import modool.lexer.LexerMatch
import modool.lexer.rule.pattern.PatternLexerRuleComponent

abstract class CharacterPatternLexerRuleComponent
    : PatternLexerRuleComponent {

    override var matchIndex = LexerMatch.NO_MATCH_INDEX
        protected set

    override val matchLength = 1

    final override fun isMatchBefore(context: LexerContext): Boolean {
        return isValueAcceptable(context.source.readCharBehind())
    }

    final override fun isMatch(context: LexerContext): Boolean {
        return context.source.isContent && isValueAcceptable(context.source.readChar())
    }

    final override fun isMatchAfter(context: LexerContext): Boolean {
        return isValueAcceptable(context.source.readCharAhead())
    }

    final override fun isMatchAt(context: LexerContext, index: Int): Boolean {
        return isValueAcceptable(context.source.readCharAt(index))
    }

    final override fun isStartable(context: LexerContext): Boolean {
        return isAcceptable(context)
    }

    override fun reset(context: LexerContext) {
        matchIndex = LexerMatch.NO_MATCH_INDEX
    }

    final override fun isAcceptable(context: LexerContext): Boolean {
        return context.source.isContent && isValueAcceptable(context.source.readChar())
    }

    protected abstract fun isValueAcceptable(value: Char?): Boolean

    override fun isMatchingContent(context: LexerContext): Boolean {
        return matchIndex != LexerMatch.NO_MATCH_INDEX
    }

    override fun isSatisfied(context: LexerContext): Boolean {
        return isMatchingContent(context)
    }

    final override fun stash(context: LexerContext) {
        matchIndex = context.source.currentIndex
    }
}
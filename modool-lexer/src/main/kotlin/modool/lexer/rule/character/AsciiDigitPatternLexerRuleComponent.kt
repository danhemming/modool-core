package modool.lexer.rule.character

class AsciiDigitPatternLexerRuleComponent : CharacterPatternLexerRuleComponent() {

    override fun isValueAcceptable(value: Char?): Boolean {
        return value in '0'..'9'
    }
}
package modool.lexer.rule.character

class AsciiLetterOrDigitCharacterPatternLexerRuleComponent : CharacterPatternLexerRuleComponent() {

    override fun isValueAcceptable(value: Char?): Boolean {
        return value in 'A'..'Z' ||
                value in 'a'..'z' ||
                value in '0'..'9' ||
                value == '_'
    }
}
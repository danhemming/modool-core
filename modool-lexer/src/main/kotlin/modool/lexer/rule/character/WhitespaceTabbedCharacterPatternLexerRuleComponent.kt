package modool.lexer.rule.character

import modool.core.content.token.WhitespaceCharacter
import modool.lexer.LexerContext

/**
 * Make a tab character appear as multiple matches in Repeat rule.
 * Useful when a tab is specified to be equal to X spaces.
 * This does not change the output tokens
 */
abstract class WhitespaceTabbedCharacterPatternLexerRuleComponent(
    private val tabMatchCount: Int
) : WhitespaceCharacterPatternLexerRuleComponent() {

    final override var matchCount: Int = 0
        private set

    override fun reset(context: LexerContext) {
        super.reset(context)
        matchCount = 0
    }

    override fun moveNext(context: LexerContext) {
        super.moveNext(context)
        // This component matches a single char so ...
        matchCount = if (context.source.readChar() == WhitespaceCharacter.TAB) tabMatchCount else 1
    }
}
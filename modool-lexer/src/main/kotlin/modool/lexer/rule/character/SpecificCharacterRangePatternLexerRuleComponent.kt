package modool.lexer.rule.character

class SpecificCharacterRangePatternLexerRuleComponent(
        private val startChar: Char,
        private val endChar: Char
) : CharacterPatternLexerRuleComponent() {

    override fun isValueAcceptable(value: Char?): Boolean {
        return value in startChar..endChar
    }
}
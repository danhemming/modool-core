package modool.lexer.rule.character

import modool.core.content.token.Whitespace
import modool.lexer.LexerContext

/**
 * Supports all the OS specific versions of line break;
 * - \n ...... unix, linux and later macs
 * - \r ...... early macs
 * - \r\n .... windows
 */
abstract class WhitespaceCharacterPatternLexerRuleComponent
    : CharacterPatternLexerRuleComponent() {

    private var isSatisfied = false

    final override var matchLength: Int = 0
        private set

    override fun reset(context: LexerContext) {
        super.reset(context)
        isSatisfied = false
        matchLength = 0
    }

    override fun moveNext(context: LexerContext) {
        isSatisfied = Whitespace.from(context.source.readChar()!!, context.source.readCharAhead()).isComplete
        matchLength ++
        super.moveNext(context)
    }

    final override fun isSatisfied(context: LexerContext): Boolean {
        return isSatisfied && super.isSatisfied(context)
    }
}
package modool.lexer.rule.selfish

import modool.lexer.LexerContext
import modool.lexer.rule.LexerRulable
import modool.lexer.rule.WrappedLexerRulable
import modool.lexer.variable.LexerRuleVariable

/**
 * Ensures that the rule(s) in the variable take precedence over the current (wrapped) rule.
 * Match will fail if the rules in the variable can be started.
 * Requires that the lexer is operating using a recovery strategy that expects unknown content
 * e.g. RestoreOverlappedLexerRecoveryStrategy
 */
abstract class YieldLexerRulable<T : LexerRulable>(
    private val variableName: String,
    rule: T
) : WrappedLexerRulable.DelegatedConsume<T>(rule) {

    private var variable: LexerRuleVariable? = null

    override fun reset(context: LexerContext) {
        super.reset(context)
        if (variable == null) {
            variable = context.variables.getOrCreateRule(variableName)
        }
    }

    override fun isAcceptable(context: LexerContext): Boolean {
        // This is the yield logic ... can't continue if one of our identified variable rules can match
        // variable may contain many rules so check the wrapped rule first
        return super.isAcceptable(context) && variable?.isStartable(context) != true
    }
}
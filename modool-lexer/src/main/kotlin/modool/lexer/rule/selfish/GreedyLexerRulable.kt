package modool.lexer.rule.selfish

import modool.lexer.LexerContext
import modool.lexer.rule.LexerRulable
import modool.lexer.rule.WrappedLexerRulable

/**
 * Lexer by default will pass control from one rule to the next in a component when that rule is satisfied and the next
 * is accepting content.  This rule allows the developer to specify that the child rule should not be considered
 * satisfied until there is no more content for it to match.
 *
 * This must be used carefully with suitable attention paid to subsequent rules that may never match.  Probably best used
 * for matching complex patterns.
 *
 * e.g. consider the string "foofoo!" and the pattern
 * component {
 *      +greedy(unlimited(constant("foo")))
 *      +optional(anyChar())
 * }
 * without greedy matching the "f" of the second foo matches optional(anyChar()) and control is passed to that rule.
 */
abstract class GreedyLexerRulable<T : LexerRulable>(
    rule: T
) : WrappedLexerRulable.DelegatedConsume<T>(rule) {

    private var isSatisfied = false

    override fun reset(context: LexerContext) {
        super.reset(context)
        isSatisfied = false
    }

    override fun isSkipable(context: LexerContext): Boolean {
        return super.isSkipable(context) && (!context.source.isMoreContentAvailable || !super.isAcceptable(context))
    }

    override fun consume(context: LexerContext) {
        return
    }

    override fun stash(context: LexerContext) {
        super.stash(context)
        if (!isSatisfied && super.isSatisfied(context)) {
            isSatisfied = !context.source.isMoreContentAvailable || context.lookAround {
                context.source.moveForward() && !super.isAcceptable(context)
            }
        }
    }

    override fun isYielding(context: LexerContext): Boolean {
        return super.isYielding(context) && !super.isMatchingContent(context)
    }

    override fun isSatisfied(context: LexerContext): Boolean {
        return isSatisfied
    }
}




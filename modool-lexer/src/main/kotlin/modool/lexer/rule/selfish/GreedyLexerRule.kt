package modool.lexer.rule.selfish

import modool.lexer.rule.LexerRule

class GreedyLexerRule(rule: LexerRule)
    : LexerRule,
        GreedyLexerRulable<LexerRule>(rule)
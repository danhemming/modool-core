package modool.lexer.rule.selfish

import modool.core.content.token.TokenWriter
import modool.lexer.LexerContext
import modool.lexer.rule.LexerRulable
import modool.lexer.rule.WrappedLexerRulable
import java.lang.Integer.max

/**
 * Overrides the normal propensity of the lexer to match one character at a time across all currently matching rules.
 * Instead we match the encapsulated rule up front in full. This has benefits in some contextual lexing where
 * the encapsulated rule starts with plain text.  In this case the lexer gets distracted on every word and has to
 * back track.
 *
 * Notes:
 * - This rule implements greedy matching logic (matching until no more content can be matched).
 */
abstract class EagerLexerRulable<T>(rule: T)
    : WrappedLexerRulable.DelegatedConsume<T>(rule)
        where T : LexerRulable {

    final override val matchIndex get() = lastAcceptableIndex
    final override val matchLength get() = max(lastMatchingContentIndex - lastAcceptableIndex + 1, 0)

    private lateinit var content: TokenWriter
    private var lastAcceptableIndex = -1
    private var lastUnacceptableIndex = -1
    private var lastSatisfiedIndex = -1
    private var lastMatchingContentIndex = -1

    override fun reset(context: LexerContext) {
        super.reset(context)
        content = context.tokens.createComponentWriter()
        lastUnacceptableIndex = -1
        lastAcceptableIndex = -1
        lastSatisfiedIndex = -1
        lastMatchingContentIndex = -1
    }

    override fun isAcceptable(context: LexerContext): Boolean {
        return when {
            lastUnacceptableIndex == context.source.currentIndex -> false

            context.source.currentIndex in lastAcceptableIndex..lastSatisfiedIndex -> true

            else -> context.lookAround { startIndex ->
                super.reset(context)
                var currentSatisfiedIndex = -1

                if (super.isSatisfied(context)) {
                    currentSatisfiedIndex = context.source.currentIndex
                }

                while (super.isAcceptable(context)) {
                    super.consume(context)
                    if (super.isSatisfied(context)) {
                        currentSatisfiedIndex = context.source.currentIndex
                    }
                    if (!context.source.moveForward()) {
                        break
                    }
                }

                if (currentSatisfiedIndex > -1) {
                    lastAcceptableIndex = startIndex
                    lastSatisfiedIndex = currentSatisfiedIndex
                    content.clear()
                    super.complete(context)
                    super.commit(context, content)
                    if (super.isMatchingContent(context)) {
                        lastMatchingContentIndex = currentSatisfiedIndex
                    } else {
                        lastAcceptableIndex = -1
                    }
                    return true
                }

                super.reset(context)
                lastUnacceptableIndex = startIndex
                false
            }
        }
    }

    override fun consume(context: LexerContext) {
        // we have already consumed as part of matching
        return
    }

    override fun  moveNext(context: LexerContext) {
        // we have already moved as part of matching
        return
    }

    override fun isContinuable(context: LexerContext): Boolean {
        return false
    }

    override fun stash(context: LexerContext) {
        // we have already stashed as part of matching
        return
    }

    override fun isMatchingContent(context: LexerContext): Boolean {
        return (lastMatchingContentIndex > -1 && context.source.currentIndex >= lastMatchingContentIndex)
    }

    override fun isYielding(context: LexerContext): Boolean {
        return false
    }

    override fun isSatisfied(context: LexerContext): Boolean {
        return (lastSatisfiedIndex > -1 && context.source.currentIndex >= lastSatisfiedIndex)
                || super.isSatisfied(context) // <- In case the rule is satisfied contextually before matching
    }

    override fun complete(context: LexerContext) {
        // we have already completed as part of matching
        return
    }

    override fun commit(context: LexerContext, content: TokenWriter) {
        content.copyFrom(this.content)
    }
}
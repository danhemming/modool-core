package modool.lexer.rule.selfish

import modool.lexer.rule.LexerRule

class YieldLexerRule(
    variableName: String,
    rule: LexerRule
) : YieldLexerRulable<LexerRule>(variableName, rule),
        LexerRule
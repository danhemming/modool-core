package modool.lexer.rule.selfish

import modool.lexer.rule.LexerRule

class EagerLexerRule(rule: LexerRule)
    : EagerLexerRulable<LexerRule>(rule),
        LexerRule
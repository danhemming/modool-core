package modool.lexer.rule

class ComponentLexerRule<T>(override val rules: List<T>)
    : ComponentLexerRulable<T>(),
        LexerRule
        where T : LexerRulable
package modool.lexer.rule.unknown

import modool.lexer.LexerContext

object RejectUnknownLexerRule: UnknownLexerRule() {

    override val matchIndex get() = -1
    override val matchLength get() = 0
    override val matchCount get() = 0

    override fun isStartable(context: LexerContext): Boolean {
        return false
    }

    override fun isAcceptable(context: LexerContext): Boolean {
        return false
    }

    override fun progressBookmark(context: LexerContext) {
        return
    }

    override fun rollbackToBookmark(context: LexerContext) {
        return
    }
}
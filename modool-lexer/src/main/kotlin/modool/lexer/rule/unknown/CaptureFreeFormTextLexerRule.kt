package modool.lexer.rule.unknown

import modool.core.content.signifier.tag.Tag
import modool.core.content.token.TokenWriter
import modool.core.content.token.WhitespaceContent
import modool.lexer.LexerContext
import modool.lexer.LexerMatch
import modool.core.content.token.TokenWriterRestorePoint
import modool.lexer.rule.whitespace.WhitespaceLexerRule

/**
 * Captures free form / unstructured text blocks
 * Emits bounding whitespace nodes and intermediate text content.  If the contiguous whitespace limits are exceeded in
 * the content then the content is split and further whitespace and content nodes are emitted.
 * Note:
 * This should NOT be used with a whitespace rule as it provides clashing functionality
 * Any non-bounding whitespace is output as content so long as the contiguous limits are not exceeded
 */
class CaptureFreeFormTextLexerRule(
        private val whitespaceRule: WhitespaceLexerRule,
        private val tags: List<Tag>,
        private val contiguousLineBreakLimit: Int = 1
) : UnknownLexerRule() {

    enum class State {
        RESET,
        MATCHING_OPENING_WHITESPACE,
        MATCHING_CONTENT,
        MATCHING_SUBSEQUENT_WHITESPACE
    }

    private var contentMatchIndex = LexerMatch.NO_MATCH_INDEX
    private var contentMatchLength = 0
    private var state = State.RESET
    private lateinit var content: TokenWriter
    private var openingMargin: Int = 0

    private var bookmarkIsSet = false
    private var bookmarkMatchIndex = LexerMatch.NO_MATCH_INDEX
    private var bookmarkMatchLength = 0
    private var bookmarkContentMatchIndex = LexerMatch.NO_MATCH_INDEX
    private var bookmarkContentMatchLength = 0
    private var bookmarkState = State.RESET
    private lateinit var bookmarkContent: TokenWriterRestorePoint
    private var bookmarkWhitespace: WhitespaceContent? = null

    override var matchIndex = LexerMatch.NO_MATCH_INDEX
        private set

    override var matchLength = 0
        private set

    override fun reset(context: LexerContext) {
        content = context.tokens.createComponentWriter()
        matchIndex = LexerMatch.NO_MATCH_INDEX
        matchLength = 0
        contentMatchIndex = LexerMatch.NO_MATCH_INDEX
        contentMatchLength = 0
        whitespaceRule.reset(context)
        openingMargin = 0
        state = State.RESET

        bookmarkIsSet = false
        bookmarkMatchIndex = LexerMatch.NO_MATCH_INDEX
        bookmarkContentMatchIndex = LexerMatch.NO_MATCH_INDEX
        bookmarkContentMatchLength = 0
        bookmarkContent = content.createRestorePoint()
        bookmarkState = State.RESET
        bookmarkContent.clear()
        bookmarkWhitespace = null
    }

    override fun isProducerOfFirstClassContent(context: LexerContext): Boolean {
        return true
    }

    private fun isSubsequentSpaceAndSeparable(): Boolean {
        return state == State.MATCHING_SUBSEQUENT_WHITESPACE &&
                (whitespaceRule.isLineCountExceeded(lineCount = contiguousLineBreakLimit)
                        || whitespaceRule.currentMargin > openingMargin)
    }

    override fun progressBookmark(context: LexerContext) {
        // Need to capture the state so it can be restored which is challenging
        bookmarkIsSet = true
        bookmarkState = state
        bookmarkMatchIndex = matchIndex
        bookmarkMatchLength = matchLength
        bookmarkContentMatchIndex = contentMatchIndex
        bookmarkContentMatchLength = contentMatchLength
        bookmarkWhitespace = whitespaceRule.createRestorePoint()
        content.updateRestorePoint(bookmarkContent)
    }

    override fun rollbackToBookmark(context: LexerContext) {
        if (bookmarkIsSet) {
            state = bookmarkState
            matchIndex = bookmarkMatchIndex
            matchLength = bookmarkMatchLength
            contentMatchIndex = bookmarkContentMatchIndex
            contentMatchLength = bookmarkContentMatchLength
            content.restore(bookmarkContent)
            whitespaceRule.restore(bookmarkWhitespace)
        } else {
            state = State.RESET
            matchIndex = LexerMatch.NO_MATCH_INDEX
            matchLength = 0
            contentMatchIndex = LexerMatch.NO_MATCH_INDEX
            contentMatchLength = 0
            content.clear()
            whitespaceRule.reset(context)
        }
    }

    override fun stash(context: LexerContext) {
        // If we have seen this content before then ignore it (probably due to recovery)
        if (matchIndex != LexerMatch.NO_MATCH_INDEX && context.source.currentIndex < matchIndex + matchLength) {
            return
        }

        state = if (whitespaceRule.isAcceptable(context)) {
            if (state == State.RESET || state == State.MATCHING_OPENING_WHITESPACE) {
                whitespaceRule.stash(context)
                State.MATCHING_OPENING_WHITESPACE
            } else {
                whitespaceRule.stash(context)
                State.MATCHING_SUBSEQUENT_WHITESPACE
            }
        } else when {
            state == State.MATCHING_OPENING_WHITESPACE -> {
                openingMargin = whitespaceRule.currentMargin
                whitespaceRule.commit(context, content)
                whitespaceRule.reset(context)
                progressContentMatch(context, 1)
                State.MATCHING_CONTENT
            }
            isSubsequentSpaceAndSeparable() -> {
                outputContentToken(context, content)
                whitespaceRule.commit(context, content)
                whitespaceRule.reset(context)
                contentMatchIndex = context.source.currentIndex
                contentMatchLength = 1
                State.MATCHING_CONTENT
            }
            state == State.MATCHING_SUBSEQUENT_WHITESPACE -> {
                progressContentMatch(context, whitespaceRule.getContentLength() + 1)
                whitespaceRule.reset(context)
                State.MATCHING_CONTENT
            }
            else -> {
                progressContentMatch(context, 1)
                State.MATCHING_CONTENT
            }
        }

        progressMatch(context)
    }

    private fun progressMatch(context: LexerContext) {
        if (matchLength == 0) {
            matchIndex = context.source.currentIndex
        }
        matchLength++
    }

    private fun progressContentMatch(context: LexerContext, count: Int) {
        if (contentMatchLength == 0) {
            contentMatchIndex = context.source.currentIndex
        }
        contentMatchLength += count
    }

    override fun isMatchingContent(context: LexerContext): Boolean {
        return state != State.RESET
    }

    override fun isSatisfied(context: LexerContext): Boolean {
        // We are greedy of whitespace, other whitespace rules need to back down.
        return state != State.RESET
    }

    override fun commit(context: LexerContext, content: TokenWriter) {
        if (content.isAccepting) {

            content.copyFrom(this.content)
            outputContentToken(context, content)

            if (whitespaceRule.isSatisfied(context)) {
                whitespaceRule.commit(context, content)
            }
        }
    }

    private fun outputContentToken(context: LexerContext, content: TokenWriter) {
        if (contentMatchIndex != LexerMatch.NO_MATCH_INDEX) {
            content.writeTerminal(contentMatchIndex, contentMatchLength, tags)
        }
    }
}
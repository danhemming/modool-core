package modool.lexer.rule.unknown

import modool.lexer.LexerContext

object IgnoreUnknownLexerRule: UnknownLexerRule() {

    override val matchIndex get() = -1
    override val matchLength get() = 0
    override val matchCount get() = 0

    override fun progressBookmark(context: LexerContext) {
        return
    }

    override fun rollbackToBookmark(context: LexerContext) {
        return
    }
}
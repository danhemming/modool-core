package modool.lexer.rule.unknown

import modool.lexer.LexerContext
import modool.lexer.rule.LexerRule

abstract class UnknownLexerRule : LexerRule {

    override fun isStartable(context: LexerContext): Boolean {
        return true
    }

    override fun isAcceptable(context: LexerContext): Boolean {
        return context.source.isContent
    }

    open fun isProducerOfFirstClassContent(context: LexerContext): Boolean {
        return false
    }

    abstract fun progressBookmark(context: LexerContext)

    abstract fun rollbackToBookmark(context: LexerContext)
}
package modool.lexer.rule.unknown

import modool.core.content.token.TokenWriter
import modool.lexer.LexerContext
import modool.lexer.LexerMatch

class CaptureUnknownTerminalLexerRule : UnknownLexerRule() {

    private var bookmarkMatchIndex = LexerMatch.NO_MATCH_INDEX
    private var bookmarkMatchLength = 0
    private var bookmarkMatchCount = 0

    override var matchIndex = LexerMatch.NO_MATCH_INDEX
        private set

    override var matchLength = 0
        private set

    override fun reset(context: LexerContext) {
        matchIndex = LexerMatch.NO_MATCH_INDEX
        matchLength = 0
        bookmarkMatchIndex = LexerMatch.NO_MATCH_INDEX
        bookmarkMatchLength = 0
        bookmarkMatchCount = 0
    }

    override fun isMatchingContent(context: LexerContext): Boolean {
        return matchIndex != LexerMatch.NO_MATCH_INDEX
    }

    override fun isSatisfied(context: LexerContext): Boolean {
        return isMatchingContent(context)
    }

    override fun progressBookmark(context: LexerContext) {
        bookmarkMatchIndex = matchIndex
        bookmarkMatchLength = matchLength
        bookmarkMatchCount = matchCount
    }

    override fun rollbackToBookmark(context: LexerContext) {
        if (bookmarkMatchIndex == LexerMatch.NO_MATCH_INDEX) {
            matchIndex = LexerMatch.NO_MATCH_INDEX
            matchLength = 0
        } else {
            matchIndex = bookmarkMatchIndex
            matchLength = bookmarkMatchLength
        }
    }

    override fun stash(context: LexerContext) {
        // If we have seen this content before then ignore it (probably due to recovery)
        if (matchIndex != LexerMatch.NO_MATCH_INDEX && context.source.currentIndex < matchIndex + matchLength) {
            return
        }

        matchLength++
        if (matchLength == 1) {
            matchIndex = context.source.currentIndex
        }
    }

    override fun commit(context: LexerContext, content: TokenWriter) {
        if (isMatchingContent(context) && content.isAccepting) {
            content.writeUnknownTerminal(matchIndex, matchLength)
        }
    }
}
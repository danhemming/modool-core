package modool.lexer.rule

import modool.lexer.LexerContext
import modool.lexer.LexerMatch
import modool.lexer.LexerStateManagement
import modool.lexer.LexerProcess

interface LexerRulable : LexerStateManagement, LexerProcess, LexerMatch {

    override fun consume(context: LexerContext) {
        if (context.source.isContent) {
            moveNext(context)
            stash(context)
        }
    }

    fun isFound(context: LexerContext, match: LexerMatch): Boolean {
        return isFoundInRange(context, match.matchIndex, match.matchIndex + match.matchLength)
    }

    fun isFoundInRange(context: LexerContext, startIndex: Int, endIndex: Int): Boolean {
        context.lookAround {
            reset(context)
            context.source.moveTo(startIndex)
            if (isSatisfied(context)) {
                return true
            }
            do {
                if (isAcceptable(context)) {
                    consume(context)
                    if (isSatisfied(context)) {
                        return true
                    }
                } else {
                    reset(context)
                }
            } while (context.source.currentIndex < endIndex && context.source.moveForward())
            return false
        }
    }

    fun isMatchBefore(context: LexerContext): Boolean {
        throw UnsupportedOperationException(
                "Unsupported operation: ${javaClass.simpleName} does not support look behind matching")
    }

    fun isMatch(context: LexerContext): Boolean {
        return isMatchAt(context, context.source.currentIndex)
    }

    fun isMatchAfter(context: LexerContext): Boolean {
        return isMatchAt(context, context.source.currentIndex + 1)
    }

    fun isMatchAt(context: LexerContext, index: Int): Boolean {
        context.lookAround {
            reset(context)
            context.source.moveTo(index)
            if (isSatisfied(context)) {
                return true
            }
            do {
                if (isAcceptable(context)) {
                    consume(context)
                } else {
                    return false
                }
                if (isSatisfied(context)) {
                    return true
                }
            } while (context.source.moveForward())
            return false
        }
    }

    fun isMatching(context: LexerContext, match: LexerMatch): Boolean {
        return isMatchInRange(context, match.matchIndex, match.matchIndex + match.matchLength)
    }

    fun isMatchInRange(context: LexerContext, startIndex: Int, endIndex: Int): Boolean {
        if (endIndex < startIndex) {
            // Will allow a no character match at start position
            context.lookAround {
                reset(context)
                context.source.moveTo(startIndex)
                return isSatisfied(context)
            }
        }
        context.lookAround {
            reset(context)
            context.source.moveTo(startIndex)
            do {
                if (isAcceptable(context)) {
                    consume(context)
                } else {
                    return false
                }
            } while (context.source.currentIndex < endIndex && context.source.moveForward())
            return context.source.currentIndex == endIndex && isSatisfied(context)
        }
    }
}


package modool.lexer.rule.repeat

import modool.core.content.token.TokenWriter
import modool.lexer.CachingLexerRulable
import modool.lexer.LexerContext
import modool.lexer.LexerMatch
import modool.lexer.rule.LexerRulable

@Suppress("UNCHECKED_CAST")
abstract class RepeaterLexerRulable<T>(
    protected val rule: T,
    private var minOccurs: Int,
    private var maxOccurs: Int

) : CachingLexerRulable()
        where T : LexerRulable {

    private var occurrenceCount = 0
    private var isCapturingContent = false

    final override var matchIndex = LexerMatch.NO_MATCH_INDEX
        private set

    final override var matchLength = 0
        private set

    override fun reset(context: LexerContext) {
        super.reset(context)
        matchLength = 0
        matchIndex = LexerMatch.NO_MATCH_INDEX
        occurrenceCount = 0
        isCapturingContent = false
        rule.reset(context)
    }

    override fun isStartable(context: LexerContext): Boolean {
        // Can ignore count constraints because we cannot violate them with out first character
        return rule.isStartable(context)
    }

    override fun isAcceptable(context: LexerContext): Boolean {
        return isAcceptableWithCache(context) {
            ((maxOccurs == -1 || occurrenceCount < maxOccurs) && rule.isAcceptable(context))
        }
    }

    override fun isContinuable(context: LexerContext): Boolean {
        return maxOccurs == -1 || occurrenceCount < maxOccurs
    }

    override fun isSkipable(context: LexerContext): Boolean {
        return minOccurs <= 0 && !isCapturingContent && !rule.isMatchingContent(context)
    }

    override fun moveNext(context: LexerContext) {
        matchLength++
        if (matchLength == 1) {
            matchIndex = context.source.currentIndex
        }
        rule.moveNext(context)
    }

    override fun stash(context: LexerContext) {
        rule.stash(context)
        isCapturingContent = true
        resetSatisfiedCache()

        // Only after we save the content can the rule decide if it is complete.
        if (rule.isSatisfied(context) && !rule.isContinuable(context)) {
            occurrenceCount += rule.matchCount
            resetAcceptableCache()
            afterOccurrence(context)
        }
    }

    protected open fun afterOccurrence(context: LexerContext) {
        isCapturingContent = false
        return
    }

    override fun isMatchingContent(context: LexerContext): Boolean {
        return occurrenceCount > 0 || rule.isMatchingContent(context)
    }

    override fun isSatisfied(context: LexerContext): Boolean {
        return isSatisfiedWithCache(context) {
            (minOccurs == -1 || occurrenceCount >= minOccurs) &&
                    (maxOccurs == -1 || occurrenceCount <= maxOccurs) &&
                    (!isCapturingContent || rule.isSatisfied(context))
        }
    }

    override fun complete(context: LexerContext) {
        rule.complete(context)
        if (isCapturingContent && rule.isSatisfied(context)) {
            occurrenceCount += rule.matchCount
            resetAcceptableCache()
            resetSatisfiedCache()
            afterOccurrence(context)
        }
    }

    override fun commit(context: LexerContext, content: TokenWriter) {
        rule.commit(context, content)
    }
}
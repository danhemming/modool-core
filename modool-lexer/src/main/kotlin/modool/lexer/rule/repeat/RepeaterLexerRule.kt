package modool.lexer.rule.repeat

import modool.lexer.LexerContext
import modool.core.content.token.TokenWriter
import modool.lexer.rule.LexerRule

class RepeaterLexerRule(
        rule: LexerRule,
        minOccurs: Int = -1,
        maxOccurs: Int = -1)
    : RepeaterLexerRulable<LexerRule>(rule, minOccurs, maxOccurs),
        LexerRule {

    private lateinit var contents: TokenWriter

    override fun reset(context: LexerContext) {
        super<RepeaterLexerRulable>.reset(context)
        contents = context.tokens.createComponentWriter()
    }

    override fun afterOccurrence(context: LexerContext) {
        super.afterOccurrence(context)
        rule.commit(context, contents)
        rule.reset(context)
    }

    override fun commit(context: LexerContext, content: TokenWriter) {
        super<RepeaterLexerRulable>.commit(context, content)
        content.copyFrom(contents)
    }
}
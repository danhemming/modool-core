package modool.lexer.rule.constant

import modool.core.content.token.TokenWriter
import modool.lexer.LexerContext
import modool.lexer.rule.LexerRule

class ConstantIndexLexerRule(
        index: ConstantIndex
) : ConstantIndexLexerRulable(index),
        LexerRule {

    override fun commit(context: LexerContext, content: TokenWriter) {
        if (content.isAccepting) {
            firstSatisfiedEntryOrNull()?.let { entry ->
                content.writeFactoryTerminal(matchIndex, matchLength, entry, emptyList())
            }
        }
    }
}
package modool.lexer.rule.constant

import modool.lexer.LexerContext
import modool.lexer.LexerMatch
import modool.lexer.rule.LexerRulable

abstract class ConstantLexerRulable(
    protected val value: String,
    private val isCaseSensitive: Boolean
) : LexerRulable {

    final override var matchIndex = LexerMatch.NO_MATCH_INDEX
        private set

    final override val matchLength = value.length

    private var currentMatchIndex = -1

    final override fun isMatchBefore(context: LexerContext): Boolean {
        return context.source.readStringBehind(value.length).equals(value, !isCaseSensitive)
    }

    final override fun isMatch(context: LexerContext): Boolean {
        return context.source.readString(value.length).equals(value, !isCaseSensitive)
    }

    final override fun isMatchAfter(context: LexerContext): Boolean {
        return context.source.readStringAhead(value.length).equals(value, !isCaseSensitive)
    }

    override fun isStartable(context: LexerContext): Boolean {
        return value.isNotEmpty() &&
                context.source.readChar()?.let { value[0].equals(it, !isCaseSensitive) } == true
    }

    override fun reset(context: LexerContext) {
        matchIndex = LexerMatch.NO_MATCH_INDEX
        currentMatchIndex = -1
    }

    override fun isAcceptable(context: LexerContext): Boolean {
        val testIndex = currentMatchIndex + 1
        if (testIndex >= matchLength) {
            return false
        }

        return value[testIndex] == context.source.readChar()
    }

    override fun moveNext(context: LexerContext) {
        currentMatchIndex++
        if (currentMatchIndex == 0) {
            matchIndex = context.source.currentIndex
        }
    }

    override fun isMatchingContent(context: LexerContext): Boolean {
        return currentMatchIndex > -1
    }

    override fun isSatisfied(context: LexerContext): Boolean {
        return currentMatchIndex == matchLength - 1
    }
}
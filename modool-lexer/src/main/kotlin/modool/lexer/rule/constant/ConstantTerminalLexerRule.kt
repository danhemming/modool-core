package modool.lexer.rule.constant

import modool.lexer.LexerContext
import modool.core.content.token.TokenWriter
import modool.core.content.signifier.tag.Tag
import modool.lexer.LexerMatch

class ConstantTerminalLexerRule(
        value: String,
        isCaseSensitive: Boolean,
        private val tags: List<Tag>
) : ConstantLexerRule(value, isCaseSensitive) {

    override fun commit(context: LexerContext, content: TokenWriter) {
        if (content.isAccepting && matchIndex != LexerMatch.NO_MATCH_INDEX) {
            content.writeTerminal(matchIndex, matchLength, tags)
        }
    }
}
package modool.lexer.rule.constant

import modool.core.content.signifier.tag.Tag
import modool.core.content.token.ConstantTerminalDescriptor
import modool.core.content.token.TokenWriter
import modool.lexer.LexerContext
import modool.lexer.LexerMatch

class ConstantTerminalFactoryLexerRule(
    private val constantTerminalDescriptor: ConstantTerminalDescriptor,
    private val adHocTags: List<Tag> = emptyList()
) : ConstantLexerRule(
    constantTerminalDescriptor.get(),
    constantTerminalDescriptor.isCaseSensitive
) {

    override fun commit(context: LexerContext, content: TokenWriter) {
        if (content.isAcceptable(constantTerminalDescriptor) && matchIndex != LexerMatch.NO_MATCH_INDEX) {
            content.writeFactoryTerminal(matchIndex, matchLength, constantTerminalDescriptor, adHocTags)
        }
    }
}
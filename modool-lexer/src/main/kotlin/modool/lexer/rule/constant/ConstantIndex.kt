package modool.lexer.rule.constant

import modool.core.content.token.ConstantTerminalDescriptor

/**
 * Where we have a lot of constant rules i.e. keywords and operators it is desirable to not have to poll each and every
 * rule.  Instead we can create this index that allows us to look up based on the first character of a symbol.  We
 * take advantage of the fact that nearly every constant symbol falls within the printable ascii range to create a sparse
 * array 95 items long.
 */
abstract class ConstantIndex {

    private val asciiLookup = Array<MutableList<ConstantTerminalDescriptor>?>(ASCII_ARRAY_MAX_INDEX + 1) { null }
    private val otherLookup = mutableListOf<ConstantTerminalDescriptor>()

    init {
        @Suppress("LeakingThis")
        initialiseEntries()
    }

    protected abstract fun initialiseEntries()

    protected fun add(entry: ConstantTerminalDescriptor) {
        if (entry.get().isEmpty()) {
            return
        }

        val firstCharIndex = entry.get().first().toAsciiIndex()
        if (firstCharIndex in 0..ASCII_ARRAY_MAX_INDEX) {
            getOrCreateListForCharIndex(firstCharIndex).add(entry)

            if (!entry.isCaseSensitive) {
                if (firstCharIndex in ARRAY_UPPERCASE_A..ARRAY_UPPERCASE_Z) {
                    // Uppercase ... also add to lowercase
                    getOrCreateListForCharIndex(firstCharIndex + 32).add(entry)
                } else if (firstCharIndex in ARRAY_LOWERCASE_A..ARRAY_LOWERCASE_Z) {
                    // Lowercase ... also add to uppercase
                    getOrCreateListForCharIndex(firstCharIndex - 32).add(entry)
                }
            }
        } else {
            otherLookup.add(entry)
        }
    }

    private fun getOrCreateListForCharIndex(charIndex: Int): MutableList<ConstantTerminalDescriptor> {
        return asciiLookup[charIndex] ?: run {
            val newList = mutableListOf<ConstantTerminalDescriptor>()
            asciiLookup[charIndex] = newList
            newList
        }
    }

    fun isEntryForFirstChar(first: Char): Boolean {
        val firstCharIndex = first.toAsciiIndex()
        return if (firstCharIndex in 0..ASCII_ARRAY_MAX_INDEX) {
            asciiLookup[firstCharIndex] != null
        } else {
            otherLookup.isNotEmpty() && otherLookup.find { it.get().startsWith(first) } != null
        }
    }

    fun listEntriesForFirstChar(first: Char): List<ConstantTerminalDescriptor> {
        val firstCharIndex = first.toAsciiIndex()
        return if (firstCharIndex in 0..ASCII_ARRAY_MAX_INDEX) {
            asciiLookup[firstCharIndex] ?: emptyList()
        } else {
            otherLookup.filter { it.get().startsWith(first) }
        }
    }

    private fun Char.toAsciiIndex(): Int {
        return toInt() - FIRST_USABLE_ASCII_CODE
    }

    companion object {
        private const val FIRST_USABLE_ASCII_CODE = 33
        private const val LAST_USABLE_ASCII_CODE = 126
        private const val ASCII_ARRAY_MAX_INDEX = LAST_USABLE_ASCII_CODE - FIRST_USABLE_ASCII_CODE

        private const val ARRAY_UPPERCASE_A = 65 - FIRST_USABLE_ASCII_CODE
        private const val ARRAY_UPPERCASE_Z = 90 - FIRST_USABLE_ASCII_CODE
        private const val ARRAY_LOWERCASE_A = 97 - FIRST_USABLE_ASCII_CODE
        private const val ARRAY_LOWERCASE_Z = 122 - FIRST_USABLE_ASCII_CODE
    }
}
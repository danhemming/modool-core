package modool.lexer.rule.constant

import modool.core.content.token.TokenWriter
import modool.lexer.LexerContext
import modool.lexer.rule.LexerRule

open class ConstantLexerRule(
    value: String,
    isCaseSensitive: Boolean
) : ConstantLexerRulable(value, isCaseSensitive),
        LexerRule {

    override fun commit(context: LexerContext, content: TokenWriter) {
        content.writeTerminal(matchIndex, matchLength, emptyList())
    }
}
package modool.lexer.rule.constant

import modool.lexer.rule.pattern.PatternLexerRuleComponent

class ConstantIndexPatternLexerRuleComponent(
        index: ConstantIndex
) : ConstantIndexLexerRulable(index),
        PatternLexerRuleComponent
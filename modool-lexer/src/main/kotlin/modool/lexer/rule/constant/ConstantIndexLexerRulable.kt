package modool.lexer.rule.constant

import modool.core.content.token.ConstantTerminalDescriptor
import modool.lexer.LexerContext
import modool.lexer.LexerMatch
import modool.lexer.rule.LexerRulable

abstract class ConstantIndexLexerRulable(
        private val index: ConstantIndex
) : LexerRulable {

    private var initialMatches: List<ConstantTerminalDescriptor>? = null
    private var currentMatches: Array<Boolean>? = null

    override var matchIndex = LexerMatch.NO_MATCH_INDEX
        protected set

    override var matchLength = 0
        protected set

    override fun reset(context: LexerContext) {
        matchIndex = LexerMatch.NO_MATCH_INDEX
        matchLength = 0
        initialMatches = null
        currentMatches = null
    }

    override fun isStartable(context: LexerContext): Boolean {
        return index.isEntryForFirstChar(context.source.readChar() ?: return false)
    }

    override fun isAcceptable(context: LexerContext): Boolean {
        val testChar = context.source.readChar() ?: return false

        if (initialMatches == null) {
            return index.isEntryForFirstChar(testChar)
        }

        return existsMatchingEntry { it.isValueEqualAt(matchLength, testChar) }
    }

    override fun moveNext(context: LexerContext) {

        matchLength++
        val testChar = context.source.readChar() ?: return

        if (matchLength == 1) {
            matchIndex = context.source.currentIndex
        }

        if (initialMatches == null) {
            initialMatches = index.listEntriesForFirstChar(testChar)
            currentMatches = Array(initialMatches!!.count()) { true }
        } else {
            forEachMatchingEntryWithIndex { entry, index ->
                if (!entry.isValueEqualAt(matchLength - 1, testChar)) {
                    currentMatches!![index] = false
                }
            }
        }
    }

    override fun isMatchingContent(context: LexerContext): Boolean {
        return matchLength > -1
    }

    override fun isSatisfied(context: LexerContext): Boolean {
        return firstSatisfiedEntryOrNull() != null
    }

    protected fun firstSatisfiedEntryOrNull(): ConstantTerminalDescriptor? {

        forEachMatchingEntryWithIndex { entry, _ ->
            if (matchLength == entry.get().length) {
                return entry
            }
        }

        return null
    }

    private inline fun existsMatchingEntry(predicate: (ConstantTerminalDescriptor) -> Boolean): Boolean {
        forEachMatchingEntryWithIndex { entry, _ ->
            if (predicate(entry)) {
                return true
            }
        }

        return false
    }

    private inline fun forEachMatchingEntryWithIndex(action: (ConstantTerminalDescriptor, Int) -> Unit) {
        if (initialMatches == null || currentMatches == null || matchLength == 0) {
            return
        }

        for (counter in 0 until currentMatches!!.count()) {
            if (currentMatches!![counter]) {
                action(initialMatches!![counter], counter)
            }
        }
    }
}
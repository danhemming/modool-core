package modool.lexer

import modool.lexer.variable.*

abstract class LexerStateContainer(
    open val parent: LexerStateContainer?,
    protected val source: SourceReader
) {

    private val variables = mutableMapOf<String, LexerVariable>()

    fun get(name: String): LexerVariable? {
        val nameParts = name.split('.')
        return when (nameParts.count()) {
            0 -> null
            1 -> localVariable(nameParts[0])
            else -> {
                var variable: LexerMatchVariable = (localVariable(nameParts[0]) as? LexerMatchVariable) ?: return null
                for (index in 1 until nameParts.count()) {
                    variable = (variable.localVariable(nameParts[index]) as? LexerMatchVariable) ?: return null
                }
                variable
            }
        }
    }

    fun getOrCreateBoolean(name: String): LexerBooleanVariable {
        return getOrCreateVariable(name.split('.'), LexerVariableType.BOOLEAN) as? LexerBooleanVariable
            ?: error("Variable $name exists but is not a boolean variable")
    }

    fun getOrCreateInteger(name: String): LexerIntegerVariable {
        return getOrCreateVariable(name.split('.'), LexerVariableType.INTEGER) as? LexerIntegerVariable
            ?: error("Variable $name exists but is not an integer variable")
    }

    fun getOrCreateMatch(name: String): LexerMatchVariable {
        return getOrCreateVariable(name.split('.'), LexerVariableType.MATCH) as? LexerMatchVariable
            ?: error("Variable $name exists but is not a match variable")
    }

    private fun getOrCreateVariable(nameParts: List<String>, type: LexerVariableType): LexerVariable {
        return when (nameParts.count()) {
            0 -> error("Empty variable name when retrieving variable")
            1 -> localVariableOrCreate(nameParts[0], type)
            else -> {
                var variable = localVariableOrCreate(nameParts[0], LexerVariableType.MATCH) as? LexerMatchVariable
                    ?: error("Variable ${nameParts[0]} is not a match variable")
                for (index in 1 until nameParts.count() - 1) {
                    variable = (variable.localVariableOrCreate(nameParts[index], LexerVariableType.MATCH) as? LexerMatchVariable)
                        ?: error("Variable ${nameParts[index]} is not a match variable")
                }
                variable.localVariableOrCreate(nameParts.last(), type)
            }
        }
    }

    internal fun localVariableOrCreate(name: String, type: LexerVariableType): LexerVariable {
        return localVariable(name) ?: type.create(this, source).apply { variables[name] = this }
    }

    internal fun localVariable(name: String): LexerVariable? {
        return variables[name]
    }

    fun variables(): Iterator<LexerVariable> {
        return variables.values.iterator()
    }
}
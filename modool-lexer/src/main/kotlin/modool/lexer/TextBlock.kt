package modool.lexer

interface TextBlock {
    fun capacity(): Int
    fun add(value: Char)
    operator fun get(index: Int): Char

    /**
     * Returns the substring of this string starting at the [startIndex] and ending right before the [endIndex].
     *
     * @param startIndex the start index (inclusive).
     * @param endIndex the end index (exclusive).
     */
    fun substring(startIndex: Int, endIndex: Int): String
}
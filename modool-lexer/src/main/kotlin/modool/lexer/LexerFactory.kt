package modool.lexer

import modool.lexer.rule.LexerRule
import modool.lexer.rule.unknown.CaptureUnknownTerminalLexerRule
import modool.lexer.rule.unknown.UnknownLexerRule
import modool.lexer.strategy.LexerCommitStrategy
import modool.lexer.strategy.LexerRecoveryStrategy

interface LexerFactory {
    fun createDocumentLexerRules(): List<LexerRule>
    fun createCommonLexerRules(): List<LexerRule>

    fun createUnknownLexerRule(): UnknownLexerRule {
        return CaptureUnknownTerminalLexerRule()
    }

    fun createRecoveryStrategy(): LexerRecoveryStrategy {
        return LexerRecoveryStrategy.Default
    }

    fun createCommitStrategy(): LexerCommitStrategy {
        return LexerCommitStrategy.Default
    }
}
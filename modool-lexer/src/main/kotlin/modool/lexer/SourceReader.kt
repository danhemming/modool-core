package modool.lexer

import java.io.InputStream
import java.io.InputStreamReader

class SourceReader(
        stream: InputStream,
        private val buffer: TextBlock) {

    private val reader = InputStreamReader(stream)
    private var lastReadIndex = UNINITIALISED_INDEX
    private var endIndex = UNINITIALISED_INDEX
    private var lastContentIndex = UNINITIALISED_INDEX

    var isConsumed = false
        private set

    val isAtStart: Boolean
        get() = currentIndex == START_INDEX

    val isAtEnd: Boolean
        get() = currentIndex == endIndex

    val isContent: Boolean
        get() = currentIndex != START_INDEX && currentIndex != endIndex

    val isMoreContentAvailable: Boolean
        get() = lastContentIndex == UNINITIALISED_INDEX || currentIndex < lastContentIndex

    var currentIndex: Int = UNINITIALISED_INDEX
        private set

    private var previousChar: Char? = null
    private var currentChar: Char? = null

    fun moveForward(): Boolean {
        return when {
            currentIndex == UNINITIALISED_INDEX -> {
                currentIndex = START_INDEX
                lastReadIndex = START_INDEX
                true
            }

            currentIndex == lastContentIndex -> {
                currentIndex = endIndex
                previousChar = currentChar
                currentChar = null
                true
            }

            currentIndex == endIndex -> false

            populateBufferAhead(1) -> {
                previousChar = currentChar
                currentChar = buffer[++currentIndex]
                true
            }

            else -> false
        }
    }

    fun moveBack(): Boolean {
        if (currentIndex <= START_INDEX) {
            return false
        }
        return moveTo(currentIndex - 1)
    }

    fun moveTo(index: Int): Boolean {
        if (index >= currentIndex && !populateBufferAhead(index - currentIndex)) {
            return if (index == endIndex && endIndex > START_INDEX) {
                currentIndex = index
                currentChar = null
                previousChar = if (lastContentIndex < 0) null else buffer[lastContentIndex]
                true
            } else {
                false
            }
        } else if (index == START_INDEX) {
            currentIndex = START_INDEX
            currentChar = null
            previousChar = null
            return true
        }

        currentIndex = index
        currentChar = buffer[currentIndex]
        previousChar = if (currentIndex == 0) null else buffer[currentIndex - 1]
        return true
    }

    /**
     * Ensure the buffer has the required content ahead read into it
     */
    private fun populateBufferAhead(size: Int): Boolean {
        if (endIndex > START_INDEX) {
            return currentIndex + size < endIndex
        }

        if (size > buffer.capacity()) {
            throw RuntimeException("Cannot increment the lexer buffer position in excess of it's capacity: [${buffer.capacity()}]")
        }

        val requiredNewContentCount = (size + 1) - (lastReadIndex - currentIndex)
        repeat(requiredNewContentCount) { index ->
            val content = reader.read()
            if (content == -1) {
                lastContentIndex = lastReadIndex
                endIndex = lastContentIndex + 1
                // We read one more than the size to know when EOF is, but only fail if we can't read requested size
                return index == requiredNewContentCount - 1
            } else {
                buffer.add(content.toChar())
                lastReadIndex++
                isConsumed = true
            }
        }

        return true
    }

    fun readCharBehind(): Char? {
        return previousChar
    }

    fun readChar(): Char? {
        return currentChar
    }

    fun readCharAhead(): Char? {
        return if (populateBufferAhead(1)) {
            buffer[currentIndex + 1]
        } else {
            null
        }
    }

    fun readCharAt(index: Int): Char? {
        if (index >= currentIndex && !populateBufferAhead(index - currentIndex)) {
            return null
        }
        return buffer[index]
    }

    fun readStringBehind(size: Int): String? {
        return if (size > currentIndex) {
            null
        } else {
            buffer.substring(currentIndex - size, currentIndex)
        }
    }

    fun readString(size: Int): String? {
        return if (populateBufferAhead(size - 1)) {
            buffer.substring(currentIndex, currentIndex + size)
        } else {
            null
        }
    }

    fun readStringAhead(size: Int): String? {
        return if (populateBufferAhead(size)) {
            buffer.substring(currentIndex + 1, currentIndex + 1 + size)
        } else {
            null
        }
    }

    fun readStringAt(index: Int, size: Int): String? {
        val requiredAhead = currentIndex - index + size
        return if (requiredAhead <= 0 || populateBufferAhead(requiredAhead)) {
            buffer.substring(index, size)
        } else {
            null
        }
    }

    companion object {
        private const val UNINITIALISED_INDEX = -2
        private const val START_INDEX = -1
    }
}
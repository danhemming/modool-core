package modool.lexer

interface LexerMatch {
    val matchIndex: Int
    val matchLength: Int
    val matchCount get() = if (matchIndex == NO_MATCH_INDEX) 0 else 1

    companion object {
        const val NO_MATCH_INDEX = Int.MIN_VALUE
    }
}
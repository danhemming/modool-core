package modool.lexer

import modool.lexer.rule.LexerRule

class ObservableLexerRuleTracker(
        private val rules: List<LexerRule>,
        private val observer: LexerRuleTrackerObserver
) : LexerRuleTracker {

    private val count = rules.size
    private val currentMatching = Array(count) { true }
    private val satisfiedWithContentComponents = Array(count) { true }
    private val nextMatching = Array(count) { true }
    private var currentMatchingCount = count
    private var nextMatchingCount = 0

    override fun reset(context: LexerContext) {
        currentMatching.fill(true)
        currentMatchingCount = count
        nextMatching.fill(true)
        nextMatchingCount = 0
        rules.forEach { it.reset(context) }
    }

    override fun isStartable(context: LexerContext): Boolean {
        for (counter in 0 until count) {
            if (rules[counter].isStartable(context)) {
                return true
            }
        }
        return false
    }

    override fun isAcceptable(context: LexerContext): Boolean {
        for (counter in 0 until count) {
            if (currentMatching[counter] && rules[counter].isAcceptable(context)) {
                return true
            }
        }

        var isSatisfiedRuleReadyToComplete = false
        for (counter in 0 until count) {
            if (currentMatching[counter] && rules[counter].isSatisfied(context)) {
                isSatisfiedRuleReadyToComplete = true
                break
            }
        }

        if (isSatisfiedRuleReadyToComplete) {
            for (counter in 0 until count) {
                if (rules[counter].isStartable(context)) {
                    return true
                }
            }
        }

        return false
    }

    override fun isSkipable(context: LexerContext): Boolean {
        for (counter in 0 until count) {
            if (currentMatching[counter]) {
                val rule = rules[counter]
                if (!rule.isSkipable(context)) {
                    return false
                }
            }
        }
        return true
    }

    override fun filterNextToAcceptable(context: LexerContext) {
        nextMatchingCount = currentMatchingCount

        for (counter in 0 until count) {
            if (currentMatching[counter]) {

                if (rules[counter].isAcceptable(context)) {
                    // Remember where we started so if necessary we can go back and skip e.g. if no match is found
                    observer.notifyAcceptable(counter, context.source.currentIndex)
                } else {
                    // If we have fully captured a rule, remember it because current ones could fail
                    if (rules[counter].isSatisfied(context)) {
                        observer.notifySatisfied(counter, context.source.currentIndex)
                    } else {
                        observer.notifyFailed(counter, context.source.currentIndex)
                    }

                    nextMatching[counter] = false
                    nextMatchingCount--
                }
            }
        }
    }

    override fun consume(context: LexerContext) {

        satisfiedWithContentComponents.fill(false)
        var satisfiedCount = 0
        var satisfiedUnyieldingCount = 0
        var unsatisfiedRulesRefuseToYield = false
        val isContent = context.source.isContent

        for (counter in 0 until count) {
            val isNextMatch = nextMatching[counter]
            currentMatching[counter] = isNextMatch

            if (isNextMatch) {
                val rule = rules[counter]
                if (isContent) {
                    rule.consume(context)
                }

                if (rule.isSatisfied(context)) {
                    satisfiedCount++
                    if (!rule.isYielding(context)) {
                        satisfiedUnyieldingCount++
                        satisfiedWithContentComponents[counter] = true
                    }
                } else if (!rule.isYielding(context)) {
                    unsatisfiedRulesRefuseToYield = true
                }
            }
        }

        if (!unsatisfiedRulesRefuseToYield
                && satisfiedUnyieldingCount > 0
                && (nextMatchingCount > satisfiedCount || satisfiedUnyieldingCount < satisfiedCount)) {
            for (counter in 0 until count) {
                if (!satisfiedWithContentComponents[counter] && nextMatching[counter]) {
                    nextMatching[counter] = false
                    currentMatching[counter] = false
                    nextMatchingCount--
                }
            }
        }

        currentMatchingCount = nextMatchingCount
    }

    override fun isSatisfied(context: LexerContext): Boolean {
        return count == 0 || getFirstMatchingSatisfiedRuleOrNull(context) != null
    }

    override fun isYielding(context: LexerContext): Boolean {
        for (counter in 0 until count) {
            if (currentMatching[counter] && !rules[counter].isYielding(context)) {
                return false
            }
        }
        return true
    }

    override fun isMatchingContent(context: LexerContext): Boolean {
        return getFirstMatchingSatisfiedRuleOrNull(context)?.isMatchingContent(context) == true
    }

    override fun getFirstMatchingSatisfiedRuleOrNull(context: LexerContext): LexerRule? {
        for (counter in 0 until count) {
            if (currentMatching[counter]) {
                val rule = rules[counter]
                if (rule.isSatisfied(context) && rule.isMatchingContent(context)) {
                    return rule
                }
            }
        }
        return null
    }

    override fun restoreMatch(ruleIndex: Int) {
        if (!currentMatching[ruleIndex]) {
            currentMatching[ruleIndex] = true
            if (!nextMatching[ruleIndex]) {
                nextMatching[ruleIndex] = true
                nextMatchingCount++
            }
        }
    }

    override fun isCurrentMatching(): Boolean {
        return currentMatchingCount > 0
    }

    override fun isNextMatching(): Boolean {
        return nextMatchingCount > 0
    }
}
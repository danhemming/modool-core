package modool.lexer

import modool.lexer.rule.LexerRule

interface LexerRuleTracker : LexerInitialization, LexerStateManagement {
    fun isCurrentMatching(): Boolean
    fun filterNextToAcceptable(context: LexerContext)
    fun isNextMatching(): Boolean
    fun getFirstMatchingSatisfiedRuleOrNull(context: LexerContext): LexerRule?
    fun restoreMatch(ruleIndex: Int)
}
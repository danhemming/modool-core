package modool.lexer

class CircularTextBlock(private val capacity: Int) : TextBlock {

    private val buffer = CharArray(capacity)
    private val lastPhysicalPos = capacity - 1
    private var lastVirtualPos = - 1
    private var virtualCount = 0
    private var pos = -1

    override fun add(value: Char) {
        if (pos == lastPhysicalPos) {
            pos = 0
        } else {
            pos ++
        }

        buffer[pos] = value
        lastVirtualPos ++
        virtualCount ++
    }

    override operator fun get(index: Int): Char {
        checkVirtualIndexInRange(index)
        return buffer[physicalIndex(index)]
    }

    /**
     * Returns the substring of this string starting at the [startIndex] and ending right before the [endIndex].
     *
     * @param startIndex the start index (inclusive).
     * @param endIndex the end index (exclusive).
     */
    override fun substring(startIndex: Int, endIndex: Int): String {
        val virtualEndIndexInclusive = endIndex - 1

        if (startIndex == virtualEndIndexInclusive) {
            return this[startIndex].toString()
        }

        checkVirtualIndexInRange(startIndex)
        checkVirtualIndexInRange(virtualEndIndexInclusive)

        if (startIndex < lastVirtualPos - capacity) {
            throw RuntimeException("substring can only come from the last $capacity characters")
        }

        val length = endIndex - startIndex
        if (length > capacity) {
            throw RuntimeException("length of substring cannot be greater than capacity of buffer")
        }

        val physicalStartIndex = physicalIndex(startIndex)
        val physicalEndIndex = physicalIndex(endIndex)
        val result = CharArray(length)

        if (physicalStartIndex < physicalEndIndex) {
            buffer.copyInto(result, startIndex = physicalStartIndex, endIndex = physicalEndIndex)
        } else {
            buffer.copyInto(result, startIndex = physicalStartIndex)
            buffer.copyInto(result, destinationOffset = capacity - physicalStartIndex , endIndex = physicalEndIndex)
        }

        return String(result)
    }

    private fun checkVirtualIndexInRange(index: Int) {
        if (index < 0 || index > lastVirtualPos) {
            throw IndexOutOfBoundsException(index)
        }
    }

    private fun physicalIndex(index: Int): Int {
        return index % capacity
    }

    fun count(): Int {
        return virtualCount
    }

    override fun capacity(): Int {
        return capacity
    }
}
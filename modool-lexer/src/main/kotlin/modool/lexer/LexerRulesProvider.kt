package modool.lexer

import modool.lexer.rule.LexerRule

interface LexerRulesProvider {
    fun createLexerRules(): List<LexerRule>
}
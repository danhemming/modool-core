package modool.lexer

import modool.core.content.signifier.tag.Tag
import modool.core.content.token.ConstantTerminalDescriptor
import modool.core.content.token.TokenReader
import modool.core.content.token.TokenReaderProviderView
import modool.core.content.token.TokenReference
import modool.lexer.rule.LexerRule
import modool.lexer.rule.builder.LexerRuleCollectionFactory
import modool.lexer.rule.unknown.UnknownLexerRule
import modool.lexer.strategy.LexerCommitStrategy
import modool.lexer.strategy.LexerRecoveryStrategy

/**
 * Tokenises text content using a best effort approach.  Each ContentToken output will have a typed text element as its
 * content.  This nodes text content will match the input text but may not be exactly the right element type e.g. A given
 * symbol may be used in multiple contexts and the lexer will pick the first one.  It is up to the parser to address this
 * as it operates with more contextual information.
 */
class Lexer(
        unknownRule: UnknownLexerRule,
        tracker: LexerRuleTracker,
        recoveryStrategy: LexerRecoveryStrategy,
        commitStrategy: LexerCommitStrategy

) : TrackingLexerRuleProcessor(unknownRule, tracker, recoveryStrategy, commitStrategy) {

    fun tokenise(context: LexerContext): TokenReader {
        val reader = context.tokenReaderProvider.createTokenReader()
        reset(context)

        return object : TokenReader {

            private var isEndWrittenToStream = false

            override fun reset() {
                reset(context)
                reader.reset()
            }

            override fun moveNext(): Boolean {
                return moveForward { reader.moveNext() }
            }

            override fun moveNextContent(): Boolean {
                return moveForward { reader.moveNextContent() }
            }

            private inline fun moveForward(action: () -> Boolean): Boolean {
                // Use up any tokens written by parser etc. first
                if (action()) {
                    return true
                }

                // Try to make some more tokens
                while (context.source.moveForward() || tidyUpStraggler(context)) {

                    // Consume one char in input
                    moveNext(context)

                    // If we made one then we can stop for now
                    if (action()) {
                        return true
                    }
                }

                // If the end is written already then the while loop above will have processed the tokens
                return if (isEndWrittenToStream || isEndOfContent()) {
                    false
                } else {
                    // We must have run out so lets finish up
                    commit(context, content = context.tokens)
                    context.tokens.writeEnd()
                    isEndWrittenToStream = true
                    action()
                }
            }

            private fun tidyUpStraggler(context: LexerContext): Boolean {
                return !tryToCommitARule(context) && tryToRecoverAndCommitARule(context)
            }

            override fun moveTo(reference: TokenReference) {
                reader.moveTo(reference)
            }

            override fun isStartOfContent(): Boolean {
                return reader.isStartOfContent()
            }

            override fun isEndOfContent(): Boolean {
                return reader.isEndOfContent()
            }

            override fun isTerminal(): Boolean {
                return reader.isTerminal()
            }

            override fun isTerminalEqual(value: String): Boolean {
                return reader.isTerminalEqual(value)
            }

            override fun isTerminalEqual(reference: TokenReference): Boolean {
                return reader.isTerminalEqual(reference)
            }

            override fun isTerminalTagged(value: String, tag: Tag): Boolean {
                return reader.isTerminalTagged(value, tag)
            }

            override fun isTerminalTagged(tag: Tag): Boolean {
                return reader.isTerminalTagged(tag)
            }

            override fun isTerminalSoftKeyword(): Boolean {
                return reader.isTerminalSoftKeyword()
            }

            override fun isTerminalMatchable(descriptor: ConstantTerminalDescriptor): Boolean {
                return reader.isTerminalMatchable(descriptor)
            }

            override fun isMarker(): Boolean {
                return reader.isMarker()
            }

            override fun isMarkerTagged(tag: Tag): Boolean {
                return reader.isMarkerTagged(tag)
            }

            override fun isMostRecentTerminalTagged(tag: Tag): Boolean {
                return reader.isMostRecentTerminalTagged(tag)
            }

            override fun isUnknown(): Boolean {
                return reader.isUnknown()
            }

            override fun isUnknown(value: String): Boolean {
                return reader.isUnknown(value)
            }

            override fun isWhiteSpace(): Boolean {
                return reader.isWhiteSpace()
            }

            override fun isWhitespaceTagged(tag: Tag): Boolean {
                return reader.isWhitespaceTagged(tag)
            }

            override fun isNewLine(): Boolean {
                return reader.isNewLine()
            }

            override fun isNewLine(count: Int): Boolean {
                return reader.isNewLine(count)
            }

            override fun isSpace(): Boolean {
                return reader.isSpace()
            }

            override fun isSpace(count: Int): Boolean {
                return reader.isSpace(count)
            }

            override fun isLeftMargin(): Boolean {
                return reader.isLeftMargin()
            }

            override fun isStartOfComment(): Boolean {
                return reader.isStartOfComment()
            }

            override fun isEndOfComment(): Boolean {
                return reader.isEndOfComment()
            }

            override fun getSpaceCount(): Int {
                return reader.getSpaceCount()
            }

            override fun getTabCount(): Int {
                return reader.getTabCount()
            }

            override fun createReference(): TokenReference {
                return reader.createReference()
            }

            override fun createPriorWhitespaceReference(): TokenReference {
                return reader.createPriorWhitespaceReference()
            }

            override fun <T : TokenReaderProviderView> createProviderView(viewType: Class<T>): T {
                return reader.createProviderView(viewType)
            }
        }
    }

    companion object {
        fun from(
                recoveryStrategy: LexerRecoveryStrategy,
                commitStrategy: LexerCommitStrategy,
                unknownRule: UnknownLexerRule,
                body: LexerRuleCollectionFactory.() -> Unit): Lexer {
            return from(
                    recoveryStrategy,
                    commitStrategy,
                    unknownRule,
                    LexerRuleCollectionFactory.createList(body))
        }

        fun from(
                recoveryStrategy: LexerRecoveryStrategy,
                commitStrategy: LexerCommitStrategy,
                unknownRule: UnknownLexerRule,
                rules: List<LexerRule>): Lexer {
            return Lexer(
                    unknownRule,
                    ObservableLexerRuleTracker(rules, recoveryStrategy),
                    recoveryStrategy,
                    commitStrategy)
        }

        fun from(
                unknownRule: UnknownLexerRule,
                body: LexerRuleCollectionFactory.() -> Unit): Lexer {
            return from(unknownRule, LexerRuleCollectionFactory.createList(body))
        }

        fun from(
                unknownRule: UnknownLexerRule,
                rules: List<LexerRule>): Lexer {
            return Lexer(
                    unknownRule,
                    ObservableLexerRuleTracker(rules, LexerRuleTrackerObserver.Default),
                    LexerRecoveryStrategy.Default,
                    LexerCommitStrategy.Default)
        }
    }
}
package modool.lexer

import modool.core.content.token.TokenWriter

/**
 * Represents a progression of rules
 */
interface LexerProcess : LexerInitialization {

    /**
     * Called to transition a sequence member on acceptance of a new char
     */
    fun moveNext(context: LexerContext) {
        return
    }

    /**
     * Called when a rule is considered complete and its controller moves onto the next rule in a sequence
     */
    fun complete(context: LexerContext) {
        return
    }

    fun commit(context: LexerContext, content: TokenWriter) {
        return
    }
}
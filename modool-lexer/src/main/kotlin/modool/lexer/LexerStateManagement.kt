package modool.lexer

interface LexerStateManagement {
    /**
     * A rule my allow itself to be skipped over, allowing a subsequent rule to match
     */
    fun isSkipable(context: LexerContext): Boolean {
        return false
    }

    // After moving to a new character ....

    /**
     * Regardless of current state, could this rule be started in the context
     */
    fun isStartable(context: LexerContext): Boolean {
        return false
    }

    fun isAcceptable(context: LexerContext): Boolean {
        return false
    }

    // Commit to new new character ...

    fun consume(context: LexerContext) {
        return
    }

    fun stash(context: LexerContext) {
        return
    }

    // Before moving away from a character ...

    /**
     * Is the rule satisfied (based on the current char) without the rule processing additional content
     * e.g. This will be true of complete rules and may be true of look around rules
     */
    fun isSatisfied(context: LexerContext): Boolean {
        return false
    }

    /**
     * The rule may be satisfied but also able to continue matching at the current position.
     */
    fun isContinuable(context: LexerContext): Boolean {
        return false
    }

    /**
     * Does this rule in it's current state give way to other rules that are satisfied. If so it may not be considered
     * for any further processing.
     * By default every rule yields if it has not matched content (even if it is satisfied)
     */
    fun isYielding(context: LexerContext): Boolean {
        return !isMatchingContent(context)
    }

    /**
     * Is the rule currently matching content.
     * Required to distinguish between a rule being satisfied with and without content.  Used to distinguish between
     * scopes e.g. choice (a scope is nearly always satisfied).
     */
    fun isMatchingContent(context: LexerContext): Boolean {
        return false
    }
}
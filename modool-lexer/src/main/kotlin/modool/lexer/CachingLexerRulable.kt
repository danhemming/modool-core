package modool.lexer

import modool.lexer.rule.LexerRulable

abstract class CachingLexerRulable : LexerRulable {
    protected var lastAcceptableIndex = LexerMatch.NO_MATCH_INDEX
    protected var lastAcceptableValue = false
    protected var lastSatisfiedIndex = LexerMatch.NO_MATCH_INDEX
    protected var lastSatisfiedValue = false

    override fun reset(context: LexerContext) {
        super.reset(context)
        resetAcceptableCache()
        resetSatisfiedCache()
    }

    protected fun resetAcceptableCache() {
        lastAcceptableIndex = LexerMatch.NO_MATCH_INDEX
        lastAcceptableValue = false
    }

    protected fun resetSatisfiedCache() {
        lastSatisfiedIndex = LexerMatch.NO_MATCH_INDEX
        lastSatisfiedValue = false
    }

    protected inline fun isAcceptableWithCache(context: LexerContext, valueProvider: () -> Boolean): Boolean {
        if (context.source.currentIndex != lastAcceptableIndex) {
            lastAcceptableValue = valueProvider()
            lastAcceptableIndex = context.source.currentIndex
        }
        return lastAcceptableValue
    }

    protected inline fun isSatisfiedWithCache(context: LexerContext, valueProvider: () -> Boolean): Boolean {
        if (context.source.currentIndex != lastSatisfiedIndex) {
            lastSatisfiedValue = valueProvider()
            lastSatisfiedIndex = context.source.currentIndex
        }
        return lastSatisfiedValue
    }
}
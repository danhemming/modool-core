package modool.lexer.variable

interface LexerVariable {
    val isPopulated: Boolean
    fun prepareNewFrame(): Boolean
    fun restorePreviousFrame(): Boolean
    fun clear()
}
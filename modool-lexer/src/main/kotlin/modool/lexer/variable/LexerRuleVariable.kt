package modool.lexer.variable

import modool.lexer.LexerContext
import modool.lexer.rule.LexerRulable

/**
 * Essentially a static variable that holds a list of lexer rules.  Unlike other variables there is no concept of a
 * frame.
 * When performing operations on contained rules we need to be careful not to mutate their state.  To this end only
 * certain compatible lexer rule methods are exposed.
 */
class LexerRuleVariable : LexerVariable {

    private val rules = mutableListOf<LexerRulable>()

    override val isPopulated: Boolean
        get() = rules.isNotEmpty()

    override fun prepareNewFrame(): Boolean {
        return true
    }

    override fun restorePreviousFrame(): Boolean {
        return true
    }

    fun add(rule: LexerRulable) {
        rules.add(rule)
    }

    fun isStartable(context: LexerContext): Boolean {
        rules.forEach {
            if (it.isStartable(context)) {
                return true
            }
        }
        return false
    }

    fun isAcceptable(context: LexerContext): Boolean {
        rules.forEach {
            if (it.isAcceptable(context)) {
                return true
            }
        }
        return false
    }

    fun isMatchingContent(context: LexerContext): Boolean {
        rules.forEach {
            if (it.isMatchingContent(context)) {
                return true
            }
        }
        return false
    }

    fun isSatisfied(context: LexerContext): Boolean {
        rules.forEach {
            if (it.isSatisfied(context)) {
                return true
            }
        }
        return false
    }

    override fun clear() {
        rules.clear()
    }
}
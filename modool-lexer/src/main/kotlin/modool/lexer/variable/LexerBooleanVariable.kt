package modool.lexer.variable

import modool.lexer.variable.LexerVariable
import java.util.*

class LexerBooleanVariable : LexerVariable {

    private var value: Boolean? = null
    private val values = ArrayDeque<Boolean?>(3)

    override val isPopulated: Boolean
        get() = value != null

    fun set(value: Boolean) {
        this.value = value
    }

    fun isEqual(value: Boolean): Boolean {
        return this.value == value
    }

    override fun prepareNewFrame(): Boolean {
        values.push(value)
        value = null
        return true
    }

    override fun restorePreviousFrame(): Boolean {
        return if (values.isEmpty()) {
            false
        } else {
            value = values.pop()
            true
        }
    }

    override fun clear() {
        this.value = null
    }
}
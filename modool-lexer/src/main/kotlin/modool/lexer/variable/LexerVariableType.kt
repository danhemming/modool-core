package modool.lexer.variable

import modool.lexer.*

enum class LexerVariableType {

    BOOLEAN {
        override fun create(parent: LexerStateContainer, source: SourceReader): LexerVariable {
            return LexerBooleanVariable()
        }
    },

    INTEGER {
        override fun create(parent: LexerStateContainer, source: SourceReader): LexerVariable {
            return LexerIntegerVariable()
        }
    },

    RULE {
        override fun create(parent: LexerStateContainer, source: SourceReader): LexerVariable {
            return LexerRuleVariable()
        }
    },

    MATCH {
        override fun create(parent: LexerStateContainer, source: SourceReader): LexerVariable {
            return LexerMatchVariable(parent, source)
        }
    };

    abstract fun create(parent: LexerStateContainer, source: SourceReader): LexerVariable
}
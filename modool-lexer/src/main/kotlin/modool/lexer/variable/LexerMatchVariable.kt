package modool.lexer.variable

import modool.lexer.LexerMatch
import modool.lexer.LexerStateContainer
import modool.lexer.SourceReader
import java.util.*

class LexerMatchVariable(
    parent: LexerStateContainer?,
    source: SourceReader
) : LexerStateContainer(parent, source),
    LexerVariable,
    LexerMatch,
    Iterable<LexerMatch> {

    private val entries = ArrayDeque<Entry>(3)

    override var matchIndex: Int = LexerMatch.NO_MATCH_INDEX
        private set

    override var matchLength: Int = 0
        private set

    override val isPopulated: Boolean
        get() = matchIndex != LexerMatch.NO_MATCH_INDEX

    /**
     * A new match entry is only created if the current match is dirty.
     * This approach allows us to set child state before setting the match.
     */
    fun add(index: Int, length: Int) {
        if (matchIndex != LexerMatch.NO_MATCH_INDEX) {
            prepareNewFrame()
        }
        set(index, length)
    }

    /**
     * Setting a match does not effect child state so caller should be careful to call clear when a match is
     * no longer required.
     */
    fun set(index: Int, length: Int) {
        matchIndex = index
        matchLength = length
        alignParent()
    }

    fun expand(includeIndex: Int, includeCount: Int) {
        if (matchIndex == LexerMatch.NO_MATCH_INDEX) {
            matchIndex = 0
        }
        matchLength = includeIndex - matchIndex + includeCount
        alignParent()
    }

    private fun alignParent() {
        (parent as? LexerMatchVariable)?.expand(matchIndex, matchLength)
    }

    fun abandon(): Boolean {
        return if (entries.isEmpty()) {
            isPopulated.apply { clear() }
        } else {
            restorePreviousFrame()
        }
    }

    fun restore(state: LexerMatch): List<LexerMatch> {
        if (!entries.contains(state)) {
            return emptyList()
        }
        val removedMatches = mutableListOf<LexerMatch>()
        while (entries.peek() != state) {
            removedMatches.add(entries.pop())
            variables().forEach { it.restorePreviousFrame() }
        }
        matchIndex = state.matchIndex
        matchLength = state.matchLength
        return removedMatches
    }

    fun count(): Int {
        return entries.count()
    }

    override fun iterator(): Iterator<LexerMatch> = iterator {
        if (isPopulated) {
            yield(this@LexerMatchVariable)
        }
        entries.forEach { yield(it) }
    }

    fun isEqualAt(offset: Int, value: Char): Boolean {
        return isEqualAt(source, offset, value)
    }

    fun isEqual(value: String): Boolean {
        return isEqual(source, value)
    }

    override fun prepareNewFrame(): Boolean {
        entries.push(Entry(matchIndex, matchLength))
        matchIndex = LexerMatch.NO_MATCH_INDEX
        matchLength = 0
        variables().forEach { it.prepareNewFrame() }
        return true
    }

    override fun restorePreviousFrame(): Boolean {
        return if (entries.isEmpty()) {
            false
        } else {
            val restored = entries.pop()
            matchIndex = restored.matchIndex
            matchLength = restored.matchLength
            variables().forEach { it.restorePreviousFrame() }
            true
        }
    }

    override fun clear() {
        matchIndex = LexerMatch.NO_MATCH_INDEX
        matchLength = 0
        entries.clear()
        variables().forEach { it.clear() }
    }

    data class Entry(
        override val matchIndex: Int,
        override val matchLength: Int
    ) : LexerMatch
}

fun LexerMatch.isEqual(source: SourceReader, value: String): Boolean {
    if (matchLength == -1 || matchLength != value.length) {
        return false
    }
    value.forEachIndexed { offset, char ->
        if (source.readCharAt(matchIndex + offset) != char) {
            return false
        }
    }
    return true
}

fun LexerMatch.isEqualAt(source: SourceReader, offset: Int, value: Char): Boolean {
    if (matchLength == -1 || offset >= matchLength) {
        return false
    }
    return source.readCharAt(matchIndex + offset) == value
}
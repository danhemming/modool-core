package modool.lexer.variable

import java.util.*

class LexerIntegerVariable : LexerVariable {

    private var value: Int? = null
    private val values = ArrayDeque<Int?>(3)

    override val isPopulated: Boolean
        get() = value != null

    fun set(value: Int) {
        this.value = value
    }

    fun increment(value: Int) {
        this.value.let {
            if (it == null) {
                set(value)
            } else {
                set(it + value)
            }
        }
    }

    fun decrement(value: Int) {
        this.value.let {
            if (it == null) {
                set(-value)
            } else {
                set(it - value)
            }
        }
    }

    fun isEqual(value: Int): Boolean {
        return this.value == value
    }

    override fun prepareNewFrame(): Boolean {
        values.push(value)
        value = null
        return true
    }

    override fun restorePreviousFrame(): Boolean {
        return if (values.isEmpty()) {
            false
        } else {
            value = values.pop()
            true
        }
    }

    override fun clear() {
        this.value = null
        this.values.clear()
    }
}
package modool.lexer

import modool.core.content.token.TokenWriter
import modool.lexer.rule.unknown.UnknownLexerRule
import modool.lexer.strategy.LexerCommitStrategy
import modool.lexer.strategy.LexerRecoveryStrategy

abstract class TrackingLexerRuleProcessor(
        private val unknownRule: UnknownLexerRule,
        private val tracker: LexerRuleTracker,
        private val recoveryStrategy: LexerRecoveryStrategy,
        private val commitStrategy: LexerCommitStrategy
) : LexerProcess {

    var rulesAreMatching = false
        private set
    var rulesHaveMatched = false
        private set

    override fun reset(context: LexerContext) {
        resetAllRules(context)
        rulesHaveMatched = false
    }

    private fun resetAllRules(context: LexerContext) {
        resetMainRules(context)
        commitStrategy.reset(context)
        unknownRule.reset(context)
    }

    private fun resetMainRules(context: LexerContext) {
        resetTracker(context)
        recoveryStrategy.reset(context)
        rulesAreMatching = false
    }

    protected open fun resetTracker(context: LexerContext) {
        tracker.reset(context)
    }

    override fun moveNext(context: LexerContext) {
        tracker.filterNextToAcceptable(context)

        if (!rulesAreMatching && tracker.isNextMatching()) {
            unknownRule.progressBookmark(context)
        }

        if (tracker.isCurrentMatching() && !tracker.isNextMatching() && tryToCommitARule(context)) {
            // We have completed a rule but we need to reprocess the current char with reset rules
            moveNext(context)
        } else if (tracker.isNextMatching()) {
            // Consume content in matching rules remember current char and continue
            tracker.consume(context)
            if (unknownRule.isAcceptable(context)) {
                unknownRule.consume(context)
            }
            rulesAreMatching = true
        } else if (tryToRecoverAndCommitARule(context)) {
            moveNext(context)
        } else {
            resetMainRules(context)
            unknownRule.consume(context)
        }
    }

    override fun complete(context: LexerContext) {
        tracker.getFirstMatchingSatisfiedRuleOrNull(context)?.complete(context)
    }

    override fun commit(context: LexerContext, content: TokenWriter) {
        if (unknownRule.isSatisfied(context)) {
            commitStrategy.commit(context, unknownRule, content)
            commitStrategy.reset(context)
            unknownRule.reset(context)
        }
    }

    protected open fun tryToRecoverAndCommitARule(context: LexerContext): Boolean {
        return recoveryStrategy.tryToRecover(context, unknownRule, tracker) && tryToCommitARule(context)
    }

    protected open fun tryToCommitARule(context: LexerContext): Boolean {
        val firstSatisfiedRule = tracker.getFirstMatchingSatisfiedRuleOrNull(context)

        return if (firstSatisfiedRule == null) {
            false
        } else {
            commitUnknownContentPriorToRule(context)
            // Accept the highest precedence previously matched content
            firstSatisfiedRule.complete(context)
            commitStrategy.commit(context, firstSatisfiedRule, getCurrentTokenWriter(context))
            commitStrategy.reset(context)
            rulesHaveMatched = true
            resetAllRules(context)
            true
        }
    }

    protected fun commitUnknownContentPriorToRule(context: LexerContext) {
        if (rulesAreMatching) {
            unknownRule.rollbackToBookmark(context)
        }
        if (unknownRule.isSatisfied(context)) {
            commitStrategy.commit(context, unknownRule, getCurrentTokenWriter(context))
            commitStrategy.reset(context)
        }
        unknownRule.reset(context)
    }

    protected open fun getCurrentTokenWriter(context: LexerContext): TokenWriter {
        return context.tokens
    }
}
package modool.lexer

class TokenisationException(source: String? = null, cause: Throwable? = null)
    : RuntimeException(if (source == null) null else "Unable to tokenise: $source", cause)
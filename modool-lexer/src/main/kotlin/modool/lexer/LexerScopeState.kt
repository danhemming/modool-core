package modool.lexer

import modool.lexer.variable.LexerRuleVariable
import modool.lexer.variable.LexerVariableType

class LexerScopeState(
    override val parent: LexerScopeState?,
    source: SourceReader
) : LexerStateContainer(parent, source) {

    fun getOrCreateRule(name: String): LexerRuleVariable {
        if (name.contains('.')) {
            error("rule names cannot be hierarchical")
        }
        return localVariableOrCreate(name, LexerVariableType.RULE) as LexerRuleVariable
    }
}
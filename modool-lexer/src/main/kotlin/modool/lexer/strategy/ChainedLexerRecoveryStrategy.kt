package modool.lexer.strategy

import modool.lexer.LexerContext
import modool.lexer.LexerRuleTracker
import modool.lexer.rule.unknown.UnknownLexerRule

abstract class ChainedLexerRecoveryStrategy(
        private val next: LexerRecoveryStrategy?
) : LexerRecoveryStrategy {

    override fun reset(context: LexerContext) {
        next?.reset(context)
    }

    override fun notifyAcceptable(ruleIndex: Int, sourceIndex: Int) {
        next?.notifyAcceptable(ruleIndex, sourceIndex)
    }

    override fun notifySatisfied(ruleIndex: Int, sourceIndex: Int) {
        next?.notifySatisfied(ruleIndex, sourceIndex)
    }

    override fun notifyFailed(ruleIndex: Int, sourceIndex: Int) {
        next?.notifyFailed(ruleIndex, sourceIndex)
    }

    override fun tryToRecover(
            context: LexerContext,
            unknownRule: UnknownLexerRule,
            tracker: LexerRuleTracker): Boolean {

        return next?.tryToRecover(context, unknownRule, tracker) ?: false
    }
}
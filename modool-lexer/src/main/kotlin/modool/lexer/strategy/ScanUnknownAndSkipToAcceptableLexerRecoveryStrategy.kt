package modool.lexer.strategy

import modool.lexer.LexerContext
import modool.lexer.LexerMatch
import modool.lexer.LexerRuleTracker
import modool.lexer.rule.unknown.UnknownLexerRule

/**
 * Records the start of the match and if it fails we reset to that position and skip content until a match is found.
 * After recover index is left prior to start position of next match.
 * Useful when unknown content is expected (not an error) e.g free form text
 * Should always be used with CaptureFreeFormTextLexerRule
 */
class ScanUnknownAndSkipToAcceptableLexerRecoveryStrategy(
        next: LexerRecoveryStrategy?)
    : ChainedLexerRecoveryStrategy(next) {

    private var fallbackStartIndex = LexerMatch.NO_MATCH_INDEX

    override fun reset(context: LexerContext) {
        super.reset(context)
        fallbackStartIndex = LexerMatch.NO_MATCH_INDEX
    }

    override fun notifyAcceptable(ruleIndex: Int, sourceIndex: Int) {
        if (fallbackStartIndex == LexerMatch.NO_MATCH_INDEX) {
            fallbackStartIndex = sourceIndex
        }
        super.notifyAcceptable(ruleIndex, sourceIndex)
    }

    override fun tryToRecover(
            context: LexerContext,
            unknownRule: UnknownLexerRule,
            tracker: LexerRuleTracker): Boolean {

        if (fallbackStartIndex == LexerMatch.NO_MATCH_INDEX
                || fallbackStartIndex == context.source.currentIndex) {
            return super.tryToRecover(context, unknownRule, tracker)
        }

        val priorStartIndex = fallbackStartIndex

        if (context.source.moveTo(priorStartIndex)) {
            tracker.reset(context)
            unknownRule.rollbackToBookmark(context)

            if (unknownRule.isAcceptable(context)) {
                unknownRule.consume(context)
            }

            while (context.source.moveForward()) {
                if (tracker.isStartable(context)) {
                    context.source.moveBack()
                    return true
                }
                if (unknownRule.isAcceptable(context)) {
                    unknownRule.consume(context)
                }
            }
        }

        return super.tryToRecover(context, unknownRule, tracker)
    }
}
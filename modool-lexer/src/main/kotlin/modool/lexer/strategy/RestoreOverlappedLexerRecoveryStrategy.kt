package modool.lexer.strategy

import modool.lexer.LexerContext
import modool.lexer.LexerMatch
import modool.lexer.LexerRuleTracker
import modool.lexer.rule.unknown.UnknownLexerRule

/**
 * Some grammars have rules that overlap e.g JavaScript divide and regex /pattern/
 * This strategy allows the tracker to recover to overlapped rules.
 */
class RestoreOverlappedLexerRecoveryStrategy(
        next: LexerRecoveryStrategy?)
    : ChainedLexerRecoveryStrategy(next) {

    private val fallbackMatched = mutableListOf<Int>()
    private var fallbackMatchedIndex: Int = LexerMatch.NO_MATCH_INDEX

    override fun reset(context: LexerContext) {
        super.reset(context)
        fallbackMatchedIndex = LexerMatch.NO_MATCH_INDEX
        fallbackMatched.clear()
    }

    /**
     * Record all rules that are satisfied at the first satisfied position
     */
    override fun notifySatisfied(ruleIndex: Int, sourceIndex: Int) {
        if (fallbackMatchedIndex == LexerMatch.NO_MATCH_INDEX) {
            fallbackMatchedIndex = sourceIndex
            fallbackMatched.add(ruleIndex)
        } else if (fallbackMatchedIndex == sourceIndex) {
            fallbackMatched.add(ruleIndex)
        }
        super.notifySatisfied(ruleIndex, sourceIndex)
    }

    override fun tryToRecover(
            context: LexerContext,
            unknownRule: UnknownLexerRule,
            tracker: LexerRuleTracker): Boolean {

        if (fallbackMatchedIndex == LexerMatch.NO_MATCH_INDEX
                || !context.source.moveTo(fallbackMatchedIndex)) {
            return super.tryToRecover(context, unknownRule, tracker)
        }

        fallbackMatched.forEach {
            tracker.restoreMatch(it)
        }

        return tracker.isNextMatching() || super.tryToRecover(context, unknownRule, tracker)
    }
}
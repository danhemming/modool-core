package modool.lexer.strategy

import modool.core.content.token.TokenWriter
import modool.lexer.LexerContext
import modool.lexer.rule.LexerRulable
import modool.lexer.rule.state.operator.GlobalEvalOperation

/**
 * Executes before and after operators for each match where both before and after operators are valid.
 * An operator acts on state and may mutate state or create markers.
 * An operator may not influence the success of a match (unlike the eval rules).
 */
class GlobalEvalLexerCommitStrategy(
        val operations: List<GlobalEvalOperation>
) : LexerCommitStrategy {

    override fun reset(context: LexerContext) {
        super.reset(context)
        operations.forEach { it.reset(context) }
    }

    override fun commit(context: LexerContext, rule: LexerRulable, content: TokenWriter) {
        val matchingOperations = operations.filter { it.isValid(context, rule) }
        matchingOperations.forEach {
            it.executeBefore(context, content, rule.matchIndex, rule.matchLength)
        }
        rule.commit(context, content)
        matchingOperations.forEach {
            it.executeAfter(context, content, rule.matchIndex, rule.matchLength)
        }
    }
}
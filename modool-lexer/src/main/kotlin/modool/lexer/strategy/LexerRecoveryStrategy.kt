package modool.lexer.strategy

import modool.lexer.LexerContext
import modool.lexer.LexerRuleTracker
import modool.lexer.LexerRuleTrackerObserver
import modool.lexer.LexerInitialization
import modool.lexer.rule.unknown.UnknownLexerRule

interface LexerRecoveryStrategy
    : LexerInitialization,
        LexerRuleTrackerObserver {

    fun tryToRecover(context: LexerContext, unknownRule: UnknownLexerRule, tracker: LexerRuleTracker): Boolean

    object Default : LexerRecoveryStrategy {

        override fun tryToRecover(
                context: LexerContext,
                unknownRule: UnknownLexerRule,
                tracker: LexerRuleTracker): Boolean {
            return false
        }
    }
}
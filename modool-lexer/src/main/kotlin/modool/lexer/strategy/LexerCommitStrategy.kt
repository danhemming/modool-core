package modool.lexer.strategy

import modool.core.content.token.TokenWriter
import modool.lexer.LexerContext
import modool.lexer.LexerInitialization
import modool.lexer.rule.LexerRulable

interface LexerCommitStrategy : LexerInitialization {

    fun commit(context: LexerContext, rule: LexerRulable, content: TokenWriter)

    object Default : LexerCommitStrategy {

        override fun commit(context: LexerContext, rule: LexerRulable, content: TokenWriter) {
            rule.commit(context, content)
        }
    }
}

package modool.lexer

interface LexerRuleTrackerObserver {

    fun notifyAcceptable(ruleIndex: Int, sourceIndex: Int) {
        return
    }

    fun notifySatisfied(ruleIndex: Int, sourceIndex: Int) {
        return
    }

    fun notifyFailed(ruleIndex: Int, sourceIndex: Int) {
        return
    }

    object Default : LexerRuleTrackerObserver
}
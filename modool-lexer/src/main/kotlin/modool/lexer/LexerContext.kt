package modool.lexer

import modool.core.content.token.TokenReaderProvider
import modool.core.content.token.TokenWriter

class LexerContext(
        val source: SourceReader,
        val tokens: TokenWriter,
        val tokenReaderProvider: TokenReaderProvider) {

    private var debugIndentLevel = 0

    var variables = LexerScopeState(parent = null, source = source)
        private set

    init {
        tokens.writeStart()
    }

    fun createScopeState(): LexerScopeState {
        return LexerScopeState(variables, source)
    }

    fun applyScopeState(variables: LexerScopeState) {
        this.variables = variables
    }

    fun restoreParentScopeState(variables: LexerScopeState) {
        this.variables = variables.parent!!
    }

    inline fun <T> lookAround(action: (originalIndex: Int)-> T): T {
        val originalIndex = source.currentIndex
        return try {
            action(originalIndex)
        } finally {
            source.moveTo(originalIndex)
        }
    }

    fun <T> debug(
            identifier: String,
            callSite: String,
            filter: ((LexerContext, String) -> Boolean)?,
            action: () -> T): T {
        debugIndentLevel++
        val result = action()
        debugIndentLevel--
        if (filter == null || filter.invoke(this, callSite)) {
            debug(identifier, callSite, if (result is Unit) "" else "-> $result")
        }
        return result
    }

    fun debug(
            identifier: String,
            callSite: String,
            message: String) {
        println("${stateToString(identifier, callSite)} $message")
    }

    private fun stateToString(identifier: String, callSite: String): String {
        return "${" ".repeat((debugIndentLevel) * 4)}${identifier}.${callSite}:${source.currentIndex}:${encode(source.readChar())}"
    }

    private fun encode(value: Char?): String {
        return when (value) {
            '\r' -> "\\r"
            '\n' -> "\\n"
            '\t' -> "\\t"
            else -> value.toString()
        }
    }
}
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

val kotlin_version: String by extra

buildscript {

    dependencies {
        // For spek ...
        classpath("org.junit.platform:junit-platform-gradle-plugin:1.0.0")
    }

    repositories {
        mavenCentral()
    }
}

repositories {
    mavenCentral()
}

plugins {
    idea
    kotlin("jvm") version "1.3.10" apply true
}

val modoolVersion = "0.1"

subprojects {
    apply {
        plugin("maven")
        plugin("idea")
        plugin("kotlin")
        plugin("org.junit.platform.gradle.plugin")
    }

    group = "org.modool"
    version = modoolVersion

    repositories {
        mavenLocal()
        mavenCentral()
        jcenter()
    }

    tasks.withType<KotlinCompile> {

        kotlinOptions {
            jvmTarget = "1.8"
            freeCompilerArgs = listOf("-Xjsr305=strict")
        }
    }

    tasks {
        val sourcesJar by registering(Jar::class) {
            archiveClassifier.set("sources")
            from(sourceSets.main.get().allSource)
            dependsOn(classes)
        }

        artifacts {
            add("archives", sourcesJar)
        }
    }

    dependencies {
        compile(kotlin("stdlib-jdk8"))

        testCompile("org.jetbrains.spek:spek-api:1.1.5")
        testCompile("org.amshove.kluent:kluent:1.43")
        testCompile("org.jetbrains.kotlin:kotlin-test:1.2.31")
        testCompile("io.mockk:mockk:1.6")
        testCompile("junit:junit:4.12")

        testRuntime("org.jetbrains.spek:spek-junit-platform-engine:1.1.5")
        testRuntime("org.junit.platform:junit-platform-runner:1.0.0")
    }
}
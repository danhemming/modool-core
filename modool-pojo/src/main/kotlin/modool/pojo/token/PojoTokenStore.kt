package modool.pojo.token

import modool.core.content.signifier.tag.Tag
import modool.core.content.signifier.tag.terminal.UnknownTerminal
import modool.core.content.token.ConstantTerminalDescriptor
import modool.core.content.token.TokenWriter
import modool.core.content.token.TokenWriterRestorePoint
import modool.core.content.token.WhitespaceContent
import modool.lexer.TextBlock

class PojoTokenStore(private val text: TextBlock) : TokenWriter {

    private val tokens = mutableListOf<PojoToken>()
    private var currentTokenIndex = 0

    override val isAccepting: Boolean
        get() = true

    override fun writeStart() {
        return
    }

    override fun isAcceptable(descriptor: ConstantTerminalDescriptor): Boolean {
        return true
    }

    override fun isEmpty(): Boolean {
        return tokens.isEmpty()
    }

    override fun createRestorePoint(): TokenWriterRestorePoint {
        TODO("Not yet implemented")
    }

    override fun updateRestorePoint(restorePoint: TokenWriterRestorePoint) {
        TODO("Not yet implemented")
    }

    override fun restore(restorePoint: TokenWriterRestorePoint) {
        TODO("Not yet implemented")
    }

    override fun writeFactoryTerminal(index: Int, count: Int, descriptor: ConstantTerminalDescriptor, tags: List<Tag>) {
        TODO("Not yet implemented")
    }

    override fun writeUnknownTerminal(index: Int, count: Int) {
        val terminal = PojoTerminal(currentTokenIndex++, text.substring(index, index + count))
        terminal.tag(UnknownTerminal)
        tokens.add(terminal)
    }

    override fun writeTerminal(index: Int, count: Int, tags: List<Tag>) {
        val terminal = PojoTerminal(currentTokenIndex++, text.substring(index, index + count))
        tags.forEach { terminal.tag(it) }
        tokens.add(terminal)
    }

    override fun writeComment(body: () -> Unit) {
        TODO("Not yet implemented")
    }

    override fun writeMarker(tags: List<Tag>, isDeferred: Boolean) {
        TODO("Not yet implemented")
    }

    override fun overwriteWhitespace(whitespace: WhitespaceContent, tags: List<Tag>) {
        TODO("Not yet implemented")
    }

    override fun copyFrom(writer: TokenWriter) {
        TODO("Not yet implemented")
    }

    override fun writeEnd() {
        TODO("Not yet implemented")
    }

    override fun clear() {
        TODO("Not yet implemented")
    }

    override fun createWhitespaceBuilder(): WhitespaceContent {
        TODO("Not yet implemented")
    }

    override fun createComponentWriter(): TokenWriter {
        TODO("Not yet implemented")
    }

    override fun createValueComponentWriter(): TokenWriter {
        TODO("Not yet implemented")
    }
}
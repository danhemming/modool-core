package modool.pojo.token

import modool.core.content.signifier.Signifier
import modool.core.content.signifier.tag.Tag
import modool.core.content.signifier.tag.TagBag
import modool.core.content.signifier.tag.Taggable

/**
 * A marker token is a zero width token.
 * A marker token is used to represent a parsable construct e.g. a list opening that does not have an explicit
 * representation in the source.
 * A marker token is dropped by the lexer to make explicit what is implicit in the source.
 */
class PojoMarkerToken(index: Int)
    : PojoToken(index),
        Taggable {

    private val tags = TagBag()

    override fun signifies(signifier: Signifier): Boolean {
        return signifier is Tag && tags.contains(signifier)
    }

    override fun tag(tag: Tag): Boolean {
        return tags.add(tag)
    }

    override fun untag(tag: Tag): Boolean {
        return tags.remove(tag)
    }
}
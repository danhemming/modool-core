package modool.pojo.token

import modool.core.content.WritableValue
import modool.core.content.signifier.Signifier
import modool.core.content.signifier.tag.Tag
import modool.core.content.signifier.tag.TagBag
import modool.core.content.signifier.tag.TagCategory
import modool.core.content.signifier.tag.Taggable
import modool.core.content.signifier.tag.terminal.UnknownTerminal

class PojoTerminal(
        index: Int,
        private var text: String,
        private val tags: TagBag
) : PojoToken(index),
        WritableValue<String>,
        Taggable {

    // Default constructor
    constructor(index: Int, text: String) : this(index, text, TagBag())

    override fun get() = text

    override fun set(value: String) {
        text = value
    }

    override fun tag(tag: Tag): Boolean {
        return tags.add(tag)
    }

    override fun untag(tag: Tag): Boolean {
        return if (tag is Tag.TerminalType) tag(UnknownTerminal) else tags.remove(tag)
    }

    override fun signifies(signifier: Signifier): Boolean {
        return signifier is Tag && tags.contains(signifier)
    }

    override fun signifies(category: TagCategory): Boolean {
        return tags.contains(category)
    }
}
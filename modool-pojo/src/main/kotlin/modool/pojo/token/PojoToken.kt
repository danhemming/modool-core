package modool.pojo.token

import modool.core.content.token.TokenReference

abstract class PojoToken(val index: Int) : TokenReference
package modool.pojo.token

import it.unimi.dsi.fastutil.bytes.ByteArrayList
import modool.core.content.Copyable
import modool.core.content.token.Whitespace
import modool.core.content.token.WhitespaceContent

/**
 * Rather than creating token for each white space encoding, we instead create something that works in both
 * modes.  This cuts down on the number of objects we need to create. It also allows us to capture the combined state of
 * multiple whitespace tokens e.g. margins
 */
open class PojoWhitespaceToken protected constructor(
        index: Int,
        protected val contents: ByteArrayList,
        protected var numberOfSpacesOnCurrentLine: Int,
        protected var numberOfTabsOnCurrentLine: Int,
        protected var numberOfNewLines: Int
) : PojoToken(index),
        WhitespaceContent,
        Copyable<PojoWhitespaceToken> {

    override val lineCount: Int get() = numberOfNewLines
    override val spaceCount: Int get() = numberOfSpacesOnCurrentLine
    override val tabCount: Int get() = numberOfTabsOnCurrentLine
    override val renderLength: Int get() = contents.count()
    override val marginX: Int get() = numberOfSpacesOnCurrentLine + (numberOfTabsOnCurrentLine * WhitespaceContent.TAB_SIZE)

    // Default constructor
    constructor(index: Int) : this(index, ByteArrayList(3), 0, 0, 0)

    // Copy constructor
    constructor(index: Int, content: WhitespaceContent)
            : this(index, content.asByteArrayList(), content.spaceCount, content.tabCount, content.lineCount)


    override fun copy(): PojoWhitespaceToken {
        return PojoWhitespaceToken(
                index,
                contents.clone(),
                numberOfSpacesOnCurrentLine,
                numberOfTabsOnCurrentLine,
                numberOfNewLines)
    }

    override fun save(): WhitespaceContent {
        return copy()
    }

    override fun asByteArrayList(): ByteArrayList {
        return contents
    }

    override fun add(encoding: Whitespace) {
        contents.add(encoding.encoding)

        when {
            encoding.isLineBreak -> {
                numberOfSpacesOnCurrentLine = 0
                numberOfTabsOnCurrentLine = 0
                numberOfNewLines++
            }
            encoding == Whitespace.SPACE ->
                numberOfSpacesOnCurrentLine++
            encoding == Whitespace.TAB ->
                numberOfTabsOnCurrentLine++
            else -> return
        }
    }
}


package modool.dom

import modool.dom.identity.Identifiable
import modool.dom.name.MandatoryNameable

/**
 * Addressable indicates something that is externally identifiable and perhaps requires some qualification
 */
interface Addressable
    : Identifiable,
        MandatoryNameable {
    /**
     * should be unique within local scope e.g. method name and possibly take into account qualifying items like params
     */
    // TODO: need to deal with global addressable requirements i.e. uri, qualified name
}
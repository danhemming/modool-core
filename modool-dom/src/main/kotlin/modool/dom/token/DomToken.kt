package modool.dom.token

import modool.core.content.token.TokenReference

abstract class DomToken : TokenReference {

    var prior: DomToken? = null
        set(value) {
            if (field != value) {
                field = value
                onContextChange()
            }
        }

    var next: DomToken? = null
        set(value) {
            if (field != value) {
                field = value
                onContextChange()
            }
        }

    open val start: DomToken
        get() = this

    open val end: DomToken
        get() = this

    private fun validate() {
        if (next != null && prior != null && next === prior) {
            throw RuntimeException("Token list in infinite loop")
        }

        var currentNext = next
        while (currentNext != null) {
            if (currentNext == this) {
                throw RuntimeException("Token list in infinite loop")
            }
            currentNext = currentNext.next
        }
    }

    protected open fun onContextChange() {
    }

    fun detachFromAndRelinkChain() {
        transaction {
            prior?.next = next
            next?.prior = prior
            next = null
            prior = null
        }
    }

    fun detachFromPriorTokenAndRelinkChain() {
        transaction {
            prior?.next = next
            prior = null
        }
    }

    fun detachFromNextTokenAndRelinkChain() {
        transaction {
            next?.prior = prior
            next = null
        }
    }

    fun detachFromChain() {
        detachFromNextToken()
        detachFromPriorToken()
    }

    fun detachFromNextToken() {
        next?.prior = null
        next = null
    }

    fun detachFromPriorToken() {
        prior?.next = null
        prior = null
    }

    fun replaceInChainWith(token: DomToken) {
        if (this != token) {

            val insertStartPrior = token.prior
            val insertEndNext = token.end.next

            transaction {
                end.next?.prior = token.end
                prior?.next = token
                token.end.next = end.next
                token.prior = prior
                end.next = null
                prior = null

                // Ensure the original chain is OK
                insertStartPrior?.next = insertEndNext
                insertEndNext?.prior = insertStartPrior
            }
        }
    }

    fun insertDetachedNext(token: DomToken) {
        if (next != token) {
            transaction {
                token.detachFromChain()
                next?.prior = token
                token.prior = this
                token.next = next
                next = token
            }
        }
    }

    fun insertExtractedChainNext(insertStart: DomToken, insertUntil: DomToken? = null) {
        if (start != insertStart && next != insertStart && end != insertStart) {

            val insertEnd = insertUntil ?: insertStart.end
            val insertEndNext = insertEnd.next
            val insertStartPrior = insertStart.prior

            transaction {
                // Ensure the original chain is OK
                insertStartPrior?.next = insertEndNext
                insertEndNext?.prior = insertStartPrior

                // Ensure this chain is ok
                next?.prior = insertEnd
                insertStart.prior = this
                insertEnd.next = next
                next = insertStart
            }
        }
    }

    fun insertDetachedPrior(token: DomToken) {
        if (prior != token) {
            transaction {
                token.detachFromChain()
                prior?.next = token
                token.next = this
                token.prior = prior
                prior = token
            }
        }
    }

    fun insertExtractedChainPrior(insertStart: DomToken, insertUntil: DomToken? = null) {
        if (prior != insertStart) {

            val insertEnd = insertUntil ?: insertStart.end

            if (this != insertEnd) {
                transaction {
                    val insertStartPrior = insertStart.prior
                    val insertEndNext = insertEnd.next

                    // Ensure the original chain is OK
                    insertStartPrior?.next = insertEndNext
                    insertEndNext?.prior = insertStartPrior

                    // Ensure this chain is ok
                    prior?.next = insertStart
                    insertEnd.next = this
                    insertStart.prior = prior
                    prior = insertEnd
                }
            }
        }
    }


    fun follows(other: DomToken): Boolean {
        var candidate = prior
        while (candidate != null && candidate != other) {
            candidate = candidate.prior
        }
        return candidate == other
    }

    fun precedes(other: DomToken): Boolean {
        var candidate = next
        while (candidate != null && candidate != other) {
            candidate = candidate.next
        }
        return candidate == other
    }

    private inline fun transaction(body: () -> Unit) {
        body()
        // For debug ...
        // validate()
    }

    protected fun inspectRecursive(tokenInspector: DomTokenInspector) {
        var candidate = next

        while (candidate != null && candidate != end) {
            candidate.inspect(tokenInspector)
            candidate = if (candidate is DomNodeContainerStartToken) {
                candidate.end.next
            } else {
                candidate.next
            }
        }

        end.inspect(tokenInspector)
    }

    abstract fun toPrettyDescription(): String
    abstract fun inspect(tokenInspector: DomTokenInspector)

    fun toDebugString(tokenInspector: DomTokenInspector): String {
        inspect(tokenInspector)
        return tokenInspector.toString()
    }
}
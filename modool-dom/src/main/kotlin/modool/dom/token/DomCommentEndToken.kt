package modool.dom.token

internal class DomCommentEndToken(start: DomCommentStartToken) : DomEndToken(start) {

    override fun toPrettyDescription(): String {
        return "[End of comment]"
    }
}
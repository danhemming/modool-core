package modool.dom.token

abstract class DomStartToken
    : DomToken() {

    @Suppress("LeakingThis")
    final override val end = createEndToken()

    init {
        next = end
        @Suppress("LeakingThis")
        end.prior = this
    }


    abstract fun createEndToken(): DomEndToken
}
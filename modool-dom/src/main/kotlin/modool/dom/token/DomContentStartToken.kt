package modool.dom.token

class DomContentStartToken : DomStartToken() {

    override fun createEndToken(): DomEndToken {
        return DomContentEndToken(this)
    }

    override fun toPrettyDescription(): String {
        return "[Start of content]"
    }

    override fun inspect(tokenInspector: DomTokenInspector) {
        tokenInspector.openOutput("Start", javaClass)
        inspectRecursive(tokenInspector)
    }
}
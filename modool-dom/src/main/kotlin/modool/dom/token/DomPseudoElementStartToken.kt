package modool.dom.token

import modool.core.content.ContentOrigin
import modool.core.content.Copyable
import modool.core.content.signifier.tag.TagBag
import modool.dom.node.DomElementBacked
import modool.dom.style.Style

internal class DomPseudoElementStartToken<T> private constructor(
        val type: Class<T>,
        requiresParsing: Boolean,
        firstDisplayed: Boolean,
        contentOrigin: ContentOrigin,
        tags: TagBag,
        style: Style?
) : DomElementalStartToken(requiresParsing, firstDisplayed, contentOrigin, tags, style),
        Copyable<DomPseudoElementStartToken<T>>
        where T : DomElementBacked<*> {

    constructor(type: Class<T>, contentOrigin: ContentOrigin) : this(
            type,
            firstDisplayed = true, requiresParsing = false,
            contentOrigin = contentOrigin,
            tags = TagBag(),
            style = null)

    constructor(type: Class<T>) : this(type, ContentOrigin.SecondarySource)

    override fun <T> isStylingEquivalentOf(styleRuleType: Class<T>): Boolean {
        /*
        The type that this represents is probably an element interface. The style rules are done against Modool elements and
        properties. This means we need to do a bit of a fudge and say if we are an interface that is implemented by the
        style class then we are equivalent.
         */
        return styleRuleType.isAssignableFrom(type) ||
                (type.isInterface && styleRuleType.isMemberClass && type.isAssignableFrom(styleRuleType))
    }

    override fun createEndToken(): DomEndToken {
        return DomPseudoElementEndToken(this)
    }

    override fun toPrettyDescription(): String {
        return "[Start of pseudo element: $type]"
    }

    override fun inspect(tokenInspector: DomTokenInspector) {
        tokenInspector.openOutput("PseudoElement", type)
        inspectRecursive(tokenInspector)
    }

    override fun copy(): DomPseudoElementStartToken<T> {
        return DomPseudoElementStartToken(type, requiresParsing, displayed, contentOrigin, tags.copy(), style)
    }
}
package modool.dom.token

import modool.dom.node.text.DomTerminalNode
import modool.dom.node.text.factory.ConstantDomTerminalFactory
import modool.dom.style.DisplayStyleable

typealias TokenFilter = (token: DomToken) -> Boolean

val FILTER_ALL_TOKENS = { _: DomToken -> true }

fun DomToken.asForwardSequence(filter: TokenFilter = FILTER_ALL_TOKENS) = sequence {
    var current = this@asForwardSequence

    if (filter(current)) {
        yield(current)
    }

    while (current.next != null) {
        current = current.next!!
        if (filter(current)) {
            yield(current)
        }
    }
}

fun DomToken.asBackwardsSequence(filter: TokenFilter = FILTER_ALL_TOKENS) = sequence {
    var current = this@asBackwardsSequence
    if (filter(current)) {
        yield(current)
    }

    while (current.prior != null) {
        current = current.prior!!
        if (filter(current)) {
            yield(current)
        }
    }
}

fun DomToken.findNextDisplayedTextNode(): DomTerminalNode? {
    var candidate = next

    while (candidate != null) {
        if (candidate is DisplayStyleable && !candidate.displayed) {
            candidate = candidate.end.next
            continue
        }

        if (candidate is DomTerminalNode) {
            return candidate
        }
        candidate = candidate.next
    }

    return null
}

fun DomToken.findPriorDisplayedTextNode(): DomTerminalNode? {
    var candidate = prior

    while (candidate != null) {
        if (candidate is DisplayStyleable && !candidate.displayed) {
            candidate = candidate.start.prior
            continue
        }

        if (candidate is DomTerminalNode) {
            return candidate
        }
        candidate = candidate.prior
    }

    return null
}

inline fun <reified T: Any> DomToken.findNext(filter: (token: T) -> Boolean = { _ -> true }): T? {
    var candidate = next

    while (candidate != null) {
        if (candidate is T && filter(candidate)) {
            return candidate
        }
        candidate = candidate.next
    }

    return null
}

inline fun <reified T: Any> DomToken.findPrior(filter: (token: T) -> Boolean = { _ -> true }): T? {
    var candidate = prior

    while (candidate != null) {
        if (candidate is T && filter(candidate)) {
            return candidate
        }
        candidate = candidate.prior
    }

    return null
}

fun DomToken.isDisplayedNext(factory: ConstantDomTerminalFactory): Boolean {
    return findNextDisplayedTextNode()?.let { factory.isProduct(it) } ?: false
}

fun DomToken.isDisplayedPrior(factory: ConstantDomTerminalFactory): Boolean {
    return findPriorDisplayedTextNode()?.let { factory.isProduct(it) } ?: false
}
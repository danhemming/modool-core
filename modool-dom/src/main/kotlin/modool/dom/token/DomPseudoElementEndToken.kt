package modool.dom.token

internal class DomPseudoElementEndToken(override val start: DomPseudoElementStartToken<*>): DomElementalEndToken(start) {

    override fun toPrettyDescription(): String {
        return "[End of pseudo element: ${start.type}]"
    }

    override fun inspect(tokenInspector: DomTokenInspector) {
        tokenInspector.closeOutput("PseudoElement", start.type)
    }
}
package modool.dom.token

internal class DomElementEndToken(override val start: DomElementStartToken): DomElementalEndToken(start) {

    override fun toPrettyDescription(): String {
        return "[End of element: ${start.javaClass.canonicalName}]"
    }

    override fun inspect(tokenInspector: DomTokenInspector) {
        tokenInspector.closeOutput("Element", start::class.java)
    }
}
package modool.dom.token

import modool.core.content.ContentOrigin
import modool.core.content.Copyable
import modool.core.content.signifier.tag.TagBag
import modool.core.content.tryCopyOrNull
import modool.dom.property.DomProperty

internal class DomPropertyStartToken private constructor(
        var property: DomProperty<*>?,
        requiresParsing: Boolean,
        displayed: Boolean,
        contentOrigin: ContentOrigin,
        tags: TagBag)
    : DomNodeContainerStartToken(requiresParsing, displayed, contentOrigin, tags),
        Copyable<DomPropertyStartToken> {

    constructor(
            property: DomProperty<*>?, requiresParsing: Boolean, displayed: Boolean, contentOrigin: ContentOrigin
    ) : this(
            property, requiresParsing, displayed, contentOrigin, TagBag())

    constructor(property: DomProperty<*>?, requiresParsing: Boolean): this(
            property = property,
            requiresParsing = requiresParsing, displayed = false,
            contentOrigin = ContentOrigin.API,
            tags = TagBag())

    override fun createEndToken(): DomEndToken {
        return DomPropertyEndToken(this)
    }

    override fun toPrettyDescription(): String {
        return "[Start of container]"
    }

    override fun inspect(tokenInspector: DomTokenInspector) {
        tokenInspector.openOutput("Property", property?.javaClass)
        inspectRecursive(tokenInspector)
    }

    override fun copy(): DomPropertyStartToken {
        return DomPropertyStartToken(property.tryCopyOrNull(), requiresParsing, displayed, contentOrigin, tags.copy())
    }
}
package modool.dom.token

import modool.core.content.ContentOrigin
import modool.core.content.signifier.tag.Tag
import modool.core.content.signifier.tag.TagBag
import modool.dom.node.*
import modool.dom.style.DisplayStyleable
import modool.core.content.token.*

abstract class DomNodeContainerStartToken protected constructor(
        requiresParsing: Boolean,
        firstDisplayed: Boolean,
        override var contentOrigin: ContentOrigin,
        tags: TagBag
) : DomTokenContainerStartToken(requiresParsing, tags),
        DomNodeContainer,
        ContentOriginProvider {

    constructor(): this(
            requiresParsing = true, firstDisplayed = true, contentOrigin = ContentOrigin.API, tags = TagBag())

    override var displayed: Boolean = firstDisplayed
        set(value) {
            if (value && !field && contentOrigin.isFromSource()) {
                contentOrigin = ContentOrigin.API
            }
            field = value
        }

    override fun countOfNodes() = asRecursiveNodeSequence().count()

    override fun countOfElements() = asRecursiveNodeSequence().filter { it is DomElement<*> }.count()

    override fun countOfLines(): Int {
        var lineCount = 1
        var candidate = next

        while (candidate != null && candidate != end) {

            if (candidate is DomNodeContainer && !candidate.displayed) {
                candidate = candidate.end.next
                continue
            }

            if (candidate is DomWhitespaceNode) {
                lineCount += candidate.lineCount
            }

            candidate = candidate.next
        }

        return lineCount
    }

    override fun containsLineBreaks(): Boolean {
        var candidate = next

        while (candidate != null && candidate != end) {

            if (candidate is DomNodeContainer && !candidate.displayed) {
                candidate = candidate.end.next
                continue
            }

            if (candidate is DomWhitespaceNode && candidate.isNewLine) {
                return true
            }

            candidate = candidate.next
        }

        return false
    }

    override fun asRecursiveNodeSequence() = sequence<DomNode> {
        var candidate = next

        while (candidate != null && candidate != end) {

            if (candidate is DomNode) {
                yield(candidate)
            }

            candidate = candidate.next
        }
    }

    override fun asRecursiveDisplayedNodeSequence(): Sequence<DomContentNode> = sequence {
        var candidate = next

        while (candidate != null && candidate != end) {

            if (candidate is DomNodeContainer && !candidate.displayed) {
                candidate = candidate.end.next
                continue
            }

            if (candidate is DomContentNode) {
                if (candidate is DisplayStyleable) {
                    if (candidate.displayed) {
                        yield(candidate as DomContentNode)
                    }
                } else {
                    yield(candidate!!)
                }
            }

            candidate = candidate.next
        }
    }

    @Suppress("UNCHECKED_CAST")
    override fun <T> asLocalTypeSequence(type: Class<T>): Sequence<T> = sequence {

        var candidate = next

        while (candidate != null && candidate != end) {

            if (type.isAssignableFrom(candidate.javaClass)) {
                yield(candidate as T)
                candidate = candidate.end
            } else if (candidate is DomElementalStartToken) {
                candidate = candidate.end
            }

            candidate = candidate.next
        }
    }

    @Suppress("UNCHECKED_CAST")
    override fun <T> asRecursiveTypeSequence(type: Class<T>): Sequence<T> = sequence {

        var candidate = next

        while (candidate != null && candidate != end) {

            if (type.isAssignableFrom(candidate.javaClass)) {
                yield(candidate as T)
            }

            candidate = candidate.next
        }
    }

    override fun containsContent(node: DomNode): Boolean {
        return asRecursiveNodeSequence().find { it == node } != null
    }

    override fun removeContent(node: DomNode): Boolean {
        val foundNode = asRecursiveNodeSequence().find { it == node }
        (foundNode as? DomToken)?.detachFromAndRelinkChain()
        return foundNode != null
    }

    /**
     * Called when this container is not part of the token stream.
     * Used to wrap the content in the token stream identified by the parameters.
     */
    override fun wrapAsElement(startToken: DomToken, endToken: DomToken, option: TokenRangeOption) {
        if (startToken is DomPropertyStartToken || startToken is DomWhitespaceNode) {
            // Lets left own all the properties and whitespace starting to the left
            var currentToken = startToken
            while (currentToken.prior is DomStartToken) {
                currentToken = currentToken.prior!!
            }

            wrap(currentToken, endToken, when (option) {
                TokenRangeOption.START_INCLUSIVE_END_EXCLUSIVE ->
                    if (currentToken is DomContentStartToken) TokenRangeOption.EXCLUSIVE else option
                TokenRangeOption.INCLUSIVE ->
                    if (currentToken is DomContentStartToken) TokenRangeOption.START_EXCLUSIVE_END_INCLUSIVE else option
                TokenRangeOption.START_EXCLUSIVE_END_INCLUSIVE ->
                    if (currentToken is DomContentStartToken) option else TokenRangeOption.INCLUSIVE
                TokenRangeOption.EXCLUSIVE ->
                    if (currentToken is DomContentStartToken) option else TokenRangeOption.START_INCLUSIVE_END_EXCLUSIVE
            })
        } else {
            // Put the element token in the stream after the starting parsed token
            wrap(startToken, endToken, option)
        }
    }

    override fun trimIndent(spaceCount: Int): Boolean {
        val firstShownIndent = asRecursiveDisplayedNodeSequence()
                .firstOrNull { it is DomWhitespaceNode && it.isLeftMargin }
                as? DomWhitespaceNode
                ?: return false

        val numberOfIndents = firstShownIndent.calculateIndentationLevel(spaceCount)

        if (numberOfIndents < 1) {
            return false
        }

        val firstShownContent = asRecursiveDisplayedNodeSequence().firstOrNull()
                ?: return false

        val numberOfIndentsToRemove = if (firstShownIndent.precedes(firstShownContent)) numberOfIndents else numberOfIndents - 1

        if (numberOfIndentsToRemove < 1) {
            return false
        }

        asRecursiveNodeSequence()
                .filter { it is DomWhitespaceNode && it.isLeftMargin }
                .map { it as DomWhitespaceNode }
                .forEach { whitespace ->
                    repeat(whitespace.marginX - numberOfIndentsToRemove) { whitespace.removeTrailingSpace() }
                }

        return true
    }
}
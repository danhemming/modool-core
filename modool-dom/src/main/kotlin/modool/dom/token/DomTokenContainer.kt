package modool.dom.token

import modool.core.content.signifier.tag.Taggable
import modool.core.content.token.TokenRangeOption

interface DomTokenContainer : Taggable {
    fun isEmpty(): Boolean
    fun isNotEmpty(): Boolean
    fun contains(token: DomToken): Boolean
    fun asRecursiveTokenSequence(): Sequence<DomToken>
    fun asLocalTokenSequence(): Sequence<DomToken>
    fun addTokenAtStart(token: DomToken)
    fun addTokenAtEnd(token: DomToken)
    fun wrap(startToken: DomToken, endToken: DomToken, option: TokenRangeOption)
    fun expandRightToInclude(token: DomToken)
    fun expandLeftToInclude(token: DomToken)
    fun remove(token: DomToken): Boolean
    fun clear()
}
package modool.dom.token

abstract class DomEndToken(override val start: DomStartToken)
    : DomToken() {

    override fun inspect(tokenInspector: DomTokenInspector) {
        tokenInspector.closeOutput("End", javaClass)
    }
}
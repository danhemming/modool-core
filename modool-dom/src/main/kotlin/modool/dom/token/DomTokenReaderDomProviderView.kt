package modool.dom.token

import modool.core.content.token.TokenReaderProviderView

interface DomTokenReaderDomProviderView : TokenReaderProviderView {
    val startToken: DomContentStartToken
    val currentToken: DomToken
}
package modool.dom.token

internal class DomPropertyEndToken(override val start: DomPropertyStartToken) : DomEndToken(start) {

    override fun toPrettyDescription(): String {
        return "[End of container]"
    }

    override fun inspect(tokenInspector: DomTokenInspector) {
        tokenInspector.closeOutput("Property", start.property?.javaClass)
    }
}
package modool.dom.token

import modool.core.content.ContentOrigin
import modool.core.content.Copyable
import modool.core.content.signifier.tag.TagBag

internal class DomCommentStartToken private constructor(
        firstDisplayed: Boolean,
        contentOrigin: ContentOrigin,
        tags: TagBag
) : DomNodeContainerStartToken(
        requiresParsing = false, firstDisplayed = firstDisplayed, contentOrigin = contentOrigin, tags = tags),
        Copyable<DomCommentStartToken> {

    // Default constructor
    constructor() : this(firstDisplayed = true, contentOrigin = ContentOrigin.API, tags = TagBag())

    override fun createEndToken(): DomEndToken {
        return DomCommentEndToken(this)
    }

    override fun toPrettyDescription(): String {
        return "[Start of comment]"
    }

    override fun copy(): DomCommentStartToken {
        return DomCommentStartToken(displayed, contentOrigin, tags.copy())
    }
}
package modool.dom.token

import modool.dom.DomContext
import modool.dom.node.DomNodeContainer
import modool.dom.property.sequence.DomBaseViewProperty

/**
 * Acts as a placeholder for collections i.e start adding fields, methods here.
 */
class DomViewPlaceHolderToken<D, T>(
        private val container: DomNodeContainer,
        private val viewProperty: DomBaseViewProperty<D, T>)
    : DomToken()
        where D : DomContext {

    private var isControllerInPlace = false

    fun isPlacementControllerFor(obj: Any) = obj == viewProperty

    fun place(token: DomToken?) {

        if (token == null) {
            return
        }

        isControllerInPlace = isControllerInPlace || container.asRecursiveTokenSequence().contains(this)

        if (isControllerInPlace) {
            this.insertExtractedChainPrior(token)
        } else {
            container.addTokenAtEnd(token)
        }
    }

    override fun toPrettyDescription(): String {
        return "[place holder]"
    }

    override fun inspect(tokenInspector: DomTokenInspector) {
        tokenInspector.outputLine("Placeholder")
    }
}
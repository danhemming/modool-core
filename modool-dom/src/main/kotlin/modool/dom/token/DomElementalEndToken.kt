package modool.dom.token

internal abstract class DomElementalEndToken(override val start: DomElementalStartToken): DomEndToken(start)
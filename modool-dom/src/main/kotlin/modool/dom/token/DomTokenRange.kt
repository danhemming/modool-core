package modool.dom.token

sealed class DomTokenRange {
    object Empty : DomTokenRange()

    class Selection(val start: DomToken, val end: DomToken) : DomTokenRange() {

        fun detachAndRelinkChain() {
            val prior = start.prior
            val next = end.next

            // detach
            start.prior = null
            end.prior = null

            // reattach
            prior?.next = next
            next?.prior = prior
        }
    }
}
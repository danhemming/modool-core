package modool.dom.token

import modool.core.content.ContentOrigin
import modool.core.content.signifier.tag.TagBag
import modool.dom.style.Style
import modool.dom.style.Styleable

abstract class DomElementalStartToken(
        requiresParsing: Boolean,
        firstDisplayed: Boolean,
        contentOrigin: ContentOrigin,
        tags: TagBag,
        override var style: Style?
) : DomNodeContainerStartToken(requiresParsing, firstDisplayed, contentOrigin, tags),
        Styleable {

    abstract fun <T> isStylingEquivalentOf(styleRuleType: Class<T>): Boolean
}
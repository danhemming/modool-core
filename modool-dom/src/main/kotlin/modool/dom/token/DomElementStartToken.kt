package modool.dom.token

import modool.core.content.ContentOrigin
import modool.core.content.signifier.tag.TagBag
import modool.dom.style.Style

abstract class DomElementStartToken(
        requiresParsing: Boolean,
        firstDisplayed: Boolean,
        contentOrigin: ContentOrigin,
        tags: TagBag,
        style: Style?
): DomElementalStartToken(requiresParsing, firstDisplayed, contentOrigin, tags, style) {

    constructor(): this(
            requiresParsing = true, firstDisplayed = true,
            contentOrigin = ContentOrigin.API,
            tags = TagBag(),
            style = null)

    override fun <T> isStylingEquivalentOf(styleRuleType: Class<T>): Boolean {
        return styleRuleType.isAssignableFrom(javaClass)
    }

    override fun createEndToken(): DomEndToken {
        return DomElementEndToken(this)
    }

    override fun toPrettyDescription(): String {
        return "[Start of element: ${javaClass.canonicalName}]"
    }

    override fun inspect(tokenInspector: DomTokenInspector) {
        tokenInspector.openOutput("Element", javaClass)
        inspectRecursive(tokenInspector)
    }
}
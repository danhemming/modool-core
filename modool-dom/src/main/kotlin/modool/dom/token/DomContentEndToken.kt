package modool.dom.token

class DomContentEndToken(start: DomContentStartToken) : DomEndToken(start) {



    override fun toPrettyDescription(): String {
        return "[End of content]"
    }

    override fun inspect(tokenInspector: DomTokenInspector) {
        tokenInspector.closeOutput("End", javaClass)
    }
}
package modool.dom.token

import modool.core.content.signifier.Signifier
import modool.core.content.signifier.tag.Tag
import modool.core.content.signifier.tag.TagBag
import modool.core.content.signifier.tag.Taggable
import modool.dom.node.DomContentNode
import modool.dom.node.text.Renderer

/**
 * A marker node is a zero width content node.
 * A marker node is used to represent a parsable construct e.g. a list opening that does not have an explicit
 * representation in the source.
 * A marker node is dropped by the lexer to make explicit what is implicit in the source.
 */
class DomMarkerNode
    : DomContentNode(),
        Taggable {

    private val tags = TagBag()

    override val renderLength: Int
        get() = 0

    override fun signifies(signifier: Signifier): Boolean {
        return signifier is Tag && tags.contains(signifier)
    }

    override fun tag(tag: Tag): Boolean {
        return tags.add(tag)
    }

    override fun untag(tag: Tag): Boolean {
        return tags.remove(tag)
    }

    override fun toPrettyDescription(): String {
        return "->[MARKER]<-"
    }

    override fun inspect(tokenInspector: DomTokenInspector) {
        tokenInspector.outputLine("[MARKER]")
    }

    override fun render(renderer: Renderer) {
        return
    }
}
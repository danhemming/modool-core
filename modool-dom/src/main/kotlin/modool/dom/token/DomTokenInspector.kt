package modool.dom.token

interface DomTokenInspector {
    fun openOutput(token: DomToken, content: Class<*>?)
    fun openOutput(label: String, content: Class<*>?)
    fun output(content: String)
    fun outputLine()
    fun outputLine(content: String)
    fun closeOutput(token: DomToken, content: Class<*>?)
    fun closeOutput(label: String, content: Class<*>?)
}

internal class DefaultTokenInspector : DomTokenInspector {

    private val content = StringBuilder(2048)
    private var indentLevel: Int = 0

    override fun openOutput(token: DomToken, content: Class<*>?) {
        outputLine("<${token::class.java.prettyName}: ${content?.prettyName ?: "Unknown"}>")
        indentLevel++
    }

    override fun openOutput(label: String, content: Class<*>?) {
        outputLine("<$label: ${content?.prettyName ?: "Unknown"}>")
        indentLevel++
    }

    override fun outputLine(content: String) {
        outputLine()
        output(content)
    }

    override fun outputLine() {
        outputIndent()
    }

    override fun output(content: String) {
        this.content.append(content)
    }

    override fun closeOutput(token: DomToken, content: Class<*>?) {
        reduceIndent()
        outputLine("</${token::class.java.prettyName}: ${content?.prettyName ?: "Unknown"}>")
    }

    override fun closeOutput(label: String, content: Class<*>?) {
        reduceIndent()
        outputLine("</$label: ${content?.prettyName ?: "Unknown"}>")
    }

    private fun reduceIndent() {
        indentLevel--
        if (indentLevel < 0) {
            indentLevel = 0
            outputLine("--INDENT REDUCED BELOW ZERO--")
        }
    }

    private fun outputIndent() {
        if (content.isNotEmpty()) {
            content.appendln()
        }
        content.append("  ".repeat(indentLevel))
    }

    private val Class<*>.prettyName: String
        get() = if (packageName.isNotEmpty()) canonicalName.drop(packageName.length + 1) else canonicalName

    override fun toString(): String {
        return content.toString()
    }
}
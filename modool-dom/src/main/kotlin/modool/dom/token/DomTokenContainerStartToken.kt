package modool.dom.token

import modool.core.content.signifier.Signifier
import modool.core.content.signifier.tag.Tag
import modool.core.content.signifier.tag.TagBag
import modool.core.content.signifier.tag.TagCategory
import modool.core.content.token.TokenRangeOption

abstract class DomTokenContainerStartToken(
        var requiresParsing: Boolean,
        protected val tags: TagBag
) : DomStartToken(),
        DomTokenContainer {

    override fun tag(tag: Tag): Boolean {
        return tags.add(tag)
    }

    override fun untag(tag: Tag): Boolean {
        return tags.remove(tag)
    }

    override fun signifies(signifier: Signifier): Boolean {
        return signifier is Tag && tags.contains(signifier)
    }

    override fun signifies(category: TagCategory): Boolean {
        return tags.contains(category)
    }

    override fun asLocalTokenSequence(): Sequence<DomToken> = sequence {
        var candidate = next

        while (candidate != null && candidate != end) {
            yield(candidate!!)
            candidate = if (candidate is DomElementalStartToken) {
                candidate.end.next
            } else {
                candidate.next
            }
        }
    }

    override fun asRecursiveTokenSequence(): Sequence<DomToken> = sequence {
        var candidate = next

        while (candidate != null && candidate != end) {
            yield(candidate!!)
            candidate = candidate.next
        }
    }

    override fun isEmpty() = next == end

    override fun isNotEmpty() = !isEmpty()

    override fun contains(token: DomToken): Boolean {
        return asRecursiveTokenSequence().find { it == token } != null
    }

    override fun remove(token: DomToken): Boolean {
        val foundToken = asRecursiveTokenSequence().find { it == token }
        foundToken?.detachFromAndRelinkChain()
        return foundToken != null
    }

    override fun addTokenAtStart(token: DomToken) {
        this.insertExtractedChainNext(token)
    }

    override fun addTokenAtEnd(token: DomToken) {
        this.end.insertExtractedChainPrior(token)
    }

    override fun wrap(startToken: DomToken, endToken: DomToken, option: TokenRangeOption) {
        when (option) {
            TokenRangeOption.INCLUSIVE -> {
                startToken.insertDetachedPrior(start)
                endToken.end.insertDetachedNext(end)
            }
            TokenRangeOption.START_EXCLUSIVE_END_INCLUSIVE -> {
                startToken.insertDetachedNext(start)
                endToken.end.insertDetachedNext(end)
            }
            TokenRangeOption.START_INCLUSIVE_END_EXCLUSIVE -> {
                startToken.insertDetachedPrior(start)
                endToken.end.insertDetachedPrior(end)
            }
            TokenRangeOption.EXCLUSIVE -> {
                startToken.insertDetachedNext(start)
                endToken.end.insertDetachedPrior(end)
            }
        }
    }

    override fun expandRightToInclude(token: DomToken) {
        if (token != this.end) {
            token.insertExtractedChainNext(this.end, this.end)
        }
    }

    override fun expandLeftToInclude(token: DomToken) {
        if (token != this) {
            token.insertExtractedChainPrior(this, this)
        }
    }

    override fun clear() {
        insertDetachedNext(end)
    }

    override fun inspect(tokenInspector: DomTokenInspector) {
        tokenInspector.openOutput("Start", javaClass)
        inspectRecursive(tokenInspector)
    }
}
package modool.dom.coder

abstract class SimpleCoder : Coder {

    open fun encodeString(value: String?): String {
        return value.encodeAsString()
    }

    open fun encodeChar(value: Char?): String {
        return value.encodeAsString()
    }

    open fun encodeBoolean(value: Boolean?): String {
        return value.encodeAsString()
    }

    open fun encodeByte(value: Byte?): String {
        return value.encodeAsString()
    }

    open fun encodeShort(value: Short?): String {
        return value.encodeAsString()
    }

    open fun encodeInt(value: Int?): String {
        return value.encodeAsString()
    }

    open fun encodeLong(value: Long?): String {
        return value.encodeAsString()
    }

    open fun encodeFloat(value: Float?): String {
        return value.encodeAsString()
    }

    open fun encodeDouble(value: Double?): String {
        return value.encodeAsString()
    }

    fun decodeString(value: String): String {
        return value
    }

    open fun decodeChar(value: String): Char {
        return if (value.length == 1) value[0] else throw RuntimeException("Unable to decode $value into a character")
    }

    open fun decodeBoolean(value: String): Boolean {
        return value.toBoolean()
    }

    open fun decodeByte(value: String): Byte {
        return value.toByte()
    }

    open fun decodeShort(value: String): Short {
        return value.toShort()
    }

    open fun decodeInt(value: String): Int {
        return value.toInt()
    }

    open fun decodeLong(value: String): Long {
        return value.toLong()
    }

    open fun decodeFloat(value: String): Float {
        return value.toFloat()
    }

    open fun decodeDouble(value: String): Double {
        return value.toDouble()
    }

    fun decodeOptionalString(value: String): String? {
        return value.decodeOptional { it }
    }

    open fun decodeOptionalChar(value: String): Char? {
        return value.decodeOptional {
            if (value.length == 1) value[0] else throw RuntimeException("Unable to decode $value into a character")
        }
    }

    open fun decodeOptionalBoolean(value: String): Boolean? {
        return value.decodeOptional { it.toBoolean() }
    }

    open fun decodeOptionalByte(value: String): Byte? {
        return value.decodeOptional { it.toByte() }
    }

    open fun decodeOptionalShort(value: String): Short? {
        return value.decodeOptional { it.toShort() }
    }

    open fun decodeOptionalInt(value: String): Int? {
        return value.decodeOptional { it.toInt() }
    }

    open fun decodeOptionalLong(value: String): Long? {
        return value.decodeOptional { it.toLong() }
    }

    open fun decodeOptionalFloat(value: String): Float? {
        return value.decodeOptional { it.toFloat() }
    }

    open fun decodeOptionalDouble(value: String): Double? {
        return value.decodeOptional { it.toDouble() }
    }
    
    private fun Any?.encodeAsString(): String {
        return this?.toString() ?: ""
    }

    private inline fun <T> String.decodeOptional(notNull: (String) -> T): T? {
        return if (this == "") null else notNull(this)
    }
}
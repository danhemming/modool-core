package modool.dom.coder

interface Coded<T : Coder> {
    val coder: T
}
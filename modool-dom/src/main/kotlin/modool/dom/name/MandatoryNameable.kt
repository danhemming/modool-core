package modool.dom.name

interface MandatoryNameable : Nameable {
    override val name: String
}
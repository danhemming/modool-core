package modool.dom.name

interface MandatoryRenameable : MandatoryNameable {
    override var name: String
}
package modool.dom.name

interface Nameable {
    val name: String?
}
package modool.dom.name

interface Renameable : Nameable {
    override var name: String?
}
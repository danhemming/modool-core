package modool.dom.lexer

import modool.dom.node.DomTokenStore
import modool.lexer.CircularTextBlock
import modool.lexer.LexerContext
import modool.lexer.SourceReader
import java.io.ByteArrayInputStream
import java.io.InputStream
import java.nio.charset.Charset

object DomLexerContext {
    const val BUFFER_SIZE = 200

    fun from(content: String, charset: Charset = Charsets.UTF_8): LexerContext {
        val text = CircularTextBlock(BUFFER_SIZE)
        val store = DomTokenStore(text, copyOnWrite = false)
        return LexerContext(
                SourceReader(
                        ByteArrayInputStream(content.toByteArray(charset)),
                        text),
                store,
                store)
    }

    fun from(stream: InputStream): LexerContext {
        val text = CircularTextBlock(BUFFER_SIZE)
        val store = DomTokenStore(text, copyOnWrite = false)
        return LexerContext(SourceReader(stream, text), store, store)
    }
}
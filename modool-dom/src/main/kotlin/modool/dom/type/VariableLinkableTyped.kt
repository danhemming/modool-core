package modool.dom.type

import modool.dom.node.Linkable
import modool.dom.node.Referable

/**
 * In common parlance this is a type reference
 */
interface VariableLinkableTyped<T>
    : Linkable<T>,
        VariableTyped<T>
        where T : Type,
              T : Referable {

    override val type: T
        get() = referee
}
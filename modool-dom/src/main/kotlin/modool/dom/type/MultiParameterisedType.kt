package modool.dom.type

interface MultiParameterisedType<T : Type> : Type {
    val typeParameterTypes: Sequence<T>
}
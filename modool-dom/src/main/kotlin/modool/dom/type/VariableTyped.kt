package modool.dom.type

interface VariableTyped<T> : Typed<T> where T : Type {
    fun setTypeFromDescriptor(descriptor: String)
}
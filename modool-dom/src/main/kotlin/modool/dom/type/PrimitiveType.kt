package modool.dom.type

import modool.dom.identity.Identifier
import modool.dom.identity.NameIndexer
import modool.core.content.signifier.Signifier
import modool.core.content.signifier.tag.Tag
import modool.core.content.signifier.tag.TagBag
import modool.core.content.signifier.tag.TagCategory

interface PrimitiveType : Type

abstract class ModoolPrimitiveType(private val languageName: String)
    : PrimitiveType {

    private val tags = TagBag()

    override val identifier: Identifier by lazy { NameIndexer("$languageName::$name") }

    override fun tag(tag: Tag): Boolean {
        return tags.add(tag)
    }

    override fun untag(tag: Tag): Boolean {
        return tags.remove(tag)
    }

    override fun signifies(signifier: Signifier): Boolean {
        return signifier is Tag && tags.contains(signifier)
    }

    override fun signifies(category: TagCategory): Boolean {
        return tags.contains(category)
    }
}
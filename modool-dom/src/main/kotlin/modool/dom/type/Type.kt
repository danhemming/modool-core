package modool.dom.type

import modool.dom.Addressable
import modool.dom.node.Referable
import modool.core.content.signifier.tag.Taggable

interface Type
    : Addressable,
        Referable,
        Taggable {

    fun isAssignableFrom(typeDescriptor: String): Boolean {
        // TODO: needs to parse and resolve the type descriptor
        return this.name == typeDescriptor
    }

    fun isAssignableFrom(type: Type): Boolean {
        return type == this
    }
}


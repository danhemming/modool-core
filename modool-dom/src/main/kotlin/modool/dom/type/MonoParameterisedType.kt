package modool.dom.type

interface MonoParameterisedType<T : Type> : Type {
    val typeParameterType: T
}
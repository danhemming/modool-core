package modool.dom.type

interface ConstantTyped<T> : Typed<T> where T : Type
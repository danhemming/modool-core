package modool.dom.type

interface Typed<T> where T : Type {
    // Call into a resolver or execute logic to resolve a type e.g. using typeReference from a TypeReferencingElement
    val type: T
}

package modool.dom.type

interface TypeProvidable<T: TypeProvider> {
    val typeProvider: T
}
package modool.dom.meta

import modool.core.meta.property.PropertyMeta
import modool.dom.api.ApiAction

class ApiOperationConstant(
    val property: PropertyMeta,
    val action: ApiAction,
    val value: Any
)
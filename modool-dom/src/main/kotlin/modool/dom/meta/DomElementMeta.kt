package modool.dom.meta

import modool.core.meta.element.ElementMeta
import modool.dom.DomContext
import modool.dom.node.DomElementBacked

interface DomElementMeta<D: DomContext, T: DomElementBacked<D>> : ElementMeta {

    val type: Class<*>

    fun create(context: D): T

    override fun isType(other: ElementMeta): Boolean {
        return other is DomElementMeta<*, *> && type.isAssignableFrom(other.type)
    }
}
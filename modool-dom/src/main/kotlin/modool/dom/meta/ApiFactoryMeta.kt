package modool.dom.meta

import modool.core.meta.property.PropertyMeta

@Deprecated("Replaced with meta on api")
interface ApiFactoryMeta : ApiMemberMeta {
    val apiFactorySignatureProperties: List<PropertyMeta>
    val apiFactoryOptionalProperties: List<PropertyMeta>
}
package modool.dom.meta

/**
 * Interface that indicates if a particular element appears on the root api or not
 */
@Deprecated("Replaced with meta on api")
interface ApiRootFactoryMeta : ApiFactoryMeta
package modool.dom.meta

import modool.core.meta.Meta
import modool.core.meta.element.ElementMeta

interface ApiDecoratorMeta
    : Meta,
    ApiOperationResult {

    val element: ElementMeta
    val operations: List<ApiOperation>
}
package modool.dom.meta

import modool.core.meta.element.ElementMeta
import modool.dom.api.ApiAction

class ApiOperationSourceParameter(
    val element: ElementMeta,
    actions: List<ApiAction>
) : ApiOperationParameter(actions)
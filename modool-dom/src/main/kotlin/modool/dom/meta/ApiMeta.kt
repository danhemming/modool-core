package modool.dom.meta

import modool.core.meta.Meta

interface ApiMeta : Meta {
    val operations: List<ApiOperation>
}
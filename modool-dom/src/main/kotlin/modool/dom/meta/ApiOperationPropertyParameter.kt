package modool.dom.meta

import modool.core.meta.property.PropertyMeta
import modool.dom.api.ApiAction

class ApiOperationPropertyParameter(
    val property: PropertyMeta,
    actions: List<ApiAction>
) : ApiOperationParameter(actions)
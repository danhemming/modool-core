package modool.dom.meta

import modool.core.meta.element.ElementMeta

/**
 * Applied to collection properties to specify which elements have specific api methods
 */
@Deprecated("Replaced with meta on api")
interface ApiCollectionMeta {
    val apiCollectionMembers: List<ElementMeta>
}
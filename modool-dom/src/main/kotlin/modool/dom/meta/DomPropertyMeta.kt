package modool.dom.meta

import modool.core.meta.property.PropertyMeta
import modool.dom.DomContext
import modool.dom.node.DomPropertyContainer
import modool.dom.property.DomProperty

interface DomPropertyMeta<D, T, U> : PropertyMeta
        where D : DomContext,
              T : DomPropertyContainer,
              U : DomProperty<D> {

    fun getProperty(propertyContainer: T): U
}
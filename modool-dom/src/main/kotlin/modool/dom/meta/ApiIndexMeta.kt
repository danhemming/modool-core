package modool.dom.meta

import modool.core.meta.property.PropertyMeta

@Deprecated("Replaced with meta on api")
interface ApiIndexMeta {
    val apiIndexNameProperty: PropertyMeta
}
package modool.dom.meta

import modool.dom.api.ApiAction

abstract class ApiOperationParameter(val actions: List<ApiAction>)
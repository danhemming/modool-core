package modool.dom.meta

class ApiOperation(
    val name: String,
    val parameters: List<ApiOperationParameter>,
    val constants: List<ApiOperationConstant>,
    val result: ApiOperationResult
)
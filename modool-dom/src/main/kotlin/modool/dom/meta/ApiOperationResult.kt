package modool.dom.meta

interface ApiOperationResult {
    object None : ApiOperationResult
}
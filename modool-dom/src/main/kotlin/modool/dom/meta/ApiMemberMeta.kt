package modool.dom.meta

interface ApiMemberMeta {
    val apiName: String
}
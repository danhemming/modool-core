package modool.dom.dependency

sealed class NPM(override val scope: String,
                 open val packageName: String,
                 open val version: String): CodeDependency {

    data class Dependency(override val packageName: String,
                          override val version: String = "latest"):
            NPM("dependency", packageName, version)

    data class DevDependency(override val packageName: String,
                             override val version: String = "latest"):
            NPM("devDependency", packageName, version)
}
package modool.dom.dependency

interface CodeDependency: Dependency {
    // Scope refers to a compile context e.g. compile, runtime, test ... enum?
    val scope: String
}
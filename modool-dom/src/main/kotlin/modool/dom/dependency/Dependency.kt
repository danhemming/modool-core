package modool.dom.dependency

/**
 * dependency could potentially be many different kinds of things, but generally represents a prerequisite not
 * implied by a dom hierarchical relationship.
 */
interface Dependency
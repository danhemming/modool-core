package modool.dom.dependency

sealed class Maven(override val scope: String,
                   open val groupId: String,
                   open val artifactId: String,
                   open val version: String,
                   open val packaging: String): CodeDependency {

    data class Compile(override val groupId: String,
                       override val artifactId: String,
                       override val version: String,
                       override val packaging: String = "jar"):
            Maven("compile", groupId, artifactId, version, packaging)

    data class Runtime(override val groupId: String,
                       override val artifactId: String,
                       override val version: String,
                       override val packaging: String = "jar"):
            Maven("runtime", groupId, artifactId, version, packaging)

    data class Test(override val groupId: String,
                       override val artifactId: String,
                       override val version: String,
                       override val packaging: String = "jar"):
            Maven("test", groupId, artifactId, version, packaging)
}


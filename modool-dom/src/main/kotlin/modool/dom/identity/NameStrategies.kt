package modool.dom.identity

import modool.core.name.NameBuilder

fun caseSensitiveIdentityEquals(a: Identifier, b: Identifier): Boolean {
    return a.value.compareTo(b.value, ignoreCase = false) == 0
}

fun caseInsensitiveIdentityEquals(a: Identifier, b: Identifier): Boolean {
    return a.value.compareTo(b.value, ignoreCase = true) == 0
}

/**
 * Use for import statements where a trailing "*" can replace a classname
 */

fun caseSensitiveIdentityTrailingWildcardEquals(a: Identifier, b: Identifier): Boolean {
    return identityTrailingWildcardEquals(a, b, ignoreCase = false)
}

fun caseInsensitiveIdentityTrailingWildcardEquals(a: Identifier, b: Identifier): Boolean {
    return identityTrailingWildcardEquals(a, b, ignoreCase = true)
}

private fun identityTrailingWildcardEquals(a: Identifier, b: Identifier, ignoreCase: Boolean): Boolean {
    if (a.value == b.value) {
        return true
    }

    if (!a.value.endsWith("*") && !b.value.endsWith("*")) {
        return false
    }

    val qualifiedA = NameBuilder.fromEncodedWithSeparators(a.value)
    val qualifiedB = NameBuilder.fromEncodedWithSeparators(b.value)

    qualifiedA.removeLastPart()
    qualifiedB.removeLastPart()

    return qualifiedA.toString().compareTo(qualifiedB.toString(), ignoreCase) == 0
}

fun caseSensitiveParameterisedIdentityEquals(signature: Identifier, test: Identifier): Boolean {
    return parameterisedIdentityEquals(signature, test, ignoreCase = false)
}

fun caseInsensitiveParameterisedIdentityEquals(signature: Identifier, test: Identifier): Boolean {
    return parameterisedIdentityEquals(signature, test, ignoreCase = true)
}

private fun parameterisedIdentityEquals(signature: Identifier, test: Identifier, ignoreCase: Boolean): Boolean {
    if (signature !is ParameterisedPropertyIdentifier<*, *, *, *> || test !is ParameterisedIndexer) {
        return false
    }

    if (signature.name.compareTo(test.name, ignoreCase) != 0) {
        return false
    }

    if (signature.parameters.count() != test.parameters.count()) {
        return false
    }

    signature.parameters.zip(test.parameters).forEach { (signatureParam, testParam) ->
        if (!testParam.isMatch(signatureParam)) return false
    }

    return true
}
package modool.dom.identity

import modool.dom.DomContext
import modool.dom.node.DomElementBacked
import modool.dom.node.Parameter
import modool.dom.property.sequence.element.map.DomElementBackedMappableProperty
import modool.dom.property.value.DomNameProperty
import modool.dom.type.Type

class ParameterisedPropertyIdentifier<D, T, P, C>(
        equalityStrategy: (Identifier, Identifier) -> Boolean,
        nameProperty: DomNameProperty<D, String>,
        private val typesProperty: C
) : NamePropertyIdentifier<D>(equalityStrategy, nameProperty),
        ParameterisedIdentifier
        where D : DomContext,
              T : Type,
              P : DomElementBacked<D>,
              P : Identifiable,
              P : Parameter<T>,
              C : DomElementBackedMappableProperty<D, P, *> {

    override val value: String
        get() = "$name${parameters.joinToString(prefix = "(", postfix = ")", separator = ",") { it.type.name }}"

    val parameters: Sequence<Parameter<T>> get() = typesProperty
}
package modool.dom.identity

/**
 * Something is identifiable if it's name or some composite involving it's name is unique
 */
interface Identifiable {
    val identifier: Identifier
}
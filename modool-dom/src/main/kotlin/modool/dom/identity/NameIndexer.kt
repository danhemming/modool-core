package modool.dom.identity

/**
 * Can be used to compare with other name only identifiers on elements e.g. NamePropertyIdentifier
 */
class NameIndexer(override val name: String): ElementIndexer(), NameIdentifier {
    override val value: String
        get() = name
}
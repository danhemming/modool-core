package modool.dom.identity

interface ParameterisedIdentifier
    : NameIdentifier {

    override fun isEquivalentType(other: Class<out Any>): Boolean {
        return ParameterisedIdentifier::class.java.isAssignableFrom(other)
    }
}
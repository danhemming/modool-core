package modool.dom.identity

class ParameterisedIndexer(
        override val name: String,
        val parameters: Sequence<ParameterisedIndexerParameter>
) : ElementIndexer(),
        ParameterisedIdentifier {

    override val value: String
        get() = "$name${parameters.joinToString(prefix = "(", postfix = ")", separator = ",") { it.typeDescriptor }}"
}
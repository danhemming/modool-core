package modool.dom.identity

abstract class ElementIdentifier(private val equalityStrategy: (Identifier, Identifier) -> Boolean) : Identifier {

    final override fun equals(other: Any?): Boolean {
        return other != null && other is Identifier &&
                equalityStrategy(this, other) &&
                // If the identifier classes are different types then we allow the other class a vote on the equality
                (isEquivalentType(other::class.java) || other == this)
    }

    final override fun hashCode(): Int {
        return value.hashCode()
    }

    override fun toString(): String {
        return value
    }
}
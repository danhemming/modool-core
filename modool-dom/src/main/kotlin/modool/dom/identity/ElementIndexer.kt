package modool.dom.identity

abstract class ElementIndexer : Identifier {

    final override fun equals(other: Any?): Boolean {
        return when (other) {
            null -> false
            is ElementIdentifier -> other == this
            is ElementIndexer -> value == other.value
            else -> false
        }
    }

    final override fun hashCode(): Int {
        return value.hashCode()
    }

    override fun toString(): String {
        return value
    }
}
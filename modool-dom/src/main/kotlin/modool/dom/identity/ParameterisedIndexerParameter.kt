package modool.dom.identity

import modool.dom.node.Parameter
import modool.dom.type.Type
import modool.dom.name.MandatoryNameable

sealed class ParameterisedIndexerParameter(
        override val name: String
) : MandatoryNameable {
    abstract val typeDescriptor: String
    abstract fun isMatch(parameter: Parameter<*>): Boolean

    class Description(
            override val name: String,
            override val typeDescriptor: String
    ) : ParameterisedIndexerParameter(name) {

        override fun isMatch(parameter: Parameter<*>): Boolean {
            // FIXME: Without resolution all parameter types are some flavour of Unknown so we will use name for now
            return parameter.name == name
        }
    }

    class ConcreteType(
            override val name: String,
            private val type: Type
    ) : ParameterisedIndexerParameter(name) {

        override val typeDescriptor: String
            get() = type.name

        override fun isMatch(parameter: Parameter<*>): Boolean {
            return parameter.type.isAssignableFrom(type)
        }
    }
}

infix fun <T : Type> String.of(type: T) = ParameterisedIndexerParameter.ConcreteType(this, type)
infix fun String.of(typeDescriptor: String) = ParameterisedIndexerParameter.Description(this, typeDescriptor)
package modool.dom.identity

import modool.dom.DomContext
import modool.dom.property.value.DomNameProperty

open class NamePropertyIdentifier<out D>(
        equalityStrategy: (Identifier, Identifier) -> Boolean,
        private val nameProperty: DomNameProperty<D, String>
) : ElementIdentifier(equalityStrategy),
        NameIdentifier
        where D : DomContext {

    override val value: String
        get() = name

    override val name: String
        get() = nameProperty.get()
}
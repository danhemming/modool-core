package modool.dom.identity

interface NameIdentifier : Identifier {
    val name: String
}
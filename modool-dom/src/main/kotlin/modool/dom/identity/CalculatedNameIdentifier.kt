package modool.dom.identity

open class CalculatedNameIdentifier(
        equalityStrategy: (Identifier, Identifier) -> Boolean,
        private val nameResolver: () -> String
) : ElementIdentifier(equalityStrategy),
        NameIdentifier {

    override val value: String
        get() = name

    override val name: String
        get() = nameResolver()
}
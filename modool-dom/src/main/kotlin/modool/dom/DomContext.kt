package modool.dom

import modool.core.content.ContentOrigin
import modool.core.content.SourceProcessingLevel
import modool.core.content.token.TokenRangeOption
import modool.core.content.token.TokenReader
import modool.core.meta.MetaFactory
import modool.core.meta.element.ElementMeta
import modool.dom.lexer.DomLexerContext
import modool.dom.meta.DomElementMeta
import modool.dom.node.*
import modool.dom.node.text.DomTerminalNode
import modool.dom.node.text.factory.CommonDynamicTerminals
import modool.dom.style.StyleProvider
import modool.dom.token.*
import modool.lexer.*
import modool.lexer.rule.LexerRule
import modool.lexer.rule.unknown.IgnoreUnknownLexerRule
import modool.parser.*
import modool.parser.rule.ParserRulable
import modool.parser.rule.calculateAnchors
import modool.parser.strategy.ParseStrategy
import java.io.ByteArrayInputStream
import java.io.InputStream
import java.nio.charset.Charset

interface DomContext {
    val meta: MetaFactory
    val dom: DomFactory
    val lexer: LexerFactory
    val parser: ParserFactory
    val styler: StyleProvider
    val defaultParseStrategy: ParseStrategy

    fun <T> process(
            processingLevel: SourceProcessingLevel,
            source: String,
            charset: Charset = Charsets.UTF_8,
            lexerRulesProvider: LexerRulesProvider,
            parserRuleProvider: ParserRuleProvider,
            parseStrategy: ParseStrategy = defaultParseStrategy,
            contentOrigin: ContentOrigin.Source = ContentOrigin.PrimarySource,
            type: Class<T>): DomNodeContainerStartToken
            where T : DomElementBacked<*> {
        when (processingLevel) {
            SourceProcessingLevel.NONE -> {
                val result = DomPseudoElementStartToken(type = type, contentOrigin = contentOrigin)
                result.addTokenAtStart(CommonDynamicTerminals.unknown(source))
                return result
            }
            SourceProcessingLevel.TOKENISE -> {
                val result = DomPseudoElementStartToken(type = type, contentOrigin = contentOrigin)
                val recoveryStrategy = lexer.createRecoveryStrategy()
                val tokenReader = Lexer(
                        lexer.createUnknownLexerRule(),
                        ObservableLexerRuleTracker(lexerRulesProvider.createLexerRules(), recoveryStrategy),
                        recoveryStrategy,
                        lexer.createCommitStrategy())
                        .tokenise(DomLexerContext.from(source, charset))

                if (!tokenReader.moveToEnd()) {
                    throw TokenisationException(source)
                }

                val domReaderProvider = tokenReader.createProviderView(DomTokenReaderDomProviderView::class.java)
                val startToken = domReaderProvider.startToken
                val endToken = domReaderProvider.startToken.end
                // Any successful tokenisation should include a set of wrapper tokens
                if (endToken is DomContentEndToken && startToken.next != null && endToken.prior != null) {
                    result.wrap(startToken, endToken, TokenRangeOption.EXCLUSIVE)
                    return result
                } else {
                    throw TokenisationException(source)
                }
            }
            SourceProcessingLevel.PARSE -> {
                val parseResult = parse(
                        lexerRulesProvider.createLexerRules(),
                        parserRuleProvider.createParserRule(),
                        source, charset,
                        parseStrategy,
                        contentOrigin)
                if (parseResult is ParseResult.Content && parseResult.reference is DomToken) {
                    return (parseResult.reference as DomToken).findNext<DomElementStartToken>()
                            ?: throw ParseException(source)
                } else {
                    throw ParseException(source)
                }
            }
        }
    }

    fun tokenise(
            rules: List<LexerRule>,
            source: String,
            charset: Charset = Charsets.UTF_8): TokenReader {
        val recoveryStrategy = lexer.createRecoveryStrategy()
        return Lexer(
                lexer.createUnknownLexerRule(),
                ObservableLexerRuleTracker(rules, recoveryStrategy),
                recoveryStrategy,
                lexer.createCommitStrategy())
                .tokenise(DomLexerContext.from(source, charset))
    }

    fun tokenise(
            rules: List<LexerRule>,
            source: InputStream): TokenReader {
        val recoveryStrategy = lexer.createRecoveryStrategy()
        return Lexer(
                lexer.createUnknownLexerRule(),
                ObservableLexerRuleTracker(rules, recoveryStrategy),
                recoveryStrategy,
                lexer.createCommitStrategy()).tokenise(DomLexerContext.from(source))
    }

    fun parse(
            lexerRules: List<LexerRule>,
            parserRule: ParserRulable,
            source: InputStream,
            strategy: ParseStrategy = defaultParseStrategy,
            contentOrigin: ContentOrigin.Source = ContentOrigin.PrimarySource): ParseResult {
        return parse(parserRule, tokenise(lexerRules, source), strategy, contentOrigin)
    }

    fun parse(
            lexerRules: List<LexerRule>,
            parserRule: ParserRulable,
            source: String,
            charset: Charset = Charsets.UTF_8,
            strategy: ParseStrategy = defaultParseStrategy,
            contentOrigin: ContentOrigin.Source = ContentOrigin.PrimarySource): ParseResult {
        return parse(parserRule, tokenise(lexerRules, source, charset), strategy, contentOrigin)
    }

    fun parse(
            rule: ParserRulable,
            source: TokenReader,
            strategy: ParseStrategy = defaultParseStrategy,
            contentOrigin: ContentOrigin.Source = ContentOrigin.PrimarySource): ParseResult {

        val elementRepo = DomElementRepository(this)
        val parseContext = ParserContext(source, elementRepo, contentOrigin)
        val parseResult = rule.parse(parseContext, strategy)

        if (parseResult is ParseResult.Success) {

            return when {
                parseContext.source.isAnotherTerminalAvailable() -> {
                    val domReaderProvider = source.createProviderView(DomTokenReaderDomProviderView::class.java)
                    ParseResult.PartialContent(domReaderProvider.startToken)
                }
                parseContext.source.isMoreContentAvailable() -> {
                    val domReaderProvider = source.createProviderView(DomTokenReaderDomProviderView::class.java)
                    // Incorporate the trailing white space content into the identified end element
                    val endOfDocument = domReaderProvider.startToken.end
                    val lastElement = endOfDocument.findPrior<DomElement<*>>()
                    endOfDocument.prior?.let { lastElement?.expandRightToInclude(it) }

                    ParseResult.CompleteContent(domReaderProvider.startToken)
                }
                else -> {
                    val domReaderProvider = source.createProviderView(DomTokenReaderDomProviderView::class.java)
                    ParseResult.CompleteContent(domReaderProvider.startToken)
                }
            }
        }

        return parseResult
    }

    fun <T : DomElementBacked<*>> parseSequence(
            lexerRules: List<LexerRule>,
            parserRule: ParserRulable,
            element: ElementMeta,
            source: String,
            charset: Charset = Charsets.UTF_8,
            strategy: ParseStrategy = defaultParseStrategy): Sequence<T> {
        return parseSequence(
                lexerRules,
                parserRule,
                element,
                ByteArrayInputStream(source.toByteArray(charset)), strategy)
    }

    /**
     * Parses a sequence of matching elements
     * Essentially uses the lexer to match any possible token used in the element rule.  The parser takes over at this
     * point and pulls the tokens it needs.  This mechanism is therefore lazy in the production of tokens.
     */
    fun <T : DomElementBacked<*>> parseSequence(
            lexerRules: List<LexerRule>,
            parserRule: ParserRulable,
            element: ElementMeta,
            source: InputStream,
            strategy: ParseStrategy = defaultParseStrategy,
            contentOrigin: ContentOrigin.Source = ContentOrigin.PrimarySource) = sequence<T> {

        // The anchors here should just be a list of top level terminals for the element (non-recursive)
        val anchors = parserRule.calculateAnchors()
        val text = CircularTextBlock(DomLexerContext.BUFFER_SIZE)

        // Use a special store that knows to filter out the stuff we aren't interested in
        val anchoredTokenWriter = DomAnchoredTokenStore(text, copyOnWrite = false, anchors = anchors)
        val elementFilteringLexerContext = LexerContext(
                SourceReader(source, text), anchoredTokenWriter, anchoredTokenWriter)

        val recoveryStrategy = lexer.createRecoveryStrategy()
        val lexer = Lexer(
                IgnoreUnknownLexerRule,
                ObservableLexerRuleTracker(lexerRules, recoveryStrategy),
                recoveryStrategy,
                lexer.createCommitStrategy())
        val tokenReader = lexer.tokenise(elementFilteringLexerContext)
        val domTokenReader = tokenReader.createProviderView(DomTokenReaderDomProviderView::class.java)
        val elementRepo = DomElementRepository(this@DomContext)
        val parseContext = ParserContext(tokenReader, elementRepo, contentOrigin)

        do {
            // We may have overshot the end of the element during parsing so scan what we have
            var lookAheadToken = domTokenReader.currentToken.next
            val constantFactory = (lookAheadToken as? DomTerminalNode)?.constantFactory
            while (lookAheadToken != null
                    && (constantFactory == null || !anchoredTokenWriter.isAcceptable(constantFactory))) {
                lookAheadToken = lookAheadToken.next
                tokenReader.moveNextContent()
            }

            val parseResult = parserRule.parse(parseContext, strategy)
            if (parseResult is ParseResult.Success && elementRepo.isElementAt(tokenReader.createReference(), element)) {
                yield(elementRepo.getAsType(tokenReader.createReference()))
            }

            anchoredTokenWriter.isElementInProgress = false

        } while (anchoredTokenWriter.isInProgress)
    }

    /**
     * This method is called to initialise those properties not configured by parsed content e.g. optional content
     */
    fun <T> ensureAbstractContent(node: T)
            where T : DomNodeContentManager {
        node.ensureAbstractContent()
    }

    fun <T> ensureConcreteContent(node: T)
            where T : DomNodeContainer,
                  T : DomNodeContentManager {
        val builder = createConcreteContentBuilder(node)
        node.ensureConcreteContent(builder)
    }

    fun createConcreteContentBuilder(
            container: DomNodeContainer,
            visitedTerminals: MutableList<DomTerminalNode>? = null): ConcreteContentBuilder {
        return DefaultConcreteContentBuilder(container, visitedTerminals)
    }

    fun <D, T> removeElement(element: T): Boolean
            where D : DomContext,
                  T : DomElement<D> {
        // TODO: Move to element?
        // Abstract removal
        element.detachFromParent()
        // Concrete removal
        element.detachFromAndRelinkChain()
        return true
    }
}

/**
 * Should only be used by the parser as that requires both abstract and concrete construction
 */
fun <D, T> D.buildElement(
        meta: DomElementMeta<D, T>,
        contentOrigin: ContentOrigin = ContentOrigin.API,
        initializer: (T.() -> Unit)? = null
): T
        where D : DomContext,
              T : DomElementBacked<D> {

    return buildElementContent(meta.create(this), contentOrigin, initializer)
}

fun <D, T> D.buildElement(
        factory: (D) -> T,
        contentOrigin: ContentOrigin = ContentOrigin.API,
        initializer: (T.() -> Unit)? = null
): T
        where D : DomContext,
              T : DomElementBacked<D> {

    return buildElementContent(factory(this), contentOrigin, initializer)
}

private fun <D, T> D.buildElementContent(
        obj: T,
        contentOrigin: ContentOrigin = ContentOrigin.API,
        initializer: (T.() -> Unit)? = null): T
        where D : DomContext,
              T : DomElementBacked<D> {

    obj.backing.contentOrigin = contentOrigin

    if (contentOrigin == ContentOrigin.API) {
        // Builder will require the containers to have the correct parents e.g. View
        ensureAbstractContent(obj.backing)
        // Builder will require the layout to have been performed e.g. View to be in place
        ensureConcreteContent(obj.backing)
        initializer?.invoke(obj)
    } else {
        // Calling the builder before creating abstract content ensures the parser gets a chance use tokens before
        // any defaults are applied (saving unnecessary object creation)
        initializer?.invoke(obj)
        ensureAbstractContent(obj.backing)
        ensureConcreteContent(obj.backing)
    }
    return obj
}
package modool.dom

import modool.dom.name.MandatoryRenameable

interface DynamicAddressable
    : Addressable,
        MandatoryRenameable


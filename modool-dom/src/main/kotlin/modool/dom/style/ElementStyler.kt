package modool.dom.style

import modool.dom.node.DomNodeContainer
import modool.dom.node.DomWhitespaceNode
import modool.dom.node.text.DomTerminalNode
import modool.dom.style.builder.StyleRuleProvider
import modool.dom.token.*

/**
 * Padding is dynamic based on surrounding structural content
 * 1. User defined element layout e.g. lambda on one line or many?
 * 2. Indentation (not dynamic per-se but requires the dynamic new lines to be in place)
 * 3. Line break formatting
 * Note: 3 causes 2
 */
object ElementStyler : Styler() {

    data class RefactorOption<T>(
            val container: T,
            val state: StyleContext.OpenElementState,
            val styleRuleProvider: StyleRuleProvider,
            val refactorStyleRuleProvider: StyleRuleProvider
    ) where T : DomNodeContainer, T : DomToken

    fun format(context: StyleContext, container: DomNodeContainerStartToken): Boolean {
        return format(context, container, mutableListOf(), isRoot = true)
    }

    private fun format(
            context: StyleContext,
            container: DomNodeContainerStartToken,
            accumulatedRefactorOptions: MutableList<RefactorOption<DomNodeContainerStartToken>>,
            isRoot: Boolean): Boolean {
        return format(context, container, container.start, accumulatedRefactorOptions, isRoot)
    }

    private fun format(
            context: StyleContext,
            container: DomNodeContainerStartToken,
            containerFrom: DomToken,
            accumulatedRefactorOptions: MutableList<RefactorOption<DomNodeContainerStartToken>>,
            isRoot: Boolean): Boolean {

        if (!container.displayed) {
            return false
        }

        val isWholeContainerProcessing = containerFrom == container.start
        val priorAccumulatedState = context.beginContainer()

        if (isWholeContainerProcessing) {
            context.setupRelativeFormatter(container)
            context.applyOpenFormatting(container)
        }

        var changed = false
        val scopedRefactorOptions = if (context.isPriorityRefactorScope) mutableListOf() else accumulatedRefactorOptions
        val scopeRefactorIndex = if (context.isPriorityRefactorScope) 0 else scopedRefactorOptions.count()

        val applyFormatting = container.contentOrigin.isFormattable()
        var applyFormattingNextTerminal = applyFormatting || context.isPaddingRequired
        var current: DomToken? = containerFrom.next

        while (current != null && current != container.end) {

            if (current is DomNodeContainerStartToken && !current.displayed) {
                current = current.end.next
                continue
            }

            if (current is DomPropertyStartToken) {
                @Suppress("UNCHECKED_CAST")
                changed = format(context, current, scopedRefactorOptions, isRoot = false) || changed
                applyFormattingNextTerminal = applyFormatting || context.isPaddingRequired
                current = current.end.next
                continue
            }

            if (current is DomPropertyEndToken) {
                context.applyTemporaryFormatter(current.start) {
                    context.applyCloseFormatting(this)
                }
            }

            if (current is DomElementalStartToken) {
                @Suppress("UNCHECKED_CAST")
                changed = format(context, current, scopedRefactorOptions, isRoot = false) || changed
                applyFormattingNextTerminal = applyFormatting || context.isPaddingRequired
                current = current.end.next
                continue
            }

            if (current is DomElementalEndToken) {
                context.applyTemporaryFormatter(current.start) {
                    context.applyCloseFormatting(this)
                }
            }

            if (current is DomTerminalNode) {

                if (context.isStartOfContent && context.indentSize > 0) {
                    context.requestPaddingRight(context.indentSize)
                }

                if (applyFormattingNextTerminal) {
                    changed = applyPadding(context, current) || changed
                    context.resetPadding()
                    applyFormattingNextTerminal = applyFormatting
                }

                context.registerContent(current.renderLength)
            }

            if (current is DomWhitespaceNode) {
                if (current.isNewLine) {
                    context.registerLineBreak()
                    if (applyFormatting) changed = applyIndent(context, current) || changed
                }
                context.registerContent(current.marginX)
            }

            current = current.next
        }

        context.applyCloseFormatting(container)

        return if (refactor(context, priorAccumulatedState, container, scopedRefactorOptions, scopeRefactorIndex, isRoot)) {
            true
        } else {
            context.commitContainer(priorAccumulatedState, resetUpcomingRequirements = isWholeContainerProcessing)
            changed
        }
    }

    private fun refactor(
            context: StyleContext,
            beginStateForContainer: StyleContext.OpenElementState,
            container: DomNodeContainerStartToken,
            accumulatedRefactorOptions: MutableList<RefactorOption<DomNodeContainerStartToken>>,
            accumulatedRefactorIndex: Int,
            isRoot: Boolean): Boolean {

        val reformattingStyleProvider = context.getConditionalRefactorFormatter(container)
                ?: return (isRoot || context.isPriorityRefactorScope)
                        && applyAccumulatedRefactoringFormatter(context, container, accumulatedRefactorOptions)

        return when {
            context.isPriorityRefactorScope -> {
                applyRefactoringFormatter(context, beginStateForContainer, container, reformattingStyleProvider)
                        || applyAccumulatedRefactoringFormatter(context, container, accumulatedRefactorOptions)
            }

            context.isNestedInPriorityRefactorScope -> {
                accumulatedRefactorOptions.add(
                        accumulatedRefactorIndex,
                        RefactorOption(
                                container, beginStateForContainer,
                                context.getEffectiveFormatter(), reformattingStyleProvider))
                false
            }

            else ->
                applyRefactoringFormatter(context, beginStateForContainer, container, reformattingStyleProvider)
        }
    }

    private fun applyRefactoringFormatter(
            context: StyleContext,
            beginStateForContainer: StyleContext.OpenElementState,
            container: DomNodeContainerStartToken,
            refactorStyleRuleProvider: StyleRuleProvider): Boolean {

        TerminalStyler.formatContainer(context.getEffectiveFormatter(), container)
        return context.applyTemporaryConditionalFormatter(refactorStyleRuleProvider, container) {
            context.rollBackContainer(beginStateForContainer)
            format(context, container)
        }
    }

    private fun applyAccumulatedRefactoringFormatter(
            context: StyleContext,
            container: DomNodeContainerStartToken,
            accumulatedRefactorOptions: List<RefactorOption<DomNodeContainerStartToken>>): Boolean {

        return accumulatedRefactorOptions.count() > 0
                && accumulatedRefactorOptions
                .asSequence()
                .map { bookMarked ->
                    applyOutOfFlowRefactoringFormatterThenRecoverState(
                            context,
                            container,
                            bookMarked.state,
                            bookMarked.container,
                            bookMarked.styleRuleProvider,
                            bookMarked.refactorStyleRuleProvider)
                }
                .firstOrNull() == true
    }

    private fun applyOutOfFlowRefactoringFormatterThenRecoverState(
            context: StyleContext,
            parentContainer: DomNodeContainerStartToken,
            bookMarkedContainerBeginState: StyleContext.OpenElementState,
            bookMarkedContainer: DomNodeContainerStartToken,
            bookMarkedEffectiveStyleRuleProvider: StyleRuleProvider,
            refactorStyleRuleProvider: StyleRuleProvider): Boolean {

        // Clear elemental formatting for the bookmarked container
        TerminalStyler.formatTokenRange(bookMarkedEffectiveStyleRuleProvider, bookMarkedContainer.start, bookMarkedContainer.end)

        val refactorChangedContent = context.applyTemporaryConditionalFormatter(refactorStyleRuleProvider, bookMarkedContainer) {
            // Apply the conditional / refactor formatter to the bookmarked container
            context.rollBackContainer(bookMarkedContainerBeginState)
            TerminalStyler.formatContainer(context.getEffectiveFormatter(), bookMarkedContainer)
            format(context, bookMarkedContainer, mutableListOf(), isRoot = true)
        }

        // Clear elemental formatting for the remainder of the block
        TerminalStyler.formatTokenRange(context.getEffectiveFormatter(), bookMarkedContainer.end, parentContainer.end)
        return format(context, parentContainer, bookMarkedContainer.end, mutableListOf(), isRoot = true) || refactorChangedContent
    }

    private fun applyPadding(
            context: StyleContext,
            content: DomTerminalNode): Boolean {

        if (!context.isPaddingRequired) {
            return false
        }

        val priorContent = content.priorDisplayedContent
        val priorMarginY = (priorContent as? DomWhitespaceNode)?.marginY ?: 0
        val priorMarginX = (priorContent as? DomWhitespaceNode)?.marginX ?: 0
        var changed = priorContent?.let {
            applyContextualNodePadding(it, content, context.requiredX, context.requiredY, absolute = false)
        } ?: false

        // Whitespace may have been inserted so reread
        val priorWhitespace = priorContent as? DomWhitespaceNode
                ?: content.priorDisplayedContent as? DomWhitespaceNode

        if (priorWhitespace == null) {
            // Start of content (Support indent but not gap i.e y = 0)
            changed = applyNodePadding(content, context.requiredX, 0, absolute = false)
        } else {
            val deltaY = priorWhitespace.marginY - priorMarginY
            if (deltaY > 0) {
                // remove the previously counted trailing spaces (replaced with EOLNs now)
                context.registerContent(-priorMarginX)
                repeat(deltaY) { context.registerLineBreak() }
                changed = applyIndent(context, priorWhitespace) || changed
                context.registerContent(priorWhitespace.marginX)
            } else {
                // We don't account for the removal of EOLNs (we can't recover context.column anyway)
                context.registerContent(priorWhitespace.marginX - priorMarginX)
            }
        }

        return changed
    }

    private fun applyIndent(
            context: StyleContext,
            newLine: DomWhitespaceNode): Boolean {

        val delta = context.indentSize - newLine.marginX

        return when {
            delta > 0 -> {
                repeat(delta) { newLine.addTrailingSpace() }
                true
            }
            delta < 0 -> {
                repeat(-delta) { newLine.removeTrailingSpace() }
                true
            }
            else -> false
        }
    }
}
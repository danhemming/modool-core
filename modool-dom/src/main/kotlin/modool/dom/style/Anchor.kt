package modool.dom.style

interface Anchor : Style {

    object Top : Anchor {
        override val requestRight: Int = 0
        override val requestBottom: Int = 0

        override fun applyLeft(currentX: Int): Int {
            return currentX
        }

        override fun applyTop(currentY: Int): Int {
            return 0
        }
    }

    object Left : Anchor {
        override val requestRight: Int = 0
        override val requestBottom: Int = 0

        override fun applyLeft(currentX: Int): Int {
            return 0
        }

        override fun applyTop(currentY: Int): Int {
            return currentY
        }
    }
}
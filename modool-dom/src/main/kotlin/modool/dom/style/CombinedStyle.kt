package modool.dom.style

import kotlin.math.max

class CombinedStyle(
        private val styles: List<Style>
) : Style {

    override val requestRight: Int = styles.fold(0) { total, style -> max(style.requestRight, total) }
    override val requestBottom: Int = styles.fold(0) { total, style -> max(style.requestBottom, total) }

    override fun applyLeft(currentX: Int): Int {
        var totalLeft = currentX
        styles.forEach {
            totalLeft = it.applyLeft(totalLeft)
        }
        return totalLeft
    }

    override fun applyTop(currentY: Int): Int {
        var totalTop = currentY
        styles.forEach {
            totalTop = it.applyTop(totalTop)
        }
        return totalTop
    }
}
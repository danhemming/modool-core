package modool.dom.style

interface Flow : Style {

    object Inline : Flow {
        override val requestRight: Int = 0
        override val requestBottom: Int = 0

        override fun applyLeft(currentX: Int): Int {
            return currentX
        }

        override fun applyTop(currentY: Int): Int {
            return 0
        }
    }

    object Block : Flow {
        override val requestRight: Int = 0
        override val requestBottom: Int = 1

        override fun applyLeft(currentX: Int): Int {
            return currentX
        }

        override fun applyTop(currentY: Int): Int {
            return if (currentY > 0) currentY else 1
        }
    }
}
package modool.dom.style

interface DisplayStyleable {
    var displayed: Boolean
}
package modool.dom.style

import java.lang.Integer.max

interface Padding : Style {
    /**
     * Used with something like a bracket which is not separated from surrounding content
     */
    object None : Padding {
        override val requestRight: Int  = 0
        override val requestBottom: Int = 0

        override fun applyLeft(currentX: Int): Int {
            return currentX
        }

        override fun applyTop(currentY: Int): Int {
            return currentY
        }
    }

    object Pad : Padding {
        override val requestRight: Int  = 1
        override val requestBottom: Int = 0

        override fun applyLeft(currentX: Int): Int {
            return max(currentX, 1)
        }

        override fun applyTop(currentY: Int): Int {
            return currentY
        }

        object Left : Padding {
            override val requestRight: Int  = 0
            override val requestBottom: Int = 0

            override fun applyLeft(currentX: Int): Int {
                return max(currentX, 1)
            }

            override fun applyTop(currentY: Int): Int {
                return currentY
            }
        }

        object Right : Padding {
            override val requestRight: Int  = 1
            override val requestBottom: Int = 0

            override fun applyLeft(currentX: Int): Int {
                return currentX
            }

            override fun applyTop(currentY: Int): Int {
                return currentY
            }
        }
    }

    class Custom(
            val top: Int = 0,
            override val requestRight: Int = 0,
            override val requestBottom: Int = 0,
            val left: Int = 0) : Padding {

        override fun applyLeft(currentX: Int): Int {
            return currentX + left
        }

        override fun applyTop(currentY: Int): Int {
            return currentY + top
        }
    }
}
package modool.dom.style

class StyleVariableScope(private val parent: StyleVariableScope?) {
    private val flags = mutableMapOf<String, Boolean>()

    fun defineFlag(name: String) {
        flags[name] = false
    }

    fun setFlag(name: String) {
        if (flags.containsKey(name)) {
            flags[name] = true
        }
    }

    fun unsetFlag(name: String) {
        if (flags.containsKey(name)) {
            flags[name] = false
        }
    }

    fun removeFlag(name: String) {
        flags.remove(name)
    }

    fun isSet(name: String): Boolean {
        // Only check parent if it has not been defined in local scope
        return flags[name] ?: parent?.isSet(name) ?: false
    }

    fun copy(): StyleVariableScope {
        // Note: this is not a deep copy, just a copy of the local flags
        return StyleVariableScope(parent).apply { this.flags.putAll(this@StyleVariableScope.flags) }
    }
}
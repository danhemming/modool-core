package modool.dom.style

interface Layout : Style {

    object Break {
        object Before : Layout {
            override val requestRight: Int = 0
            override val requestBottom: Int = 0

            override fun applyLeft(currentX: Int): Int {
                return currentX
            }

            override fun applyTop(currentY: Int): Int {
                return if (currentY > 0) currentY else 1
            }
        }

        object After : Layout {
            override val requestRight: Int = 0
            override val requestBottom: Int = 1

            override fun applyLeft(currentX: Int): Int {
                return currentX
            }

            override fun applyTop(currentY: Int): Int {
                return currentY
            }
        }
    }

    object Gap {

        object Before : Layout {
            override val requestRight: Int  = 0
            override val requestBottom: Int = 0

            override fun applyLeft(currentX: Int): Int {
                return currentX
            }

            override fun applyTop(currentY: Int): Int {
                return if (currentY > 1) currentY else 2
            }
        }

        object After : Layout {
            override val requestRight: Int  = 0
            override val requestBottom: Int = 2

            override fun applyLeft(currentX: Int): Int {
                return currentX
            }

            override fun applyTop(currentY: Int): Int {
                return currentY
            }
        }
    }
}
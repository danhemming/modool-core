package modool.dom.style

interface FormatCommand {
    fun indent(level: Int = 1)
    fun unindent(level: Int = 1)
    fun requestPaddingRight(size: Int)
    fun requestPaddingBottom(size: Int)
    fun applyStyle(style: Style)
    fun defineFlag(name: String)
    fun setFlag(name: String)
    fun unsetFlag(name: String)
    fun removeFlag(name: String)
    fun setAsRefactorScope()
}
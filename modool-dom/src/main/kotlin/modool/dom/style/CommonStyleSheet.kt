package modool.dom.style

import modool.dom.node.text.DomSymbol
import modool.dom.style.builder.StyleSheet
import modool.core.content.signifier.tag.terminal.Keyword

val CommonStyleSheet = StyleSheet {
    padding(DomSymbol.COMMA, Padding.Pad.Right)
    padding(DomSymbol.EQUALS, Padding.Pad)
    anchor(DomSymbol.SEMI_COLON, Anchor.Left)
    padding(DomSymbol.COLON, Padding.Pad.Right)
    padding(DomSymbol.PARENTHESES_OPEN, Padding.None)
    padding(DomSymbol.PARENTHESES_CLOSE, Padding.None)
    padding(Keyword, Padding.Pad)
}
package modool.dom.style

import modool.dom.node.DomContentNode
import modool.dom.node.text.DomTerminalNode
import modool.dom.token.DomElementalStartToken
import modool.dom.token.DomNodeContainerStartToken
import modool.dom.token.DomEndToken
import modool.dom.token.DomToken

internal val DomToken.priorDisplayedContent: DomContentNode?
    get() {

        var candidate = prior

        while (candidate != null) {

            if (candidate.isHiddenContainer) {
                candidate = candidate.start.prior
                continue
            }

            if (candidate is DomTerminalNode && candidate.get().isEmpty()) {
                candidate = candidate.prior
                continue
            }

            if (candidate is DomContentNode) {
                return candidate
            }

            candidate = candidate.prior
        }

        return null
    }

val DomToken.isHiddenContainer: Boolean
    get() = this is DomNodeContainerStartToken && !this.displayed ||
            this is DomEndToken && this.start.let { it is DomNodeContainerStartToken && !it.displayed }

val DomToken.isNotPrecededByDisplayedTextInContainer: Boolean
    get() {
        val container = nodeContainer ?: return false
        val priorText = priorDisplayedText ?: return false
        val priorContainer = priorText.nodeContainer ?: return false
        return container != priorContainer
    }

val DomToken.isNotFollowedByDisplayedTextInContainer: Boolean
    get() {
        val container = nodeContainer ?: return false
        val nextText = nextDisplayedText ?: return false
        val nextContainer = nextText.nodeContainer ?: return false
        return container != nextContainer
    }

val DomToken.priorDisplayedText: DomTerminalNode?
    get() {

        var candidate = prior

        while (candidate != null) {

            if (candidate.isHiddenContainer) {
                candidate = candidate.start.prior
                continue
            }

            if (candidate is DomTerminalNode && candidate.get().isNotEmpty()) {
                return candidate
            }

            candidate = candidate.prior
        }

        return null
    }

val DomToken.nextDisplayedText: DomTerminalNode?
    get() {

        var candidate = next

        while (candidate != null) {

            if (candidate.isHiddenContainer) {
                candidate = candidate.end.next
                continue
            }

            if (candidate is DomTerminalNode && candidate.get().isNotEmpty()) {
                return candidate
            }

            candidate = candidate.next
        }

        return null
    }

val DomToken.elemental: DomElementalStartToken?
    get() {
        var candidate = prior

        while (candidate != null) {

            if (candidate is DomEndToken) {
                candidate = candidate.start.prior
                continue
            }

            if (candidate is DomElementalStartToken) {
                return candidate
            }

            candidate = candidate.prior
        }

        return null
    }

val DomToken.nodeContainer: DomNodeContainerStartToken?
    get() {
        var candidate = prior

        while (candidate != null) {

            if (candidate is DomEndToken) {
                candidate = candidate.start.prior
                continue
            }

            if (candidate is DomNodeContainerStartToken) {
                return candidate
            }

            candidate = candidate.prior
        }

        return null
    }
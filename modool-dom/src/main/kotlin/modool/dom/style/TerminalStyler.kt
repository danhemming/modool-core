package modool.dom.style

import modool.dom.node.DomNodeContainer
import modool.dom.node.text.DomTerminalNode
import modool.dom.property.DomProperty
import modool.dom.style.builder.StyleRuleProvider
import modool.dom.style.builder.TerminalLayoutQuery
import modool.dom.style.builder.TerminalLayoutQueryManager
import modool.dom.token.DomElementalStartToken
import modool.dom.token.DomNodeContainerStartToken
import modool.dom.token.DomPropertyStartToken
import modool.dom.token.DomToken

/**
 * Padding is deterministic based on neighbouring tokens
 * Notes:
 * - SecondarySources are essentially open to trimming / padding at both ends
 */
object TerminalStyler : Styler() {

    fun <T> formatContainer(
            formatter: StyleRuleProvider,
            container: T) where T : DomNodeContainer, T : DomToken {
        formatTokenRange(formatter, container.start, container.end, TerminalLayoutQueryManager())
    }

    fun <T> formatTokenRange(
            formatter: StyleRuleProvider,
            fromToken: T,
            toToken: T) where T : DomToken {
        formatTokenRange(formatter, fromToken, toToken, TerminalLayoutQueryManager())
    }

    private fun <T> formatTokenRange(
            formatter: StyleRuleProvider,
            fromToken: T,
            toToken: T,
            query: TerminalLayoutQueryManager) where T : DomToken {
        var current = fromToken.next

        while (current != null && current != toToken) {

            if (current is DomNodeContainerStartToken && !current.displayed) {
                current = current.end.next
                continue
            }

            if (current is DomElementalStartToken) {
                val elementFormatter = formatter.getRelativeProvider(current)
                formatTokenRange(elementFormatter, current.start, current.end, query)
                current = current.end.next
                continue
            }

            if (current is DomPropertyStartToken) {
                val property = current.property
                if (property is DomProperty<*>) {
                    val propertyFormatter = formatter.getRelativeProvider(property)
                    val propertyContainer = property.container!!
                    formatTokenRange(propertyFormatter, propertyContainer.start, propertyContainer.end, query)
                    current = current.end.next
                    continue
                }
            }

            if (current is DomTerminalNode) {
                query.with(current) { applyPadding(formatter, current as DomTerminalNode, it) }
            }

            current = current.next
        }
    }

    private fun applyPadding(formatter: StyleRuleProvider, terminal: DomTerminalNode, query: TerminalLayoutQuery) {
        // Occasionally a node is displayed but has no content
        if (terminal.get().isEmpty()) {
            return
        }

        val requiredNodePadding = formatter.calculatePadding(terminal, query)

        val priorContentToken = terminal.priorDisplayedContent
        if (priorContentToken == null) {
            terminal.style = requiredNodePadding
            return
        }

        val priorTextNode =
                if (priorContentToken is DomTerminalNode) priorContentToken
                else priorContentToken.priorDisplayedText

        val inPlaceNodeStyle = if (priorTextNode is DomTerminalNode) priorTextNode.style else null
        val requiredY = requiredNodePadding.applyTop(inPlaceNodeStyle?.requestBottom ?: 0)
        val requiredX = requiredNodePadding.applyLeft(inPlaceNodeStyle?.requestRight ?: 0)
        applyContextualNodePadding(priorContentToken, terminal, requiredX, requiredY, true)
        terminal.style = requiredNodePadding
    }
}
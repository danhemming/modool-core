package modool.dom.style

import modool.dom.property.DomProperty
import modool.dom.style.builder.StyleRuleProvider
import modool.dom.token.DomElementalStartToken

abstract class StyleProvider {

    open val indentSpaceCount = 4
    open val maxColumnWidth = 120

    fun createStyleContext(): StyleContext {
        return StyleContext(this, stylesheet)
    }

    protected open val stylesheet = CommonStyleSheet

    fun getStyleRuleProvider(property: DomProperty<*>): StyleRuleProvider {
        return stylesheet.getContextualisedProvider(property)
    }

    fun getStyleRuleProvider(elemental: DomElementalStartToken): StyleRuleProvider {
        return stylesheet.getContextualisedProvider(elemental)
    }
}
package modool.dom.style.builder

import modool.dom.node.text.factory.ConstantDomTerminalFactory
import modool.dom.style.StyleVariableScope
import modool.dom.token.DomToken
import modool.dom.token.isDisplayedNext
import modool.dom.token.isDisplayedPrior

class TerminalLayoutQueryManager : TerminalLayoutQuery {

    private var token: DomToken? = null
    private var flags: StyleVariableScope? = null

    fun with(token: DomToken?, flags: StyleVariableScope? = null, action: (TerminalLayoutQueryManager)-> Unit) {
        this.token = token
        this.flags = flags
        action(this)
        this.token = null
        this.flags = null
    }

    override fun after(factory: ConstantDomTerminalFactory): Boolean {
        return token?.isDisplayedPrior(factory) ?: false
    }

    override fun before(factory: ConstantDomTerminalFactory): Boolean {
        return token?.isDisplayedNext(factory) ?: false
    }

    override fun isSet(name: String): Boolean {
        return flags?.isSet(name) ?: false
    }
}
package modool.dom.style.builder

import modool.dom.node.DomElement

interface ElementStyleBuilder<C> : StyleBuilder where C : DomElement<*>
package modool.dom.style.builder

import modool.dom.node.DomElementBacked

interface CollectionLayoutQuery {
    fun isEmpty(): Boolean
    fun isNotEmpty(): Boolean = !isEmpty()
    fun isEmptyAndUserDisplayed(): Boolean
    fun isEmptyAndUserHidden(): Boolean
    fun containsSingle(): Boolean
    fun containsMultiple(): Boolean = isNotEmpty() && !containsSingle()
    fun isItemFirst(): Boolean
    fun isItemLast(): Boolean
    fun <T : DomElementBacked<*>> isItemOfType(type: Class<T>): Boolean
    fun <T : DomElementBacked<*>> isFirstOfType(type: Class<T>): Boolean
    fun <T : DomElementBacked<*>> isLastOfType(type: Class<T>): Boolean
    fun isItem(predicate: (Any) -> Boolean): Boolean
    fun isFirst(predicate: (Any) -> Boolean): Boolean
    fun isLast(predicate: (Any) -> Boolean): Boolean
}

inline fun <reified T : DomElementBacked<*>> CollectionLayoutQuery.isItemOfType(): Boolean {
    return isItemOfType(T::class.java)
}

inline fun <reified T : DomElementBacked<*>> CollectionLayoutQuery.isFirstOfType(): Boolean {
    return isFirstOfType(T::class.java)
}

inline fun <reified T : DomElementBacked<*>> CollectionLayoutQuery.isLastOfType(): Boolean {
    return isLastOfType(T::class.java)
}
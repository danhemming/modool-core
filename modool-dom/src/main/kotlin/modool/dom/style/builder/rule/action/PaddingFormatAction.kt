package modool.dom.style.builder.rule.action

import modool.dom.style.FormatCommand
import modool.dom.style.Padding
import modool.dom.style.builder.StyleActionable

class PaddingFormatAction(private val padding: Padding) : FormatAction() {

    override fun merge(other: FormatAction): FormatAction {
        return if (other is PaddingFormatAction) this else other
    }

    override fun open(command: FormatCommand) {
        super.open(command)
        command.applyStyle(padding)
    }

    override fun close(command: FormatCommand) {
        command.requestPaddingRight(padding.requestRight)
        command.requestPaddingBottom(padding.requestBottom)
        super.close(command)
    }
}

fun StyleActionable.padding(style: Padding) = PaddingFormatAction(style).apply { registerAction(this) }
fun StyleActionable.padding(top: Int, right: Int, bottom: Int, left: Int) =
        PaddingFormatAction(Padding.Custom(top, right, bottom, left)).apply { registerAction(this) }
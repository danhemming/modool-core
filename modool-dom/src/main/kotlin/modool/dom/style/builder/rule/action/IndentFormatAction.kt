package modool.dom.style.builder.rule.action

import modool.dom.style.FormatCommand
import modool.dom.style.builder.ComplexStyleActionable

sealed class IndentFormatAction : FormatAction() {

    override fun merge(other: FormatAction): FormatAction {
        return if (other is IndentFormatAction) this else other
    }

    object NoIndent : IndentFormatAction()

    class Indent(private val increment: Int) : IndentFormatAction() {

        override fun open(command: FormatCommand) {
            super.open(command)
            command.indent(increment)
        }

        override fun close(command: FormatCommand) {
            command.unindent(increment)
            super.close(command)
        }
    }
}

fun ComplexStyleActionable.indent(increment: Int = 1) = IndentFormatAction.Indent(increment).apply { registerAction(this) }
fun ComplexStyleActionable.noIndent() = IndentFormatAction.NoIndent.apply { registerAction(this) }
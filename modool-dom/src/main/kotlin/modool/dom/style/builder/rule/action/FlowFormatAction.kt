package modool.dom.style.builder.rule.action

import modool.dom.style.Flow
import modool.dom.style.FormatCommand
import modool.dom.style.Padding
import modool.dom.style.builder.ComplexStyleActionable

class FlowFormatAction(private val flow: Flow) : FormatAction() {

    override fun merge(other: FormatAction): FormatAction {
        return if (other is FlowFormatAction) this else other
    }

    override fun open(command: FormatCommand) {
        super.open(command)
        if (flow is Flow.Block) {
            command.setAsRefactorScope()
        }
        command.applyStyle(flow)
    }

    override fun close(command: FormatCommand) {
        command.requestPaddingBottom(flow.requestBottom)
        command.requestPaddingRight(flow.requestRight)
        super.close(command)
    }
}

fun ComplexStyleActionable.flow(flow: Flow) = FlowFormatAction(flow).apply { registerAction(this) }
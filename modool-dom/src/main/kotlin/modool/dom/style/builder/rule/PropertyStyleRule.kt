package modool.dom.style.builder.rule

import modool.dom.node.DomNodeAttachable
import modool.dom.node.text.DomTerminalNode
import modool.dom.node.text.factory.ConstantDomTerminalFactory
import modool.dom.property.DomProperty
import modool.dom.property.sequence.element.DomElementBackedCollectionProperty
import modool.dom.property.value.DomNameProperty
import modool.dom.style.FormatCommand
import modool.dom.style.builder.PropertyStyleBuilder
import modool.dom.style.builder.StylePath
import modool.dom.style.builder.TerminalLayoutQuery
import modool.dom.token.DomElementalStartToken
import modool.core.name.NameConvention

class PropertyStyleRule<T>(
        parent: StylePath,
        inherited: StylePath?,
        propertyClass: Class<T>
) : ChildStyleRule<T, DomProperty<*>>(parent, inherited, propertyClass),
        PropertyStyleBuilder<T>
        where T : DomProperty<*> {

    override fun <S, C> S.decoration(
            open: ConstantDomTerminalFactory?,
            separator: ConstantDomTerminalFactory?,
            close: ConstantDomTerminalFactory?
    ): CollectionDecorationStyleRule
            where S : PropertyStyleBuilder<C>,
                  C : DomElementBackedCollectionProperty<*, *> {
        return super.decoration(open, separator, close)
    }

    override fun <S, C> S.namingConvention(convention: NameConvention): NameConventionStyleRule
            where S : PropertyStyleBuilder<C>,
                  C : DomNameProperty<*, *> {
        return super.namingConvention(convention)
    }

    override fun isApplicableToObject(item: DomProperty<*>): Boolean {
        return contentType.isAssignableFrom(item::class.java)
    }

    override fun isApplicableInContext(property: DomProperty<*>): Boolean {
        if (!isApplicableToObject(property)) {
            return false
        }

        var currentRule = parent as? ChildStyleRule<*, *> ?: return true
        var currentContainer: DomNodeAttachable = property
        while (true) {
            currentContainer = currentRule.getAncestorForContentType(currentContainer) ?: return false
            currentRule = currentRule.parent as? ChildStyleRule<*, *> ?: return true
        }
    }

    override fun applyOpenFormatting(elemental: DomElementalStartToken, query: TerminalLayoutQuery, command: FormatCommand) {
        return
    }

    override fun applyOpenFormatting(property: DomProperty<*>, query: TerminalLayoutQuery, command: FormatCommand) {
        if (isApplicableToObject(property)) {
            super.applyOpenFormatting(property, query, command)
        }
    }

    override fun applyCloseFormatting(elemental: DomElementalStartToken, query: TerminalLayoutQuery, command: FormatCommand) {
        return
    }

    override fun applyCloseFormatting(property: DomProperty<*>, query: TerminalLayoutQuery, command: FormatCommand) {
        if (isApplicableToObject(property)) {
            super.applyCloseFormatting(property, query, command)
        }
    }
}
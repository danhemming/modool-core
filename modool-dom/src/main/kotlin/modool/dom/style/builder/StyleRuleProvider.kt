package modool.dom.style.builder

import modool.dom.node.text.DomTerminalNode
import modool.dom.property.DomProperty
import modool.dom.property.sequence.CollectionConcreteInitialiser
import modool.dom.style.FormatCommand
import modool.dom.style.Style
import modool.dom.style.builder.rule.NameConventionStyleRule
import modool.dom.token.DomElementalStartToken

interface StyleRuleProvider {
    fun calculatePadding(terminal: DomTerminalNode, query: TerminalLayoutQuery): Style
    fun getContextualisedProvider(elemental: DomElementalStartToken): StyleRuleProvider
    fun getContextualisedProvider(property: DomProperty<*>): StyleRuleProvider
    fun getRelativeCollectionConcreteInitialiser(query: CollectionLayoutQuery): CollectionConcreteInitialiser?
    fun getRelativeNameConvention(): NameConventionStyleRule?
    fun getRelativeProvider(elemental: DomElementalStartToken): StyleRuleProvider
    fun getRelativeProvider(property: DomProperty<*>): StyleRuleProvider
    fun getRelativeConditionalProvider(elemental: DomElementalStartToken, query: PostLayoutQuery): StyleRuleProvider?
    fun getRelativeConditionalProvider(property: DomProperty<*>, query: PostLayoutQuery): StyleRuleProvider?
    // TODO the following operations are not inheritable, maybe they should be?
    fun applyOpenFormatting(elemental: DomElementalStartToken, query: TerminalLayoutQuery, command: FormatCommand)
    fun applyOpenFormatting(property: DomProperty<*>, query: TerminalLayoutQuery, command: FormatCommand)
    fun applyCloseFormatting(elemental: DomElementalStartToken, query: TerminalLayoutQuery, command: FormatCommand)
    fun applyCloseFormatting(property: DomProperty<*>, query: TerminalLayoutQuery, command: FormatCommand)
}
package modool.dom.style.builder.rule

import modool.core.name.NameConvention

class NameConventionStyleRule(
        val convention: NameConvention
) : StyleRule
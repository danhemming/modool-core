package modool.dom.style.builder.rule

import modool.dom.node.DomElement
import modool.dom.style.builder.ElementStyleBuilder
import modool.dom.style.builder.StylePath

class AnyElementalStyleRule(
        parent: StylePath,
        inherited: StylePath?
) : StylePath(parent, inherited),
        StyleRule,
        ElementStyleBuilder<DomElement<*>>
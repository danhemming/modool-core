package modool.dom.style.builder.rule

import modool.dom.style.builder.PostLayoutQuery
import modool.dom.style.builder.StylePath
import modool.dom.token.DomElementalStartToken

class ElementalConditionalStyleRule(
        parent: StylePath,
        val elementalRule: ElementalStyleRule<*>,
        private val predicate: (PostLayoutQuery) -> Boolean
) : StylePath(parent, inherited = null),
        StyleRule {

    fun isMatch(elemental: DomElementalStartToken, query: PostLayoutQuery): Boolean {
        return elementalRule.isApplicableToObject(elemental) && predicate(query)
    }
}
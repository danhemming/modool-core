package modool.dom.style.builder

class StyleSheet(rules: StyleSheet.() -> Unit)
    : StylePath(parent = null, inherited = null) {

    init {
        rules(this)
    }

    fun include(styleSheet: StyleSheet) {
        merge(styleSheet)
    }
}
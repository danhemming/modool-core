package modool.dom.style.builder.rule

import modool.dom.node.DomNodeAttachable
import modool.dom.property.DomProperty
import modool.dom.style.builder.StylePath
import modool.dom.token.DomElementalStartToken

abstract class ChildStyleRule<T, C>(
        parent: StylePath,
        inherited: StylePath?,
        protected val contentType: Class<T>
) : StylePath(parent, inherited),
        StyleRule {

    fun isEquivalent(other: ChildStyleRule<*, *>): Boolean {
        return contentType == other.contentType
    }

    fun isApplicableToType(type: Class<*>): Boolean {
        return contentType.isAssignableFrom(type)
    }

    abstract fun isApplicableToObject(item: C): Boolean

    /**
     * This function is called to get initial context for a property.  Subsequent rule matching is done on a
     * relative basis.
     */
    fun getMostSpecificPropertyRule(
            property: DomProperty<*>,
            depth: Int = 0,
            visited: MutableList<ChildStyleRule<*, *>> = mutableListOf()): FormatPathMatch {

        if (visited.contains(this)) {
            return FormatPathMatch.NoMatch
        }

        visited.add(this)

        return children.map { it.getMostSpecificPropertyRule(property, depth + 1, visited) }
                .filterIsInstance<FormatPathMatch.Success>()
                .maxBy { it.depth }
                ?: if (isApplicableInContext(property)) FormatPathMatch.Success(this, depth)
                else FormatPathMatch.NoMatch
    }

    /**
    * This function is called to get initial context for an element.  Subsequent rule matching is done on a
    * relative basis.
    */
    fun getMostSpecificElementalRule(
            elemental: DomElementalStartToken,
            depth: Int = 0,
            visited: MutableList<ChildStyleRule<*, *>> = mutableListOf()): FormatPathMatch {

        if (visited.contains(this)) {
            return FormatPathMatch.NoMatch
        }

        visited.add(this)

        return children.map { it.getMostSpecificElementalRule(elemental, depth + 1, visited) }
                .filterIsInstance<FormatPathMatch.Success>()
                .maxBy { it.depth }
                ?: if (isApplicableInContext(elemental)) FormatPathMatch.Success(this, depth)
                else FormatPathMatch.NoMatch
    }

    open fun isApplicableInContext(property: DomProperty<*>): Boolean = false

    open fun isApplicableInContext(elemental: DomElementalStartToken): Boolean = false

    fun getAncestorForContentType(attachment: DomNodeAttachable): DomNodeAttachable? {
        return attachment.getFirstAncestor(contentType) as? DomNodeAttachable?
    }
}
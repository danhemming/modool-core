package modool.dom.style.builder.rule

import modool.dom.style.builder.StylePath

sealed class FormatPathMatch {
    object NoMatch : FormatPathMatch()
    data class Success(val path: StylePath, val depth: Int) : FormatPathMatch()
}
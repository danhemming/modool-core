package modool.dom.style.builder

import modool.dom.node.DomElement
import modool.dom.node.text.factory.ConstantDomTerminalFactory
import modool.dom.property.DomProperty
import modool.dom.style.builder.rule.TerminalStyleRule
import modool.core.content.signifier.tag.Tag
import modool.core.content.signifier.tag.TagCategory
import modool.dom.style.Anchor
import modool.dom.style.Flow
import modool.dom.style.Layout
import modool.dom.style.Padding

/**
 * Notes:
 * - Cannot style a view
 */
@DomStyleDsl
interface StyleBuilder : ComplexStyleActionable {
    fun description(text: String)
    fun padding(factory: ConstantDomTerminalFactory, padding: Padding): TerminalStyleRule
    fun padding(tag: Tag, padding: Padding): TerminalStyleRule
    fun padding(category: TagCategory, padding: Padding): TerminalStyleRule
    fun flow(factory: ConstantDomTerminalFactory, flow: Flow): TerminalStyleRule
    fun flow(tag: Tag, flow: Flow): TerminalStyleRule
    fun flow(category: TagCategory, flow: Flow): TerminalStyleRule
    fun layout(factory: ConstantDomTerminalFactory, layout: Layout): TerminalStyleRule
    fun layout(tag: Tag, layout: Layout): TerminalStyleRule
    fun anchor(factory: ConstantDomTerminalFactory, anchor: Anchor): TerminalStyleRule
    fun anchor(tag: Tag, anchor: Anchor): TerminalStyleRule
    fun anchor(category: TagCategory, anchor: Anchor): TerminalStyleRule
    fun layout(category: TagCategory, layout: Layout): TerminalStyleRule
    fun <C> element(elementClass: Class<C>, builder: ElementStyleBuilder<C>.() -> Unit) where C : DomElement<*>
    fun <C> ifElement(
            elementClass: Class<C>,
            predicate: (PostLayoutQuery) -> Boolean,
            builder: ElementStyleBuilder<C>.() -> Unit) where C : DomElement<*>

    fun elements(builder: ElementStyleBuilder<*>.() -> Unit)
    fun <C> property(
            propertyClass: Class<C>,
            builder: PropertyStyleBuilder<C>.() -> Unit) where C : DomProperty<*>
    fun <C> ifProperty(
            propertyClass: Class<C>,
            predicate: (PostLayoutQuery) -> Boolean,
            builder: PropertyStyleBuilder<C>.() -> Unit) where C : DomProperty<*>
}

@DomStyleDsl
inline fun <reified C> StyleBuilder.element(noinline builder: ElementStyleBuilder<C>.() -> Unit)
        where C : DomElement<*> {
    element(C::class.java, builder)
}

@DomStyleDsl
inline fun <reified C> StyleBuilder.ifElement(
        noinline predicate: (PostLayoutQuery) -> Boolean,
        noinline builder: ElementStyleBuilder<C>.() -> Unit
) where C : DomElement<*> {
    ifElement(C::class.java, predicate, builder)
}

@DomStyleDsl
inline fun <reified C> StyleBuilder.property(noinline builder: PropertyStyleBuilder<C>.() -> Unit)
        where C : DomProperty<*> {
    property(C::class.java, builder)
}

@DomStyleDsl
inline fun <reified C> StyleBuilder.ifProperty(
        noinline predicate: (PostLayoutQuery) -> Boolean,
        noinline builder: PropertyStyleBuilder<C>.() -> Unit
) where C : DomProperty<*> {
    ifProperty(C::class.java, predicate, builder)
}

package modool.dom.style.builder.rule.action

import modool.dom.style.FormatCommand
import modool.dom.style.builder.StyleActionable

class SetFormatAction(private val name: String) : FormatAction() {

    override fun merge(other: FormatAction): FormatAction {
        return if (other is SetFormatAction && other.name == name) this else other
    }

    override fun open(command: FormatCommand) {
        super.open(command)
        command.setFlag(name)
    }
}

fun StyleActionable.set(name: String) = SetFormatAction(name).apply { registerAction(this) }
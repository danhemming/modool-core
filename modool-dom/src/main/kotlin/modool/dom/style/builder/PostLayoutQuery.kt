package modool.dom.style.builder

import modool.dom.node.DomWhitespaceNode
import modool.dom.node.text.DomTerminalNode
import modool.dom.style.StyleProvider
import modool.dom.token.DomElementalStartToken
import modool.dom.token.DomNodeContainerStartToken
import modool.dom.token.DomEndToken
import modool.dom.token.DomStartToken

interface PostLayoutQuery {
    val isSpaceAvailableToInline: Boolean
    val childElementCount: Int
    val isOutsideRightMargin: Boolean
    val isMultiLine: Boolean
    val isSingleLine: Boolean
    fun isLinePercentageFree(required: Double): Boolean
    fun isLineCharactersFree(required: Int): Boolean
    fun isLinePercentageConsumed(required: Double): Boolean
    fun isLineCharactersConsumed(required: Int): Boolean
}

class PostLayoutQueryManager(private val styleProvider: StyleProvider) : PostLayoutQuery {

    private var initialised: Boolean = false
    private var container: DomNodeContainerStartToken? = null
    private var startColumn: Int = 1
    private var column: Int = 1
    private var contentLineCount: Int = 0
    private var inlineLength: Int = 0
    private var lineBreakCount: Int = 0
    private var calculatedChildElementCount: Int = 0
    private var calculatedIsOutsideRightMargin: Boolean = false

    override val isSpaceAvailableToInline: Boolean
        get() = lineBreakCount <= 1 && (startColumn + inlineLength) <= styleProvider.maxColumnWidth

    override val childElementCount: Int
        get() {
            calculateStatistics()
            return calculatedChildElementCount
        }

    override val isOutsideRightMargin: Boolean
        get() {
            calculateStatistics()
            return calculatedIsOutsideRightMargin
        }

    override val isMultiLine: Boolean
        get() = contentLineCount > 1


    override val isSingleLine: Boolean
        get() = contentLineCount <= 1


    override fun isLinePercentageFree(required: Double): Boolean {
        return (1 - (column / styleProvider.maxColumnWidth)) >= required
    }

    override fun isLineCharactersFree(required: Int): Boolean {
        return styleProvider.maxColumnWidth - column >= required
    }

    override fun isLinePercentageConsumed(required: Double): Boolean {
        return (column / styleProvider.maxColumnWidth) >= required
    }

    override fun isLineCharactersConsumed(required: Int): Boolean {
        return column >= required
    }

    private fun calculateStatistics() {

        if (initialised || container == null) {
            return
        }

        var renderLengthUntilEOLN = 0
        var depth = 0
        var newLineEncountered = false

        var current = container?.next
        while (current != container?.end) {

            if (current is DomNodeContainerStartToken && !current.displayed) {
                current = current.end.next
                continue
            }

            if (depth == 0 && current is DomElementalStartToken) {
                calculatedChildElementCount++
            }

            if (current is DomStartToken) {
                depth++
            }

            if (current is DomEndToken) {
                depth--
            }

            if (!newLineEncountered && current is DomWhitespaceNode) {
                if (current.isNewLine) newLineEncountered = true
                else renderLengthUntilEOLN += current.renderLength
            }

            if (!newLineEncountered && current is DomTerminalNode) {
                renderLengthUntilEOLN += current.renderLength
            }

            current = current?.next
        }

        calculatedIsOutsideRightMargin = startColumn + renderLengthUntilEOLN > styleProvider.maxColumnWidth
        initialised = true
    }

    fun reset(
            container: DomNodeContainerStartToken,
            startColumn: Int,
            column: Int,
            contentLineCount: Int,
            inlineLength: Int,
            lineBreakCount: Int) {
        initialised = false

        this.container = container
        this.startColumn = startColumn
        this.column = column
        this.contentLineCount = contentLineCount
        this.inlineLength = inlineLength
        this.lineBreakCount = lineBreakCount

        this.calculatedChildElementCount = 0
        this.calculatedIsOutsideRightMargin = false
    }
}

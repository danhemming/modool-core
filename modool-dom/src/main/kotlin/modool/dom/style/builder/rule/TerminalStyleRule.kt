package modool.dom.style.builder.rule

import modool.core.content.signifier.tag.Tag
import modool.core.content.signifier.tag.TagCategory
import modool.dom.node.text.DomTerminalNode
import modool.dom.node.text.factory.ConstantDomTerminalFactory
import modool.dom.style.CombinedStyle
import modool.dom.style.Style
import modool.dom.style.builder.TerminalLayoutQuery

sealed class TerminalStyleRule(private val style: Style) : StyleRule {

    private val exceptions = mutableListOf<Exception>()

    fun exceptWhere(vararg styles: Style, predicate: (TerminalLayoutQuery) -> Boolean): TerminalStyleRule {
        exceptions.add(Exception(predicate, CombinedStyle(styles.toList())))
        return this
    }

    fun exceptWhere(style: Style, predicate: (TerminalLayoutQuery) -> Boolean): TerminalStyleRule {
        exceptions.add(Exception(predicate, style))
        return this
    }

    abstract fun isApplicable(terminal: DomTerminalNode): Boolean

    fun calculateStyle(query: TerminalLayoutQuery): Style {
        return exceptions.firstOrNull { it.predicate(query) }?.style ?: style
    }

    private data class Exception(
            val predicate: ((TerminalLayoutQuery) -> Boolean),
            val style: Style)

    class FactoryStyle(
            private val factory: ConstantDomTerminalFactory,
            style: Style
    ) : TerminalStyleRule(style) {

        override fun isApplicable(terminal: DomTerminalNode): Boolean {
            return factory.isProduct(terminal)
        }
    }

    class TagStyle(
            private val tag: Tag,
            style: Style
    ) : TerminalStyleRule(style) {

        constructor(tag: Tag, vararg styles: Style): this(tag, CombinedStyle(styles.toList()))

        override fun isApplicable(terminal: DomTerminalNode): Boolean {
            return terminal.signifies(tag)
        }
    }

    class TagCategoryStyle(
            private val category: TagCategory,
            style: Style
    ) : TerminalStyleRule(style) {

        constructor(category: TagCategory, vararg styles: Style): this(category, CombinedStyle(styles.toList()))

        override fun isApplicable(terminal: DomTerminalNode): Boolean {
            return terminal.signifies(category)
        }
    }
}
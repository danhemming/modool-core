package modool.dom.style.builder

import modool.dom.style.builder.rule.action.FormatAction

/**
 * A simple styleable action will have an open action only e.g. set a variable
 */
@DomStyleDsl
interface StyleActionable {
    fun registerAction(action: FormatAction)
}
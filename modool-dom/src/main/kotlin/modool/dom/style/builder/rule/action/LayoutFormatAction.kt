package modool.dom.style.builder.rule.action

import modool.dom.style.Layout
import modool.dom.style.FormatCommand
import modool.dom.style.Padding
import modool.dom.style.builder.ComplexStyleActionable

class LayoutFormatAction(private val layout: Layout) : FormatAction() {

    override fun merge(other: FormatAction): FormatAction {
        return if (other is LayoutFormatAction) this else other
    }

    override fun open(command: FormatCommand) {
        super.open(command)
        command.applyStyle(layout)
    }

    override fun close(command: FormatCommand) {
        command.requestPaddingBottom(layout.requestBottom)
        command.requestPaddingRight(layout.requestRight)
        super.close(command)
    }
}

fun ComplexStyleActionable.layout(layout: Layout) = LayoutFormatAction(layout).apply { registerAction(this) }
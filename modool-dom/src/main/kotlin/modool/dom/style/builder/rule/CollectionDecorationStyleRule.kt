package modool.dom.style.builder.rule

import modool.dom.node.ConcreteContentBuilder
import modool.dom.node.text.factory.ConstantDomTerminalFactory
import modool.dom.property.sequence.CollectionConcreteInitialiser
import modool.dom.style.builder.CollectionLayoutQuery

class CollectionDecorationStyleRule(
        val open: ConstantDomTerminalFactory?,
        val separator: ConstantDomTerminalFactory?,
        val close: ConstantDomTerminalFactory?
) : StyleRule,
        CollectionConcreteInitialiser {

    private var predicate: ((CollectionLayoutQuery) -> Boolean)? = null

    infix fun where(predicate: (CollectionLayoutQuery) -> Boolean): CollectionDecorationStyleRule {
        this.predicate = predicate
        return this
    }

    fun isApplicable(query: CollectionLayoutQuery): Boolean {
        return predicate?.invoke(query) ?: true
    }

    override fun initialiseConcreteOpen(builder: ConcreteContentBuilder) {
        open?.let { builder.then(it) }
    }

    override fun initialiseConcreteSeparator(builder: ConcreteContentBuilder) {
        separator?.let { builder.then(it) }
    }

    override fun initialiseConcreteClose(builder: ConcreteContentBuilder) {
        close?.let { builder.then(it) }
    }
}
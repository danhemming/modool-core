package modool.dom.style.builder

import modool.dom.node.text.factory.ConstantDomTerminalFactory
import modool.dom.property.DomProperty
import modool.dom.property.sequence.element.DomElementBackedCollectionProperty
import modool.dom.property.value.DomNameProperty
import modool.dom.style.builder.rule.CollectionDecorationStyleRule
import modool.dom.style.builder.rule.NameConventionStyleRule
import modool.core.name.NameConvention

@DomStyleDsl
interface PropertyStyleBuilder<C> : StyleBuilder where C : DomProperty<*> {
    fun <S, C> S.noDecoration(): CollectionDecorationStyleRule
            where S : PropertyStyleBuilder<C>,
                  C : DomElementBackedCollectionProperty<*, *> = decoration(open = null, separator = null, close = null)

    fun <S, C> S.decoration(
            open: ConstantDomTerminalFactory? = null,
            separator: ConstantDomTerminalFactory? = null,
            close: ConstantDomTerminalFactory? = null
    ): CollectionDecorationStyleRule
            where S : PropertyStyleBuilder<C>,
                  C : DomElementBackedCollectionProperty<*, *>

    fun <S, C> S.namingConvention(convention: NameConvention): NameConventionStyleRule
            where S : PropertyStyleBuilder<C>,
                  C : DomNameProperty<*, *>
}
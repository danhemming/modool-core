package modool.dom.style.builder.rule

import modool.dom.node.DomElement
import modool.dom.node.DomNodeAttachable
import modool.dom.node.text.DomTerminalNode
import modool.dom.property.DomProperty
import modool.dom.style.FormatCommand
import modool.dom.style.builder.ElementStyleBuilder
import modool.dom.style.builder.StylePath
import modool.dom.style.builder.TerminalLayoutQuery
import modool.dom.token.DomElementalStartToken

class ElementalStyleRule<T>(
        parent: StylePath,
        inherited: StylePath?,
        elementClass: Class<T>
) : ChildStyleRule<T, DomElementalStartToken>(parent, inherited, elementClass),
        ElementStyleBuilder<T>
        where T : DomElement<*> {

    override fun isApplicableToObject(item: DomElementalStartToken): Boolean {
        return item.isStylingEquivalentOf(contentType)
    }

    override fun isApplicableInContext(elemental: DomElementalStartToken): Boolean {
        if (!isApplicableToObject(elemental)) {
            return false
        }

        var currentRule = parent as? ChildStyleRule<*, *> ?: return true
        // TODO fix to work with pseudo element token (although this should never happen)
        var currentContainer: DomNodeAttachable = elemental as? DomNodeAttachable ?: return false
        while (true) {
            currentContainer = currentRule.getAncestorForContentType(currentContainer) ?: return false
            currentRule = currentRule.parent as? ChildStyleRule<*, *> ?: return true
        }
    }

    override fun applyOpenFormatting(elemental: DomElementalStartToken, query: TerminalLayoutQuery, command: FormatCommand) {
        if (isApplicableToObject(elemental)) {
            super.applyOpenFormatting(elemental, query, command)
        }
    }

    override fun applyOpenFormatting(property: DomProperty<*>, query: TerminalLayoutQuery, command: FormatCommand) {
        return
    }

    override fun applyCloseFormatting(elemental: DomElementalStartToken, query: TerminalLayoutQuery, command: FormatCommand) {
        if (isApplicableToObject(elemental)) {
            super.applyCloseFormatting(elemental, query, command)
        }
    }

    override fun applyCloseFormatting(property: DomProperty<*>, query: TerminalLayoutQuery, command: FormatCommand) {
        return
    }
}
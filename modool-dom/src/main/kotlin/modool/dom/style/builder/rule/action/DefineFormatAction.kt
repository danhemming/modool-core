package modool.dom.style.builder.rule.action

import modool.dom.style.FormatCommand
import modool.dom.style.builder.StyleActionable

class DefineFormatAction(private val name: String) : FormatAction() {

    override fun merge(other: FormatAction): FormatAction {
        return if (other is DefineFormatAction && name == name) this else other
    }

    override fun open(command: FormatCommand) {
        super.open(command)
        command.defineFlag(name)
    }

    override fun close(command: FormatCommand) {
        command.removeFlag(name)
        super.close(command)
    }
}

fun StyleActionable.let(name: String) {
    registerAction(DefineFormatAction(name))
}
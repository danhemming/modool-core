package modool.dom.style.builder

import modool.dom.node.text.factory.ConstantDomTerminalFactory

interface TerminalLayoutQuery {
    fun after(factory: ConstantDomTerminalFactory): Boolean
    fun before(factory: ConstantDomTerminalFactory): Boolean
    fun notBefore(factory: ConstantDomTerminalFactory): Boolean = !before(factory)
    fun notAfter(factory: ConstantDomTerminalFactory): Boolean = !after(factory)
    fun isSet(name: String): Boolean
    fun isNotSet(name: String): Boolean = !isSet(name)
}
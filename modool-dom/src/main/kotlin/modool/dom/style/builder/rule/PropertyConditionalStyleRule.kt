package modool.dom.style.builder.rule

import modool.dom.property.DomProperty
import modool.dom.style.builder.PostLayoutQuery
import modool.dom.style.builder.StylePath

class PropertyConditionalStyleRule(
        parent: StylePath,
        val propertyRule: PropertyStyleRule<*>,
        private val predicate: (PostLayoutQuery) -> Boolean
) : StylePath(parent, inherited = null),
        StyleRule {

    fun isApplicable(property: DomProperty<*>, query: PostLayoutQuery): Boolean {
        return propertyRule.isApplicableToObject(property) && predicate(query)
    }
}


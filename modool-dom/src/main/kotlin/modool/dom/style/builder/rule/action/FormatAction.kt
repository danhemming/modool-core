package modool.dom.style.builder.rule.action

import modool.dom.style.FormatCommand
import modool.dom.style.builder.TerminalLayoutQuery

abstract class FormatAction {
    private var wherePredicate: ((TerminalLayoutQuery) -> Boolean)? = null

    infix fun where(predicate: (TerminalLayoutQuery) -> Boolean): FormatAction {
        this.wherePredicate = predicate
        return this
    }

    fun isApplicable(query: TerminalLayoutQuery): Boolean {
        return wherePredicate?.invoke(query) ?: true
    }

    open fun open(command: FormatCommand) {
        return
    }

    open fun close(command: FormatCommand) {
        return
    }

    /**
     * Assumes this has precedence when performing merge;
     * If return this throw away the new object
     * If return a new object it replace both this and the passed object
     * If return other object keep both objects
     */
    abstract fun merge(other: FormatAction): FormatAction
}
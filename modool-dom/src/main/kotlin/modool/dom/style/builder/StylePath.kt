package modool.dom.style.builder

import modool.core.content.signifier.tag.Tag
import modool.core.content.signifier.tag.TagCategory
import modool.core.name.NameConvention
import modool.dom.node.DomElement
import modool.dom.node.DomElementBacked
import modool.dom.node.text.DomTerminalNode
import modool.dom.node.text.factory.ConstantDomTerminalFactory
import modool.dom.property.DomProperty
import modool.dom.property.sequence.CollectionConcreteInitialiser
import modool.dom.style.*
import modool.dom.style.builder.rule.*
import modool.dom.style.builder.rule.action.FormatAction
import modool.dom.token.DomElementalStartToken

/**
 * Rules are executed for the most specific path match.
 * Terminal rules are executed at the most specific path where there is a match for that terminal.
 * TODO
 * 1. merge terminal rules
 * 2. consider inherited(terminal)
 * 3. action rules are "where" and terminal rules "exceptWhere"?!
 */
abstract class StylePath(
        val parent: StylePath? = null,
        val inherited: StylePath?)
    : StyleBuilder,
        StyleRuleProvider {

    private var description: String? = null
    private val terminalRules = mutableListOf<TerminalStyleRule>()
    private val elementalRules = mutableListOf<ElementalStyleRule<*>>()
    private val elementalConditionalRules = mutableListOf<ElementalConditionalStyleRule>()
    private var anyElementalRule: AnyElementalStyleRule? = null
    private val propertyRules = mutableListOf<PropertyStyleRule<*>>()
    private val propertyConditionalRules = mutableListOf<PropertyConditionalStyleRule>()
    private val collectionDecorationRules = mutableListOf<CollectionDecorationStyleRule>()
    private val nameConventionRules = mutableListOf<NameConventionStyleRule>()
    private val actions = mutableListOf<FormatAction>()

    protected val children: Sequence<ChildStyleRule<*, *>>
        get() = sequence {
            elementalRules.forEach { yield(it) }
            propertyRules.forEach { yield(it) }
        }

    override fun description(text: String) {
        description = text
    }

    override fun padding(factory: ConstantDomTerminalFactory, padding: Padding): TerminalStyleRule {
        return TerminalStyleRule.FactoryStyle(factory, padding).apply { terminalRules.add(this) }
    }

    override fun padding(tag: Tag, padding: Padding): TerminalStyleRule {
        return TerminalStyleRule.TagStyle(tag, padding).apply { terminalRules.add(this) }
    }

    override fun padding(category: TagCategory, padding: Padding): TerminalStyleRule {
        return TerminalStyleRule.TagCategoryStyle(category, padding).apply { terminalRules.add(this) }
    }

    override fun flow(factory: ConstantDomTerminalFactory, flow: Flow): TerminalStyleRule {
        return TerminalStyleRule.FactoryStyle(factory, flow).apply { terminalRules.add(this) }
    }

    override fun flow(tag: Tag, flow: Flow): TerminalStyleRule {
        return TerminalStyleRule.TagStyle(tag, flow).apply { terminalRules.add(this) }
    }

    override fun flow(category: TagCategory, flow: Flow): TerminalStyleRule {
        return TerminalStyleRule.TagCategoryStyle(category, flow).apply { terminalRules.add(this) }
    }

    override fun layout(factory: ConstantDomTerminalFactory, layout: Layout): TerminalStyleRule {
        return TerminalStyleRule.FactoryStyle(factory, layout).apply { terminalRules.add(this) }
    }

    override fun layout(tag: Tag, layout: Layout): TerminalStyleRule {
        return TerminalStyleRule.TagStyle(tag, layout).apply { terminalRules.add(this) }
    }

    override fun layout(category: TagCategory, layout: Layout): TerminalStyleRule {
        return TerminalStyleRule.TagCategoryStyle(category, layout).apply { terminalRules.add(this) }
    }

    override fun anchor(factory: ConstantDomTerminalFactory, anchor: Anchor): TerminalStyleRule {
        return TerminalStyleRule.FactoryStyle(factory, anchor).apply { terminalRules.add(this) }
    }

    override fun anchor(tag: Tag, anchor: Anchor): TerminalStyleRule {
        return TerminalStyleRule.TagStyle(tag, anchor).apply { terminalRules.add(this) }
    }

    override fun anchor(category: TagCategory, anchor: Anchor): TerminalStyleRule {
        return TerminalStyleRule.TagCategoryStyle(category, anchor).apply { terminalRules.add(this) }
    }

    fun decoration(
            open: ConstantDomTerminalFactory?,
            separator: ConstantDomTerminalFactory?,
            close: ConstantDomTerminalFactory?): CollectionDecorationStyleRule {
        return CollectionDecorationStyleRule(open, separator, close).apply { collectionDecorationRules.add(this) }
    }

    fun namingConvention(convention: NameConvention): NameConventionStyleRule {
        return NameConventionStyleRule(convention).apply { nameConventionRules.add(this) }
    }

    override fun <C> element(elementClass: Class<C>, builder: ElementStyleBuilder<C>.() -> Unit) where C : DomElement<*> {

        val inherited = this.inherited?.elementalRules?.mapNotNull { if (it.isApplicableToType(elementClass)) it else null }?.firstOrNull()
                ?: getAncestorElementPath(elementClass)

        ElementalStyleRule(this, inherited, elementClass).let { elemental ->
            elementalRules.add(elemental)
            builder(elemental)
            inherited?.let { elemental.merge(it) }
        }
    }

    override fun <C> ifElement(
            elementClass: Class<C>,
            predicate: (PostLayoutQuery) -> Boolean,
            builder: ElementStyleBuilder<C>.() -> Unit) where C : DomElement<*> {

        val inherited = elementalRules.mapNotNull { if (it.isApplicableToType(elementClass)) it else null }.firstOrNull()
                ?: this.inherited?.elementalRules?.mapNotNull { if (it.isApplicableToType(elementClass)) it else null }?.firstOrNull()
                ?: getAncestorElementPath(elementClass)

        ElementalStyleRule(this, inherited, elementClass).let { elemental ->
            elementalConditionalRules.add(ElementalConditionalStyleRule(this, elemental, predicate))
            builder(elemental)
            inherited?.let { elemental.merge(it) }
        }
    }

    private fun <C : DomElementBacked<*>> getAncestorElementPath(type: Class<C>): StylePath? {
        return parent?.elementalRules?.mapNotNull { if (it.isApplicableToType(type)) it else null }?.firstOrNull()
                ?: parent?.getAncestorElementPath(type)
    }

    override fun elements(builder: ElementStyleBuilder<*>.() -> Unit) {
        if (anyElementalRule == null) {
            anyElementalRule = AnyElementalStyleRule(this, inherited = null)
        }
        anyElementalRule?.let { builder(it) }
    }

    override fun <C> property(
            propertyClass: Class<C>,
            builder: PropertyStyleBuilder<C>.() -> Unit)
            where C : DomProperty<*> {

        val inherited = this.inherited?.propertyRules?.mapNotNull { if (it.isApplicableToType(propertyClass)) it else null }
                ?.firstOrNull()
                ?: getAncestorPropertyPath(propertyClass)

        PropertyStyleRule(this, inherited, propertyClass).let { property ->
            propertyRules.add(property)
            builder(property)
            inherited?.let { property.merge(it) }
        }
    }

    override fun <C> ifProperty(
            propertyClass: Class<C>,
            predicate: (PostLayoutQuery) -> Boolean,
            builder: PropertyStyleBuilder<C>.() -> Unit)
            where C : DomProperty<*> {

        val inherited = propertyRules.mapNotNull { if (it.isApplicableToType(propertyClass)) it else null }
                .firstOrNull()
                ?: this.inherited?.propertyRules?.mapNotNull { if (it.isApplicableToType(propertyClass)) it else null }?.firstOrNull()
                ?: getAncestorPropertyPath(propertyClass)

        PropertyStyleRule(this, inherited, propertyClass).let { property ->
            propertyConditionalRules.add(PropertyConditionalStyleRule(this, property, predicate))
            builder(property)
            inherited?.let { property.merge(it) }
        }
    }

    private fun <C : DomProperty<*>> getAncestorPropertyPath(type: Class<C>): StylePath? {
        return parent?.propertyRules?.mapNotNull { if (it.isApplicableToType(type)) it else null }?.firstOrNull()
                ?: parent?.getAncestorPropertyPath(type)
    }

    protected fun merge(other: StylePath) {
        // FIXME terminal rules should merge
        terminalRules.addAll(other.terminalRules)
        collectionDecorationRules.addAll(other.collectionDecorationRules)
        nameConventionRules.addAll(other.nameConventionRules)
        // Note: conditional rules cannot extend / inherit other conditional rules (no way to merge conditions)
        elementalConditionalRules.addAll(other.elementalConditionalRules)
        propertyConditionalRules.addAll(other.propertyConditionalRules)

        mergeActions(actions, other.actions)
        mergeElemental(elementalRules, other.elementalRules)
        mergeProperties(propertyRules, other.propertyRules)
    }

    private fun mergeElemental(
            mainElementals: MutableList<ElementalStyleRule<*>>,
            inheritedElementals: List<ElementalStyleRule<*>>) {

        val newElementals = mutableListOf<ElementalStyleRule<*>>()
        inheritedElementals.forEach { inherited ->
            val existing = mainElementals.find { it.isEquivalent(inherited) }
            if (existing == null) {
                newElementals.add(inherited)
            } else {
                existing.merge(inherited)
            }
        }
        mainElementals.addAll(newElementals)
    }

    private fun mergeProperties(
            mainProperties: MutableList<PropertyStyleRule<*>>,
            inheritedProperties: List<PropertyStyleRule<*>>) {

        val newElementals = mutableListOf<PropertyStyleRule<*>>()
        inheritedProperties.forEach { inherited ->
            val existing = mainProperties.find { it.isEquivalent(inherited) }
            if (existing == null) {
                newElementals.add(inherited)
            } else {
                existing.merge(inherited)
            }
        }
        mainProperties.addAll(newElementals)
    }

    private fun mergeActions(mainActions: MutableList<FormatAction>, inheritedActions: List<FormatAction>) {
        if (inheritedActions.isEmpty()) {
            return
        }

        if (mainActions.isEmpty()) {
            mainActions.addAll(inheritedActions)
            return
        }

        val newActions = mutableListOf<FormatAction>()
        inheritedActions.forEach inherited@{ inherited ->
            var addInherited = true
            mainActions.forEach { main ->
                val result = main.merge(inherited)
                if (main == inherited || result != inherited) {
                    addInherited = false
                    return@inherited
                }
            }
            if (addInherited) {
                newActions.add(inherited)
            }
        }
        mainActions.addAll(newActions)
    }

    override fun registerAction(action: FormatAction) {
        actions.add(action)
    }

    override fun calculatePadding(terminal: DomTerminalNode, query: TerminalLayoutQuery): Style {
        return terminalRules.asSequence()
                .mapNotNull {
                    if (it.isApplicable(terminal)) it.calculateStyle(query) else null
                }.combineOrNull()
                ?: parent?.calculatePadding(terminal, query)
                ?: Padding.None
    }

    private fun Sequence<Style>.combineOrNull(): Style? {
        return when (count()) {
            0 -> null
            1 -> first()
            else -> CombinedStyle(toList())
        }
    }

    override fun getContextualisedProvider(elemental: DomElementalStartToken): StyleRuleProvider {
        return children.map { it.getMostSpecificElementalRule(elemental) }
                .filterIsInstance<FormatPathMatch.Success>()
                .maxBy { it.depth }
                ?.path
                ?: this
    }

    override fun getRelativeCollectionConcreteInitialiser(query: CollectionLayoutQuery): CollectionConcreteInitialiser? {
        return collectionDecorationRules.firstOrNull { it.isApplicable(query) }
    }

    override fun getRelativeNameConvention(): NameConventionStyleRule? {
        return nameConventionRules.firstOrNull()
    }

    override fun getContextualisedProvider(property: DomProperty<*>): StyleRuleProvider {
        return children.map { it.getMostSpecificPropertyRule(property) }
                .filterIsInstance<FormatPathMatch.Success>()
                .maxBy { it.depth }
                ?.path
                ?: this
    }

    override fun getRelativeProvider(elemental: DomElementalStartToken): StyleRuleProvider {
        return getProviderRecursively(elemental) ?: this
    }

    private fun getProviderRecursively(elemental: DomElementalStartToken): StyleRuleProvider? {
        return elementalRules.mapNotNull { if (it.isApplicableToObject(elemental)) it else null }.firstOrNull()
                ?: anyElementalRule as? StyleRuleProvider
                ?: parent?.getProviderRecursively(elemental)
    }

    override fun getRelativeProvider(property: DomProperty<*>): StyleRuleProvider {
        return getProviderRecursively(property) ?: this
    }

    private fun getProviderRecursively(property: DomProperty<*>): StyleRuleProvider? {
        return propertyRules.mapNotNull { if (it.isApplicableToObject(property)) it else null }.firstOrNull()
                ?: parent?.getProviderRecursively(property)
    }

    override fun getRelativeConditionalProvider(elemental: DomElementalStartToken, query: PostLayoutQuery): StyleRuleProvider? {
        return elementalConditionalRules.mapNotNull { if (it.isMatch(elemental, query)) it.elementalRule else null }.firstOrNull()
                ?: parent?.getRelativeConditionalProvider(elemental, query)
    }

    override fun getRelativeConditionalProvider(property: DomProperty<*>, query: PostLayoutQuery): StyleRuleProvider? {
        return propertyConditionalRules.mapNotNull { if (it.isApplicable(property, query)) it.propertyRule else null }.firstOrNull()
                ?: parent?.getRelativeConditionalProvider(property, query)
    }

    override fun applyOpenFormatting(property: DomProperty<*>, query: TerminalLayoutQuery, command: FormatCommand) {
        actions.forEach { if (it.isApplicable(query)) it.open(command) }
    }

    override fun applyOpenFormatting(elemental: DomElementalStartToken, query: TerminalLayoutQuery, command: FormatCommand) {
        actions.forEach { if (it.isApplicable(query)) it.open(command) }
    }

    override fun applyCloseFormatting(elemental: DomElementalStartToken, query: TerminalLayoutQuery, command: FormatCommand) {
        actions.asReversed().forEach { if (it.isApplicable(query)) it.close(command) }
    }

    override fun applyCloseFormatting(property: DomProperty<*>, query: TerminalLayoutQuery, command: FormatCommand) {
        actions.asReversed().forEach { if (it.isApplicable(query)) it.close(command) }
    }
}
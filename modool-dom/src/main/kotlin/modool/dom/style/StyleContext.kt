package modool.dom.style

import modool.dom.node.DomNodeContainer
import modool.dom.node.text.DomTerminalNode
import modool.dom.property.DomProperty
import modool.dom.style.builder.PostLayoutQueryManager
import modool.dom.style.builder.StyleRuleProvider
import modool.dom.style.builder.TerminalLayoutQueryManager
import modool.dom.token.DomElementalStartToken
import modool.dom.token.DomNodeContainerStartToken
import modool.dom.token.DomPropertyStartToken
import kotlin.math.max

data class StyleContext(
        val styleProvider: StyleProvider,
        private var _formatter: StyleRuleProvider
) : FormatCommand {

    private var variableScope = StyleVariableScope(null)

    // Restored onClose
    private var indentLevel: Int = 0
    var isNestedInPriorityRefactorScope: Boolean = false
        private set
    var isPriorityRefactorScope: Boolean = false
        private set

    // Ongoing
    private var column: Int = 1

    // Ongoing until reset
    var requiredX: Int = 0
        private set
    var requiredY: Int = 0
        private set

    // Snapshot on onOpen
    private var startColumn: Int = 0

    // Zeroed onOpen, added to original value onClose
    private var lineBreakCount: Int = 0
    private var contentLineCount: Int = 0
    private var inlineLength: Int = 0
    private var conditionalFormatter: StyleRuleProvider? = null

    val indentSize get() = indentLevel * styleProvider.indentSpaceCount
    val isPaddingRequired get() = requiredX > 0 || requiredY > 0
    val isStartOfContent get() = contentLineCount == 0 && column == 1

    // Layout Query
    private var layoutQuery = TerminalLayoutQueryManager()
    private var postLayoutQuery = PostLayoutQueryManager(styleProvider)

    override fun indent(level: Int) {
        indentLevel += level
    }

    override fun unindent(level: Int) {
        indentLevel -= level
    }

    override fun requestPaddingRight(size: Int) {
        requiredX = max(requiredX, size)
    }

    override fun requestPaddingBottom(size: Int) {
        requiredY = max(requiredY, size)
    }

    override fun applyStyle(style: Style) {
        requiredX = style.applyLeft(requiredX)
        requiredY = style.applyTop(requiredY)
    }

    override fun setAsRefactorScope() {
        isPriorityRefactorScope = true
    }

    fun registerContent(length: Int) {
        if (inlineLength == 0) {
            startColumn = column
        }
        if (column == 1) {
            contentLineCount++
        }
        column += length
        inlineLength += length
    }

    fun registerLineBreak() {
        if (inlineLength == 0) {
            startColumn = 1
        }
        lineBreakCount++
        inlineLength += 1
        column = 1
    }

    fun resetPadding() {
        requiredX = 0
        requiredY = 0
    }

    override fun defineFlag(name: String) {
        variableScope.defineFlag(name)
    }

    override fun setFlag(name: String) {
        variableScope.setFlag(name)
    }

    override fun unsetFlag(name: String) {
        variableScope.unsetFlag(name)
    }

    override fun removeFlag(name: String) {
        variableScope.removeFlag(name)
    }

    fun setupRelativeFormatter(container: DomNodeContainer) {
        _formatter = conditionalFormatter ?: when (container) {
            is DomPropertyStartToken -> _formatter.getRelativeProvider(container.property ?: return)
            is DomElementalStartToken -> _formatter.getRelativeProvider(container)
            else -> return
        }
        conditionalFormatter = null
    }

    fun getEffectiveFormatter(): StyleRuleProvider {
        return conditionalFormatter ?: _formatter
    }

    fun getConditionalRefactorFormatter(container: DomNodeContainerStartToken): StyleRuleProvider? {
        resetPostLayoutQuery(container)
        return when (container) {
            is DomPropertyStartToken ->
                _formatter.getRelativeConditionalProvider(container.property ?: return null, postLayoutQuery)
            is DomElementalStartToken ->
                _formatter.getRelativeConditionalProvider(container, postLayoutQuery)
            else -> return null
        }?.let { conditionalFormatter ->
            if (_formatter == conditionalFormatter) {
                return null
            }
            conditionalFormatter
        }
    }

    fun applyOpenFormatting(container: DomNodeContainer) {
        when (container) {
            is DomElementalStartToken ->
                layoutQuery.with(container, variableScope) {
                    _formatter.applyOpenFormatting(container, it, this)
                }

            is DomPropertyStartToken -> {
                val property = container.property ?: return
                layoutQuery.with(container, variableScope) {
                    _formatter.applyOpenFormatting(property, it, this)
                }
            }
        }
    }

    fun applyCloseFormatting(container: DomNodeContainer) {
        when (container) {
            is DomElementalStartToken ->
                layoutQuery.with(container.end, variableScope) {
                    _formatter.applyCloseFormatting(container, it, this)
                }

            is DomPropertyStartToken -> {
                val property = container.property ?: return
                layoutQuery.with(container.end, variableScope) {
                    _formatter.applyCloseFormatting(property, it, this)
                }
            }
        }
    }

    internal inline fun applyTemporaryFormatter(
            container: DomNodeContainer,
            builder: DomNodeContainer.() -> Unit) {

        val priorConditionalFormatter = conditionalFormatter
        val priorFormatter = _formatter

        setupRelativeFormatter(container)
        builder(container)

        conditionalFormatter = priorConditionalFormatter
        _formatter = priorFormatter
    }

    internal inline fun <T> applyTemporaryConditionalFormatter(
            conditionalFormatter: StyleRuleProvider,
            container: DomNodeContainer,
            builder: DomNodeContainer.() -> T): T {

        val priorConditionalFormatter = this.conditionalFormatter
        val priorFormatter = _formatter

        this.conditionalFormatter = conditionalFormatter
        val result = builder(container)

        this.conditionalFormatter = priorConditionalFormatter
        this._formatter = priorFormatter

        return result
    }

    private fun resetPostLayoutQuery(container: DomNodeContainerStartToken) {
        postLayoutQuery.reset(
                container = container,
                contentLineCount = contentLineCount,
                column = column,
                inlineLength = inlineLength,
                lineBreakCount = lineBreakCount,
                startColumn = startColumn)
    }

    fun beginContainer(): OpenElementState {
        val openElementState = OpenElementState(
                formatter = _formatter,
                indentLevel = indentLevel, column = column, startColumn = startColumn,
                lineBreakCount = lineBreakCount, inlineLength = inlineLength, contentLineCount = contentLineCount,
                requiredX = requiredX, requiredY = requiredY,
                isNestedInPriorityRefactorScope = isNestedInPriorityRefactorScope,
                isPriorityRefactorScope = isPriorityRefactorScope,
                variableScope = variableScope.copy())

        isNestedInPriorityRefactorScope = isNestedInPriorityRefactorScope || isPriorityRefactorScope
        isPriorityRefactorScope = false
        variableScope = StyleVariableScope(variableScope)

        lineBreakCount = 0
        inlineLength = 0
        contentLineCount = 0
        startColumn = column
        return openElementState
    }

    fun commitContainer(openElementState: OpenElementState, resetUpcomingRequirements: Boolean) {
        _formatter = openElementState.formatter
        lineBreakCount += openElementState.lineBreakCount
        contentLineCount += openElementState.contentLineCount
        inlineLength += openElementState.inlineLength
        startColumn = openElementState.startColumn

        isNestedInPriorityRefactorScope = openElementState.isNestedInPriorityRefactorScope
        isPriorityRefactorScope = openElementState.isPriorityRefactorScope
        variableScope = openElementState.variableScope

        if (resetUpcomingRequirements) {
            indentLevel = openElementState.indentLevel
        }
    }

    fun rollBackContainer(openElementState: OpenElementState) {
        _formatter = openElementState.formatter
        lineBreakCount = openElementState.lineBreakCount
        contentLineCount = openElementState.contentLineCount
        indentLevel = openElementState.indentLevel
        inlineLength = openElementState.inlineLength
        isPriorityRefactorScope = openElementState.isPriorityRefactorScope
        isNestedInPriorityRefactorScope = openElementState.isNestedInPriorityRefactorScope
        column = openElementState.column
        startColumn = openElementState.startColumn
        requiredX = openElementState.requiredX
        requiredY = openElementState.requiredY
        variableScope = openElementState.variableScope
    }

    data class OpenElementState(
            val formatter: StyleRuleProvider,
            val indentLevel: Int,
            val lineBreakCount: Int,
            val contentLineCount: Int,
            val inlineLength: Int,
            val isNestedInPriorityRefactorScope: Boolean,
            val isPriorityRefactorScope: Boolean,
            // For Undo
            val startColumn: Int,
            val column: Int,
            val requiredX: Int,
            val requiredY: Int,
            val variableScope: StyleVariableScope)
}
package modool.dom.style


interface Style {
    val requestRight: Int
    val requestBottom: Int
    fun applyLeft(currentX: Int): Int
    fun applyTop(currentY: Int): Int
}
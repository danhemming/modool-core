package modool.dom.style

import modool.core.content.ContentOrigin
import modool.dom.node.*
import modool.dom.node.text.DomTerminalNode
import kotlin.math.max

abstract class Styler {

    protected fun applyNodePadding(
            terminal: DomTerminalNode,
            requiredX: Int,
            requiredY: Int,
            absolute: Boolean): Boolean {

        return ensurePriorPadding(
                terminal,
                requiredTop = requiredY,
                requiredLeft = requiredX,
                previousToken = null,
                absolute = absolute)
    }

    protected fun applyContextualNodePadding(
            priorContentToken: DomContentNode,
            terminal: DomTerminalNode,
            requiredX: Int,
            requiredY: Int,
            absolute: Boolean): Boolean {

        val applyStyleToLeftOfCurrent = isStyleableLeft(terminal)
        val applyStyleToRightOfPrevious = !applyStyleToLeftOfCurrent && isStyleableRight(priorContentToken)

        // Prefer to add left padding to the current node (parsing associates whitespace with the following element)
        if (applyStyleToLeftOfCurrent) {
            // Note: we are choosing to potentially extend whitespace from another element without checking ownership
            return ensurePriorPadding(
                    terminal,
                    requiredTop = requiredY,
                    requiredLeft = requiredX,
                    previousToken = priorContentToken,
                    absolute = absolute)

        }

        if (applyStyleToRightOfPrevious) {
            // If we don't own an element but we do own the previous token / element then we can apply our left padding
            // as right padding, even inserting a new padding if needed
            return ensureFollowingPadding(
                    priorContentToken,
                    requiredRight = requiredX, requiredBottom = requiredY,
                    absolute = absolute)
        }

        return false
    }

    private fun isStyleableLeft(node: DomTerminalNode): Boolean {
        val origin = node.nodeContainer?.contentOrigin ?: return false

        if (origin == ContentOrigin.API) {
            return true
        }

        if (origin != ContentOrigin.SecondarySource || !node.isNotPrecededByDisplayedTextInContainer) {
            return false
        }

        val priorDisplayedTextNode = node.priorDisplayedText ?: return false
        val priorContainerOrigin = priorDisplayedTextNode.nodeContainer?.contentOrigin ?: return false

        // First node in a SecondarySource preceded by a PrimarySource or SecondarySource
        return priorContainerOrigin == ContentOrigin.PrimarySource || priorContainerOrigin == ContentOrigin.SecondarySource
    }

    private fun isStyleableRight(node: DomContentNode): Boolean {
        val origin = node.nodeContainer?.contentOrigin ?: return false

        if (origin == ContentOrigin.API) {
            return true
        }

        if (origin != ContentOrigin.SecondarySource || !node.isNotFollowedByDisplayedTextInContainer) {
            return false
        }

        val nextDisplayedTextNode = node.nextDisplayedText ?: return false
        val nextContainerOrigin = nextDisplayedTextNode.nodeContainer?.contentOrigin ?: return false

        // Last node in a SecondarySource followed by a PrimarySource
        return nextContainerOrigin == ContentOrigin.PrimarySource
    }

    private fun ensurePriorPadding(
            terminal: DomTerminalNode,
            requiredTop: Int,
            requiredLeft: Int,
            previousToken: DomContentNode?,
            absolute: Boolean): Boolean {

        if (previousToken == null || previousToken !is DomWhitespaceNode) {

            if (requiredTop == 0 && requiredLeft == 0) {
                return false
            }

            val newPadding = DomWhitespaceNode()
            terminal.insertDetachedPrior(newPadding)

            repeat(requiredTop) {
                newPadding.addTrailingNewLine()
            }

            if (requiredTop == 0) repeat(requiredLeft) {
                newPadding.addTrailingSpace()
            }

            return true
        } else {

            val deltaLineBreaksRequired = requiredTop - previousToken.marginY
            val deltaSpacesRequired =
                    // trim all the whitespace if an EOLN is required or if we need to remove an EOLN
                    if (requiredTop > 0 || deltaLineBreaksRequired < 0) -previousToken.marginX
                    else requiredLeft - previousToken.marginX

            // Remove trailing whitespace before adding a new line
            if ((absolute || deltaLineBreaksRequired > 0) && deltaSpacesRequired < 0) {
                repeat(-deltaSpacesRequired) { previousToken.removeTrailingSpace() }
            }

            // Add in the line breaks
            if (deltaLineBreaksRequired > 0) {
                repeat(deltaLineBreaksRequired) { previousToken.addTrailingNewLine() }
            }

            // Add in the spaces (assume new line trumps spaces as they are assumed to precede the new line)
            if (deltaSpacesRequired > 0 && !previousToken.isNewLine) {
                repeat(deltaSpacesRequired) { previousToken.addTrailingSpace() }
            }

            // Remove the newlines that are not needed
            if (absolute && deltaLineBreaksRequired < 0) {
                repeat(-deltaLineBreaksRequired) { previousToken.removeTrailingNewLine() }

                // Having deleted a linebreak we may now need to add in some padding (previous processing should take care of trailing)
                repeat(requiredLeft - previousToken.marginX) { previousToken.addTrailingSpace() }
            }

            return true
        }
    }

    private fun ensureFollowingPadding(
            previousToken: DomContentNode,
            requiredRight: Int,
            requiredBottom: Int,
            absolute: Boolean): Boolean {

        if (previousToken !is DomWhitespaceNode) {

            if (requiredBottom == 0 && requiredRight == 0) {
                return false
            }

            val newPadding = DomWhitespaceNode()
            previousToken.insertDetachedNext(newPadding)

            if (requiredBottom == 0) repeat(requiredRight) {
                newPadding.addTrailingSpace()
            }

            repeat(requiredBottom) {
                newPadding.addTrailingNewLine()
            }

            return true

        } else {

            val deltaLineBreaksRequired = requiredBottom - previousToken.marginY
            val deltaSpacesRequired =
                    // trim all the whitespace if an EOLN is required or if we need to remove an EOLN
                    if (requiredBottom > 0 || deltaLineBreaksRequired < 0) -previousToken.marginX
                    else requiredRight - previousToken.marginX

            if (deltaSpacesRequired > 0 && !previousToken.isNewLine) {
                repeat(deltaSpacesRequired) { previousToken.addTrailingSpace() }
            }

            // Remove trailing whitespace before adding a new line
            if ((absolute || deltaLineBreaksRequired > 0) && deltaSpacesRequired < 0) {
                repeat(-deltaSpacesRequired) { previousToken.removeTrailingSpace() }
            }

            if (deltaLineBreaksRequired > 0) {
                repeat(deltaLineBreaksRequired) { previousToken.addTrailingNewLine() }
            }

            if (absolute) {
                if (deltaLineBreaksRequired < 0) {
                    repeat(-deltaLineBreaksRequired) { previousToken.removeTrailingNewLine()}
                }

                if (deltaSpacesRequired < 0) {
                    repeat(-deltaSpacesRequired) { previousToken.removeTrailingSpace() }
                }
            }

            return true
        }
    }
}
package modool.dom.style

interface Styleable {
    var style: Style?
}
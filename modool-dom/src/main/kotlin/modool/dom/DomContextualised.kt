package modool.dom

interface DomContextualised<out T> where T : DomContext {
    val context: T
}
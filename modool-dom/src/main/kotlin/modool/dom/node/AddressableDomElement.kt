package modool.dom.node

import modool.dom.DomContext
import modool.dom.DynamicAddressable
import modool.core.meta.element.ElementMeta
import modool.dom.property.value.DomNameProperty
import modool.dom.resource.Resource

abstract class AddressableDomElement<D>(
        meta: ElementMeta,
        context: D,
        nameBacking: DomNameProperty<D, String>
) : IdentifiableByNameDomElement<D>(meta, context, nameBacking),
        DynamicAddressable,
        Resource
        where D : DomContext {

    // TODO: machinery required to make this globally addressable
}
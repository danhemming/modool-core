package modool.dom.node.expression

import modool.dom.DomContext
import modool.dom.type.Type

interface DomBinaryGroupExpression<D, T, E>
    : DomBinaryExpression<D, T, E>
        where D : DomContext,
              T : Type,
              E : DomExpression<D, T>
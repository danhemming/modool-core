package modool.dom.node.expression

import modool.dom.DomContext
import modool.dom.type.Type
import modool.dom.type.Typed

interface DomExpressionGroup<D, T, E>
    : DomExpression<D, T>,
        Typed<T>
        where D : DomContext,
              T : Type,
              E : DomExpression<D, T> {

    var expression: E
}
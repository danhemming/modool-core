package modool.dom.node.expression

import modool.dom.DomContext
import modool.dom.type.Type

interface DomBinaryExpression<D, T, E>
    : DomExpression<D, T>
        where D : DomContext,
              T : Type,
              E : DomExpression<D, T> {

    var lhs: E
    var rhs: E
}


package modool.dom.node.expression

import modool.dom.DomContext
import modool.dom.type.Type

/**
 * Note: It is up to the implementor of the expression to provide the concrete layout i.e. the positioning of the
 * operator.  It is this that determines postfix or prefix.
 */
interface DomUnaryExpression<D, T, E>
    : DomExpression<D, T>
        where D : DomContext,
              T : Type,
              E : DomExpression<D, T> {

    var expression: E
}
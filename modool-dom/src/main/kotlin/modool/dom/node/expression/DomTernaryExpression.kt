package modool.dom.node.expression

import modool.dom.DomContext
import modool.dom.type.Type

interface DomTernaryExpression<D, T, E>
    : DomExpression<D, T>
        where D : DomContext,
              T : Type,
              E : DomExpression<D, T> {

    var first: E
    var second: E
    var third: E
}
package modool.dom.node.expression

import modool.dom.DomContext
import modool.dom.node.SourceCodePrimitive
import modool.dom.type.Type
import modool.dom.type.Typed

/**
 * Expressions that are reusable in any lang
 */
interface DomExpression<D, T>
    : SourceCodePrimitive<D>,
        Typed<T>
        where D : DomContext,
              T : Type
package modool.dom.node

/**
 * A element attachment will most likely be a element or a property
 */
open class DomNodeAttachment : DomNodeAttachable {

    private var parent: DomNodeAttachable? = null
    private var document: DomDocument<*>? = null
    final override var version = 0L
        private set

    override val ancestors: Sequence<DomNodeAttachable>
        get() = sequence {
            var currentContainer = parent

            while (currentContainer != null) {
                yield(currentContainer as DomNodeAttachable)
                currentContainer = currentContainer.getParentOrNull()
            }
        }

    override fun getParent(): DomNodeAttachable {
        return if (parent == null) {
            throw Exception("Parent is not set for element")
        } else {
            parent!!
        }
    }

    override fun getParentOrNull(): DomNodeAttachable? {
        return parent
    }

    override fun <T> getFromParent(sender: DomNodeAttachable.() -> T): T {
        return if (parent == null) {
            throw Exception("Parent is not set for element")
        } else {
            sender.invoke(parent!!)
        }
    }

    override fun getDocument(): DomDocument<*> {
        return getDocumentOrNull() ?: throw Exception("document is not set for element")
    }

    override fun getDocumentOrNull(): DomDocument<*>? {
        if (document == null) {
            document = getFirstAncestor(DomDocument::class.java)
        }
        return document
    }

    override fun <T> getFromParentOrNull(sender: DomNodeAttachable.() -> T?): T? {
        return if (parent == null) {
            null
        } else {
            sender.invoke(parent!!)
        }
    }

    @Suppress("UNCHECKED_CAST")
    override fun <T> getFirstAncestor(type: Class<T>): T? {
        var currentContainer = parent

        while (currentContainer != null && !type.isAssignableFrom(currentContainer::class.java)) {
            currentContainer = currentContainer.getParentOrNull()
        }

        return currentContainer as T?
    }

    override fun notifyChanged() {
        getParentOrNull()?.notifyChanged()
        this.version++
    }

    override fun attachTo(parent: DomNodeAttachable) {
        this.parent = parent
    }

    override fun detachFromParent(): Boolean {
        return if (parent == null) {
            false
        } else {
            this.parent = null
            this.document = null
            true
        }
    }
}
package modool.dom.node

import modool.core.content.ContentOrigin
import modool.core.content.SourceProcessingLevel
import modool.core.content.signifier.tag.TagBag
import modool.core.meta.Describeable
import modool.core.meta.element.ElementMeta
import modool.dom.DomContext
import modool.dom.DomContextualised
import modool.dom.node.text.Renderer
import modool.dom.property.DomProperty
import modool.dom.property.ReplaceValueWithParsableContentReceiver
import modool.dom.style.ElementStyler
import modool.dom.style.Style
import modool.dom.style.TerminalStyler
import modool.dom.token.DomElementStartToken

abstract class DomElement<D> protected constructor(
        override val meta: ElementMeta,
        override val context: D,
        private val propertyList: MutableList<DomProperty<*>>,
        private var initialisedAbstract: Boolean,
        private var initialisedConcrete: Boolean,
        requiresParsing: Boolean,
        firstDisplayed: Boolean,
        contentOrigin: ContentOrigin,
        tags: TagBag,
        style: Style?
) : DomElementStartToken(requiresParsing, firstDisplayed, contentOrigin, tags, style),
        DomNode,
        Referable,
        DomContextualised<D>,
        DomNodeContentManager,
        Comparable<DomElement<D>>,
        DomElementBacked<D>,
        Describeable<ElementMeta>,
        DomNodeContainer,
        DomPropertyContainer,
        ContentOriginProvider,
        DomNodeAttachable by DomNodeAttachment()
        where D : DomContext {

    // Non-state properties
    override val backing: DomElement<D> get() = this
    override val renderLength: Int get() = asRecursiveDisplayedNodeSequence().sumBy { it.renderLength }
    override val modelProperties: Sequence<DomProperty<*>> get() = propertyList.asSequence()
    private val formatter by lazy { context.styler.getStyleRuleProvider(this) }

    // Default constructor
    constructor(meta: ElementMeta, context: D) : this(
            meta, context,
            propertyList = mutableListOf<DomProperty<*>>(),
            initialisedAbstract = false, initialisedConcrete = false,
            requiresParsing = true, firstDisplayed = true,
            contentOrigin = ContentOrigin.API,
            tags = TagBag(),
            style = null)

    override fun attachModelProperty(property: DomProperty<*>) {
        property.attachTo(this)
        propertyList.add(property)
    }

    override fun detachModelProperty(property: DomProperty<*>): Boolean {
        return if (propertyList.remove(property)) {
            property.detachFromParent()
            true
        } else {
            false
        }
    }

    override fun refreshFormatting() {
        context.ensureConcreteContent(this)
        val formatter = context.styler.getStyleRuleProvider(this)
        TerminalStyler.formatContainer(formatter, this)
        ElementStyler.format(context.styler.createStyleContext(), this)
    }

    override fun render(renderer: Renderer) {
        renderDisplayed(renderer)
    }

    override operator fun compareTo(other: DomElement<D>) = when {
        this.hashCode() == other.hashCode() -> 0
        this.hashCode() < other.hashCode() -> -1
        this.hashCode() > other.hashCode() -> 1
        else -> throw Exception("compareTo was unable to perform comparison")
    }

    override fun ensureAbstractContent() {
        if (!initialisedAbstract) {
            initialisedAbstract = true
            initialiseAbstractContent()
            // This ensures the state of the properties just adopted TODO could we do this in adoption?
            modelProperties.forEach { it.ensureAbstractContent() }
        }
    }

    override fun initialiseAbstractContent() {

    }

    override fun ensureConcreteContent(builder: ConcreteContentBuilder) {
        ensureAbstractContent()

        if (!initialisedConcrete) {
            initialisedConcrete = true
            initialiseConcreteContent(builder)
        }
    }

    /**
     * A custom element should replace this with properties added in a deterministic builder order
     */
    override fun initialiseConcreteContent(builder: ConcreteContentBuilder) {
        modelProperties.forEach { it.ensureConcreteContent(builder) }
    }

    fun replaceWith(content: String) {
        if ((getParentOrNull() as? ReplaceValueWithParsableContentReceiver)
                        ?.replaceValueWithContent(this, content, SourceProcessingLevel.PARSE) != true) {
            throw RuntimeException("Unable to replace element with parsed content")
        }
    }

    fun replaceWithTokens(content: String) {
        if ((getParentOrNull() as? ReplaceValueWithParsableContentReceiver)
                        ?.replaceValueWithContent(this, content, SourceProcessingLevel.TOKENISE) != true) {
            throw RuntimeException("Unable to replace element with tokenised content")
        }
    }

    fun replaceWithText(content: String) {
        if ((getParentOrNull() as? ReplaceValueWithParsableContentReceiver)
                        ?.replaceValueWithContent(this, content, SourceProcessingLevel.NONE) != true) {
            throw RuntimeException("Unable to replace element with content")
        }
    }
}
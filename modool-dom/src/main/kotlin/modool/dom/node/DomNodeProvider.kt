package modool.dom.node

internal interface DomNodeProvider {
    val node: DomNode?
}
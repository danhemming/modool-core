package modool.dom.node

import modool.core.content.repository.ElementRepositorySnapshot
import modool.dom.token.DomToken

internal class DomElementRepositorySnapshot : ArrayList<DomToken>(20), ElementRepositorySnapshot
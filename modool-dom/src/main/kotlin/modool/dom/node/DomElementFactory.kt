package modool.dom.node

typealias DomElementFactory<D, T> = (D) -> T
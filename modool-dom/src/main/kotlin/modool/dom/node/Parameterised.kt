package modool.dom.node

import modool.dom.DomContext
import modool.dom.property.sequence.DomContextualisedSequence
import modool.dom.type.Type

interface Parameterised<D, T, P>
        where D : DomContext,
              T : Type,
              P : Parameter<T> {

    val parameters: DomContextualisedSequence<D, P>
}
package modool.dom.node

import modool.dom.node.text.Renderer
import modool.dom.node.text.StringRenderer
import modool.dom.token.DomNodeContainerStartToken

interface Renderable {
    fun refreshFormatting() {}

    fun appendFormattedString(content: StringBuilder, refreshFormatting: Boolean = true) {
        if (refreshFormatting) refreshFormatting()
        val renderer = StringRenderer(content)
        render(renderer)
    }

    fun toFormattedString(refreshFormatting: Boolean = true): String {
        if (refreshFormatting) refreshFormatting()
        val renderer = StringRenderer()
        render(renderer)
        return renderer.toString()
    }

    fun render(renderer: Renderer)
    val renderLength: Int
}

fun DomNodeContainerStartToken.renderDisplayed(renderer: Renderer) {
    var current = next

    while (current != null && current != end) {

        current = if (current is DomNodeContainerStartToken && !current.displayed) {
            current.end.next
        } else if (current is Renderable) {
            current.render(renderer)
            current.end.next
        } else {
            current.next
        }
    }
}
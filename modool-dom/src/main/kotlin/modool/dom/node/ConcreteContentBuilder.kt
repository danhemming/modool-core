package modool.dom.node

import modool.dom.node.text.factory.ConstantDomTerminalFactory
import modool.dom.property.DomProperty
import modool.dom.token.DomNodeContainerStartToken
import modool.dom.token.DomEndToken
import modool.dom.token.DomToken

/**
 * A forward only append only content builder
 */
interface ConcreteContentBuilder {
    var insertionPointToken: DomToken?
    var searchPointToken: DomToken?
    var createContentIfNotPresent: Boolean

    fun positionBefore(token: DomToken)
    fun positionAfter(token: DomToken)
    fun seekAfterEquivalent(token: DomToken): Boolean
    fun seekAfter(constantTerminalFactory: ConstantDomTerminalFactory): Boolean
    fun thenStart(startToken: DomNodeContainerStartToken)
    fun thenEnd(endToken: DomEndToken)
    fun then(property: DomProperty<*>)
    fun then(constantTerminalFactory: ConstantDomTerminalFactory)
    fun then(token: DomToken)
    fun supportsUnorderedContent(): Boolean

    operator fun ConstantDomTerminalFactory.unaryPlus() {
        then(this)
    }

    operator fun DomProperty<*>.unaryPlus() {
        then(this)
    }

    fun unordered(builder: ConcreteContentBuilder.() -> Unit) {
        val unorderedContentBuilder = UnorderedPartialConcreteContentBuilder(this)
        builder(unorderedContentBuilder)
        unorderedContentBuilder.moveLast()
    }
}
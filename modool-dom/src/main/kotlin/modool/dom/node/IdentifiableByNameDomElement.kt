package modool.dom.node

import modool.core.meta.element.ElementMeta
import modool.dom.DomContext
import modool.dom.identity.Identifiable
import modool.dom.identity.NamePropertyIdentifier
import modool.dom.identity.caseSensitiveIdentityEquals
import modool.dom.property.value.DomNameProperty


/**
 * Distinction between name and identifier: A name may not be unique e.g. function name with overloads whereas an id is.
 */

abstract class IdentifiableByNameDomElement<D>(
        meta: ElementMeta,
        context: D,
        private val nameBacking: DomNameProperty<D, String>
) : IdentifiableDomElement<D>(meta, context),
        Identifiable
        where D : DomContext {

    /**
     * Unique id within a collection (perhaps more than name e.g. function name plus encoded params)
     */
    override val identifier: NamePropertyIdentifier<D> by lazy { NamePropertyIdentifier(::caseSensitiveIdentityEquals, nameBacking) }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is IdentifiableDomElement<*>) return false

        if (identifier != other.identifier) return false

        return true
    }

    override fun hashCode(): Int {
        var result = super.hashCode()
        result = 31 * result + identifier.hashCode()
        return result
    }
}
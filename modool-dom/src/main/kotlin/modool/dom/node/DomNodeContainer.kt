package modool.dom.node

import modool.dom.style.DisplayStyleable
import modool.dom.token.DomToken
import modool.dom.token.DomTokenContainer
import modool.core.content.token.TokenRangeOption

interface DomNodeContainer : DomTokenContainer, DisplayStyleable {
    fun asRecursiveNodeSequence(): Sequence<DomNode>
    fun asRecursiveDisplayedNodeSequence(): Sequence<DomContentNode>
    fun <T> asLocalTypeSequence(type: Class<T>): Sequence<T>
    fun <T> asRecursiveTypeSequence(type: Class<T>): Sequence<T>
    fun countOfNodes(): Int
    fun countOfElements(): Int
    fun countOfLines(): Int
    fun containsLineBreaks(): Boolean
    fun containsContent(node: DomNode): Boolean
    fun removeContent(node: DomNode): Boolean
    fun wrapAsElement(startToken: DomToken, endToken: DomToken, option: TokenRangeOption)
    fun trimIndent(spaceCount: Int): Boolean
}
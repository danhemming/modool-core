package modool.dom.node

import modool.dom.node.text.DomTerminalNode
import modool.dom.signifier.tag.terminal.DomKeyword
import modool.core.content.token.ConstantTerminalDescriptor
import modool.dom.style.priorDisplayedText
import modool.core.content.token.TokenReader
import modool.core.content.token.TokenReference
import modool.core.content.token.TokenReaderProviderView
import modool.core.content.signifier.tag.Tag
import modool.core.content.signifier.tag.terminal.UnknownTerminal
import modool.dom.token.*

internal class DomTokenReader(
        override val startToken: DomContentStartToken)
    : TokenReader,
        DomTokenReaderDomProviderView {

    override var currentToken: DomToken = startToken
        private set

    override fun reset() {
        currentToken = startToken
    }

    override fun moveNext(): Boolean {
        currentToken.next?.let { currentToken = it } ?: return false
        return true
    }

    override fun moveNextContent(): Boolean {
        var next: DomToken? = currentToken.next

        // Need to skip over any added "non-content"
        while (next != null) {
            // Skip over already parsed containers e.g. comments
            if (next is DomNodeContainerStartToken) {
                next = if (next.requiresParsing) {
                    next.next
                    break
                } else {
                    next.end
                }
            } else if (next is DomPropertyEndToken || next is DomElementalEndToken || next is DomCommentEndToken) {
                next = next.next
            } else {
                break
            }
        }

        return if (next is DomContentNode) {
            currentToken = next
            true
        } else {
            false
        }
    }

    override fun moveTo(reference: TokenReference) {
        if (reference !is DomToken) {
            error("Attempted to restore a token reader with an invalid position")
        }
        currentToken = reference
    }

    override fun isStartOfContent(): Boolean {
        return currentToken is DomContentStartToken
    }

    override fun isEndOfContent(): Boolean {
        return currentToken is DomContentEndToken
    }

    override fun isTerminal(): Boolean {
        return currentToken is DomTerminalNode
    }

    override fun isTerminalEqual(value: String): Boolean {
        return currentToken.let { it is DomTerminalNode && it.get() == value }
    }

    override fun isTerminalEqual(reference: TokenReference): Boolean {
        if (reference !is DomTerminalNode) {
            error("Attempted to compare a terminal with an invalid position")
        }
        return currentToken.let { it is DomTerminalNode && it.get() == reference.get() }
    }

    override fun isTerminalTagged(value: String, tag: Tag): Boolean {
        return currentToken.let { it is DomTerminalNode && it.get() == value && it.signifies(tag) }
    }

    override fun isTerminalTagged(tag: Tag): Boolean {
        return currentToken.let { it is DomTerminalNode && it.signifies(tag) }
    }

    override fun isTerminalSoftKeyword(): Boolean {
        return currentToken.let { it is DomTerminalNode && it.signifies(DomKeyword.Soft) }
    }

    override fun isTerminalMatchable(descriptor: ConstantTerminalDescriptor): Boolean {
        // Don't do a factory equality check as the terminal may not have been parsed and been tagged correctly
        return currentToken.let { it is DomTerminalNode && it.get() == descriptor.get() }
    }

    override fun isMarker(): Boolean {
        return currentToken is DomMarkerNode
    }

    override fun isMarkerTagged(tag: Tag): Boolean {
        return currentToken.let { it is DomMarkerNode && it.signifies(tag) }
    }

    override fun isMostRecentTerminalTagged(tag: Tag): Boolean {
        return (currentToken as? DomTerminalNode)?.signifies(tag) ?: (currentToken.priorDisplayedText?.signifies(tag) == true)
    }

    override fun isUnknown(): Boolean {
        return currentToken.let { it is DomTerminalNode && it.signifies(UnknownTerminal) }
    }

    override fun isUnknown(value: String): Boolean {
        return isTerminalTagged(value, UnknownTerminal)
    }

    override fun isWhiteSpace(): Boolean {
        return currentToken is DomWhitespaceNode
    }

    override fun isWhitespaceTagged(tag: Tag): Boolean {
        return (currentToken as? DomTaggedWhitespaceNode)?.signifies(tag) ?: false
    }

    override fun isNewLine(): Boolean {
        return currentToken.let {
            it is DomWhitespaceNode && it.isNewLine
                    || it is DomWhitespaceNode && it.prior is DomContentStartToken
                    || it is DomContentStartToken }
    }

    override fun isNewLine(count: Int): Boolean {
        return currentToken.let { it is DomWhitespaceNode && it.lineCount == count }
    }

    override fun isSpace(): Boolean {
        return currentToken.let { it is DomWhitespaceNode && it.spaceCount > 0 }
    }

    override fun isSpace(count: Int): Boolean {
        return currentToken.let { it is DomWhitespaceNode && it.spaceCount == count }
    }

    override fun isLeftMargin(): Boolean {
        return currentToken.let { it is DomWhitespaceNode && it.isLeftMargin }
    }

    override fun isStartOfComment(): Boolean {
        return currentToken is DomCommentStartToken
    }

    override fun isEndOfComment(): Boolean {
        return currentToken is DomCommentEndToken
    }

    override fun getSpaceCount(): Int {
        currentToken.let { return if (it is DomWhitespaceNode) it.spaceCount else 0 }
    }

    override fun getTabCount(): Int {
        currentToken.let { return if (it is DomWhitespaceNode) it.tabCount else 0 }
    }

    override fun createReference(): TokenReference {
        return currentToken
    }

    override fun createPriorWhitespaceReference(): TokenReference {
        return if (currentToken.prior is DomWhitespaceNode) currentToken.prior!! else currentToken
    }

    override fun <T : TokenReaderProviderView> createProviderView(viewType: Class<T>): T {
        if (viewType.isAssignableFrom(DomTokenReaderDomProviderView::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return this as T
        }
        error("Unable to create provider view for: ${viewType::class.java}")
    }
}
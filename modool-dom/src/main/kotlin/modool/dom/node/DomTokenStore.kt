package modool.dom.node

import modool.core.content.signifier.tag.Tag
import modool.core.content.signifier.tag.terminal.UnknownTerminal
import modool.core.content.token.*
import modool.core.content.tryCopy
import modool.dom.node.text.DomTerminalNode
import modool.dom.node.text.factory.ConstantDomTerminalFactory
import modool.dom.token.*
import modool.lexer.TextBlock

internal class DomTokenStore(
        private val text: TextBlock,
        private val copyOnWrite: Boolean
) : TokenWriter,
        TokenReaderProvider {

    private var firstToken: DomToken? = null
    private var lastToken: DomToken? = null
    private var deferredToken: DomToken? = null

    private var startToken = DomContentStartToken()
    override val isAccepting: Boolean = true

    override fun writeStart() {
        startToken.end.detachFromChain()
        insertToken(startToken)
    }

    override fun isAcceptable(descriptor: ConstantTerminalDescriptor): Boolean {
        return true
    }

    override fun isEmpty(): Boolean {
        return firstToken == null
    }

    override fun createRestorePoint(): RestorePoint {
        return RestorePoint(firstToken, lastToken, deferredToken)
    }

    override fun updateRestorePoint(restorePoint: TokenWriterRestorePoint) {
        if (restorePoint !is RestorePoint) {
            error("Incompatible restore point received")
        }
        restorePoint.firstToken = firstToken
        restorePoint.lastToken = lastToken
        restorePoint.deferredToken = deferredToken
    }

    override fun restore(restorePoint: TokenWriterRestorePoint) {
        if (restorePoint !is RestorePoint) {
            error("Incompatible restore point received")
        }
        firstToken = restorePoint.firstToken
        lastToken = restorePoint.lastToken
        deferredToken = restorePoint.deferredToken
        lastToken?.next = null
    }

    override fun writeFactoryTerminal(index: Int, count: Int, descriptor: ConstantTerminalDescriptor, tags: List<Tag>) {
        if (descriptor !is ConstantDomTerminalFactory) {
            error("Attempted writeFactoryTerminal with an incorrect factory type. Expected: ${ConstantDomTerminalFactory::class.java} but got: ${descriptor::class.java}")
        }
        val token = if (descriptor.isCaseSensitive) {
            descriptor.createNode(/* Use value in descriptor */)
        } else {
            descriptor.createNode(text.substring(index, index + count))
        }
        tags.forEach { token.tag(it) }
        updateLastToken()
        insertToken(token)
    }

    override fun writeUnknownTerminal(index: Int, count: Int) {
        val token = DomTerminalNode(text.substring(index, index + count))
        token.tag(UnknownTerminal)
        updateLastToken()
        insertToken(token)
    }

    override fun writeTerminal(index: Int, count: Int, tags: List<Tag>) {
        val token = DomTerminalNode(text.substring(index, index + count))
        tags.forEach { token.tag(it) }
        updateLastToken()
        insertToken(token)
    }

    /**
     * Write implementation differs to commit by copying the token to protect itself against linked amendments
     */
    override fun copyFrom(writer: TokenWriter) {
        if (writer !is DomTokenStore) {
            error("Attempted to write incompatible tokens")
        }

        updateLastToken()

        var current = writer.firstToken
        while (current != null) {
            val currentNext = current.next
            insertToken(if (copyOnWrite) current.tryCopy() else current)
            current = currentNext
        }
    }

    override fun writeComment(body: () -> Unit) {
        val startOfComment = DomCommentStartToken()
        updateLastToken()
        insertToken(startOfComment)
        body()
        insertToken(startOfComment.end)
    }

    override fun writeMarker(tags: List<Tag>, isDeferred: Boolean) {
        // Note: we do not write the stashed terminal content as the intention is to drop the marker before the terminal
        val token = DomMarkerNode()
        tags.forEach { token.tag(it) }
        if (isDeferred) {
            if (deferredToken == null) {
                deferredToken = token
            } else {
                deferredToken?.insertDetachedNext(token)
            }
        } else {
            updateLastToken()
            insertToken(token)
        }
    }

    override fun overwriteWhitespace(whitespace: WhitespaceContent, tags: List<Tag>) {
        updateLastToken()
        insertToken(
                if (tags.isEmpty()) DomWhitespaceNode(whitespace)
                else DomTaggedWhitespaceNode(whitespace).apply { tags.forEach { tag(it) } })
    }

    private fun updateLastToken() {
        // Parser may have added some control tokens so skip to the new end
        while (lastToken?.next != null) {
            lastToken = lastToken?.next
        }
    }

    private fun insertToken(token: DomToken) {
        when {
            firstToken == null ->
                firstToken = token

            lastToken is DomWhitespaceNode && token is DomWhitespaceNode ->
                lastToken?.replaceInChainWith(token)

            firstToken == lastToken ->
                firstToken?.insertDetachedNext(token)

            lastToken != token -> {
                if (lastToken is DomContentEndToken) {
                    // Can happen when writeEnd has been called but a consumer subsequently performs a writeX i.e. parser
                    lastToken?.insertDetachedPrior(token)
                } else {
                    lastToken?.insertDetachedNext(token)
                }
            }
            else -> return
        }

        deferredToken?.let {
            if (token is DomContentEndToken) {
                token.insertExtractedChainPrior(it)
            } else {
                token.next = deferredToken
            }
            deferredToken = null
            updateLastToken()
        }

        lastToken = token
    }

    override fun writeEnd() {
        updateLastToken()
        insertToken(startToken.end)
    }

    override fun clear() {
        firstToken = null
        lastToken = null
        deferredToken = null
    }

    override fun createWhitespaceBuilder(): WhitespaceContent {
        return WhitespaceContentBuilder()
    }

    override fun createComponentWriter(): TokenWriter {
        return DomTokenStore(text, copyOnWrite = false)
    }

    override fun createValueComponentWriter(): TokenWriter {
        return DomTokenStore(text, copyOnWrite = true)
    }

    data class RestorePoint(
            internal var firstToken: DomToken?,
            internal var lastToken: DomToken?,
            internal var deferredToken: DomToken?
    ) : TokenWriterRestorePoint {

        override fun clear() {
            firstToken = null
            lastToken = null
            deferredToken = null
        }
    }

    override fun createTokenReader(): TokenReader {
        return DomTokenReader(startToken)
    }
}
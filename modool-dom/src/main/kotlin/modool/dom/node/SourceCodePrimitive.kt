package modool.dom.node

import modool.dom.DomContext

interface SourceCodePrimitive<D> : DomElementBacked<D> where D : DomContext
package modool.dom.node

import modool.core.content.ContentOrigin

interface ContentOriginProvider {
    var contentOrigin: ContentOrigin
}
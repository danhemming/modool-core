package modool.dom.node.text

import modool.dom.node.text.factory.ConstantDomTerminalFactory
import modool.core.content.enumeration.Enumeration

abstract class DomEnumeration<T: ConstantDomTerminalFactory>
    : Enumeration<ConstantDomTerminalFactory> {

    override val entries = emptyList<T>()
    fun parse(source: String): T? {
        return entries.find { it.isValueEqual(source) }
    }
}
package modool.dom.node.text

class StringRenderer(
        private val contents: StringBuilder = StringBuilder()
): Renderer {

    override fun add(content: Char) {
        contents.append(content)
    }

    override fun add(content: String) {
       contents.append(content)
    }

    override fun toString(): String {
        return contents.toString()
    }
}
package modool.dom.node.text.factory

import modool.dom.node.text.DomTerminalNode
import modool.core.content.signifier.tag.Tag
import modool.core.content.signifier.tag.terminal.JvmLiteral
import modool.core.content.signifier.tag.terminal.UnknownTerminal

object CommonDynamicTerminals {
    fun unknown(value: String) = DomTerminalNode(value).apply { tag(UnknownTerminal) }
    fun untagged(value: String) = DomTerminalNode(value)
    fun name(value: String, name: Tag) = DomTerminalNode(value).apply { tag(name) }
    fun name(name: Tag) = DomTerminalNode("").apply { tag(name) }
    fun untypedLiteral(value: String) = DomTerminalNode(value).apply { tag(JvmLiteral.Untyped) }
}
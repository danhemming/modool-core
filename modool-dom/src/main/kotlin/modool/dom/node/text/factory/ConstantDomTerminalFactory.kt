package modool.dom.node.text.factory

import modool.core.content.ReadableValue
import modool.core.content.signifier.tag.Tag
import modool.core.content.token.ConstantTerminalDescriptor
import modool.dom.node.text.DomTerminalNode
import modool.dom.token.DomToken

open class ConstantDomTerminalFactory(
    value: String,
    type: Tag.ConstantTerminalType,
    isCaseSensitive: Boolean,
    categories: List<Tag.Categorical>
) : ConstantTerminalDescriptor(value, type, isCaseSensitive, categories),
    ReadableValue<String> {

    constructor (value: String, type: Tag.ConstantTerminalType) : this(
        value,
        type,
        isCaseSensitive = true,
        categories = emptyList()
    )

    constructor (value: String, type: Tag.ConstantTerminalType, categories: List<Tag.Categorical>) : this(
        value,
        type,
        isCaseSensitive = true,
        categories = categories
    )

    fun isProduct(token: DomToken): Boolean {
        return isOfProducedType(token) && isProducibleContent(token)
    }

    fun assimilate(terminal: DomTerminalNode): Boolean {
        return if (isProducibleContent(terminal)) {
            if (!isOfProducedType(terminal)) {
                terminal.makeConstant(taggedType, this)
            }
            true
        } else {
            false
        }
    }

    private fun isProducibleContent(token: DomToken): Boolean {
        return token is DomTerminalNode && token.get() == value
    }

    private fun isOfProducedType(token: DomToken): Boolean {
        return token is DomTerminalNode && token.signifies(taggedType) && token.constantFactory == this
    }

    fun createNode(): DomTerminalNode {
        return createNode(value)
    }

    /**
     * Overload exists to accommodate values using different casing
     */
    fun createNode(value: String): DomTerminalNode {
        return DomTerminalNode(value).apply {
            taggedCategories.forEach { tag(it) }
            makeConstant(taggedType, this@ConstantDomTerminalFactory)
        }
    }
}
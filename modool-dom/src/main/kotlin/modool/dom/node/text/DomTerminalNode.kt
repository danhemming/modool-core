package modool.dom.node.text

import modool.core.content.Copyable
import modool.core.content.WritableValue
import modool.core.content.signifier.Signifier
import modool.core.content.signifier.tag.*
import modool.core.content.signifier.tag.terminal.UnknownTerminal
import modool.dom.node.DomContentNode
import modool.dom.node.text.factory.ConstantDomTerminalFactory
import modool.dom.style.Style
import modool.dom.style.Styleable
import modool.dom.token.DomTokenInspector

class DomTerminalNode private constructor(
        private var text: String,
        private val tags: TagBag,
        override var style: Style?,
        factory: ConstantDomTerminalFactory?
) : DomContentNode(),
        Taggable,
        Styleable,
        WritableValue<String>,
        Copyable<DomTerminalNode> {

    // Default constructor
    constructor(text: String) : this(text, TagBag(), style = null, factory = null)

    var constantFactory: ConstantDomTerminalFactory? = factory
        private set

    override val renderLength: Int
        get() = text.length

    override fun get() = text

    override fun set(value: String) {
        text = value
    }

    override fun render(renderer: Renderer) {
        renderer.add(text)
    }

    override fun toPrettyDescription(): String {
        return "->$text<-"
    }

    fun <T : ConstantDomTerminalFactory> isProducedFromFactoryType(type: Class<T>): Boolean {
        return constantFactory != null && constantFactory?.let { type.isAssignableFrom(it::class.java) } ?: false
    }

    fun makeConstant(type: Tag.ConstantTerminalType, constantTerminalFactory: ConstantDomTerminalFactory) {
        if (tag(type)) {
            constantFactory = constantTerminalFactory
        }
    }

    override fun tag(tag: Tag): Boolean {
        val added = tags.add(tag)
        if (added && tag is Tag.TerminalType) {
            constantFactory = null
        }
        return added
    }

    override fun untag(tag: Tag): Boolean {
        return if (tag is Tag.TerminalType) tag(UnknownTerminal) else tags.remove(tag)
    }

    override fun signifies(signifier: Signifier): Boolean {
        return signifier is Tag && tags.contains(signifier)
    }

    override fun signifies(category: TagCategory): Boolean {
        return tags.contains(category)
    }

    override fun onContextChange() {
        super.onContextChange()
        style = null
    }

    override fun inspect(tokenInspector: DomTokenInspector) {
        tokenInspector.outputLine(get())
    }

    override fun copy(): DomTerminalNode {
        return DomTerminalNode(text, tags.copy(), style, constantFactory)
    }
}
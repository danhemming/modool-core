package modool.dom.node.text

import java.io.OutputStreamWriter

class StreamRenderer(private val output: OutputStreamWriter): Renderer {

    override fun add(content: Char) {
        output.write(content.toInt())
    }

    override fun add(content: String) {
        output.write(content)
    }
}
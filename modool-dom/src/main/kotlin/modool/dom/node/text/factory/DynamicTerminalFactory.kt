package modool.dom.node.text.factory

import modool.dom.node.text.DomTerminalNode

typealias DynamicTerminalFactory = (value: String) -> DomTerminalNode
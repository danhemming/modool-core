package modool.dom.node.text

import modool.dom.signifier.tag.terminal.DomSeparator

object DomSymbol {
    val PARENTHESES_OPEN = DomSeparator.Symbol("(")
    val PARENTHESES_CLOSE = DomSeparator.Symbol(")")
    val BRACE_OPEN = DomSeparator.Symbol("{")
    val BRACE_CLOSE = DomSeparator.Symbol("}")
    val BRACKET_OPEN = DomSeparator.Symbol("[")
    val BRACKET_CLOSE = DomSeparator.Symbol("]")
    val COMMA = DomSeparator.Symbol(",")
    val COLON = DomSeparator.Symbol(":")
    val PERIOD = DomSeparator.Symbol(".")
    val SEMI_COLON = DomSeparator.Symbol(";")
    val HASH = DomSeparator.Symbol("#")
    val QUESTION_MARK = DomSeparator.Symbol("?")
    val EXCLAMATION_MARK = DomSeparator.Symbol("!")
    val QUOTE_SINGLE = DomSeparator.Symbol("'")
    val QUOTE_DOUBLE = DomSeparator.Symbol("\"")
    val BACK_TICK = DomSeparator.Symbol("`")
    val PLUS = DomSeparator.Symbol("+")
    val MINUS = DomSeparator.Symbol("-")
    val MULTIPLY = DomSeparator.Symbol("*")
    val DIVIDE = DomSeparator.Symbol("/")
    val EQUALS = DomSeparator.Symbol("=")
    val LESS_THAN = DomSeparator.Symbol("<")
    val GREATER_THAN = DomSeparator.Symbol(">")
    val DOUBLE_FORWARD_SLASH = DomSeparator.Symbol("//")
}

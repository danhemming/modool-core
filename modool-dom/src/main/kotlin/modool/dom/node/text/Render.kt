package modool.dom.node.text

interface Renderer {
    fun add(content: Char)
    fun add(content: String)
}
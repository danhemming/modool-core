package modool.dom.node.statement

import modool.dom.DomContext
import modool.dom.node.SourceCodePrimitive

interface DomStatement<D>
    : SourceCodePrimitive<D>
        where D : DomContext
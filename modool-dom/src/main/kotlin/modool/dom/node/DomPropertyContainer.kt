package modool.dom.node

import modool.dom.property.DomProperty

interface DomPropertyContainer : DomNodeAttachable {
    val modelProperties: Sequence<DomProperty<*>>
    fun attachModelProperty(property: DomProperty<*>)
    fun detachModelProperty(property: DomProperty<*>): Boolean
    fun <T> adopt(property: T): T where T : DomProperty<*> {
        property.attachTo(this)
        this.attachModelProperty(property)
        return property
    }
}
package modool.dom.node

import it.unimi.dsi.fastutil.bytes.ByteArrayList
import modool.core.content.Copyable
import modool.core.content.token.Whitespace
import modool.core.content.token.WhitespaceContent
import modool.dom.node.text.Renderer
import modool.dom.token.DomTokenInspector

/**
 * Rather than creating a node and a token for each white space encoding, we instead create something that works in both
 * modes.  This cuts down on the number of objects we need to create. It also allows us to capture the combined state of
 * multiple whitespace tokens e.g. margins
 */
open class DomWhitespaceNode protected constructor(
        protected val contents: ByteArrayList,
        protected var numberOfSpacesOnCurrentLine: Int,
        protected var numberOfTabsOnCurrentLine: Int,
        protected var numberOfNewLines: Int
) : DomContentNode(),
        WhitespaceContent,
        Copyable<DomWhitespaceNode> {

    override val lineCount: Int get() = numberOfNewLines
    override val spaceCount: Int get() = numberOfSpacesOnCurrentLine
    override val tabCount: Int get() = numberOfTabsOnCurrentLine
    override val renderLength: Int get() = contents.count()
    override val marginX: Int get() = numberOfSpacesOnCurrentLine + (numberOfTabsOnCurrentLine * WhitespaceContent.TAB_SIZE)

    // Default constructor
    constructor() : this(ByteArrayList(3), 0, 0, 0)

    // Copy constructor
    constructor(content: WhitespaceContent)
            : this(content.asByteArrayList(), content.spaceCount, content.tabCount, content.lineCount)


    override fun copy(): DomWhitespaceNode {
        return DomWhitespaceNode(
                contents.clone(),
                numberOfSpacesOnCurrentLine,
                numberOfTabsOnCurrentLine,
                numberOfNewLines)
    }

    override fun save(): WhitespaceContent {
        return copy()
    }

    override fun asByteArrayList(): ByteArrayList {
        return contents
    }

    fun calculateIndentationLevel(spaceCount: Int): Int {
        return if (isLeftMargin) numberOfTabsOnCurrentLine + (numberOfSpacesOnCurrentLine / spaceCount) else 0
    }

    fun addTrailingSpace() {
        add(Whitespace.SPACE)
    }

    fun addTrailingNewLine() {
        // TODO: need to consider carriage return (need a way to inject config)
        add(Whitespace.LINE_FEED)
    }

    fun add(whitespaceContent: WhitespaceContent) {
        whitespaceContent.asByteArrayList().forEach {
            add(Whitespace.from(it.toInt()))
        }
    }

    override fun add(encoding: Whitespace) {
        contents.add(encoding.encoding)

        when {
            encoding.isLineBreak -> {
                numberOfSpacesOnCurrentLine = 0
                numberOfTabsOnCurrentLine = 0
                numberOfNewLines++
            }
            encoding == Whitespace.SPACE ->
                numberOfSpacesOnCurrentLine++
            encoding == Whitespace.TAB ->
                numberOfTabsOnCurrentLine++
            else -> return
        }
    }

    fun removeTrailingSpace(): Boolean {
        val lastOccurrence = contents.lastIndexOf(Whitespace.SPACE.encoding)
        if (lastOccurrence == -1 || lastOccurrence != contents.count() - 1) {
            return false
        }

        if (numberOfSpacesOnCurrentLine.compareTo(0) == 0) {
            return false
        }
        numberOfSpacesOnCurrentLine--

        contents.removeByte(lastOccurrence)
        return true
    }

    fun removeTrailingNewLine(): Boolean {
        if (numberOfSpacesOnCurrentLine > 0) {
            return false
        }

        val lastLFIndex = contents.lastIndexOf(Whitespace.LINE_FEED.encoding)
        if (lastLFIndex == -1 || lastLFIndex != contents.count() - 1) {
            return false
        }

        numberOfNewLines--
        numberOfSpacesOnCurrentLine = 0
        for (content in contents.asReversed()) {
            if (content == Whitespace.SPACE.encoding) {
                numberOfSpacesOnCurrentLine++
            } else if (content == Whitespace.TAB.encoding) {
                numberOfTabsOnCurrentLine++
            } else {
                break
            }
        }

        contents.removeByte(lastLFIndex)

        val lastCRIndex = contents.lastIndexOf(Whitespace.CARRIAGE_RETURN.encoding)
        if (lastCRIndex > -1 && lastCRIndex == contents.count() - 1) {
            contents.removeByte(lastCRIndex)
        }

        return true
    }

    fun isEmpty() = renderLength == 0

    override fun render(renderer: Renderer) {
        contents.forEach {
            renderer.add(Whitespace.from(it.toInt()).outputRepresentation)
        }
    }

    override fun toPrettyDescription(): String {
        return "[Whitespace]"
    }

    override fun inspect(tokenInspector: DomTokenInspector) {
        tokenInspector.outputLine()
        contents.forEach {
            tokenInspector.output(Whitespace.from(it.toInt()).description)
        }
    }
}


package modool.dom.node

import modool.dom.node.text.DomTerminalNode
import modool.dom.node.text.factory.ConstantDomTerminalFactory
import modool.dom.property.DomProperty
import modool.dom.token.DomElementalEndToken
import modool.dom.token.DomElementalStartToken
import modool.dom.token.DomNodeContainerStartToken
import modool.dom.token.DomEndToken
import modool.dom.token.DomToken
import modool.dom.token.DomTokenContainer
import modool.dom.token.findNext

@Suppress("UNCHECKED_CAST")
class DefaultConcreteContentBuilder(
        private val container: DomTokenContainer,
        private val visitedTerminals: MutableList<DomTerminalNode>? = null
) : ConcreteContentBuilder {

    override var insertionPointToken: DomToken? = null
    override var searchPointToken: DomToken? = null
    override var createContentIfNotPresent: Boolean = true

    override fun supportsUnorderedContent(): Boolean {
        return false
    }

    override fun then(property: DomProperty<*>) {
        property.ensureConcreteContent(this)
    }

    override fun then(constantTerminalFactory: ConstantDomTerminalFactory) {
        if (!seekAfter(constantTerminalFactory)) {
            moveToNextContentInsertionPoint()
            if (createContentIfNotPresent) {
                val newToken = constantTerminalFactory.createNode()
                addTokenAfterInsertionPoint(newToken)
                registerVisitedTerminalToken(newToken)
            }
        }
    }

    override fun thenStart(startToken: DomNodeContainerStartToken) {
        if (!seekAfterEquivalent(startToken)) {
            moveToNextContainerInsertionPoint()
            if (insertionPointToken is DomWhitespaceNode) {
                insertionPointToken = insertionPointToken?.prior
            }
            addTokenAfterInsertionPoint(startToken)
        }
        moveTo(startToken.start)
    }

    override fun thenEnd(endToken: DomEndToken) {
        insertionPointToken?.let {
            if (it.follows(endToken.end)) {
                it.insertExtractedChainNext(endToken.end)
            }
        }
        positionAfter(endToken)
    }

    override fun then(token: DomToken) {
        if (!seekAfterEquivalent(token)) {
            moveToNextInsertionPoint(token)
            if (createContentIfNotPresent) {
                addTokenAfterInsertionPoint(token)
                if (token is DomTerminalNode) {
                    registerVisitedTerminalToken(token)
                }
            }
        }
    }

    private fun addTokenAfterInsertionPoint(token: DomToken) {
        if (insertionPointToken == null) container.addTokenAtStart(token.start)
        else insertionPointToken?.insertExtractedChainNext(token.start)
        positionAfter(token)
    }

    override fun seekAfter(constantTerminalFactory: ConstantDomTerminalFactory): Boolean {

        val nextToken: DomToken? = if (searchPointToken == null) {
            val firstToken = container.asRecursiveTokenSequence().firstOrNull()
            if (firstToken != null && constantTerminalFactory.isProduct(firstToken)) {
                firstToken
            } else {
                firstToken?.findNextConstantWithinElementBoundary(constantTerminalFactory)
            }
        } else {
            searchPointToken?.findNextConstantWithinElementBoundary(constantTerminalFactory)
        }

        return if (nextToken == null) {
            false
        } else {
            if (nextToken is DomTerminalNode) {
                registerVisitedTerminalToken(nextToken)
            }
            moveTo(nextToken)
            true
        }
    }

    private fun DomToken.findNextConstantWithinElementBoundary(
            constantTerminalFactory: ConstantDomTerminalFactory): DomToken? {

        var candidate = next
        while (candidate != null) {
            if (candidate is DomElementalStartToken || candidate is DomElementalEndToken) {
                return null
            }
            if (constantTerminalFactory.isProduct(candidate)) {
                return candidate
            }
            candidate = candidate.next
        }
        return null
    }

    override fun seekAfterEquivalent(token: DomToken): Boolean {

        val nextToken: DomToken? = if (searchPointToken == null) {
            val firstToken = container.asRecursiveTokenSequence().firstOrNull()
            if (firstToken != null && isTokenEquivalent(firstToken, token)) {
                firstToken
            } else {
                firstToken?.findNextEquivalentTokenWithinElementBoundary(token)
            }
        } else {
            // Note: this pointer equivalent search is the only reason we can be unbounded
            searchPointToken?.findNext { token == it }
        }

        return if (nextToken == null) {
            false
        } else {
            positionAfter(nextToken)
            true
        }
    }

    private fun DomToken.findNextEquivalentTokenWithinElementBoundary(token: DomToken): DomToken? {

        var candidate = next
        while (candidate != null) {
            if (candidate is DomElementalStartToken || candidate is DomElementalEndToken) {
                return null
            }
            if (isTokenEquivalent(candidate, token)) {
                return candidate
            }
            candidate = candidate.next
        }
        return null
    }

    override fun positionBefore(token: DomToken) {
        token.prior?.let { moveTo(it) }
    }

    override fun positionAfter(token: DomToken) {
        moveTo(token.end)
    }

    private fun isTokenEquivalent(first: DomToken, second: DomToken): Boolean {
        // Terminals are considered equivalent based on content
        (first as? DomTerminalNode)?.let { firstTerminal ->
            firstTerminal.constantFactory?.let { return it.isProduct(second) }
            // FIXME: check tags too?
            (second as? DomTerminalNode)?.let { return firstTerminal.get() == it.get() }
        }

        return first == second
    }

    private fun moveToNextInsertionPoint(item: Any) {
        when (item) {
            is DomTokenContainer -> moveToNextContainerInsertionPoint()
            else -> moveToNextContentInsertionPoint()
        }
    }

    private fun moveToNextContentInsertionPoint() {
        val nextToken = insertionPointToken ?: if (container is DomToken) container else return
        val nextContentToken = nextToken.findNextInsertionPointWithoutLeavingContainer()
        insertionPointToken = if (nextContentToken is DomWhitespaceNode) nextContentToken else nextToken
    }

    private fun moveToNextContainerInsertionPoint() {
        val nextToken = insertionPointToken ?: if (container is DomToken) container else return
        val nextContentToken = nextToken.findNextInsertionPointWithoutLeavingContainer()
        insertionPointToken = if (nextContentToken is DomWhitespaceNode) nextContentToken.prior else nextToken
    }

    private fun moveTo(token: DomToken) {
        insertionPointToken = token
        searchPointToken = token
    }

    private fun DomToken.findNextInsertionPointWithoutLeavingContainer(): DomContentNode? {
        if (this is DomEndToken) {
            return null
        }

        // It's ok if we are currently on a start token (this implicitly takes care of that)
        var candidate = next

        while (candidate != null) {
            if (candidate is DomNodeContainerStartToken || candidate is DomEndToken) {
                return null
            }

            if (candidate is DomContentNode) {
                return candidate
            }

            candidate = candidate.next
        }

        return null
    }

    private fun registerVisitedTerminalToken(terminal: DomTerminalNode) {
        visitedTerminals?.add(terminal)
    }
}
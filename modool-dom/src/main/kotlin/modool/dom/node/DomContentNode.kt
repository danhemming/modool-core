package modool.dom.node

import modool.dom.token.DomToken

/**
 * distinguishes a node as outputting content as opposed to a control node
 */
abstract class DomContentNode : DomToken(), DomNode
package modool.dom.node

interface DomNodeContentManager {
    fun ensureAbstractContent()
    /*protected*/ fun initialiseAbstractContent()
    fun ensureConcreteContent(builder: ConcreteContentBuilder)
    /*protected*/ fun initialiseConcreteContent(builder: ConcreteContentBuilder)
}
package modool.dom.node

import modool.dom.name.MandatoryRenameable
import modool.dom.DomContext
import modool.core.meta.element.ElementMeta
import modool.dom.property.value.DomNameProperty

/**
 * Distinction between name and identifier: A name may not be unique e.g. function name with overloads whereas an id is.
 */

abstract class MandatoryNameableDomElement<D>(
        meta: ElementMeta,
        context: D,
        open val nameBacking: DomNameProperty<D, String>
) : DomElement<D>(meta, context),
        MandatoryRenameable
        where D : DomContext {

    override var name: String
        get() = nameBacking.get()
        set(value) {
            nameBacking.set(value)
        }
}


package modool.dom.node

import modool.core.content.signifier.tag.Tag
import modool.core.content.token.*
import modool.lexer.TextBlock

internal class DomAnchoredTokenStore(
        text: TextBlock,
        copyOnWrite: Boolean,
        private val anchors: Collection<ConstantTerminalDescriptor>
) : TokenWriter,
        TokenReaderProvider {

    private val tokenStore = DomTokenStore(text, copyOnWrite)

    var isInProgress = true
    var isElementInProgress = false

    override val isAccepting: Boolean
        get() = isElementInProgress

    override fun writeStart() {
        tokenStore.writeStart()
    }

    override fun isAcceptable(descriptor: ConstantTerminalDescriptor): Boolean {
        isElementInProgress = isElementInProgress || anchors.contains(descriptor)
        return isElementInProgress
    }

    override fun isEmpty(): Boolean {
        return tokenStore.isEmpty()
    }

    override fun createRestorePoint(): DomTokenStore.RestorePoint {
        return tokenStore.createRestorePoint()
    }

    override fun updateRestorePoint(restorePoint: TokenWriterRestorePoint) {
        return tokenStore.updateRestorePoint(restorePoint)
    }

    override fun restore(restorePoint: TokenWriterRestorePoint) {
        tokenStore.restore(restorePoint)
    }

    override fun writeMarker(tags: List<Tag>, isDeferred: Boolean) {
        tokenStore.writeMarker(tags, isDeferred)
    }

    override fun writeFactoryTerminal(index: Int, count: Int, descriptor: ConstantTerminalDescriptor, tags: List<Tag>) {
        tokenStore.writeFactoryTerminal(index, count, descriptor, tags)
    }

    override fun writeTerminal(index: Int, count: Int, tags: List<Tag>) {
        tokenStore.writeTerminal(index, count, tags)
    }

    override fun writeUnknownTerminal(index: Int, count: Int) {
        tokenStore.writeUnknownTerminal(index, count)
    }

    override fun overwriteWhitespace(whitespace: WhitespaceContent, tags: List<Tag>) {
        tokenStore.overwriteWhitespace(whitespace, tags)
    }

    override fun writeComment(body: () -> Unit) {
        tokenStore.writeComment(body)
    }

    override fun copyFrom(writer: TokenWriter) {
        tokenStore.copyFrom(writer)
    }

    override fun writeEnd() {
        tokenStore.writeEnd()
        isInProgress = false
    }

    override fun clear() {
        tokenStore.clear()
    }

    override fun createWhitespaceBuilder(): WhitespaceContent {
        return tokenStore.createWhitespaceBuilder()
    }

    override fun createComponentWriter(): TokenWriter {
        return tokenStore.createComponentWriter()
    }

    override fun createValueComponentWriter(): TokenWriter {
        return tokenStore.createValueComponentWriter()
    }

    override fun createTokenReader(): TokenReader {
        return tokenStore.createTokenReader()
    }
}
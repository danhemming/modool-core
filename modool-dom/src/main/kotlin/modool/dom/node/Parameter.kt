package modool.dom.node

import modool.dom.type.Type
import modool.dom.type.VariableTyped
import modool.dom.name.MandatoryNameable

interface Parameter<T>
    : VariableTyped<T>,
        MandatoryNameable where T : Type
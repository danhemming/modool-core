package modool.dom.node

import modool.core.content.ContentOrigin
import modool.core.content.signifier.tag.TagBag
import modool.dom.DomContext
import modool.core.meta.element.ElementMeta
import modool.dom.identity.Identifiable
import modool.dom.property.DomProperty
import modool.dom.style.Style

/**
 * Unique id within a collection (perhaps more than name e.g. function name plus encoded params, or package name qualifier)
 */
abstract class IdentifiableDomElement<D>(
        meta: ElementMeta,
        context: D,
        propertyList: MutableList<DomProperty<*>>,
        initialisedAbstract: Boolean,
        initialisedConcrete: Boolean,
        requiresParsing: Boolean,
        firstDisplayed: Boolean,
        contentOrigin: ContentOrigin,
        tags: TagBag,
        style: Style?
) : DomElement<D>(
        meta, context,
        propertyList,
        initialisedAbstract, initialisedConcrete,
        requiresParsing, firstDisplayed,
        contentOrigin,
        tags,
        style),
        Identifiable
        where D : DomContext {

    constructor(meta: ElementMeta, context: D): this(
            meta, context,
            propertyList = mutableListOf<DomProperty<*>>(),
            initialisedAbstract = false, initialisedConcrete = false,
            requiresParsing = true, firstDisplayed = true,
            contentOrigin = ContentOrigin.API,
            tags = TagBag(),
            style = null)

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is IdentifiableDomElement<*>) return false

        if (identifier != other.identifier) return false

        return true
    }

    override fun hashCode(): Int {
        var result = super.hashCode()
        result = 31 * result + identifier.hashCode()
        return result
    }
}
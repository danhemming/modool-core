package modool.dom.node

import modool.dom.DomContext
import modool.core.content.signifier.tag.Taggable

@ModoolDsl
interface DomElementBacked<D> : Taggable
        where D : DomContext {

    val backing: DomElement<D>
}
package modool.dom.node

import it.unimi.dsi.fastutil.bytes.ByteArrayList
import modool.core.content.signifier.tag.Tag
import modool.core.content.signifier.tag.TagBag
import modool.core.content.signifier.tag.Taggable
import modool.core.content.token.WhitespaceContent

class DomTaggedWhitespaceNode(
        contents: ByteArrayList,
        numberOfSpacesOnCurrentLine: Int,
        numberOfTabsOnCurrentLine: Int,
        numberOfNewLines: Int,
        private var tags: TagBag
) : DomWhitespaceNode(
        contents,
        numberOfSpacesOnCurrentLine,
        numberOfTabsOnCurrentLine,
        numberOfNewLines),
        Taggable {


    // Default constructor
    constructor() : this(ByteArrayList(3), 0, 0, 0, TagBag())

    // Copy constructor
    constructor(content: WhitespaceContent)
            : this(content.asByteArrayList(), content.spaceCount, content.tabCount, content.lineCount, TagBag())

    override fun copy(): DomTaggedWhitespaceNode {
        return DomTaggedWhitespaceNode(
                contents.clone(),
                numberOfSpacesOnCurrentLine,
                numberOfTabsOnCurrentLine,
                numberOfNewLines,
                tags)
    }

    override fun tag(tag: Tag): Boolean {
        return tags.add(tag)
    }

    override fun untag(tag: Tag): Boolean {
        return tags.remove(tag)
    }
}
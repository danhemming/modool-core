package modool.dom.node

import modool.core.content.token.TokenReference
import modool.core.content.repository.ElementRepositoryProviderView

interface ElementRepositoryDomProviderView : ElementRepositoryProviderView {
    fun <T> getAsType(reference: TokenReference): T
}
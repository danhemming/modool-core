package modool.dom.node

/**
 * Represents either structural (e.g. element) or terminal content that represents presentation.
 */
interface DomNode : Renderable
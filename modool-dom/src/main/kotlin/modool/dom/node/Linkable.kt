package modool.dom.node

/**
 * Represents a reference that is resolvable to another object in the graph
 */
interface Linkable<T> {
    val referee: T
}
package modool.dom.node

interface DomNodeAttachable {
    fun attachTo(parent: DomNodeAttachable)
    fun detachFromParent(): Boolean
    fun getParent(): DomNodeAttachable
    fun getParentOrNull(): DomNodeAttachable?
    fun getDocument(): DomDocument<*>
    fun getDocumentOrNull(): DomDocument<*>?
    fun <T> getFromParent(sender: DomNodeAttachable.() -> T): T
    fun <T> getFromParentOrNull(sender: DomNodeAttachable.() -> T?): T?
    fun <T> getFirstAncestor(type: Class<T>): T?
    fun notifyChanged()
    val ancestors: Sequence<DomNodeAttachable>
    val version: Long
}
package modool.dom.node

import modool.core.content.ContentOrigin
import modool.core.content.signifier.tag.TagBag
import modool.dom.DomContext
import modool.core.meta.element.ElementMeta
import modool.dom.property.DomProperty
import modool.dom.style.Style

abstract class DomDocument<D>(
        meta: ElementMeta,
        context: D,
        propertyList: MutableList<DomProperty<*>>,
        initialisedAbstract: Boolean,
        initialisedConcrete: Boolean,
        requiresParsing: Boolean,
        firstDisplayed: Boolean,
        contentOrigin: ContentOrigin,
        tags: TagBag,
        style: Style?
): DomElement<D>(
        meta, context,
        propertyList,
        initialisedAbstract, initialisedConcrete,
        requiresParsing, firstDisplayed,
        contentOrigin,
        tags,
        style)
        where D : DomContext {

    constructor(meta: ElementMeta, context: D): this(
            meta, context,
            propertyList = mutableListOf<DomProperty<*>>(),
            initialisedAbstract = false, initialisedConcrete = false,
            requiresParsing = true, firstDisplayed = true,
            contentOrigin = ContentOrigin.API,
            tags = TagBag(),
            style = null)
}


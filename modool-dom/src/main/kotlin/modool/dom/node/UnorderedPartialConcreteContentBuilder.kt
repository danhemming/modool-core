package modool.dom.node

import modool.dom.node.text.factory.ConstantDomTerminalFactory
import modool.dom.property.DomProperty
import modool.dom.token.DomNodeContainerStartToken
import modool.dom.token.DomEndToken
import modool.dom.token.DomToken

internal class UnorderedPartialConcreteContentBuilder(private val builder: ConcreteContentBuilder) : ConcreteContentBuilder {

    private val startSearchPointToken = builder.searchPointToken
    private var furthestInsertPointToken = builder.searchPointToken
    override var createContentIfNotPresent: Boolean
        get() = builder.createContentIfNotPresent
        set(value) {
            builder.createContentIfNotPresent = value
        }

    override var searchPointToken: DomToken?
        get() = builder.searchPointToken
        set(value) {
            builder.searchPointToken = value
        }

    override var insertionPointToken
        get() = builder.insertionPointToken
        set(value) {
            builder.insertionPointToken = value
        }

    override fun supportsUnorderedContent(): Boolean {
        return true
    }

    override fun seekAfterEquivalent(token: DomToken): Boolean {
        return builder.seekAfterEquivalent(token)
    }

    override fun seekAfter(constantTerminalFactory: ConstantDomTerminalFactory): Boolean {
        return builder.seekAfter(constantTerminalFactory)
    }

    override fun positionBefore(token: DomToken) {
        builder.positionBefore(token)
    }

    override fun positionAfter(token: DomToken) {
        builder.positionAfter(token)
    }

    override fun thenStart(startToken: DomNodeContainerStartToken) {
        builder.thenStart(startToken)
        trackAndReset()
    }

    override fun thenEnd(endToken: DomEndToken) {
        builder.thenEnd(endToken)
        trackAndReset()
    }

    override fun then(property: DomProperty<*>) {
        property.ensureConcreteContent(this)
        trackAndReset()
    }

    override fun then(constantTerminalFactory: ConstantDomTerminalFactory) {
        builder.then(constantTerminalFactory)
        trackAndReset()
    }

    override fun then(token: DomToken) {
        builder.then(token)
        trackAndReset()
    }

    private fun trackAndReset() {
        val currentIP = builder.insertionPointToken
        val furthestIP = furthestInsertPointToken

        if (currentIP != null && furthestIP != null) {
            furthestInsertPointToken = if (furthestIP.follows(currentIP)) furthestIP else currentIP
        } else if (furthestIP == null) {
            furthestInsertPointToken = currentIP
        }

        builder.searchPointToken = startSearchPointToken
    }

    fun moveLast() {
        builder.insertionPointToken = furthestInsertPointToken
        builder.searchPointToken = furthestInsertPointToken
    }
}
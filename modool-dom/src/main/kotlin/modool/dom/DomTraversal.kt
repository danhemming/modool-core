package modool.dom

import modool.dom.name.MandatoryRenameable
import modool.dom.node.DomElement
import modool.dom.property.sequence.element.DomElementBackedView

// function / { params } / { params } where { name.getValue() == "" }

inline operator fun <D : DomContext, T: DomElement<D>, U: DomElement<D>> T.div(mapper: T.()-> DomElementBackedView<D, U>): Sequence<U> {
    return mapper().asSequence()
}

inline operator fun <D : DomContext, T: DomElement<D>, U: DomElement<D>> Sequence<T>.div(crossinline mapper: T.()-> DomElementBackedView<D, U>): Sequence<U> {
    return flatMap { mapper(it).asSequence() }
}

infix fun <T: DomElement<*>> Sequence<T>.where(mapper: T.()-> Boolean): Sequence<T> {
    return filter(mapper)
}

class NameFilter<out T>(private val source: Sequence<T>) where T: MandatoryRenameable {
    fun startsWith(name: String) = source.filter { it.name.startsWith(name) }
    fun contains(name: String) = source.filter { it.name.contains(name) }
    fun endsWith(name: String) = source.filter {  it.name.endsWith(name) }
    fun equals(name: String) = source.filter { it.name == name }
}

val <T> Sequence<T>.name where T: MandatoryRenameable get() = NameFilter(this)
fun <T> Sequence<T>.exists() = count() > 0
fun <T> Sequence<T>.has(transform: (T) -> Any) = map(transform).exists()
fun <T,U> Sequence<T>.ancestor(type: Class<U>): Sequence<U> where T: DomElement<*>, U: DomElement<*> =
        mapNotNull { it -> it.getFirstAncestor(type) }

package modool.dom.property

import modool.dom.DomContext
import modool.core.meta.property.NoPropertyMeta
import modool.dom.property.value.DomNameProperty
import modool.dom.property.value.MandatorySimpleNameDomProperty

fun <D> nameProperty(context: D): DomNameProperty<D, String> where D : DomContext =
        MandatorySimpleNameDomProperty(NoPropertyMeta, context)
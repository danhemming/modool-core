package modool.dom.property.value

import modool.core.content.ContentOrigin
import modool.core.content.SourceProcessingLevel
import modool.lexer.LexerRulesProvider
import modool.core.meta.property.PropertyMeta
import modool.parser.ParserRuleProvider
import modool.dom.DomContext
import modool.dom.node.ConcreteContentBuilder
import modool.dom.node.DomElementBacked
import modool.dom.node.DomNodeAttachable
import modool.dom.node.DomNodeContentManager
import modool.dom.property.DomBaseProperty
import modool.dom.property.ReplaceValueWithParsableContentReceiver
import modool.dom.token.DomNodeContainerStartToken
import modool.dom.token.DomToken

abstract class DomElementProperty<D, T>(
        metadata: PropertyMeta,
        context: D,
        private val contentType: Class<T>,
        private var valueElement: DomToken?,
        initialisedAbstract: Boolean,
        initialisedConcrete: Boolean,
        container: DomNodeContainerStartToken?,
        contentOrigin: ContentOrigin
) : DomBaseProperty<D>(
        metadata, context, initialisedAbstract, initialisedConcrete, container, contentOrigin),
        DomElementValue<D, T>,
        LexerRulesProvider, ParserRuleProvider,
        ReplaceValueWithParsableContentReceiver
        where D : DomContext,
              T : DomElementBacked<D> {

    constructor(metadata: PropertyMeta, context: D, contentType: Class<T>) : this(
            metadata, context,
            contentType,
            valueElement = null,
            initialisedAbstract = false, initialisedConcrete = false,
            container = null,
            contentOrigin = ContentOrigin.API)

    var onContentProvided: (() -> Unit)? = null

    override val isContainerInitiallyDisplayed: Boolean
        get() = valueElement != null

    fun get(): T? {
        return getObject()
    }

    override fun set(dsl: T?) {
        val originalBacking = valueElement
        if (originalBacking != dsl) {
            setBacking(dsl?.backing)

            dsl?.backing?.let { element ->
                getParentOrNull()?.let { parent -> element.attachTo(parent) }
                context.ensureConcreteContent(element)
            }

            if (originalBacking == null) {
                onContentProvided?.invoke()
            }

            notifyChanged()
        }
    }

    override fun set(source: String, processingLevel: SourceProcessingLevel) {
        setBacking(
                context.process(
                        processingLevel = processingLevel,
                        source = source,
                        type = contentType,
                        lexerRulesProvider = this,
                        parserRuleProvider = this,
                        contentOrigin = ContentOrigin.SecondarySource))
        notifyChanged()
    }

    override fun attachTo(parent: DomNodeAttachable) {
        super.attachTo(parent)
        valueElement?.let {
            if (it is DomNodeAttachable) {
                it.attachTo(parent)
            }
        }
    }

    private fun setBacking(token: DomToken?) {
        val previousBacking = valueElement
        valueElement = token
        setConcreteContent(previousBacking, token)
    }

    fun getObject(): T? {
        ensureAbstractContent()
        return valueElement?.let {
            @Suppress("UNCHECKED_CAST")
            if (contentType.isAssignableFrom(it::class.java)) it as T else null
        }
    }

    override fun initialiseAbstractContent() {
        (valueElement as? DomNodeContentManager)?.ensureAbstractContent()
    }

    override fun initialiseConcreteContainerContent(builder: ConcreteContentBuilder) {
        if (builder.supportsUnorderedContent()) {
            getObject()?.let { builder.positionBefore(it.backing) }
        }
        super.initialiseConcreteContainerContent(builder)
    }

    final override fun initialiseConcreteContent(builder: ConcreteContentBuilder) {
        valueElement?.let {
            initialiseConcreteOpen(builder)
            builder.then(it)
            initialiseConcreteClose(builder)
        }
    }

    open fun initialiseConcreteOpen(builder: ConcreteContentBuilder) {
        return
    }

    open fun initialiseConcreteClose(builder: ConcreteContentBuilder) {
        return
    }

    override fun replaceValueWithContent(value: Any, content: String, processingLevel: SourceProcessingLevel): Boolean {
        return if (valueElement == value) {
            set(content, processingLevel)
            true
        } else {
            false
        }
    }

    override fun clear() {
        set(null)
    }
}
package modool.dom.property.value

import modool.core.content.ContentOrigin
import modool.core.meta.property.PropertyMeta
import modool.dom.DomContext
import modool.dom.node.DomElementBacked
import modool.dom.token.DomNodeContainerStartToken
import modool.dom.token.DomToken

abstract class DomDynamicElementProperty<D, T>(
        metadata: PropertyMeta,
        context: D,
        contentType: Class<T>,
        valueElement: DomToken?,
        initialisedAbstract: Boolean,
        initialisedConcrete: Boolean,
        container: DomNodeContainerStartToken?,
        contentOrigin: ContentOrigin
) : DomElementProperty<D, T>(
        metadata, context, contentType, valueElement, initialisedAbstract, initialisedConcrete, container, contentOrigin),
        DomDynamicElementValue<D, T>
        where D : DomContext,
              T : DomElementBacked<D> {

    constructor(metadata: PropertyMeta, context: D, contentType: Class<T>) : this(
            metadata, context,
            contentType,
            valueElement = null,
            initialisedAbstract = false, initialisedConcrete = false,
            container = null,
            contentOrigin = ContentOrigin.API)
}
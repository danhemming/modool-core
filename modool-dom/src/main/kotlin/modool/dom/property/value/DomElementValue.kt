package modool.dom.property.value

import modool.core.content.SourceProcessingLevel
import modool.dom.DomContext
import modool.dom.node.DomElementBacked

interface DomElementValue<D, in T>
        where D : DomContext,
              T : DomElementBacked<D> {

    fun setAsText(source: String) {
        set(source, SourceProcessingLevel.NONE)
    }

    fun setAsTokens(source: String) {
        set(source, SourceProcessingLevel.TOKENISE)
    }

    fun setParsed(source: String) {
        set(source, SourceProcessingLevel.PARSE)
    }

    fun set(source: String, processingLevel: SourceProcessingLevel)
    fun set(dsl: T?)
}
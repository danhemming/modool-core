package modool.dom.property.value

import modool.core.meta.property.PropertyMeta
import modool.dom.DomContext
import modool.dom.node.text.DomTerminalNode
import modool.dom.node.text.factory.ConstantDomTerminalFactory

/**
 * Use when a constant is optional.
 * Creates a container to wrap the constants placement.
 */
@Deprecated("Use ModoolConstantProperty with container support")
open class DomOptionalConstantProperty<D, F>(
        metadata: PropertyMeta,
        context: D
) : DomValueProperty<D, F?>(metadata, context)
        where D : DomContext,
              F : ConstantDomTerminalFactory {

    private var factory: F? = null

    override val isContainerInitiallyDisplayed: Boolean get() = factory != null

    override fun setBackingTerminal(terminal: DomTerminalNode?) {
        super.setBackingTerminal(terminal)
        @Suppress("UNCHECKED_CAST")
        factory = terminal?.constantFactory as F?
    }

    override fun set(value: F?) {
        factory = value
        setBackingTerminal(factory?.createNode())
    }

    override fun get(): F? {
        return factory
    }
}
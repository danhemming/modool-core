package modool.dom.property.value

import modool.core.content.ContentOrigin
import modool.core.meta.property.PropertyMeta
import modool.dom.DomContext
import modool.dom.node.ConcreteContentBuilder
import modool.dom.node.text.DomTerminalNode
import modool.dom.property.DomBaseProperty
import modool.dom.token.DomNodeContainerStartToken

/**
 * Extend and override to add necessary concrete content.
 */
abstract class DomValueProperty<out D, V>(
        metadata: PropertyMeta,
        context: D,
        private var valueTerminal: DomTerminalNode?,
        initialisedAbstract: Boolean,
        initialisedConcrete: Boolean,
        container: DomNodeContainerStartToken?,
        contentOrigin: ContentOrigin
) : DomBaseProperty<D>(metadata, context, initialisedAbstract, initialisedConcrete, container, contentOrigin),
        DomTerminalProperty<D, V>
        where D : DomContext {

    constructor(metadata: PropertyMeta, context: D) : this(
            metadata, context,
            valueTerminal = null,
            initialisedAbstract = false, initialisedConcrete = false,
            container = null,
            contentOrigin = ContentOrigin.API)

    override val isContainerInitiallyDisplayed: Boolean
        get() = valueTerminal != null

    var onContentProvided: (() -> Unit)? = null

    override fun setBackingTerminal(terminal: DomTerminalNode?) {
        if (terminal != valueTerminal) {
            val oldNode = valueTerminal
            valueTerminal = terminal
            setConcreteContent(oldNode, terminal)
            notifyChanged()
        }
    }

    protected fun setMandatory(value: String) {
        if (value != getOptional()) {
            getMandatoryBackingTerminal().set(value)
            notifyChanged()
        }
    }

    protected fun getMandatory(): String {
        return getMandatoryBackingTerminal().get()
    }

    protected fun setOptional(value: String?) {
        val originalValue = get()
        if (value == null) {
            setBackingTerminal(null)
        } else if (value != originalValue) {
            if (getOptionalBackingTerminal() == null) {
                setBackingTerminal(createTerminal(value))
                onContentProvided?.invoke()
            } else {
                getMandatoryBackingTerminal().set(value)
                if (originalValue == null) {
                    onContentProvided?.invoke()
                }
            }
            notifyChanged()
        }
    }

    protected fun getOptional(): String? {
        return getOptionalBackingTerminal()?.get()
    }

    protected fun getOptionalBackingTerminal(): DomTerminalNode? {
        return valueTerminal
    }

    protected fun getMandatoryBackingTerminal(): DomTerminalNode {
        ensureAbstractContent()
        return valueTerminal!!
    }

    override fun initialiseAbstractContent() {
        if (valueTerminal == null) {
            valueTerminal = createDefaultTerminal()
        }
    }

    final override fun initialiseConcreteContent(builder: ConcreteContentBuilder) {
        valueTerminal?.let {
            initialiseConcreteOpen(builder)
            builder.then(it)
            initialiseConcreteClose(builder)
        }
    }

    open fun initialiseConcreteOpen(builder: ConcreteContentBuilder) {
        return
    }

    open fun initialiseConcreteClose(builder: ConcreteContentBuilder) {
        return
    }

    /**
     * Required for optional string processing
     */
    protected open fun createTerminal(value: String): DomTerminalNode {
        throw RuntimeException("An empty terminal could not be constructed")
    }

    /**
     * Required for setting initial state (never provided for optional content)
     */
    protected open fun createDefaultTerminal(): DomTerminalNode? {
        return null
    }

    override fun clear() {
        setOptional(null)
    }
}
package modool.dom.property.value

import modool.core.content.ContentOrigin
import modool.core.meta.property.PropertyMeta
import modool.dom.DomContext
import modool.dom.node.text.DomTerminalNode
import modool.dom.style.builder.StyleRuleProvider
import modool.dom.token.DomNodeContainerStartToken

abstract class DomBaseNameProperty<D, V>(
        metadata: PropertyMeta,
        context: D,
        valueTerminal: DomTerminalNode?,
        initialisedAbstract: Boolean,
        initialisedConcrete: Boolean,
        container: DomNodeContainerStartToken?,
        contentOrigin: ContentOrigin
) : DomValueProperty<D, V>(
        metadata, context, valueTerminal, initialisedAbstract, initialisedConcrete, container, contentOrigin),
        DomNameProperty<D, V>
        where D : DomContext {

    constructor(metadata: PropertyMeta, context: D): this(
            metadata, context,
            valueTerminal = null,
            initialisedAbstract = false, initialisedConcrete = false,
            container = null,
            contentOrigin = ContentOrigin.API)

    private val formatter: StyleRuleProvider by lazy { context.styler.getStyleRuleProvider(this) }

    protected fun applyStyling(name: String): String {
        return formatter.getRelativeNameConvention()?.convention?.apply(name) ?: name
    }
}
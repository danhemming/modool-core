package modool.dom.property.value

import modool.dom.DomContext
import modool.dom.node.DomElementBacked

interface DomDynamicElementValue<D, T>
    : DomElementValue<D, T>
        where D : DomContext,
              T : DomElementBacked<D> {

    fun get(): T?
}
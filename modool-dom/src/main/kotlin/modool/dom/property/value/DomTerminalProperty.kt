package modool.dom.property.value

import modool.dom.DomContext
import modool.dom.node.text.DomTerminalNode
import modool.core.content.WritableValue
import modool.dom.property.DomProperty

/**
 * Primitive properties containing simple values.
 */

interface DomTerminalProperty<out D, V>
    : DomProperty<D>,
        WritableValue<V>
        where D : DomContext {

    fun setBackingTerminal(terminal: DomTerminalNode?)
}
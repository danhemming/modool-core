package modool.dom.property.value

import modool.core.content.ContentOrigin
import modool.dom.DomContext
import modool.core.meta.property.PropertyMeta
import modool.dom.node.text.DomTerminalNode
import modool.dom.token.DomNodeContainerStartToken

abstract class DomCharacterProperty<out D>(
        metadata: PropertyMeta,
        context: D,
        valueTerminal: DomTerminalNode?,
        initialisedAbstract: Boolean,
        initialisedConcrete: Boolean,
        container: DomNodeContainerStartToken?,
        contentOrigin: ContentOrigin
) : DomStringEncodedValueProperty<D, Char?>(
        metadata, context, valueTerminal, initialisedAbstract, initialisedConcrete, container, contentOrigin)
        where D : DomContext {

    constructor(metadata: PropertyMeta, context: D): this(
            metadata, context,
            valueTerminal = null,
            initialisedAbstract = false, initialisedConcrete = false,
            container = null,
            contentOrigin = ContentOrigin.API)

    override fun encodeValue(value: Char?): String {
        return value?.toString() ?: ""
    }

    override fun decodeValue(value: String): Char? {
        return if (value == "") null else value[0]
    }
}
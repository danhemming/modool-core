package modool.dom.property.value

import modool.core.content.ContentOrigin
import modool.dom.DomContext
import modool.core.meta.property.PropertyMeta
import modool.dom.node.text.DomTerminalNode
import modool.dom.node.text.factory.CommonDynamicTerminals
import modool.dom.token.DomNodeContainerStartToken

/**
 * A string must always be decode-able into a non-nullable value. A NULL value is most likely represented by another
 * element type!
 * e.g. 1.0 may be a numeric literal with a string encoded value property of "1.0".  Whereas "null" is represented by
 * another element type entirely.
 */
abstract class DomStringEncodedValueProperty<out D, V>(
        metadata: PropertyMeta,
        context: D,
        valueTerminal: DomTerminalNode?,
        initialisedAbstract: Boolean,
        initialisedConcrete: Boolean,
        container: DomNodeContainerStartToken?,
        contentOrigin: ContentOrigin
) : DomValueProperty<D, V>(
        metadata, context, valueTerminal, initialisedAbstract, initialisedConcrete, container, contentOrigin)
        where D : DomContext {

    constructor(metadata: PropertyMeta, context: D): this(
            metadata, context,
            valueTerminal = null,
            initialisedAbstract = false, initialisedConcrete = false,
            container = null,
            contentOrigin = ContentOrigin.API)

    override fun set(value: V) {
        ensureAbstractContent()
        setMandatory(encodeValue(value))
    }

    override fun get(): V {
        return decodeValue(getMandatoryBackingTerminal().get())
    }

    protected abstract fun encodeValue(value: V): String

    protected abstract fun decodeValue(value: String): V

    override fun createDefaultTerminal(): DomTerminalNode {
        return CommonDynamicTerminals.untypedLiteral("")
    }
}
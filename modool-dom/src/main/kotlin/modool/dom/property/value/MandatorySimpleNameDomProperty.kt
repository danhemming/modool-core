package modool.dom.property.value

import modool.dom.DomContext
import modool.core.meta.property.PropertyMeta
import modool.dom.node.text.DomTerminalNode
import modool.dom.node.text.factory.CommonDynamicTerminals

/**
 * Represents an id on an element where that id can be represented by a single token
 */
@Deprecated("This should be replaced with specific implementations")
open class MandatorySimpleNameDomProperty<D>(
        metadata: PropertyMeta,
        context: D
) : DomBaseNameProperty<D, String>(metadata, context)
        where D : DomContext {

    override fun set(value: String) {
        setMandatory(applyStyling(value))
    }

    override fun setUnstyled(value: String) {
        setMandatory(value)
    }

    override fun get(): String {
        return getMandatory()
    }

    override fun createDefaultTerminal(): DomTerminalNode {
        return CommonDynamicTerminals.untagged("")
    }
}
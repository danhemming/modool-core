package modool.dom.property.value

import modool.core.content.ContentOrigin
import modool.dom.DomContext
import modool.core.meta.property.PropertyMeta
import modool.dom.node.text.DomTerminalNode
import modool.dom.token.DomNodeContainerStartToken

open class DomInt64Property<out D>(
        metadata: PropertyMeta,
        context: D,
        valueTerminal: DomTerminalNode?,
        initialisedAbstract: Boolean,
        initialisedConcrete: Boolean,
        container: DomNodeContainerStartToken?,
        contentOrigin: ContentOrigin
) : DomNumericProperty<D, Long>(
        metadata, context, valueTerminal, initialisedAbstract, initialisedConcrete, container, contentOrigin)
        where D : DomContext {

    constructor(metadata: PropertyMeta, context: D): this(
            metadata, context,
            valueTerminal = null,
            initialisedAbstract = false, initialisedConcrete = false,
            container = null,
            contentOrigin = ContentOrigin.API)

    override fun encodeValue(value: Long): String {
        return value.toString()
    }

    override fun decodeValue(value: String): Long {
        return value.toLong()
    }
}
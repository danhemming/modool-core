package modool.dom.property.value

import modool.dom.DomContext
import modool.dom.property.DomProperty

interface DomNameProperty<D, V>
    : DomTerminalProperty<D, V>
        where D : DomContext {

    fun setUnstyled(value: V)
}
package modool.dom.property.value

import modool.core.content.ContentOrigin
import modool.core.content.WritableValue
import modool.core.meta.property.PropertyMeta
import modool.dom.DomContext
import modool.dom.node.text.DomTerminalNode
import modool.dom.node.text.factory.ConstantDomTerminalFactory
import modool.dom.token.DomNodeContainerStartToken

abstract class DomToggleProperty<D>(
        metadata: PropertyMeta,
        context: D,
        private val toggledContentFactory: ConstantDomTerminalFactory,
        private val defaultToggleValue: Boolean,
        valueTerminal: DomTerminalNode?,
        initialisedAbstract: Boolean,
        initialisedConcrete: Boolean,
        container: DomNodeContainerStartToken?,
        contentOrigin: ContentOrigin
) : DomValueProperty<D, Boolean>(
        metadata, context, valueTerminal, initialisedAbstract, initialisedConcrete, container, contentOrigin),
        WritableValue<Boolean>
        where D : DomContext {

    constructor(
            metadata: PropertyMeta,
            context: D,
            toggledContentFactory: ConstantDomTerminalFactory,
            defaultToggleValue: Boolean
    ) : this(
            metadata, context,
            toggledContentFactory, defaultToggleValue,
            valueTerminal = null,
            initialisedAbstract = false,
            initialisedConcrete = false,
            container = null,
            contentOrigin = ContentOrigin.API)

    override val isContainerInitiallyDisplayed get() = defaultToggleValue || getOptionalBackingTerminal() != null

    override fun set(value: Boolean) {
        ensureAbstractContent()
        container!!.displayed = value
    }

    override fun get(): Boolean {
        ensureAbstractContent()
        return container!!.displayed
    }

    override fun initialiseAbstractManagementContent() {
        super.initialiseAbstractManagementContent()
        // As we are using the container visible property as a proxy for our property value, we need to make sure it exists
        if (container == null) {
            container = createAndConfigureContainer()
        }
    }

    override fun createDefaultTerminal(): DomTerminalNode? {
        return toggledContentFactory.createNode()
    }
}
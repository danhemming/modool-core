package modool.dom.property.value

import modool.dom.DomContext
import modool.core.meta.property.PropertyMeta
import modool.dom.node.text.DomTerminalNode
import modool.dom.node.text.factory.ConstantDomTerminalFactory

/**
 * Should be used ONLY when the constant is mandatory because we do not create a wrapping container.  Without a container
 * we cannot place late arriving (API) values.
 */
@Deprecated("Use ModoolConstantProperty instead")
open class MandatoryConstantDomProperty<D, F>(
        metadata: PropertyMeta,
        context: D,
        private val defaultValueFactory: F
) : DomValueProperty<D, F>(metadata, context)
        where D : DomContext,
              F : ConstantDomTerminalFactory {

    private var factory: F? = null

    override val isContainerInitiallyDisplayed = true

    override fun setBackingTerminal(terminal: DomTerminalNode?) {
        super.setBackingTerminal(terminal)
        @Suppress("UNCHECKED_CAST")
        factory = terminal?.constantFactory as F?
    }

    override fun set(value: F) {
        factory = value
        setBackingTerminal(factory?.createNode())
    }

    override fun get(): F {
        return factory!!
    }

    override fun createDefaultTerminal(): DomTerminalNode? {
        return defaultValueFactory.createNode()
    }
}
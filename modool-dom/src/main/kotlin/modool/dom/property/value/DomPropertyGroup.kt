package modool.dom.property.value

import modool.core.content.ContentOrigin
import modool.core.meta.property.PropertyMeta
import modool.dom.DomContext
import modool.dom.node.DomPropertyContainer
import modool.dom.property.DomBaseProperty
import modool.dom.property.DomProperty
import modool.dom.style.DisplayStyleable
import modool.dom.token.DomNodeContainerStartToken

/**
 * A collection of properties
 */

abstract class DomPropertyGroup<D>(
        metadata: PropertyMeta,
        context: D,
        private val propertyList: MutableList<DomProperty<*>>,
        initialisedAbstract: Boolean,
        initialisedConcrete: Boolean,
        container: DomNodeContainerStartToken?,
        contentOrigin: ContentOrigin
) : DomBaseProperty<D>(
        metadata, context, initialisedAbstract, initialisedConcrete, container, contentOrigin),
        DomProperty<D>,
        DomPropertyContainer,
        DisplayStyleable
        where D : DomContext {

    constructor(metadata: PropertyMeta, context: D) : this(
            metadata, context,
            propertyList = mutableListOf<DomProperty<*>>(),
            initialisedConcrete = false, initialisedAbstract = false,
            container = null,
            contentOrigin = ContentOrigin.API)

    override val modelProperties: Sequence<DomProperty<*>> get() = propertyList.asSequence()

    override var displayed: Boolean
        get() = container?.displayed ?: false
        set(value) {
            container?.displayed = value
        }

    override fun attachModelProperty(property: DomProperty<*>) {
        property.attachTo(this)
        propertyList.add(property)
    }

    override fun detachModelProperty(property: DomProperty<*>): Boolean {
        return if (propertyList.remove(property)) {
            property.detachFromParent()
            true
        } else {
            false
        }
    }

    override fun notifyChanged() {
        // If the user changes the content of one of our properties then lets assume they intend for it to be displayed
        container?.displayed = true
        super.notifyChanged()
    }

    override fun initialiseAbstractManagementContent() {
        super.initialiseAbstractManagementContent()
        modelProperties.forEach { it.ensureAbstractContent() }
    }

    override fun clear() {
        modelProperties.forEach { it.clear() }
    }
}
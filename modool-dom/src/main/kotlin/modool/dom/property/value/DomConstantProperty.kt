package modool.dom.property.value

import modool.core.content.ContentOrigin
import modool.dom.DomContext
import modool.core.meta.property.PropertyMeta
import modool.dom.node.ConcreteContentBuilder
import modool.dom.node.text.DomTerminalNode
import modool.dom.node.text.factory.ConstantDomTerminalFactory
import modool.dom.token.DomNodeContainerStartToken

/**
 * Holds values that are constants e.g. enum values
 * Container support must be added by descendants to support optional placement
 */
open class DomConstantProperty<D, F>(
        metadata: PropertyMeta,
        context: D,
        private val defaultValueFactory: F?,
        private var factory: F? = null,
        valueTerminal: DomTerminalNode?,
        initialisedAbstract: Boolean,
        initialisedConcrete: Boolean,
        container: DomNodeContainerStartToken?,
        contentOrigin: ContentOrigin
) : DomValueProperty<D, F?>(
        metadata, context, valueTerminal, initialisedAbstract, initialisedConcrete, container, contentOrigin)
        where D : DomContext,
              F : ConstantDomTerminalFactory {

    constructor(metadata: PropertyMeta, context: D, defaultValueFactory: F?): this(
            metadata, context,
            defaultValueFactory,
            valueTerminal = null,
            initialisedAbstract = false, initialisedConcrete = false,
            container = null,
            contentOrigin = ContentOrigin.API)

    constructor(metadata: PropertyMeta, context: D): this(metadata, context, defaultValueFactory = null)

    override fun setBackingTerminal(terminal: DomTerminalNode?) {
        super.setBackingTerminal(terminal)
        @Suppress("UNCHECKED_CAST")
        factory = terminal?.constantFactory as F?
    }

    override fun set(value: F?) {
        factory = value
        setBackingTerminal(factory?.createNode())
    }

    override fun get(): F? {
        return factory
    }

    override fun initialiseConcreteContainerContent(builder: ConcreteContentBuilder) {
        if (builder.supportsUnorderedContent()) {
            getOptionalBackingTerminal()?.let { builder.positionBefore(it) }
        }
        super.initialiseConcreteContainerContent(builder)
    }

    override fun createDefaultTerminal(): DomTerminalNode? {
        return defaultValueFactory?.createNode()
    }

    protected fun hasValue(): Boolean {
        return factory != null
    }
}
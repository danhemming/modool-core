package modool.dom.property.sequence.element.map

import modool.dom.DomContext
import modool.dom.node.DomElementBacked
import modool.dom.property.DomProperty
import modool.dom.property.sequence.DomContextualisedSequence
import modool.dom.property.sequence.element.DomElementMapMutableOperable
import modool.dom.property.sequence.element.definition.ConstructableElementDefinition
import modool.dom.property.sequence.element.definition.IdentifiableElementDefinition
import modool.dom.identity.Identifiable
import modool.dom.identity.Identifier

interface DomElementBackedMappableProperty<D, T, in K>
    : DomContextualisedSequence<D, T>,
        DomElementMapMutableOperable<D, T, K>,
        DomProperty<D>
        where D : DomContext,
              T : DomElementBacked<D>,
              T : Identifiable,
              K : Identifier {

    override fun <W, U> add(definition: U, builder: (W.() -> Unit)?): W
            where W : T,
                  U : IdentifiableElementDefinition<D, K, W>,
                  U : ConstructableElementDefinition<D, W> {
        ensureAbstractContent()
        val newElement = definition.create(context)
        builder?.invoke(newElement)
        return add(newElement)
    }

    override fun addParsed(source: String): T {
        ensureAbstractContent()
        @Suppress("UNCHECKED_CAST")
        return add(createParsed(source))
    }

    override fun <W, U> addIfNotPresent(definition: U, builder: (W.() -> Unit)?): W
            where W : T,
                  U : IdentifiableElementDefinition<D, K, W>,
                  U : ConstructableElementDefinition<D, W> {
        ensureAbstractContent()
        @Suppress("UNCHECKED_CAST")
        return get(definition.key) as W? ?: add(definition, builder)
    }

    override fun addIfNotPresent(source: String): T? {
        ensureAbstractContent()
        @Suppress("UNCHECKED_CAST")
        val obj = createParsed(source)
        return if (contains(obj)) {
            null
        } else {
            add(obj)
            obj
        }
    }

    override fun <W, U> update(definition: U, builder: (W.() -> Unit)?): W
            where W : T,
                  U : IdentifiableElementDefinition<D, K, W> {
        ensureAbstractContent()
        @Suppress("UNCHECKED_CAST")
        val obj = get(definition.key) as W?
        if (obj == null) {
            throw RuntimeException("object with key: ${definition.key} not found")
        } else {
            builder?.invoke(obj)
        }
        return obj
    }

    override fun <W, U> updateIfPresent(definition: U, builder: (W.() -> Unit)?): W?
            where W : T,
                  U : IdentifiableElementDefinition<D, K, W> {
        ensureAbstractContent()
        @Suppress("UNCHECKED_CAST")
        val obj = get(definition.key) as W?
        if (obj == null) {
            return null
        } else {
            builder?.invoke(obj)
        }
        return obj
    }

    override fun <W, U> merge(definition: U, builder: (W.() -> Unit)?): W
            where W : T,
                  U : IdentifiableElementDefinition<D, K, W>,
                  U : ConstructableElementDefinition<D, W> {
        ensureAbstractContent()
        @Suppress("UNCHECKED_CAST")
        var obj = get(definition.key) as W?
        if (obj == null) {
            obj = definition.create(context)
            builder?.invoke(obj)
            add(obj)
        } else {
            builder?.invoke(obj)
        }
        return obj
    }
}
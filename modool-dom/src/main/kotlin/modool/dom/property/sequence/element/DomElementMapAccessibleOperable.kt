package modool.dom.property.sequence.element

import modool.dom.DomContext
import modool.dom.node.DomElementBacked
import modool.dom.identity.Identifier

interface DomElementMapAccessibleOperable<D, T, in K>
        where D : DomContext,
              T : DomElementBacked<D>,
              K : Identifier {

    operator fun get(key: K): T?
    fun <W : T> filter(key: K, type: Class<W>): Sequence<W>
}
package modool.dom.property.sequence.element.map

import modool.dom.DomContext
import modool.dom.identity.Identifiable
import modool.dom.identity.NameIndexer
import modool.dom.node.DomElementBacked

/** Note: Identifiable by name does not mean that the element itself has a name property **/

interface DomNamedElementBackedMap<D, T>
    : DomElementBackedMap<D, T, NameIndexer>
        where D : DomContext,
              T : DomElementBacked<D>,
              T : Identifiable {

    operator fun get(name: String): T? {
        return get(NameIndexer(name))
    }

    fun remove(name: String): Boolean {
        return remove(NameIndexer(name))
    }
}
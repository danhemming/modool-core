package modool.dom.property.sequence.element

import modool.dom.DomContext
import modool.dom.node.DomElementBacked

/**
 * Used by the element factory to populate the collection, not exposed to AST classes in most cases
 */
interface DomElementModifiableOperable<D, T, in K>
    : DomElementAppendableOperable<D, T>
        where D : DomContext,
              T : DomElementBacked<D> {

    fun update(key: K, builder: T.() -> Unit): T?

    fun <W> merge(element: W): W
            where W : T {
        TODO("Merging an element without an identity is not yet implemented")
    }
}
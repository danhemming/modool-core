package modool.dom.property.sequence.node

import modool.core.content.ContentOrigin
import modool.dom.DomContext
import modool.core.meta.property.PropertyMeta
import modool.dom.node.DomNode
import modool.dom.property.sequence.DomBaseViewProperty
import modool.dom.token.DomNodeContainerStartToken
import modool.dom.token.DomToken
import modool.dom.token.DomViewPlaceHolderToken

abstract class DomNodeViewDomProperty<D, T>(
        metadata: PropertyMeta,
        context: D,
        placeholder: DomViewPlaceHolderToken<D, T>?,
        contentType: Class<T>,
        initialisedAbstract: Boolean,
        initialisedConcrete: Boolean,
        container: DomNodeContainerStartToken?,
        contentOrigin: ContentOrigin
) : DomBaseViewProperty<D, T>(
        metadata, context, placeholder, contentType, initialisedAbstract, initialisedConcrete, container, contentOrigin),
      DomTerminalView<D, T>
        where D : DomContext,
              T : DomNode,
              T : DomToken {

    constructor(meta: PropertyMeta, context: D, contentType: Class<T>) : this(
            meta, context,
            placeholder = null,
            contentType = contentType,
            initialisedAbstract = false, initialisedConcrete = false,
            container = null,
            contentOrigin = ContentOrigin.API)

    override fun convertMemberToToken(member: T): DomToken {
        return member
    }
}
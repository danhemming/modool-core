package modool.dom.property.sequence.node.set

import modool.dom.DomContext
import modool.dom.node.text.DomTerminalNode
import modool.dom.node.text.factory.ConstantDomTerminalFactory
import modool.dom.property.sequence.DomSetOperable
import modool.dom.property.sequence.node.DomTerminalView

interface DomConstantTerminalSetView<D, F>
    : DomTerminalView<D, DomTerminalNode>,
        DomSetOperable<D, F>
        where D : DomContext,
              F : ConstantDomTerminalFactory {

    fun contains(itemFactory: F): Boolean
}
package modool.dom.property.sequence.node

import modool.dom.DomContext
import modool.dom.node.DomNode
import modool.dom.property.sequence.DomCollectionProperty
import modool.dom.token.DomToken

interface DomNodeCollection<D, T>
    : DomCollectionProperty<D, T>
        where D : DomContext,
              T : DomNode,
              T : DomToken
package modool.dom.property.sequence.node.set

import modool.core.content.ContentOrigin
import modool.dom.DomContext
import modool.core.meta.property.PropertyMeta
import modool.dom.exists
import modool.dom.node.text.DomTerminalNode
import modool.dom.node.text.factory.ConstantDomTerminalFactory
import modool.dom.property.sequence.DomViewProperty
import modool.dom.property.sequence.node.DomNodeViewDomProperty
import modool.dom.token.DomNodeContainerStartToken
import modool.dom.token.DomViewPlaceHolderToken

/**
 * The mechanism by which the view works, in conjunction with the fact we are value/text matching constants MAY allow
 * for errors.  This can only occur where two sets can contain the same constants, in the same parent container.
 */
abstract class DomConstantTerminalSetViewProperty<D, F>(
        metadata: PropertyMeta,
        context: D,
        private val actualContentType: Class<F>,
        placeholder: DomViewPlaceHolderToken<D, DomTerminalNode>?,
        initialisedAbstract: Boolean,
        initialisedConcrete: Boolean,
        container: DomNodeContainerStartToken?,
        contentOrigin: ContentOrigin
) : DomNodeViewDomProperty<D, DomTerminalNode>(
        metadata, context, placeholder, DomTerminalNode::class.java, initialisedAbstract, initialisedConcrete, container, contentOrigin),
        DomConstantTerminalSetView<D, F>,
        DomViewProperty<D, DomTerminalNode>
        where D : DomContext,
              F : ConstantDomTerminalFactory {

    constructor(meta: PropertyMeta, context: D, actualContentType: Class<F>) : this(
            meta, context,
            placeholder = null,
            actualContentType = actualContentType,
            initialisedAbstract = false, initialisedConcrete = false,
            container = null,
            contentOrigin = ContentOrigin.API)

    override fun iterator(): Iterator<DomTerminalNode> {
        ensureAbstractContent()
        return container!!.asRecursiveTypeSequence(contentType).filter { it.isProducedFromFactoryType(actualContentType) }.iterator()
    }

    override fun contains(itemFactory: F): Boolean {
        ensureAbstractContent()
        return container!!.asRecursiveTypeSequence(contentType).filter { itemFactory.isProduct(it) }.exists()
    }

    override fun put(item: F) {
        if (!contains(item)) {
            addToken(item.createNode())
        }
    }

    override fun remove(item: F): Boolean {
        return removeToken(item.createNode())
    }
}
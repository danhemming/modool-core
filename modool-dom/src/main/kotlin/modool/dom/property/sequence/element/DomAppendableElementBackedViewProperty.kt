package modool.dom.property.sequence.element

import modool.core.content.SourceProcessingLevel
import modool.core.content.ContentOrigin
import modool.core.meta.property.PropertyMeta
import modool.lexer.LexerRulesProvider
import modool.parser.ParserRuleProvider
import modool.dom.DomContext
import modool.dom.node.DomElementBacked
import modool.dom.token.DomNodeContainerStartToken
import modool.dom.token.DomViewPlaceHolderToken

abstract class DomAppendableElementBackedViewProperty<D, T>(
        metadata: PropertyMeta,
        context: D,
        placeholder: DomViewPlaceHolderToken<D, T>?,
        contentType: Class<T>,
        initialisedAbstract: Boolean,
        initialisedConcrete: Boolean,
        container: DomNodeContainerStartToken?,
        contentOrigin: ContentOrigin
) : DomElementBackedViewDomProperty<D, T>(
        metadata, context, placeholder, contentType, initialisedAbstract, initialisedConcrete, container, contentOrigin),
        DomElementAppendableOperable<D, T>,
        LexerRulesProvider,
        ParserRuleProvider
        where D : DomContext,
              T : DomElementBacked<D> {

    constructor(meta: PropertyMeta, context: D, contentType: Class<T>) : this(
            meta, context,
            placeholder = null,
            contentType = contentType,
            initialisedAbstract = false, initialisedConcrete = false,
            container = null,
            contentOrigin = ContentOrigin.API)

    override fun <W : T> add(element: W): W {
        addToken(convertMemberToToken(element))
        return element
    }

    override fun add(source: String, processingLevel: SourceProcessingLevel) {
        addToken(context.process(
                processingLevel = processingLevel,
                source = source,
                type = contentType,
                lexerRulesProvider = this,
                parserRuleProvider = this,
                contentOrigin = ContentOrigin.SecondarySource))
    }

    @Suppress("UNCHECKED_CAST")
    override fun createParsed(source: String): T {
        return context.process(
                processingLevel = SourceProcessingLevel.PARSE,
                source = source,
                type = contentType,
                lexerRulesProvider = this,
                parserRuleProvider = this,
                contentOrigin = ContentOrigin.SecondarySource) as T
    }
}
package modool.dom.property.sequence.element.definition

import modool.core.content.ContentOrigin
import modool.dom.DomContext
import modool.dom.buildElement
import modool.dom.node.*
import modool.dom.property.sequence.DomAppendableOperable
import modool.dom.type.Type
import modool.dom.identity.Identifiable
import modool.dom.identity.ParameterisedIndexer
import modool.dom.name.MandatoryRenameable

class ParameterisedElementObjectDefinition<D, T, P, C, K>(
        key: K,
        parameterisedContainer: DomElementFactory<D, C>,
        private val parameter: DomElementFactory<D, P>
) : IdentifiableElementObjectDefinition<D, C, K>(key, parameterisedContainer),
        ConstructableElementDefinition<D, C>
        where D : DomContext,
              T : Type,
              P : DomElementBacked<D>,
              P : Identifiable,
              P : MandatoryRenameable,
              P : Parameter<T>,
              C : Identifiable,
              C : MandatoryRenameable,
              C : DomElementBacked<D>,
              C : Parameterised<D, T, P>,
              K : ParameterisedIndexer {

    override fun create(context: D): C {
        return super.create(context).apply {
            @Suppress("UNCHECKED_CAST")
            (parameters as? DomAppendableOperable<D, P>)?.let { targetParameters ->
                key.parameters.forEach {
                    context.buildElement(parameter, ContentOrigin.API) {
                        name = it.name
                        setTypeFromDescriptor(it.typeDescriptor)
                        targetParameters.add(this)
                    }
                }
            }
        }
    }

    override fun applyKeyToElement(key: K, element: C) {
        element.name = key.name
    }
}
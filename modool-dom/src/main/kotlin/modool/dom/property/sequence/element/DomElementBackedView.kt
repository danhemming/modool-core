package modool.dom.property.sequence.element

import modool.dom.DomContext
import modool.dom.node.DomElementBacked
import modool.dom.property.sequence.DomContextualisedSequence

interface DomElementBackedView<D, T>
    : DomContextualisedSequence<D, T>
        where D : DomContext,
              T : DomElementBacked<D>


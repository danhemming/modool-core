package modool.dom.property.sequence.element

import modool.core.content.ContentOrigin
import modool.core.meta.property.PropertyMeta
import modool.dom.DomContext
import modool.dom.node.DomElementBacked
import modool.dom.property.sequence.DomBaseViewProperty
import modool.dom.token.DomNodeContainerStartToken
import modool.dom.token.DomToken
import modool.dom.token.DomViewPlaceHolderToken

/**
 * A view is one where the contents are spread over a container in an indeterminate way with other contents
 * e.g. fields, methods, constructors can appear anywhere in the body of a class.
 */
abstract class DomElementBackedViewDomProperty<D, T>(
        metadata: PropertyMeta,
        context: D,
        placeholder: DomViewPlaceHolderToken<D, T>?,
        contentType: Class<T>,
        initialisedAbstract: Boolean,
        initialisedConcrete: Boolean,
        container: DomNodeContainerStartToken?,
        contentOrigin: ContentOrigin
) : DomBaseViewProperty<D, T>(
        metadata, context, placeholder, contentType, initialisedAbstract, initialisedConcrete, container, contentOrigin),
        ElementBackedViewDomProperty<D, T>
        where D : DomContext,
              T : DomElementBacked<D> {

    constructor(meta: PropertyMeta, context: D, contentType: Class<T>) : this(
            meta, context,
            placeholder = null,
            contentType = contentType,
            initialisedAbstract = false, initialisedConcrete = false,
            container = null,
            contentOrigin = ContentOrigin.API)

    override fun initialiseAbstractContent() {
        super.initialiseAbstractContent()
        forEach { it.backing.attachTo(this) }
    }

    override fun convertMemberToToken(member: T): DomToken {
        return member.backing
    }

    override fun initialiseNewToken(token: DomToken) {
        if (token is DomElementBacked<*>) {
            token.backing.attachTo(this)
            context.ensureConcreteContent(token.backing)
        }
    }

    override fun finaliseRemovedMember(member: T) {
        member.backing.detachFromChain()
    }
}
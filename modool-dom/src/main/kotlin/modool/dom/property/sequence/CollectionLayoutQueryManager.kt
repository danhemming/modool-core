package modool.dom.property.sequence

import modool.dom.node.DomElementBacked
import modool.dom.style.builder.CollectionLayoutQuery

class CollectionLayoutQueryManager(val collection: DomCollectionProperty<*, *>)
    : CollectionLayoutQuery {

    private var isInit: Boolean = false
    private var isEmpty: Boolean = true
    private var isSingleItem: Boolean = false
    private var first: Any? = null
    private var last: Any? = null
    var current: Any? = null

    override fun isEmpty(): Boolean {
        init()
        return isEmpty
    }

    override fun isEmptyAndUserDisplayed(): Boolean {
        return isEmpty() && collection.isDisplayedWhenEmpty
    }

    override fun isEmptyAndUserHidden(): Boolean {
        return isEmpty() && !collection.isDisplayedWhenEmpty
    }

    override fun containsSingle(): Boolean {
        init()
        return isSingleItem
    }

    override fun isItemFirst(): Boolean {
        init()
        return current != null && current == first
    }

    override fun isItemLast(): Boolean {
        init()
        return current != null && current == last
    }

    override fun <T : DomElementBacked<*>> isItemOfType(type: Class<T>): Boolean {
        return current?.let { type.isAssignableFrom(it::class.java) } ?: false
    }

    override fun <T : DomElementBacked<*>> isFirstOfType(type: Class<T>): Boolean {
        init()
        return first?.let { type.isAssignableFrom(it::class.java) } ?: false
    }

    override fun <T : DomElementBacked<*>> isLastOfType(type: Class<T>): Boolean {
        init()
        return last?.let { type.isAssignableFrom(it::class.java) } ?: false
    }

    override fun isItem(predicate: (Any) -> Boolean): Boolean {
        return current?.let { predicate(it) } ?: false
    }

    override fun isFirst(predicate: (Any) -> Boolean): Boolean {
        init()
        return first?.let { predicate(it) } ?: false
    }

    override fun isLast(predicate: (Any) -> Boolean): Boolean {
        init()
        return last?.let { predicate(it) } ?: false
    }

    private fun init() {
        if (isInit) {
            return
        }

        collection.asRepresentationSequence().forEachIndexed { index, item ->
            if (index == 0) {
                first = item
                isEmpty = false
                isSingleItem = true
            } else {
                isSingleItem = false
            }
            last = item
        }

        isInit = true
    }
}
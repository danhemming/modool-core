package modool.dom.property.sequence

import modool.dom.DomContext

interface DomMapOperable<D, T>
        where D : DomContext {
    fun remove(item: T): Boolean
}

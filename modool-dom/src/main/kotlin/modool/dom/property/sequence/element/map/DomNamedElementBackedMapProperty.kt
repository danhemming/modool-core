package modool.dom.property.sequence.element.map

import modool.core.content.ContentOrigin
import modool.dom.DomContext
import modool.core.meta.property.PropertyMeta
import modool.dom.node.DomElementBacked
import modool.dom.identity.Identifiable
import modool.dom.identity.NameIndexer
import modool.dom.token.DomNodeContainerStartToken

abstract class DomNamedElementBackedMapProperty<D, T>(
        metadata: PropertyMeta,
        context: D,
        contentType: Class<T>,
        isDisplayedWhenEmpty: Boolean,
        initialisedAbstract: Boolean,
        initialisedConcrete: Boolean,
        container: DomNodeContainerStartToken?,
        contentOrigin: ContentOrigin
) : DomElementBackedMapProperty<D, T, NameIndexer>(
        metadata, context, contentType, isDisplayedWhenEmpty, initialisedConcrete, initialisedAbstract, container, contentOrigin),
        DomNamedElementBackedMap<D, T>
        where D : DomContext,
              T : DomElementBacked<D>,
              T : Identifiable {

    constructor(metadata: PropertyMeta, context: D, contentType: Class<T>) : this(
            metadata, context, contentType,
            isDisplayedWhenEmpty = false,
            initialisedConcrete = false, initialisedAbstract = false,
            container = null,
            contentOrigin = ContentOrigin.API)
}
package modool.dom.property.sequence

import modool.dom.DomContext

interface DomListOperable<D, T>
    : DomAppendableOperable<D, T>
        where D : DomContext {

    fun <W> addAt(index: Int, element: W): W where W : T
    operator fun get(index: Int): T?
    fun remove(index: Int): Boolean = remove(this[index]!!)
    fun remove(member: T): Boolean
}
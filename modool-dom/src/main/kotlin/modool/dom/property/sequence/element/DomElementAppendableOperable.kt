package modool.dom.property.sequence.element

import modool.dom.DomContext
import modool.core.content.SourceProcessingLevel
import modool.dom.node.DomElementBacked
import modool.dom.property.sequence.DomAppendableOperable

interface DomElementAppendableOperable<D, T>
    : DomAppendableOperable<D, T>
        where D : DomContext,
              T : DomElementBacked<D> {

    fun addAsText(source: String) {
        add(source, SourceProcessingLevel.NONE)
    }

    fun addAsTokens(source: String) {
        add(source, SourceProcessingLevel.TOKENISE)
    }

    fun addParsed(source: String): T {
        return add(createParsed(source))
    }

    fun createParsed(source: String): T

    fun add(source: String, processingLevel: SourceProcessingLevel)
}

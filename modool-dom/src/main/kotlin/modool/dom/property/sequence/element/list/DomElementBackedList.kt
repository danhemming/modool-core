package modool.dom.property.sequence.element.list

import modool.dom.DomContext
import modool.dom.node.DomElementBacked
import modool.dom.property.sequence.DomCollection
import modool.dom.property.sequence.DomListOperable
import modool.dom.property.sequence.element.DomElementModifiableOperable
import modool.dom.property.sequence.element.DomElementBackedCollectionProperty

interface DomElementBackedList<D, T>
    : DomCollection<D, T>,
        DomListOperable<D, T>,
        DomElementModifiableOperable<D, T, Int>
        where D : DomContext,
              T : DomElementBacked<D> {

    operator fun invoke(vararg items: T)
    override operator fun get(index: Int): T? = elementAtOrNull(index)
}
package modool.dom.property.sequence.element.definition

import modool.dom.DomContext
import modool.dom.node.DomElementBacked
import modool.dom.identity.Identifiable
import modool.dom.identity.Identifier
import modool.dom.name.MandatoryRenameable
import modool.dom.node.DomElementFactory

class NamedElementObjectDefinition<D, T, K>(
        key: K,
        elementFactory: DomElementFactory<D, T>
) : IdentifiableElementObjectDefinition<D, T, K>(key, elementFactory),
        ConstructableElementDefinition<D, T>
        where D : DomContext,
              T : DomElementBacked<D>,
              T : Identifiable,
              T : MandatoryRenameable,
              K : Identifier {

    override fun applyKeyToElement(key: K, element: T) {
        element.name = key.value
    }
}
package modool.dom.property.sequence.element.definition

import modool.dom.DomContext
import modool.dom.node.DomElementBacked

interface ConstructableElementDefinition<D, T>
        where D : DomContext,
              T : DomElementBacked<D> {

    fun create(context: D): T
}
package modool.dom.property.sequence.element.definition

import modool.dom.DomContext
import modool.dom.buildElement
import modool.core.content.ContentOrigin
import modool.dom.node.DomElementBacked
import modool.dom.identity.Identifiable
import modool.dom.node.DomElementFactory

abstract class IdentifiableElementObjectDefinition<D, T, K>(
        key: K,
        private val elementFactory: DomElementFactory<D, T>
) : IdentifiableElementDefinition<D, K, T>(key),
        ConstructableElementDefinition<D, T>
        where D : DomContext,
              T : DomElementBacked<D>,
              T : Identifiable {

    override fun create(context: D): T {
        val newElement = context.buildElement(elementFactory, ContentOrigin.API)
        applyKeyToElement(key, newElement)
        return newElement
    }

    protected abstract fun applyKeyToElement(key: K, element: T)
}
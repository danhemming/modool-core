package modool.dom.property.sequence.element.map

import modool.core.content.ContentOrigin
import modool.core.meta.property.PropertyMeta
import modool.dom.DomContext
import modool.dom.identity.Identifiable
import modool.dom.identity.Identifier
import modool.dom.node.DomElementBacked
import modool.dom.property.sequence.element.DomElementBackedCollectionProperty
import modool.dom.token.DomNodeContainerStartToken

abstract class DomElementBackedMapProperty<D, T, K>(
        metadata: PropertyMeta,
        context: D,
        contentType: Class<T>,
        isDisplayedWhenEmpty: Boolean,
        initialisedAbstract: Boolean,
        initialisedConcrete: Boolean,
        container: DomNodeContainerStartToken?,
        contentOrigin: ContentOrigin
) : DomElementBackedCollectionProperty<D, T>(
        metadata, context, contentType, isDisplayedWhenEmpty, initialisedConcrete, initialisedAbstract, container, contentOrigin),
        DomElementBackedMappableProperty<D, T, K>,
        DomElementBackedMap<D, T, K>
        where D : DomContext,
              T : DomElementBacked<D>,
              T : Identifiable,
              K : Identifier {

    constructor(metadata: PropertyMeta, context: D, contentType: Class<T>) : this(
            metadata, context, contentType,
            isDisplayedWhenEmpty = false,
            initialisedConcrete = false, initialisedAbstract = false,
            container = null,
            contentOrigin = ContentOrigin.API)

    override operator fun get(key: K): T? {
        return filter(key, contentType).singleOrNull()
    }

    override fun <W : T> filter(key: K, type: Class<W>): Sequence<W> {
        @Suppress("UNCHECKED_CAST")
        return asSequence().filter { type.isAssignableFrom(it.javaClass) && it.identifier == key }.map { it as W }
    }

    override fun updateAt(index: Int, builder: T.() -> Unit): T {
        ensureAbstractContent()
        @Suppress("UNCHECKED_CAST")
        val element = container?.asRecursiveTypeSequence(contentType)?.elementAtOrNull(index)
        if (element == null) {
            throw RuntimeException("No element found at index $index")
        } else {
            builder.invoke(element)
            return element
        }
    }

    override fun update(key: K, builder: T.() -> Unit): T? {
        ensureAbstractContent()
        @Suppress("UNCHECKED_CAST")
        val element = get(key)
        element?.let {
            builder.invoke(element)
        }
        return element
    }

    override fun remove(item: T): Boolean {
        ensureAbstractContent()
        return removeToken(convertMemberToToken(item))
    }
}
package modool.dom.property.sequence.element.map

import modool.core.content.ContentOrigin
import modool.dom.DomContext
import modool.core.meta.property.PropertyMeta
import modool.dom.node.DomElementBacked
import modool.dom.property.sequence.element.ElementBackedViewDomProperty
import modool.dom.property.sequence.element.DomAppendableElementBackedViewProperty
import modool.dom.identity.Identifiable
import modool.dom.identity.Identifier
import modool.dom.token.DomNodeContainerStartToken
import modool.dom.token.DomViewPlaceHolderToken

abstract class DomElementBackedMapViewProperty<D, T, K>(
        metadata: PropertyMeta,
        context: D,
        placeholder: DomViewPlaceHolderToken<D, T>?,
        contentType: Class<T>,
        initialisedAbstract: Boolean,
        initialisedConcrete: Boolean,
        container: DomNodeContainerStartToken?,
        contentOrigin: ContentOrigin
) : DomAppendableElementBackedViewProperty<D, T>(
        metadata, context, placeholder, contentType, initialisedAbstract, initialisedConcrete, container, contentOrigin),
        DomElementBackedMapView<D, T, K>,
        ElementBackedViewDomProperty<D, T>,
        DomElementBackedMappableProperty<D, T, K>
        where D : DomContext,
              T : DomElementBacked<D>,
              T : Identifiable,
              K : Identifier {

    constructor(meta: PropertyMeta, context: D, contentType: Class<T>) : this(
            meta, context,
            placeholder = null,
            contentType = contentType,
            initialisedAbstract = false, initialisedConcrete = false,
            container = null,
            contentOrigin = ContentOrigin.API)

    override operator fun get(key: K): T? {
        return filter(key, contentType).singleOrNull()
    }

    override fun <W : T> filter(key: K, type: Class<W>): Sequence<W> {
        @Suppress("UNCHECKED_CAST")
        return asSequence().filter { type.isAssignableFrom(it.javaClass) && it.identifier == key }.map { it as W }
    }

    override fun <W : T> add(element: W): W {
        if (contains(element)) {
            throw RuntimeException("Element: ${element.identifier} is already contained in the collection use merge instead")
        } else {
            return super<DomAppendableElementBackedViewProperty>.add(element)
        }
    }

    override fun update(key: K, builder: T.() -> Unit): T? {
        ensureAbstractContent()
        @Suppress("UNCHECKED_CAST")
        val element = get(key)
        element?.let {
            builder.invoke(element)
        }
        notifyChanged()
        return element
    }

    override fun remove(item: T): Boolean {
        ensureAbstractContent()
        val removed = context.removeElement(item.backing)
        if (removed) {
            notifyChanged()
        }
        return removed
    }
}


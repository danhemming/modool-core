package modool.dom.property.sequence.node.list

import modool.dom.DomContext
import modool.dom.node.text.DomTerminalNode
import modool.dom.property.sequence.DomListOperable
import modool.dom.property.sequence.DomContextualisedSequence

interface DomTerminalList<D>
    : DomContextualisedSequence<D, DomTerminalNode>,
        DomListOperable<D, DomTerminalNode>
        where D : DomContext  {

    override operator fun get(index: Int): DomTerminalNode? = elementAtOrNull(index)
}
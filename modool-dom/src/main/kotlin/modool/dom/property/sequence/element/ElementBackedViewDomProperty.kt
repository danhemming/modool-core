package modool.dom.property.sequence.element

import modool.dom.DomContext
import modool.dom.node.DomElementBacked
import modool.dom.property.sequence.DomViewProperty

interface ElementBackedViewDomProperty<D, T>
    : DomElementBackedView<D, T>,
        DomViewProperty<D, T>
        where D : DomContext,
              T : DomElementBacked<D>
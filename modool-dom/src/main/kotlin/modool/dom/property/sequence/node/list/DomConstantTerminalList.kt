package modool.dom.property.sequence.node.list

import modool.dom.DomContext
import modool.dom.node.text.DomTerminalNode
import modool.dom.node.text.factory.ConstantDomTerminalFactory
import modool.dom.property.sequence.DomListOperable
import modool.dom.property.sequence.DomContextualisedSequence

interface DomConstantTerminalList<D, F>
    : DomContextualisedSequence<D, DomTerminalNode>,
        DomListOperable<D, F>
        where D : DomContext,
              F : ConstantDomTerminalFactory {

    @Suppress("UNCHECKED_CAST")
    override operator fun get(index: Int): F? = elementAtOrNull(index)?.constantFactory as F
    fun contains(itemFactory: F): Boolean
}
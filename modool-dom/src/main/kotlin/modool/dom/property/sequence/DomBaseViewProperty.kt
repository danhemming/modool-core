package modool.dom.property.sequence

import modool.core.content.ContentOrigin
import modool.core.meta.property.PropertyMeta
import modool.dom.DomContext
import modool.dom.node.ConcreteContentBuilder
import modool.dom.property.DomBaseProperty
import modool.dom.property.DomProperty
import modool.dom.token.DomNodeContainerStartToken
import modool.dom.token.DomToken
import modool.dom.token.DomViewPlaceHolderToken

abstract class DomBaseViewProperty<D, T>(
        metadata: PropertyMeta,
        context: D,
        private var placeholder: DomViewPlaceHolderToken<D, T>?,
        protected val contentType: Class<T>,
        initialisedAbstract: Boolean,
        initialisedConcrete: Boolean,
        container: DomNodeContainerStartToken?,
        contentOrigin: ContentOrigin
) : DomBaseProperty<D>(metadata, context, initialisedAbstract, initialisedConcrete, container, contentOrigin),
        DomViewProperty<D, T>
        where D : DomContext {

    constructor(meta: PropertyMeta, context: D, contentType: Class<T>) : this(
            meta, context,
            placeholder = null,
            contentType = contentType,
            initialisedAbstract = false, initialisedConcrete = false,
            container = null,
            contentOrigin = ContentOrigin.API)

    override var container: DomNodeContainerStartToken?
        get() = super.container
        set(value) {
            super.container = value
        }

    override fun iterator(): Iterator<T> {
        ensureAbstractContent()
        // NOTE: Recursive checking is expensive, changed to local to see if we can get away with it.
        return container!!.asLocalTypeSequence(contentType).iterator()
    }

    override fun initialiseAbstractContent() {
        if (container == null) {
            container = createAndConfigureContainer()
        }
        @Suppress("UNCHECKED_CAST")
        placeholder = container!!.asRecursiveNodeSequence()
                .find {
                    it is DomViewPlaceHolderToken<*, *>
                            && it.isPlacementControllerFor(this)
                } as DomViewPlaceHolderToken<D, T>?

        // Create placeholder if it wasn't found
        if (placeholder == null) {
            placeholder = DomViewPlaceHolderToken(container!!, this)
        }
    }

    final override fun createAndConfigureContainer(): DomNodeContainerStartToken {

        var currentContainer = getParentOrNull()

        while (currentContainer != null) {
            if (currentContainer is DomProperty<*>) {
                return currentContainer.container!!
            }

            if (currentContainer is DomNodeContainerStartToken) {
                return currentContainer
            }

            currentContainer = currentContainer.getParentOrNull()
        }

        throw RuntimeException("No container provided to view and unable to derive one")
    }

    final override fun initialiseConcreteContainerContent(builder: ConcreteContentBuilder) {
        // We don't want the default processing which adds the container to the output (as the container is our parents)
        initialiseConcreteContent(builder)
    }

    final override fun initialiseConcreteContent(builder: ConcreteContentBuilder) {
        if (!builder.supportsUnorderedContent()) {
            throw RuntimeException("A view property must be rendered using a ConcreteContentBuilder that supports unordered content")
        }

        val precedingItem = lastOrNull()?.let { convertMemberToToken(it) }
        if (precedingItem == null) {
            builder.then(placeholder!!)
        } else {
            container!!.asRecursiveTokenSequence().find { it == precedingItem }?.end?.let { builder.positionAfter(it) }
            builder.then(placeholder!!)
        }
    }

    override fun contains(member: T): Boolean {
        ensureAbstractContent()
        return asSequence().firstOrNull { it == convertMemberToToken(member) } != null
    }

    protected fun addTokenAt(index: Int, token: DomToken) {
        ensureAbstractContent()
        convertMemberToToken(elementAt(index)).start.insertExtractedChainPrior(token)
        initialiseNewToken(token)
        notifyChanged()
    }

    protected fun addToken(token: DomToken) {
        ensureAbstractContent()
        placeholder!!.place(token)
        initialiseNewToken(token)
        notifyChanged()
    }

    override fun clear() {
        forEach { removeToken(convertMemberToToken(it)) }
    }

    protected open fun removeToken(token: DomToken): Boolean {
        ensureAbstractContent()
        val removed = container!!.remove(token)
        if (removed) {
            notifyChanged()
        }
        return removed
    }

    protected open fun initialiseNewToken(token: DomToken) {
        return
    }

    protected open fun finaliseRemovedMember(member: T) {
        return
    }

    abstract fun convertMemberToToken(member: T): DomToken
}
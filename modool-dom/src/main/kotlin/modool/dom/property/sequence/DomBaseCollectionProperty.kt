package modool.dom.property.sequence

import modool.core.content.ContentOrigin
import modool.core.meta.property.PropertyMeta
import modool.dom.DomContext
import modool.dom.exists
import modool.dom.node.ConcreteContentBuilder
import modool.dom.node.ContentOriginProvider
import modool.dom.node.renderDisplayed
import modool.dom.node.text.DomTerminalNode
import modool.dom.node.text.Renderer
import modool.dom.property.DomBaseProperty
import modool.dom.style.ElementStyler
import modool.dom.style.TerminalStyler
import modool.dom.style.builder.StyleRuleProvider
import modool.dom.token.DomNodeContainerStartToken
import modool.dom.token.DomPseudoElementStartToken
import modool.dom.token.DomToken
import modool.dom.token.DomTokenRange

/**
 * A collection will have a container collection of its own to manage and therefore its container will not be mixed
 * with the contents of other collections e.g. a parameter list (Nameable, y)
 */
abstract class DomBaseCollectionProperty<D, T>(
        metadata: PropertyMeta,
        context: D,
        override var isDisplayedWhenEmpty: Boolean,
        initialisedAbstract: Boolean,
        initialisedConcrete: Boolean,
        container: DomNodeContainerStartToken?,
        contentOrigin: ContentOrigin
) : DomBaseProperty<D>(
        metadata, context, initialisedConcrete, initialisedAbstract, container, contentOrigin),
        DomCollectionProperty<D, T>
        where D : DomContext {

    constructor(metadata: PropertyMeta, context: D) : this(
            metadata, context,
            isDisplayedWhenEmpty = false,
            initialisedConcrete = false, initialisedAbstract = false,
            container = null,
            contentOrigin = ContentOrigin.API)

    override val isContainerInitiallyDisplayed = true  // Constant so not required in copy operation

    // Layout fields (represent layout state and it is not necessary to include them in a copy operation)
    private var calculatedConcreteContent: Boolean = false
    private var openContentRange: DomTokenRange = DomTokenRange.Empty

    // Record which content ranges belong with which items
    // TODO: due to map semantics each object may only be added once, might break lists of nodes (same instance e.g. keyword)
    private val itemContentRangeMap = mutableMapOf<DomToken, DomTokenRange>()
    private var closeContentRange: DomTokenRange = DomTokenRange.Empty
    // End layout fields

    // TODO: if parent changes then we need to recalculate this
    protected val formatter: StyleRuleProvider by lazy { context.styler.getStyleRuleProvider(this) }

    var onContentProvided: (() -> Unit)? = null

    override var displayed: Boolean
        get() {
            return container?.displayed ?: true
        }
        set(value) {
            container?.displayed = value
        }

    override val renderLength: Int
        get() = 0

    override fun initialiseAbstractContent() {
        if (container == null) {
            getFirstAncestor(ContentOriginProvider::class.java)?.let { contentOrigin = it.contentOrigin }
        }
    }

    final override fun initialiseConcreteContent(builder: ConcreteContentBuilder) {
        // Note: A collection only gets called here IF it is a property of another element i.e. attached
        ensureMemberConcreteContent()
    }

    protected fun addToken(token: DomToken) {
        addTokenAfter(asRepresentationSequence().lastOrNull(), token)
    }

    protected fun addTokenAt(index: Int, token: DomToken) {
        if (index == 0) {
            addTokenBefore(asRepresentationSequence().firstOrNull(), token)
        } else {
            addTokenBefore(asRepresentationSequence().elementAt(index), token)
        }
    }

    private fun addTokenBefore(beforeToken: DomToken?, token: DomToken) {
        addTokenRelative(beforeToken, token) { it.start.insertExtractedChainPrior(token) }
    }

    private fun addTokenAfter(afterToken: DomToken?, token: DomToken) {
        addTokenRelative(afterToken, token) { it.end.insertExtractedChainNext(token) }
    }

    private inline fun addTokenRelative(
            relativeToken: DomToken?,
            token: DomToken,
            relativeAction: (DomTokenRange.Selection) -> Unit) {

        initialiseNewToken(token)
        ensureMemberConcreteContent()

        val isEmptyPrior = openContentRange is DomTokenRange.Empty

        if (relativeToken == null) {
            when (val range = openContentRange) {
                is DomTokenRange.Empty -> container?.addTokenAtStart(token)
                is DomTokenRange.Selection -> range.end.insertExtractedChainNext(token)
            }
        } else {
            val contentRange = itemContentRangeMap[relativeToken] as DomTokenRange.Selection?
                    ?: throw RuntimeException("relative content does not exist whilst adding to a collection")
            relativeAction(contentRange)
        }

        refreshConcreteContent()
        if (isEmptyPrior) {
            onContentProvided?.invoke()
        }
        notifyChanged()
    }

    abstract fun convertMemberToToken(member: T): DomToken

    protected open fun initialiseNewToken(token: DomToken) {
        return
    }

    override fun clear() {
        ensureAbstractContent()
        container!!.clear()
        refreshConcreteContent()
        notifyChanged()
    }

    protected fun removeToken(token: DomToken): Boolean {
        ensureMemberConcreteContent()
        val contentRange = itemContentRangeMap[token]

        if (contentRange != null && contentRange is DomTokenRange.Selection) {
            contentRange.detachAndRelinkChain()
            refreshConcreteContent()
            notifyChanged()
        }

        return contentRange != null
    }

    private fun ensureMemberConcreteContent() {
        if (!calculatedConcreteContent) {
            refreshConcreteContent()
        }
    }

    @Suppress("UNCHECKED_CAST")
    fun refreshConcreteContent() {
        val renderTokens = asRepresentationSequence().toList()
        val renderItemsExists = renderTokens.count() > 0
        val renderedSeparators = mutableListOf<DomTerminalNode>()
        val builder = context.createConcreteContentBuilder(container!!, renderedSeparators)
        val attemptRenderOpenClose = contentOrigin.isFormattable()
        val attemptReadUserProvidedOpenClose = !attemptRenderOpenClose &&
                (renderItemsExists || container!!.asRecursiveTokenSequence().exists())

        val query = CollectionLayoutQueryManager(this)

        itemContentRangeMap.clear()
        var startToken: DomToken? = builder.insertionPointToken
        var userProvidedOpenContent: DomTokenRange = DomTokenRange.Empty
        var userProvidedCloseContent: DomTokenRange = DomTokenRange.Empty

        openContentRange = when {
            attemptRenderOpenClose -> {
                val openDecorator: CollectionConcreteInitialiser = formatter.getRelativeCollectionConcreteInitialiser(query)
                        ?: this
                openDecorator.initialiseConcreteOpen(builder)
                buildTokenRange(startToken, builder.insertionPointToken)
            }
            attemptReadUserProvidedOpenClose -> {
                builder.createContentIfNotPresent = false
                // Only captures user content if it is the expected default
                initialiseConcreteOpen(builder)
                userProvidedOpenContent = buildTokenRange(startToken, builder.insertionPointToken)
                builder.createContentIfNotPresent = true
                userProvidedOpenContent
            }
            else -> DomTokenRange.Empty
        }

        if (openContentRange is DomTokenRange.Selection) {
            startToken = (openContentRange as DomTokenRange.Selection).end.next
        }

        if (renderItemsExists) {

            renderTokens.forEachIndexed { index, item ->
                val typedItem = if (item is DomPseudoElementStartToken<*>) null else item as T
                query.current = typedItem

                if (index > 0) {
                    val separatorDecorator: CollectionConcreteInitialiser = formatter.getRelativeCollectionConcreteInitialiser(query)
                            ?: this
                    separatorDecorator.initialiseConcreteSeparator(builder)
                }
                if (typedItem == null) {
                    builder.then(item)
                } else {
                    val memberToken = convertMemberToToken(typedItem)
                    builder.then(memberToken)
                    startToken = startToken ?: memberToken.start
                }
                val contentRange = buildTokenRange(startToken, builder.insertionPointToken)
                itemContentRangeMap[item] = contentRange

                if (contentRange is DomTokenRange.Selection) {
                    startToken = contentRange.end.next
                }
            }
        }

        query.current = null

        closeContentRange = when {
            attemptRenderOpenClose -> {
                val closeDecorator: CollectionConcreteInitialiser = formatter.getRelativeCollectionConcreteInitialiser(query)
                        ?: this
                closeDecorator.initialiseConcreteClose(builder)
                buildTokenRange(startToken, builder.insertionPointToken)
            }
            attemptReadUserProvidedOpenClose -> {
                builder.createContentIfNotPresent = false
                // Only captures user content if it is the expected default
                initialiseConcreteClose(builder)
                userProvidedCloseContent = buildTokenRange(startToken, builder.insertionPointToken)
                builder.createContentIfNotPresent = true
                userProvidedCloseContent
            }
            else -> DomTokenRange.Empty
        }

        if (!renderItemsExists &&
                !attemptRenderOpenClose &&
                userProvidedOpenContent == DomTokenRange.Empty &&
                userProvidedCloseContent == DomTokenRange.Empty) {
            container!!.clear()
        } else {
            removeLocalTerminalsNotIn(renderTokens, renderedSeparators)
        }

        calculatedConcreteContent = true
    }

    private fun removeLocalTerminalsNotIn(tokens: List<DomToken>, separators: List<DomTerminalNode>) {
        var candidate = container!!.next

        while (candidate != null && candidate != container!!.end) {

            if (candidate is DomNodeContainerStartToken) {
                candidate = candidate.end.next
                continue
            }

            candidate = if (candidate is DomTerminalNode && candidate !in separators && candidate !in tokens) {
                val next = candidate.next
                candidate.detachFromAndRelinkChain()
                next
            } else {
                candidate.next
            }
        }
    }

    private fun buildTokenRange(start: DomToken?, end: DomToken?): DomTokenRange {
        return if (start == null && end == null) {
            DomTokenRange.Empty
        } else {
            DomTokenRange.Selection(start ?: end!!, end ?: start!!)
        }
    }

    override fun refreshFormatting() {
        TerminalStyler.formatContainer(formatter, container!!)
        ElementStyler.format(context.styler.createStyleContext(), container!!)
    }

    override fun render(renderer: Renderer) {
        container?.renderDisplayed(renderer)
    }
}
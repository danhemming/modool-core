package modool.dom.property.sequence.node.list

import modool.core.content.ContentOrigin
import modool.core.meta.property.PropertyMeta
import modool.dom.DomContext
import modool.dom.exists
import modool.dom.node.text.DomTerminalNode
import modool.dom.node.text.factory.ConstantDomTerminalFactory
import modool.dom.property.sequence.DomCollectionProperty
import modool.dom.property.sequence.node.DomNodeCollectionProperty
import modool.dom.token.DomNodeContainerStartToken

abstract class DomConstantTerminalListProperty<D, F>(
        metadata: PropertyMeta,
        context: D,
        isDisplayedWhenEmpty: Boolean,
        initialisedAbstract: Boolean,
        initialisedConcrete: Boolean,
        container: DomNodeContainerStartToken?,
        contentOrigin: ContentOrigin
) : DomNodeCollectionProperty<D, DomTerminalNode>(
        metadata, context,
        DomTerminalNode::class.java,
        isDisplayedWhenEmpty,
        initialisedAbstract, initialisedConcrete,
        container,
        contentOrigin),
        DomConstantTerminalList<D, F>,
        DomCollectionProperty<D, DomTerminalNode>
        where D : DomContext,
              F : ConstantDomTerminalFactory {

    constructor(metadata: PropertyMeta, context: D) : this(
            metadata, context,
            isDisplayedWhenEmpty = false,
            initialisedConcrete = false, initialisedAbstract = false,
            container = null,
            contentOrigin = ContentOrigin.API)

    override fun <W : F> add(element: W): W {
        addToken(element.createNode())
        return element
    }

    override fun <W : F> addAt(index: Int, element: W): W {
        addTokenAt(index, element.createNode())
        return element
    }

    override fun remove(member: F): Boolean {
        // Note: this will just remove the first one
        return removeToken(member.createNode())
    }

    override fun contains(itemFactory: F): Boolean {
        return asRepresentationSequence().filter { itemFactory.isProduct(it) }.exists()
    }
}
package modool.dom.property.sequence.node.list

import modool.core.content.ContentOrigin
import modool.core.meta.property.PropertyMeta
import modool.dom.DomContext
import modool.dom.node.text.DomTerminalNode
import modool.dom.property.sequence.DomCollectionProperty
import modool.dom.property.sequence.node.DomNodeCollectionProperty
import modool.dom.token.DomNodeContainerStartToken

abstract class DomTerminalListProperty<D>(
        metadata: PropertyMeta,
        context: D,
        isDisplayedWhenEmpty: Boolean,
        initialisedAbstract: Boolean,
        initialisedConcrete: Boolean,
        container: DomNodeContainerStartToken?,
        contentOrigin: ContentOrigin
) : DomNodeCollectionProperty<D, DomTerminalNode>(
        metadata, context,
        DomTerminalNode::class.java,
        isDisplayedWhenEmpty,
        initialisedAbstract, initialisedConcrete,
        container,
        contentOrigin),
        DomTerminalList<D>,
        DomCollectionProperty<D, DomTerminalNode>
        where D : DomContext {

    constructor(metadata: PropertyMeta, context: D) : this(
            metadata, context,
            isDisplayedWhenEmpty = false,
            initialisedConcrete = false, initialisedAbstract = false,
            container = null,
            contentOrigin = ContentOrigin.API)

    override fun <W : DomTerminalNode> add(element: W): W {
        addToken(element)
        return element
    }

    override fun <W : DomTerminalNode> addAt(index: Int, element: W): W {
        addTokenAt(index, element)
        return element
    }

    override fun remove(member: DomTerminalNode): Boolean {
        return removeToken(member)
    }
}
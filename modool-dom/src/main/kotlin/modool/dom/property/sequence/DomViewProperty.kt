package modool.dom.property.sequence

import modool.dom.DomContext

interface DomViewProperty<D, T>
    : DomView<D, T>
        where D : DomContext
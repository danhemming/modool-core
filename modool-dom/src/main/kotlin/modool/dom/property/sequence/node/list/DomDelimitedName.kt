package modool.dom.property.sequence.node.list

import modool.dom.DomContext

/**
 * Used to represent a qualified name e.g. org.x.y as a list of identifier tokens.
 */
interface DomDelimitedName<D>
    : DomTerminalList<D>
        where D : DomContext
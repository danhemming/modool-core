package modool.dom.property.sequence.element.definition

import modool.dom.DomContext
import modool.dom.node.DomElementBacked

abstract class IdentifiableElementDefinition<D, out K, T>(
        val key: K
) : ElementDefinition()
        where D : DomContext,
              T : DomElementBacked<D>



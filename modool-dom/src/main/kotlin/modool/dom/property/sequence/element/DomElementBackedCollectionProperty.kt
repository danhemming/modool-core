package modool.dom.property.sequence.element

import modool.core.content.ContentOrigin
import modool.core.content.SourceProcessingLevel
import modool.lexer.LexerRulesProvider
import modool.core.meta.property.PropertyMeta
import modool.parser.ParserRuleProvider
import modool.dom.DomContext
import modool.dom.node.DomElementBacked
import modool.dom.property.sequence.DomBaseCollectionProperty
import modool.dom.token.DomNodeContainerStartToken
import modool.dom.token.DomPseudoElementStartToken
import modool.dom.token.DomToken

/**
 * A cohesive collection will have a container collection of its own to manage and therefore its container will not be mixed
 * with the contents of other collections e.g. a parameter list (x, y)
 */
abstract class DomElementBackedCollectionProperty<D, T>(
        metadata: PropertyMeta,
        context: D,
        protected val contentType: Class<T>,
        isDisplayedWhenEmpty: Boolean,
        initialisedAbstract: Boolean,
        initialisedConcrete: Boolean,
        container: DomNodeContainerStartToken?,
        contentOrigin: ContentOrigin
) : DomBaseCollectionProperty<D, T>(
        metadata, context, isDisplayedWhenEmpty, initialisedConcrete, initialisedAbstract, container, contentOrigin),
        DomElementAppendableOperable<D, T>,
        LexerRulesProvider,
        ParserRuleProvider
        where D : DomContext,
              T : DomElementBacked<D> {

    constructor(metadata: PropertyMeta, context: D, contentType: Class<T>) : this(
            metadata, context, contentType,
            isDisplayedWhenEmpty = false,
            initialisedConcrete = false, initialisedAbstract = false,
            container = null,
            contentOrigin = ContentOrigin.API)

    final override fun initialiseAbstractContent() {
        super.initialiseAbstractContent()
        if (container == null) {
            container = createAndConfigureContainer()
        }
        forEach { it.backing.attachTo(this) }
    }

    override fun <W : T> add(element: W): W {
        ensureAbstractContent()
        addToken(convertMemberToToken(element))
        return element
    }

    override fun add(source: String, processingLevel: SourceProcessingLevel) {
        addToken(
                context.process(
                        processingLevel = processingLevel,
                        source = source,
                        type = contentType,
                        lexerRulesProvider = this,
                        parserRuleProvider = this,
                        contentOrigin = ContentOrigin.SecondarySource))
    }

    @Suppress("UNCHECKED_CAST")
    override fun createParsed(source: String): T {
        return context.process(
                processingLevel = SourceProcessingLevel.PARSE,
                source = source,
                type = contentType,
                lexerRulesProvider = this,
                parserRuleProvider = this,
                contentOrigin = ContentOrigin.SecondarySource) as T
    }

    override fun initialiseNewToken(token: DomToken) {
        if (token is DomElementBacked<*>) {
            token.backing.attachTo(this)
            context.ensureConcreteContent(token.backing)
        }
    }

    override fun convertMemberToToken(member: T): DomToken {
        return member.backing
    }

    override fun iterator(): Iterator<T> {
        ensureAbstractContent()
        return container!!.asLocalTypeSequence(contentType).iterator()
    }

    override fun asRepresentationSequence(): Sequence<DomToken> {
        ensureAbstractContent()
        return container!!.asLocalTokenSequence().filter {
            contentType.isAssignableFrom(it::class.java)
                    || (it is DomPseudoElementStartToken<*> && contentType.isAssignableFrom(it.type))
        }
    }

    override fun contains(member: T): Boolean = container?.containsContent(member.backing) ?: false
}
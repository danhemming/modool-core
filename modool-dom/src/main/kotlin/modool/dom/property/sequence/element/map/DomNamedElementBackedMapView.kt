package modool.dom.property.sequence.element.map

import modool.dom.DomContext
import modool.dom.identity.Identifiable
import modool.dom.identity.NameIndexer
import modool.dom.name.MandatoryNameable
import modool.dom.node.DomElementBacked

interface DomNamedElementBackedMapView<D, T>
    : DomElementBackedMapView<D, T, NameIndexer>
        where D : DomContext,
              T : DomElementBacked<D>,
              T : Identifiable,
              T : MandatoryNameable {

    operator fun get(name: String): T? {
        return get(NameIndexer(name))
    }

    fun remove(name: String): Boolean {
        return remove(NameIndexer(name))
    }
}
package modool.dom.property.sequence.element

import modool.dom.DomContext
import modool.dom.node.DomElementBacked
import modool.dom.property.sequence.element.definition.ConstructableElementDefinition
import modool.dom.property.sequence.element.definition.IdentifiableElementDefinition
import modool.dom.identity.Identifiable
import modool.dom.identity.Identifier

interface DomElementMapMutableOperable<D, T, in K>
    : DomElementModifiableOperable<D, T, K>,
        DomElementMapAccessibleOperable<D, T, K>
        where D : DomContext,
              T : DomElementBacked<D>,
              T : Identifiable,
              K : Identifier {

    fun remove(item: T): Boolean

    fun remove(key: K): Boolean {
        val element = get(key)
        return element != null && remove(element)
    }

    fun <W, U> add(definition: U, builder: (W.() -> Unit)? = null): W
            where W : T,
                  U : IdentifiableElementDefinition<D, K, W>,
                  U : ConstructableElementDefinition<D, W>

    fun <W, U> addIfNotPresent(definition: U, builder: (W.() -> Unit)? = null): W
            where W : T,
                  U : IdentifiableElementDefinition<D, K, W>,
                  U : ConstructableElementDefinition<D, W>

    fun addIfNotPresent(source: String): T?

    fun <W, U> update(definition: U, builder: (W.() -> Unit)? = null): W
            where W : T,
                  U : IdentifiableElementDefinition<D, K, W>

    fun <W, U> updateIfPresent(definition: U, builder: (W.() -> Unit)? = null): W?
            where W : T,
                  U : IdentifiableElementDefinition<D, K, W>

    fun <W, U> merge(definition: U, builder: (W.() -> Unit)? = null): W
            where W : T,
                  U : IdentifiableElementDefinition<D, K, W>,
                  U : ConstructableElementDefinition<D, W>
}
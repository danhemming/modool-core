package modool.dom.property.sequence.node.set

import modool.dom.DomContext
import modool.dom.node.DomNode
import modool.dom.property.sequence.DomContextualisedSequence
import modool.dom.property.sequence.DomSetOperable
import modool.dom.token.DomToken

interface DomNodeSet<D, T>
    : DomContextualisedSequence<D, T>,
        DomSetOperable<D, T>
        where D : DomContext,
              T : DomNode,
              T : DomToken
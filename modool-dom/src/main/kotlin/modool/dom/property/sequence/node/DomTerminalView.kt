package modool.dom.property.sequence.node

import modool.dom.DomContext
import modool.dom.node.DomNode
import modool.dom.property.sequence.DomView
import modool.dom.token.DomToken

interface DomTerminalView<D, T>
    : DomView<D, T>
        where D : DomContext,
              T : DomNode,
              T : DomToken
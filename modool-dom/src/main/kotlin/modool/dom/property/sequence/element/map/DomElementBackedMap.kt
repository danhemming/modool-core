package modool.dom.property.sequence.element.map

import modool.dom.DomContext
import modool.dom.identity.Identifiable
import modool.dom.identity.Identifier
import modool.dom.node.DomElementBacked
import modool.dom.property.sequence.DomContextualisedSequence
import modool.dom.property.sequence.element.DomElementMapMutableOperable

interface DomElementBackedMap<D, T, K> :
        DomContextualisedSequence<D, T>,
        DomElementMapMutableOperable<D, T, K>
        where D : DomContext,
              T : DomElementBacked<D>,
              T : Identifiable,
              K : Identifier {

    fun updateAt(index: Int, builder: T.() -> Unit): T
}
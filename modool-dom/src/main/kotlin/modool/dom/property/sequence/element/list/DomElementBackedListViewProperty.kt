package modool.dom.property.sequence.element.list

import modool.dom.DomContext
import modool.core.meta.property.PropertyMeta
import modool.dom.node.DomElementBacked
import modool.dom.property.sequence.element.DomAppendableElementBackedViewProperty

/**
 * Represents an appendable bucket of objects e.g. Statements
 */
abstract class DomElementBackedListViewProperty<D, T>(
        metadata: PropertyMeta,
        context: D,
        contentType: Class<T>)
    : DomAppendableElementBackedViewProperty<D, T>(metadata, context, contentType),
        DomElementBackedListView<D, T>
        where D : DomContext,
              T : DomElementBacked<D> {

    override fun <W : T> addAt(index: Int, element: W): W {
        addTokenAt(index, convertMemberToToken(element))
        return element
    }

    override fun update(key: Int, builder: T.() -> Unit): T? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun remove(member: T): Boolean {
        return removeToken(convertMemberToToken(member))
    }

    override fun clear() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun invoke(vararg items: T) {
        items.forEach { add(it) }
    }
}
package modool.dom.property.sequence

import modool.dom.DomContext

interface DomAppendableOperable<D, T>
        where D : DomContext {

    fun <W> add(element: W): W where W : T
}
package modool.dom.property.sequence

import modool.dom.DomContext
import modool.dom.node.DomNodeContentManager
import modool.dom.node.Renderable
import modool.dom.style.DisplayStyleable
import modool.dom.token.DomToken

interface DomCollection<D, T>
    : DomContextualisedSequence<D, T>,
        DomNodeContentManager,
        CollectionConcreteInitialiser,
        DisplayStyleable,
        Renderable
        where D : DomContext {

    fun asRepresentationSequence(): Sequence<DomToken>
}
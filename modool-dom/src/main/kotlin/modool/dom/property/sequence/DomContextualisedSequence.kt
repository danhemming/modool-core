package modool.dom.property.sequence

import modool.dom.DomContext
import modool.dom.DomContextualised

interface DomContextualisedSequence<D, T>
    : DomContextualised<D>,
        Sequence<T>
        where D : DomContext {

    fun contains(member: T): Boolean
}
package modool.dom.property.sequence

import modool.dom.DomContext

/**
 * A view is one where the contents are spread over a container in an indeterminate way with other contents
 * e.g. fields, methods, constructors can appear anywhere in the body of a class.
 */

interface DomView<D, T>
    : DomContextualisedSequence<D, T>
        where D : DomContext

package modool.dom.property.sequence.node

import modool.core.content.ContentOrigin
import modool.core.meta.property.PropertyMeta
import modool.dom.DomContext
import modool.dom.node.DomNode
import modool.dom.property.sequence.DomBaseCollectionProperty
import modool.dom.token.DomNodeContainerStartToken
import modool.dom.token.DomPseudoElementStartToken
import modool.dom.token.DomToken

abstract class DomNodeCollectionProperty<D, T>(
        metadata: PropertyMeta,
        context: D,
        private val contentType: Class<T>,
        isDisplayedWhenEmpty: Boolean,
        initialisedAbstract: Boolean,
        initialisedConcrete: Boolean,
        container: DomNodeContainerStartToken?,
        contentOrigin: ContentOrigin
) : DomBaseCollectionProperty<D, T>(
        metadata, context, isDisplayedWhenEmpty, initialisedAbstract, initialisedConcrete, container, contentOrigin),
        DomNodeCollection<D, T>
        where D : DomContext,
              T : DomNode,
              T : DomToken {

    constructor(metadata: PropertyMeta, context: D, contentType: Class<T>) : this(
            metadata, context, contentType,
            isDisplayedWhenEmpty = false,
            initialisedConcrete = false, initialisedAbstract = false,
            container = null,
            contentOrigin = ContentOrigin.API)

    final override fun initialiseAbstractContent() {
        super.initialiseAbstractContent()
    }

    override fun iterator(): Iterator<T> {
        ensureAbstractContent()
        return container!!.asLocalTypeSequence(contentType).iterator()
    }

    override fun asRepresentationSequence(): Sequence<DomToken> {
        ensureAbstractContent()
        return container!!.asLocalTokenSequence().filter {
            contentType.isAssignableFrom(it::class.java)
                    || (it is DomPseudoElementStartToken<*> && contentType.isAssignableFrom(it.type))}
    }

    override fun convertMemberToToken(member: T): DomToken {
        return member
    }

    override fun contains(member: T): Boolean = container?.containsContent(member) ?: false
}
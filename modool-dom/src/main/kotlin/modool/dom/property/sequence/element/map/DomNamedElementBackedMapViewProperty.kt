package modool.dom.property.sequence.element.map

import modool.core.content.ContentOrigin
import modool.dom.DomContext
import modool.core.meta.property.PropertyMeta
import modool.dom.node.DomElementBacked
import modool.dom.property.sequence.element.ElementBackedViewDomProperty
import modool.dom.identity.Identifiable
import modool.dom.identity.NameIndexer
import modool.dom.name.MandatoryNameable
import modool.dom.token.DomNodeContainerStartToken
import modool.dom.token.DomViewPlaceHolderToken

abstract class DomNamedElementBackedMapViewProperty<D, T>(
        metadata: PropertyMeta,
        context: D,
        placeholder: DomViewPlaceHolderToken<D, T>?,
        contentType: Class<T>,
        initialisedAbstract: Boolean,
        initialisedConcrete: Boolean,
        container: DomNodeContainerStartToken?,
        contentOrigin: ContentOrigin
) : DomElementBackedMapViewProperty<D, T, NameIndexer>(
        metadata, context, placeholder, contentType, initialisedAbstract, initialisedConcrete, container, contentOrigin),
        DomNamedElementBackedMapView<D, T>,
        ElementBackedViewDomProperty<D, T>
        where D : DomContext,
              T : DomElementBacked<D>,
              T : Identifiable,
              T : MandatoryNameable {

    constructor(meta: PropertyMeta, context: D, contentType: Class<T>) : this(
            meta, context,
            placeholder = null,
            contentType = contentType,
            initialisedAbstract = false, initialisedConcrete = false,
            container = null,
            contentOrigin = ContentOrigin.API)
}
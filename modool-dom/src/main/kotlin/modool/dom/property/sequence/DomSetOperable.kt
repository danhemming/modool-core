package modool.dom.property.sequence

import modool.dom.DomContext

interface DomSetOperable<D, T>
        where D : DomContext {

    fun put(item: T)
    fun remove(item: T): Boolean
}
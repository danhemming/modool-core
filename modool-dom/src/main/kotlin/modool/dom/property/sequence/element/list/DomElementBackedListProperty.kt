package modool.dom.property.sequence.element.list

import modool.core.content.ContentOrigin
import modool.dom.DomContext
import modool.core.meta.property.PropertyMeta
import modool.dom.node.DomElementBacked
import modool.dom.property.sequence.element.DomElementBackedCollectionProperty
import modool.dom.token.DomNodeContainerStartToken

/**
 * Represents an appendable bucket of objects e.g. Statements
 */
 abstract class DomElementBackedListProperty<D, T>(
        metadata: PropertyMeta,
        context: D,
        contentType: Class<T>,
        isDisplayedWhenEmpty: Boolean,
        initialisedAbstract: Boolean,
        initialisedConcrete: Boolean,
        container: DomNodeContainerStartToken?,
        contentOrigin: ContentOrigin
) : DomElementBackedCollectionProperty<D, T>(
        metadata, context, contentType, isDisplayedWhenEmpty, initialisedConcrete, initialisedAbstract, container, contentOrigin),
        DomElementBackedList<D, T>
        where D : DomContext,
              T : DomElementBacked<D> {

    constructor(metadata: PropertyMeta, context: D, contentType: Class<T>) : this(
            metadata, context, contentType,
            isDisplayedWhenEmpty = false,
            initialisedConcrete = false, initialisedAbstract = false,
            container = null,
            contentOrigin = ContentOrigin.API)

    override fun <W : T> addAt(index: Int, element: W): W {
        addTokenAt(index, convertMemberToToken(element))
        return element
    }

    override fun update(key: Int, builder: T.() -> Unit): T? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun remove(member: T): Boolean {
        return removeToken(convertMemberToToken(member))
    }

    override fun invoke(vararg items: T) {
        items.forEach { add(it) }
    }
}
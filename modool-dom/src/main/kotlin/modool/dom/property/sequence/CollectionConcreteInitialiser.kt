package modool.dom.property.sequence

import modool.dom.node.ConcreteContentBuilder

interface CollectionConcreteInitialiser {
    fun initialiseConcreteOpen(builder: ConcreteContentBuilder) {}
    fun initialiseConcreteSeparator(builder: ConcreteContentBuilder) {}
    fun initialiseConcreteClose(builder: ConcreteContentBuilder) {}
}
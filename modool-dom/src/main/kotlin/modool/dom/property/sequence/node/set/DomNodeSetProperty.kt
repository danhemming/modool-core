package modool.dom.property.sequence.node.set

import modool.core.content.ContentOrigin
import modool.dom.DomContext
import modool.core.meta.property.PropertyMeta
import modool.dom.node.DomNode
import modool.dom.property.sequence.node.DomNodeCollectionProperty
import modool.dom.token.DomNodeContainerStartToken
import modool.dom.token.DomToken

abstract class DomNodeSetProperty<D, T>(
        metadata: PropertyMeta,
        context: D,
        contentType: Class<T>,
        isDisplayedWhenEmpty: Boolean,
        initialisedAbstract: Boolean,
        initialisedConcrete: Boolean,
        container: DomNodeContainerStartToken?,
        contentOrigin: ContentOrigin
) : DomNodeCollectionProperty<D, T>(
        metadata, context, contentType, isDisplayedWhenEmpty, initialisedAbstract, initialisedConcrete, container, contentOrigin),
        DomNodeSet<D, T>
        where D : DomContext,
              T : DomNode,
              T : DomToken {

    constructor(metadata: PropertyMeta, context: D, contentType: Class<T>) : this(
            metadata, context, contentType,
            isDisplayedWhenEmpty = false,
            initialisedConcrete = false, initialisedAbstract = false,
            container = null,
            contentOrigin = ContentOrigin.API)

    override fun put(item: T) {
        if (!contains(item)) {
            addToken(item)
        }
    }

    override fun remove(item: T): Boolean {
        return removeToken(item)
    }
}
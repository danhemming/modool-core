package modool.dom.property.sequence.element.map

import modool.dom.DomContext
import modool.dom.identity.Identifiable
import modool.dom.identity.Identifier
import modool.dom.node.DomElementBacked
import modool.dom.property.sequence.DomMapOperable
import modool.dom.property.sequence.element.DomElementBackedView
import modool.dom.property.sequence.element.DomElementMapMutableOperable

interface DomElementBackedMapView<D, T, K>
    : DomElementBackedView<D, T>,
        DomMapOperable<D, T>,
        DomElementMapMutableOperable<D, T, K>
        where D : DomContext,
              T : DomElementBacked<D>,
              T : Identifiable,
              K : Identifier
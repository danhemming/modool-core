package modool.dom.property.sequence

import modool.dom.DomContext

interface DomCollectionProperty<D, T>
    : DomCollection<D, T>
        where D : DomContext {

    /**
     * Indicates whether or not open and close content is displayed when the collection is empty.
     * This is nearly always bracketing like () or <>
     */
    val isDisplayedWhenEmpty: Boolean
}
package modool.dom.property.sequence.node.list

import modool.core.content.ContentOrigin
import modool.core.content.signifier.tag.terminal.Name
import modool.core.meta.property.PropertyMeta
import modool.parser.ParseException
import modool.parser.ParseResult
import modool.dom.DomContext
import modool.dom.node.ConcreteContentBuilder
import modool.dom.node.text.DomTerminalNode
import modool.dom.node.text.StringRenderer
import modool.dom.node.text.factory.ConstantDomTerminalFactory
import modool.dom.property.value.DomNameProperty
import modool.dom.token.DomNodeContainerStartToken
import modool.dom.token.DomToken
import modool.dom.token.findNext

abstract class DomDelimitedNameProperty<D, V>(
        metadata: PropertyMeta,
        context: D,
        private val open: ConstantDomTerminalFactory?,
        private val separator: ConstantDomTerminalFactory,
        private val close: ConstantDomTerminalFactory?,
        private val displayOpenCloseWithoutContent: Boolean,
        private val displayOpenCloseWithSinglePart: Boolean,
        isDisplayedWhenEmpty: Boolean,
        initialisedAbstract: Boolean,
        initialisedConcrete: Boolean,
        container: DomNodeContainerStartToken?,
        contentOrigin: ContentOrigin
) : DomTerminalListProperty<D>(
        metadata, context, isDisplayedWhenEmpty, initialisedAbstract, initialisedConcrete, container, contentOrigin),
        DomNameProperty<D, V>,
        DomDelimitedName<D>
        where D : DomContext {

    constructor(
            metadata: PropertyMeta,
            context: D,
            open: ConstantDomTerminalFactory?,
            separator: ConstantDomTerminalFactory,
            close: ConstantDomTerminalFactory?,
            displayOpenCloseWithoutContent: Boolean,
            displayOpenCloseWithSinglePart: Boolean
    ) : this (
            metadata, context, open, separator, close, displayOpenCloseWithoutContent, displayOpenCloseWithSinglePart,
            isDisplayedWhenEmpty = false,
            initialisedAbstract = false, initialisedConcrete = false,
            container = null, contentOrigin = ContentOrigin.API)

    override fun get(index: Int): DomTerminalNode? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun asRepresentationSequence(): Sequence<DomToken> {
        ensureAbstractContent()
        return container!!.asLocalTokenSequence().filter { it is DomTerminalNode && it.signifies(Name) }
    }

    override fun initialiseConcreteOpen(builder: ConcreteContentBuilder) {
        when (count()) {
            0 -> if (displayOpenCloseWithoutContent) open?.let { builder.then(it) }
            1 -> if (displayOpenCloseWithSinglePart) open?.let { builder.then(it) }
            else -> open?.let { builder.then(it) }
        }
    }

    override fun initialiseConcreteSeparator(builder: ConcreteContentBuilder) {
        builder.then(separator)
    }

    override fun initialiseConcreteClose(builder: ConcreteContentBuilder) {
        when (count()) {
            0 -> if (displayOpenCloseWithoutContent) close?.let { builder.then(it) }
            1 -> if (displayOpenCloseWithSinglePart) close?.let { builder.then(it) }
            else -> close?.let { builder.then(it) }
        }
    }

    /**
     * Inherited as a consequence of being a value property as well as a list.
     * There is no reason for this to be called as the initialisation mechanism for a collection is to set the container.
     */
    override fun setBackingTerminal(terminal: DomTerminalNode?) {
        throw UnsupportedOperationException("A collection should be initialised through the container property")
    }

    protected fun setOptional(value: String?) {
        if (value == null) {
            clear()
        } else {
            setMandatory(value)
        }
    }

    protected fun getOptional(): String? {
        return if (count() == 0) {
            null
        } else {
            getMandatory()
        }
    }

    protected fun setMandatory(value: String) {
        // FIXME: The content we are replacing may have leading (and following?) whitespace which needs preserving
        val parseResult = parse(value)
        if (parseResult is ParseResult.Content && parseResult.reference is DomToken) {
            (parseResult.reference as DomToken).findNext<DomNodeContainerStartToken>()?.let { newContent -> container = newContent }
        } else {
            throw ParseException(value)
        }
    }

    protected fun getMandatory(): String {
        val renderer = StringRenderer()
        container!!.asRecursiveDisplayedNodeSequence().forEach { it.render(renderer) }
        return renderer.toString().trim()
    }

    protected fun applyStyling(name: String): String {
        return formatter.getRelativeNameConvention()?.convention?.apply(name) ?: name
    }

    abstract fun parse(value: String): ParseResult
}
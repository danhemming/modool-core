package modool.dom.property

import modool.core.meta.Describeable
import modool.core.meta.property.PropertyMeta
import modool.dom.DomContext
import modool.dom.DomContextualised
import modool.dom.node.ContentOriginProvider
import modool.dom.node.DomNodeAttachable
import modool.dom.node.DomNodeContentManager
import modool.dom.node.ModoolDsl
import modool.dom.token.DomNodeContainerStartToken

@ModoolDsl
interface DomProperty<out D>
    : DomNodeAttachable,
        DomContextualised<D>,
        DomNodeContentManager,
        ContentOriginProvider,
        Describeable<PropertyMeta>
        where D : DomContext {

    var container: DomNodeContainerStartToken?
    fun clear()
}
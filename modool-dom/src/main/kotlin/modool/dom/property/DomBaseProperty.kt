package modool.dom.property

import modool.core.content.ContentOrigin
import modool.core.meta.property.PropertyMeta
import modool.dom.DomContext
import modool.dom.node.ConcreteContentBuilder
import modool.dom.node.ContentOriginProvider
import modool.dom.node.DomNodeAttachment
import modool.dom.token.DomNodeContainerStartToken
import modool.dom.token.DomPropertyStartToken
import modool.dom.token.DomToken

abstract class DomBaseProperty<out D>(
        override val meta: PropertyMeta,
        override val context: D,
        private var initialisedAbstract: Boolean,
        private var initialisedConcrete: Boolean,
        container: DomNodeContainerStartToken?,
        contentOrigin: ContentOrigin
) : DomNodeAttachment(),
        DomProperty<D>
        where D : DomContext {

    constructor(meta: PropertyMeta, context: D) : this(
            meta, context,
            initialisedAbstract = false, initialisedConcrete = false,
            container = null,
            contentOrigin = ContentOrigin.API)

    // The origin of the property is always that of the parent and can be distinct from the container it manages
    final override var contentOrigin: ContentOrigin = contentOrigin
        get() = (getParentOrNull() as? ContentOriginProvider)?.contentOrigin ?: field

    override var container: DomNodeContainerStartToken? = container
        set(value) {
            if (field != value) {
                field?.let {
                    if (value != null) {
                        it.replaceInChainWith(value)
                        it.detachFromAndRelinkChain()
                    }
                }
                field = value
            }
        }

    protected open val isContainerInitiallyDisplayed: Boolean = contentOrigin.isFromSource()

    override fun ensureAbstractContent() {
        if (!initialisedAbstract) {
            initialisedAbstract = true
            initialiseAbstractManagementContent()
            initialiseAbstractContent()
        }
    }

    protected open fun initialiseAbstractManagementContent() {
        return
    }

    protected open fun createAndConfigureContainer(): DomNodeContainerStartToken {
        return DomPropertyStartToken(
                requiresParsing = false,
                property = this,
                contentOrigin = contentOrigin,
                displayed = isContainerInitiallyDisplayed)
    }

    override fun ensureConcreteContent(builder: ConcreteContentBuilder) {
        ensureAbstractContent()

        if (!initialisedConcrete) {
            initialisedConcrete = true
            initialiseConcreteContainerContent(builder)
        }
    }

    protected open fun initialiseConcreteContainerContent(builder: ConcreteContentBuilder) {
        if (container == null) {
            container = createAndConfigureContainer()
        }
        container?.let { builder.thenStart(it) }
        initialiseConcreteContent(builder)
        container?.let { builder.thenEnd(it.end) }
    }

    protected fun setConcreteContent(oldContent: DomToken?, newContent: DomToken?) {

        // Need to do this here because we may be replacing content even before concrete layout has happened
        if (oldContent != null && newContent != null && oldContent != newContent) {
            oldContent.replaceInChainWith(newContent)
            oldContent.detachFromAndRelinkChain()
            return
        }

        // Only attempt to update concrete content if it has been created.  InitialiseConcreteContent will do it the first time.
        if (!initialisedConcrete) {
            return
        }

        container?.let {
            if (oldContent == null && newContent != null) {
                val builder = context.createConcreteContentBuilder(container!!)
                initialiseConcreteContent(builder)
            } else if (oldContent != null && newContent == null) {
                // Our assumption is that if the value disappears then so does the associated content
                it.clear()
            }
        }
    }

    override fun notifyChanged() {
        container?.let { it.displayed = true }
        super.notifyChanged()
    }
}
package modool.dom.property

import modool.core.content.SourceProcessingLevel

interface ReplaceValueWithParsableContentReceiver {
    fun replaceValueWithContent(value: Any, content: String, processingLevel: SourceProcessingLevel): Boolean
}
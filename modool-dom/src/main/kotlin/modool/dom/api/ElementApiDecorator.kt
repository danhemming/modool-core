package modool.dom.api

/**
 * All element decorators within APIs will implement this base class.  It allows a wrapped element to be passed
 * between APIs without exposing the internal (element) state.
 */
abstract class ElementApiDecorator<T>(decorated: T): ApiDecorator<T>(decorated) {

    protected fun unwrap(decorator: ElementApiDecorator<T>): T {
        return decorator.decorated
    }
}
package modool.dom.api

enum class ApiAction {
    CREATE,
    SET,
    SET_UNPROCESSED_TEXT,
    SET_TOKENS,
    SET_PARSED,
    ADD,
    ADD_UNPROCESSED_TEXT,
    ADD_TOKENS,
    ADD_PARSED,
    ADD_IF_NOT_PRESENT,
    UPDATE;
}
package modool.dom.api

abstract class ApiDecorator<T>(protected val decorated: T)
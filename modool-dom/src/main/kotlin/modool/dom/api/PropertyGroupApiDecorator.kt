package modool.dom.api

/**
 * Included for completeness. Currently no functionality added.
 * It is worth noting that property groups are not passable between APIs.
 */
abstract class PropertyGroupApiDecorator<T>(decorated: T): ApiDecorator<T>(decorated)
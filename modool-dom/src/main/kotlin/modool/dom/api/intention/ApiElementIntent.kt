package modool.dom.api.intention

import modool.core.meta.element.ElementMeta
import modool.core.meta.property.PropertyMeta
import modool.dom.api.ApiAction

class ApiElementIntent(
    val meta: ElementMeta
) : ApiIntent() {

    @ApiIntentDsl
    fun set(meta: PropertyMeta, value: Any?) {
        addAction(ApiAction.SET, meta, value)
    }

    @ApiIntentDsl
    fun setUnprocessedText(meta: PropertyMeta, value: Any?) {
        addAction(ApiAction.SET_UNPROCESSED_TEXT, meta, value)
    }

    @ApiIntentDsl
    fun setTokens(meta: PropertyMeta, value: Any?) {
        addAction(ApiAction.SET_TOKENS, meta, value)
    }

    @ApiIntentDsl
    fun setParsed(meta: PropertyMeta, value: Any?) {
        addAction(ApiAction.SET_PARSED, meta, value)
    }

    @ApiIntentDsl
    fun add(meta: PropertyMeta, value: Any?) {
        addAction(ApiAction.ADD, meta, value)
    }

    @ApiIntentDsl
    fun addUnprocessedText(meta: PropertyMeta, value: Any?) {
        addAction(ApiAction.ADD_UNPROCESSED_TEXT, meta, value)
    }

    @ApiIntentDsl
    fun addTokens(meta: PropertyMeta, value: Any?) {
        addAction(ApiAction.ADD_TOKENS, meta, value)
    }

    @ApiIntentDsl
    fun addParsed(meta: PropertyMeta, value: Any?) {
        addAction(ApiAction.ADD_PARSED, meta, value)
    }

    @ApiIntentDsl
    fun addIfNotPresent(meta: PropertyMeta, value: Any?) {
        addAction(ApiAction.ADD_IF_NOT_PRESENT, meta, value)
    }

    @ApiIntentDsl
    fun update(meta: PropertyMeta, value: Any?) {
        addAction(ApiAction.UPDATE, meta, value)
    }
}
package modool.dom.api.intention

import modool.core.meta.Meta
import modool.dom.api.ApiAction

data class ApiTargetedAction(
    val action: ApiAction,
    val target: Meta,
    val value: Any?
)
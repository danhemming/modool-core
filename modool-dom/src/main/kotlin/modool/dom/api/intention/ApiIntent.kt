package modool.dom.api.intention

import modool.core.meta.Meta
import modool.core.meta.element.ElementMeta
import modool.dom.api.ApiAction

/**
 * Build a list of actions to be translated into api calls.
 * Captured actions can be mapped against an ApiMeta structure to determine which operations to call.
 */
abstract class ApiIntent {

    private val actions = mutableListOf<ApiTargetedAction>()

    protected fun addAction(
        action: ApiAction,
        target: Meta,
        value: Any?
    ) {
        actions.add(ApiTargetedAction(action, target, value))
    }

    @ApiIntentDsl
    fun createElement(meta: ElementMeta, builder: (ApiElementIntent.() -> Unit)?): ApiElementIntent {
        return ApiElementIntent(meta).apply {
            addAction(ApiAction.CREATE, meta, value = null)
            builder?.invoke(this)
        }
    }
}
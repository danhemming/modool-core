package modool.dom.api.intention

class ApiRootIntent : ApiIntent()

@ApiIntentDsl
fun createApiIntent(builder: ApiRootIntent.() -> Unit): ApiRootIntent {
    return ApiRootIntent().apply(builder)
}
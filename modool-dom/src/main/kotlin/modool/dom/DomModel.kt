package modool.dom

interface DomModel<D: DomContext> {
    val contextType: Class<D>
    val supportedFileExtensions: List<String>
}
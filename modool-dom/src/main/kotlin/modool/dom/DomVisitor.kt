package modool.dom

import modool.core.content.Visitor
import modool.dom.node.DomElementBacked

interface DomVisitor<D, T, U> : Visitor<T, DomElementBacked<D>>
        where D : DomContext,
              U : DomElementBacked<D> {

    fun visitDocumentElement(context: T, documentElement: U)
}
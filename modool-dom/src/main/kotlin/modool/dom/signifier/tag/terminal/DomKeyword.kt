package modool.dom.signifier.tag.terminal

import modool.core.content.signifier.Signifier
import modool.dom.signifier.tag.DomConstantTerminalType
import modool.core.content.signifier.tag.Tag
import modool.core.content.signifier.tag.terminal.Keyword

object DomKeyword {

    object Hard : DomConstantTerminalType(category = Keyword) {
        override fun isA(signifier: Signifier): Boolean {
            return signifier == Keyword.Hard || super.isA(signifier)
        }
    }

    object Soft : DomConstantTerminalType(category = Keyword) {
        override fun isA(signifier: Signifier): Boolean {
            return signifier == Keyword.Soft || super.isA(signifier)
        }
    }

    object Modifier : Tag.Categorical() {
        override fun isA(signifier: Signifier): Boolean {
            return signifier == Keyword.Modifier || super.isA(signifier)
        }
    }
}
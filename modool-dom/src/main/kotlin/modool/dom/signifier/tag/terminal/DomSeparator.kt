package modool.dom.signifier.tag.terminal

import modool.core.content.signifier.tag.terminal.Separator
import modool.dom.signifier.tag.DomConstantTerminalType

object DomSeparator  {
    object Symbol : DomConstantTerminalType(category = Separator)
}
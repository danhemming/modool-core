package modool.dom.signifier.tag

import modool.core.content.signifier.tag.Tag
import modool.dom.node.text.factory.ConstantDomTerminalFactory
import modool.core.content.signifier.tag.TagCategory
import modool.core.content.signifier.tag.terminal.Terminal
import modool.core.content.token.ConstantTerminalDescriptor

abstract class DomConstantTerminalType internal constructor(
        category: TagCategory = Terminal
) : Tag.ConstantTerminalType(category) {

    override operator fun invoke(value: String, vararg tags: Categorical): ConstantDomTerminalFactory {
        return ConstantDomTerminalFactory(
            value,
            isCaseSensitive = true,
            type = this,
            categories = tags.toList())
    }

    override operator fun invoke(
        value: String,
        isCaseSensitive: Boolean,
        vararg tags: Categorical
    ): ConstantDomTerminalFactory {
        return ConstantDomTerminalFactory(
            value,
            isCaseSensitive = isCaseSensitive,
            type = this,
            categories = tags.toList())
    }
}
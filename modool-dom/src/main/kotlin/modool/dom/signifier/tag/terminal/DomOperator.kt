package modool.dom.signifier.tag.terminal

import modool.dom.signifier.tag.DomConstantTerminalType
import modool.core.content.signifier.tag.Tag
import modool.core.content.signifier.tag.terminal.Operator

object DomOperator {

    object Binary : DomConstantTerminalType(category = Operator)

    object UnaryPrefix : DomConstantTerminalType(category = Operator)

    object UnaryPostfix : DomConstantTerminalType(category = Operator)

    object Modifier : Tag.Categorical()
}
package modool.dom.resource

import modool.dom.node.AddressableDomElement
import modool.dom.node.DomNodeAttachable
import modool.dom.node.DomElement
import modool.dom.node.DomDocument
import java.util.*

/**
 * An URI should act as a co-ordinate to a Modool resource
 */

abstract class Uri

class DocumentUri private constructor(): Uri() {

    private lateinit var strUri: String
    private lateinit var _documentId: String
    private val _components = mutableListOf<DocumentUriComponent>()

    companion object {

        const val ROOT_NAME = "modool_document"

        fun from(element: AddressableDomElement<*>): DocumentUri {
            val uri = DocumentUri()
            uri.populateStateFromElement(element)
            return uri
        }

        fun from(uriText: String): DocumentUri {
            val uri = DocumentUri()
            uri.populateStateFromText(uriText)
            return uri
        }
    }

    val documentId: String get() = _documentId
    val components: List<DocumentUriComponent> get() = _components.toList()

    override fun toString(): String {
        return strUri
    }

    private fun populateStateFromText(text: String) {
        if (!text.startsWith(ROOT_NAME)) {
            invaliudUri(text)
        }

        val parts = text.split("/", ignoreCase = true)

        if (parts.isNotEmpty()) {
            val documentParts = parts.first().split(":", ignoreCase = true)
            if (documentParts.size == 2) {
               _documentId = documentParts.last()
            } else {
                invaliudUri(text)
            }
        }

        if (parts.size > 1) {
            parts.drop(1).forEach {
                val pathParts = it.split(":", ignoreCase = true)
                if (pathParts.size == 2) {
                    @Suppress("UNCHECKED_CAST")
                    _components.add(DocumentUriComponent(
                            javaClass.classLoader.loadClass(pathParts.first()) as Class<AddressableDomElement<*>>,
                            pathParts.last()))
                } else {
                    invaliudUri(text)
                }
            }
        }
    }

    private fun invaliudUri(text: String) {
        throw RuntimeException("Invalid modool uri: '$text'")
    }

    private fun populateStateFromElement(element: DomElement<*>) {

        var curr: DomNodeAttachable? = element
        val path = ArrayDeque<AddressableDomElement<*>>()
        while(curr != null) {
            if (curr is AddressableDomElement<*>) {
                path.push(curr)
            } else {
                throw Exception("An object without identity found when building uri (class: ${curr.javaClass.name})")
            }
            curr = curr.getParent()
        }

        if (path.isEmpty()) {
            return
        }

        val rootElement = path.pop()

        if (rootElement is DomDocument<*>) {
            _documentId = rootElement.identifier.toString()
        } else {
            throw Exception("Root object not a documentId when building uri (class: ${rootElement.javaClass.name})")
        }

        val calc = StringBuilder(1024)
        calc.append("$ROOT_NAME:")
        calc.append(_documentId)

        path.forEach {
            _components.add(DocumentUriComponent(it.javaClass, it.identifier.toString()))
            calc.append("/")
            calc.append(it.javaClass.name)
            calc.append(":")
            calc.append(it.identifier.toString())
        }

        strUri = calc.toString()
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is DocumentUri) return false

        if (strUri != other.strUri) return false

        return true
    }

    override fun hashCode(): Int {
        return strUri.hashCode()
    }
}

class DocumentUriComponent(val type: Class<AddressableDomElement<*>>, val identifier: String)
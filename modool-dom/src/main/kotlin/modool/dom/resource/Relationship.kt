package modool.dom.resource

abstract class Relationship<out S: Resource, out T: Resource>(val source: S, val target: T) {
    fun bind() {
        source.addOutRelationship(this)
        target.addInRelationship(this)
    }
}

class Represents private constructor(source: Resource, target: Resource)
    : Relationship<Resource, Resource>(source, target) {
    companion object {
        fun create(source: Resource, target: Resource) : Represents {
            val rel = Represents(source, target)
            rel.bind()
            return rel
        }
    }
}

fun Resource.addRepresentation(representation: Resource) = Represents.create(this, representation)
val Resource.REPRESENTS get() = this.listOutRelationships(Represents::class).map { it.target }
val Resource.REPRESENTED_BY get() = this.listInRelationships(Represents::class).map { it.source }

package modool.dom.resource

import kotlin.reflect.KClass

/**
 * A resource is something that can appear as a vertex in a relationship.  The most common resource is a named document
 * element.  But a resource could also be a reference to external container e.g. a JIRA ticket.
 */
interface Resource {
    fun addInRelationship(rel: Relationship<Resource, Resource>) {
        TODO("Not implemented")
    }

    fun addOutRelationship(rel: Relationship<Resource, Resource>) {
        TODO("Not implemented")
    }

    fun <T: Relationship<Resource, Resource>> listInRelationships(type: KClass<T>): Sequence<T> {
        TODO("Not implemented")
    }

    fun <T: Relationship<Resource, Resource>> listOutRelationships(type: KClass<T>): Sequence<T> {
        TODO("Not implemented")
    }
}
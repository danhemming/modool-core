package modool.test.parser

import modool.parser.ParseResult
import modool.dom.token.DomContentStartToken
import modool.dom.token.DomToken
import modool.dom.token.findNext

fun ParseResult.assertCompletelySuccessful(): DomContentStartToken {
    when (this) {
        is ParseResult.Content ->
            if (reference is DomContentStartToken) return reference as DomContentStartToken
            else throw RuntimeException("Success but reference returned is not a token")
        is ParseResult.Success -> throw RuntimeException("Success but expected complete success")
        is ParseResult.FailWithMessage -> throw RuntimeException("Failed: message is still TODO")
        is ParseResult.Fail -> throw RuntimeException("Failed without a message")
        else -> throw RuntimeException("Expected complete success but found: '${this.javaClass.simpleName}'")
    }
}

fun ParseResult.assertSuccessful() {
    when (this) {
        is ParseResult.Success -> return
        is ParseResult.FailWithMessage -> throw RuntimeException("Failed: message is still TODO")
        is ParseResult.Fail -> throw RuntimeException("Failed without a message")
        else -> throw RuntimeException("Expected success but found: '${this.javaClass.simpleName}'")
    }
}

fun ParseResult.assertNoEffect() {
    if (this != ParseResult.Nothing) {
        throw RuntimeException("Expected content to be ignored but it was not")
    }
}

inline fun <reified T> ParseResult.assertElementSuccessful(): T
        where T : DomToken {

    return when (this) {
        is ParseResult.Content ->
            if (reference is DomContentStartToken) (reference as DomContentStartToken).findNext<T>()
                    ?: throw RuntimeException("Expected element not found: '${T::class.java}'")
            else throw RuntimeException("Success but reference returned is not a token")
        is ParseResult.Success -> throw RuntimeException("Parse reports partial success but no content is provided")
        is ParseResult.FailWithMessage -> throw RuntimeException(failure.toString())
        else -> throw RuntimeException("Parse failed when element expected: '${this.javaClass.simpleName}'")
    }
}

fun ParseResult.assertFailure() {
    if (this !is ParseResult.Fail) {
        throw RuntimeException("Expected failure but found: '${this.javaClass.simpleName}'")
    }
}
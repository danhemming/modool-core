
dependencies {

    compile(project(":modool-core"))
    compile(project(":modool-dom"))
    compile("junit:junit:4.12")

    compile("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.0.0")
    compile("org.jetbrains.kotlin:kotlin-reflect:1.3.0")
    compile("org.jetbrains.kotlin:kotlin-compiler-embeddable:1.3.10")

    compile("com.google.jimfs:jimfs:1.1")
}

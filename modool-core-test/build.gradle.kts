

dependencies {
    testCompile(project(":modool-core"))
    testCompile(project(":modool-dom"))
    testCompile(project(":modool-dom-solution"))
    testCompile(project(":modool-test-core"))
    testCompile(project(":modool-test-dom"))
}

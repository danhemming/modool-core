package modool.core

import modool.lexer.LexerFactory
import modool.lexer.rule.LexerRule
import modool.lexer.rule.builder.LexerRuleCollectionFactory
import modool.core.meta.MetaFactory
import modool.parser.ParserFactory
import modool.parser.TestFunction
import modool.parser.TestParameter
import modool.parser.rule.ParserRulable
import modool.parser.strategy.ParseStrategy
import modool.parser.strategy.StrictParseStrategy
import modool.dom.DomContext
import modool.dom.DomFactory
import modool.dom.style.Padding
import modool.dom.style.StyleProvider
import modool.dom.style.builder.StyleSheet

object TestDomContext : DomContext {

    override val defaultParseStrategy: ParseStrategy = StrictParseStrategy

    override val styler: StyleProvider = object : StyleProvider() {
        override val stylesheet by lazy {
            StyleSheet {
                padding(TestParameter.Terminals.COLON, Padding.Pad.Right)
                padding(TestFunction.ParametersDomProperty.Terminals.COMMA, Padding.Pad.Right)
            }
        }
    }

    override val meta = object : MetaFactory {}

    override val dom = object : DomFactory {}

    override val lexer = object : LexerFactory {

        override fun createDocumentLexerRules(): List<LexerRule> {
            return createCommonLexerRules()
        }

        override fun createCommonLexerRules(): List<LexerRule> {
            return LexerRuleCollectionFactory.createList {

                +pattern {
                    +asciiLetterChar()
                    +unlimited(asciiLetterOrDigitChar())
                }

                +unicodeWhitespace()
            }
        }
    }

    override val parser = object : ParserFactory {
        override fun createDocumentParserRule(): ParserRulable {
            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }
    }
}
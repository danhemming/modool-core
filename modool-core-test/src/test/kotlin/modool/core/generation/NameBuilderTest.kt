package modool.core.generation

import modool.core.name.NameBuilder
import org.amshove.kluent.`should equal`
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it

class NameBuilderTest : Spek({

    describe("Name splitting") {

        it("should split based on capitals (leading capital)") {
            val parts = NameBuilder.splitOnCapitalsAndSeparators("HelloThere")
            parts[0] `should equal` "Hello"
            parts[1] `should equal` "There"
            parts.count() `should equal` 2
        }

        it("should split based on capitals (no leading capital)") {
            val parts = NameBuilder.splitOnCapitalsAndSeparators("helloThere")
            parts[0] `should equal` "hello"
            parts[1] `should equal` "There"
            parts.count() `should equal` 2
        }

        it("should split based on capitals and a separator (no leading capital)") {
            val parts = NameBuilder.splitOnCapitalsAndSeparators("hel_loThere")
            parts[0] `should equal` "hel"
            parts[1] `should equal` "lo"
            parts[2] `should equal` "There"
            parts.count() `should equal` 3
        }

        it("should preserve acronyms when splitting on capitals") {
            val parts = NameBuilder.splitOnCapitalsAndSeparators("aNiceTLA")
            parts[0] `should equal` "a"
            parts[1] `should equal` "Nice"
            parts[2] `should equal` "TLA"
            parts.count() `should equal` 3
        }
    }

    describe("replacement") {
        it("should expand illegal characters to description") {
            NameBuilder.fromEncodedWithSeparators("hello\$There").toKebabCase() `should equal` "hello-dollar-there"
        }
    }

    describe("equality") {
        it("split name should be equal") {
            val a = NameBuilder.fromEncodedWithSeparators("helloThere")
            val b = NameBuilder.fromEncodedWithSeparators("hello_There")
            a `should equal` b
        }

        it("split name with reduction should be equal") {
            val a = NameBuilder.fromEncodedWithSeparators("helloThere")
            a.removeLastPart()
            val b = NameBuilder.fromEncodedWithSeparators("hello_There")
            b.removeLastPart()
            a `should equal` b
        }
    }

    describe("Conversions") {
        it("should support a conversion from shouting snake case to lower camel case") {
            NameBuilder.fromEncodedWithSeparators("A_SHOUTING_SNAKE").toLowerCamelCase() `should equal` "aShoutingSnake"
        }

        it("should preserve acronyms so long as we are not shouting") {
            NameBuilder.fromEncodedWithSeparators("graphQL").toUpperCamelCase() `should equal` "GraphQL"
        }

        it("should support a conversion from kebab case to upper camel case") {
            NameBuilder.fromEncodedWithSeparators("a-kebab-is-nice").toUpperCamelCase() `should equal` "AKebabIsNice"
        }

        it("should support a conversion from lower camel case to snake case") {
            NameBuilder.fromEncodedWithSeparators("aNiceSnake").toSnakeCase() `should equal` "a_nice_snake"
        }
    }

})
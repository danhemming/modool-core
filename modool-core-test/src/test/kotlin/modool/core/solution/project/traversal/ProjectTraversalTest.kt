package modool.core.solution.project.traversal

import com.google.common.jimfs.Configuration
import com.google.common.jimfs.Jimfs
import modool.dom.solution.project.DomProject
import modool.dom.solution.vfs.NativeVfs
import modool.dom.solution.vfs.VfsDirectory
import modool.dom.solution.vfs.VfsEventObserver
import org.amshove.kluent.`should equal`
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it
import java.nio.file.FileSystem
import java.nio.file.Files

class ProjectTraversalTest : Spek({
    describe("Project Traversal operations") {

        val fs = Jimfs.newFileSystem(Configuration.forCurrentPlatform())
        val dir = createProjectDirectoryStructure(fs)
        val project = DomProject(dir)

        it("Should be able to discover a single directory") {
            val traversal = project.traversal()
            traversal.listDirectories("a").asSequence().count() `should equal` 1
        }

        it("Should be able to discover a single directory by a pattern") {
            val traversal = project.traversal()
            traversal.listDirectories { it.name == "c" && it.parent?.name == "root" }.asSequence().count() `should equal` 1
        }

        it("Should be able to discover files through directory and file search") {
            val traversal = project.traversal()
            traversal.listDirectories("a").listFiles("2.txt").asSequence().count() `should equal` 1
        }

        it("Should be able to discover files through multiple match directory and file search") {
            val traversal = project.traversal()
            traversal
                    .listDirectories { it.name == "a" || it.name == "b" }
                    .listFiles("2.txt")
                    .asSequence()
                    .count() `should equal` 2
        }
    }
})

fun createProjectDirectoryStructure(fs: FileSystem): VfsDirectory {
    val root = fs.getPath("/root")
    Files.createDirectory(root)
    Files.createDirectory(fs.getPath("/root/a"))
    Files.createDirectory(fs.getPath("/root/b"))
    Files.createDirectory(fs.getPath("/root/c"))
    Files.createFile(fs.getPath("/root/a/1.txt"))
    Files.createFile(fs.getPath("/root/a/2.txt"))
    Files.createFile(fs.getPath("/root/b/2.txt"))
    return VfsDirectory(NativeVfs(VfsEventObserver(), root), null, "root")
}
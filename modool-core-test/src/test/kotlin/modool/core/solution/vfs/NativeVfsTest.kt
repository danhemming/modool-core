package modool.dom.solution.vfs

import com.google.common.jimfs.Configuration
import com.google.common.jimfs.Jimfs
import org.amshove.kluent.`should equal`
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it
import java.io.InputStreamReader
import java.io.OutputStreamWriter
import java.nio.file.FileSystem
import java.nio.file.Files


class NativeVfsTest : Spek({
    describe("Project IO operations") {

        it("Should be able to create a directory") {
            val fs = Jimfs.newFileSystem(Configuration.forCurrentPlatform())
            // Wrap the NIO interface with our API
            val dir = createProjectDirectory(fs)
            // Use our API to create a directory
            dir.cd("1")
            // Check it is there using th NIO API
            Files.exists(fs.getPath("/root/1")) `should equal` true
        }

        it("Should be able to create a sub directory") {
            val fs = Jimfs.newFileSystem(Configuration.forCurrentPlatform())
            val dir = createProjectDirectory(fs)
            dir.cd("1").cd("2")
            Files.exists(fs.getPath("/root/1/2")) `should equal` true
        }

        it("Should be able to create a file") {
            val fs = Jimfs.newFileSystem(Configuration.forCurrentPlatform())
            val dir = createProjectDirectory(fs)
            dir.open("1.txt")
            Files.exists(fs.getPath("/root/1.txt")) `should equal` true
        }

        it("Should be able to delete a file") {
            val fs = Jimfs.newFileSystem(Configuration.forCurrentPlatform())
            val dir = createProjectDirectory(fs)
            dir.open("1.txt").delete()
            Files.exists(fs.getPath("/root/1.txt")) `should equal` false
        }

        it("Should be able to create multiple files / directories and list them") {
            val fs = Jimfs.newFileSystem(Configuration.forCurrentPlatform())
            val dir = createProjectDirectory(fs)
            dir.open("1.txt")
            dir.open("2.txt")
            dir.cd("3")
            Files.exists(fs.getPath("/root/1.txt")) `should equal` true
            Files.exists(fs.getPath("/root/2.txt")) `should equal` true
            Files.exists(fs.getPath("/root/3")) `should equal` true
            dir.files.count() `should equal` 2
            dir.directories.count() `should equal` 1
            dir.contents.size `should equal` 3
        }

        it("Should be able to create file content and retrieve it") {
            val fs = Jimfs.newFileSystem(Configuration.forCurrentPlatform())
            val dir = createProjectDirectory(fs)
            val file = dir.open("1.txt")
            file.createOutputStream().use {
                val writer = OutputStreamWriter(it)
                writer.write("test")
                writer.flush()
            }
            file.createInputStream().use {
                InputStreamReader(it).readText() `should equal` "test"
            }
        }
    }
})

fun createProjectDirectory(fs: FileSystem): VfsDirectory {
    val root = fs.getPath("/root")
    Files.createDirectory(root)
    return VfsDirectory(NativeVfs(VfsEventObserver(), root), null, "root")
}
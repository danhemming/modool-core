package modool.core.content.token

import modool.dom.node.text.DomTerminalNode
import modool.dom.node.text.factory.CommonDynamicTerminals
import modool.dom.token.DomToken
import modool.test.dom.assertJoinToEnd
import modool.test.dom.assertJoinToStart
import org.amshove.kluent.`should equal`
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it

class TokenTest : Spek({

    describe("simple chain") {

        it("should be constructed correctly") {
            val tokens = createTokenChain("A", "B", "C")
            tokens[0].prior `should equal` null
            tokens[0].next `should equal` tokens[1]
            tokens[1].prior `should equal`tokens[0]
            tokens[1].next `should equal` tokens[2]
            tokens[2].prior `should equal` tokens[1]
            tokens[2].next `should equal` null
        }
    }

    describe("insert detached token") {

        it("should detach a single node from a chain and insert it into a new chain after the current token") {
            val letters = createTokenChain("A", "B", "C")
            val numbers = createTokenChain("1", "2", "3")

            numbers.first().insertDetachedNext(letters[1])
            numbers.first().assertJoinToEnd("1,B,2,3")
            letters.first().assertJoinToEnd("A")
            letters.last().assertJoinToStart("C")
            letters[1].assertJoinToEnd("B,2,3")
            letters[1].assertJoinToStart("B,1")
        }

        it("should detach a single node from a chain and insert it into a new chain before the current token") {
            val letters = createTokenChain("A", "B", "C")
            val numbers = createTokenChain("1", "2", "3")

            numbers.first().insertDetachedPrior(letters[1])
            letters[1].assertJoinToEnd("B,1,2,3")
            letters[1].assertJoinToStart("B")
            letters.first().assertJoinToEnd("A")
            letters.last().assertJoinToStart("C")
        }
    }


    describe("insert chain of tokens") {

        it("should detach a partial chain, heal it and insert it into another chain following a specified token") {
            val letters = createTokenChain("A", "B", "C")
            val numbers = createTokenChain("1", "2", "3")

            letters[1].insertExtractedChainNext(numbers[1], numbers[2])
            letters[0].assertJoinToEnd("A,B,2,3,C")
            numbers[0].assertJoinToEnd("1")
        }

        it("should detach a partial chain, heal it and insert it into another chain preceding a specified token") {
            val letters = createTokenChain("A", "B", "C")
            val numbers = createTokenChain("1", "2", "3", "4")

            letters[1].insertExtractedChainPrior(numbers[1], numbers[2])
            letters[0].assertJoinToEnd("A,2,3,B,C")
            numbers[0].assertJoinToEnd("1,4")
        }

        it("should detach a single token, heal it and insert it into the same chain prior") {
            val numbers = createTokenChain("1", "2", "3", "4")

            numbers[1].insertExtractedChainPrior(numbers[2], numbers[2])
            numbers[0].assertJoinToEnd("1,3,2,4")
        }

        it("should detach a partial chain, heal it and insert it into the same chain prior") {
            val numbers = createTokenChain("1", "2", "3", "4")

            numbers[1].insertExtractedChainPrior(numbers[2], numbers[3])
            numbers[0].assertJoinToEnd("1,3,4,2")
        }

        it("should detach a single token, heal it and insert it into the same chain next") {
            val numbers = createTokenChain("1", "2", "3", "4")

            numbers[2].insertExtractedChainNext(numbers[1], numbers[1])
            numbers[0].assertJoinToEnd("1,3,2,4")
        }

        it("should detach a partial chain, heal it and insert it into the same chain next") {
            val numbers = createTokenChain("1", "2", "3", "4")

            numbers[0].insertExtractedChainNext(numbers[2], numbers[3])
            numbers[0].assertJoinToEnd("1,3,4,2")
        }
    }

    describe("detach and heal") {

        it("should remove an entry from a chain and heal the chain") {
            val letters = createTokenChain("A", "B", "C")
            letters[1].detachFromAndRelinkChain()
            letters[1].prior `should equal` null
            letters[1].next `should equal` null
            letters[0].assertJoinToEnd("A,C")
        }
    }
})


fun createTokenChain(vararg texts: String): List<DomTerminalNode> {
    var currentToken : DomToken? = null
    val list = mutableListOf<DomTerminalNode>()
    texts.forEach {
        val token = CommonDynamicTerminals.unknown(it)
        currentToken?.next = token
        token.prior = currentToken
        currentToken = token
        list.add(token)
    }

    return list
}
package modool.parser

import modool.dom.property.sequence.element.definition.NamedElementObjectDefinition
import modool.dom.identity.NameIndexer
import modool.test.parser.assertCompletelySuccessful
import modool.test.parser.assertElementSuccessful
import modool.test.parser.assertSuccessful
import modool.dom.token.asForwardSequence
import org.amshove.kluent.`should be equal to`
import org.amshove.kluent.`should be`
import org.amshove.kluent.`should equal`
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it

@Suppress("UNCHECKED_CAST")
class StrictParserTest : Spek({

    describe("A simple function parser") {
        
        it("should match a simple function declaration") {
            TestParser.parse("fun my_function() { }").assertSuccessful()
        }

        it("should match a function declaration with parameters") {
            val result = TestParser.parse("fun my_function(a : int, b: string) { }")
            val function = result.assertElementSuccessful<TestFunction>()
            function.params.count() `should be` 2
            function.params["a"]!!.type.get() `should be equal to` "int"
            function.params["b"]!!.type.get() `should be equal to` "string"
        }

        it("should allow removal of parameters") {
            val result = TestParser.parse("fun my_function(a : int, b: string) { }")
            val function = result.assertElementSuccessful<TestFunction>()
            function.params.count() `should equal` 2
            function.params.remove("b")
            function.params.count() `should equal` 1
            function.toFormattedString() `should equal` "fun my_function(a : int) { }"
        }

        it("should match multiple functions") {
            val result = TestParser.parse("fun fun1(a : int) { } fun fun2(b : int) { }")
            val source = result.assertCompletelySuccessful()
            val functions = source.asForwardSequence { it is TestFunction }
            functions.count() `should be` 2
        }

        it("should support mutation and rendering") {
            val result = TestParser.parse("fun my_function(a : int) { }")
            val function = result.assertElementSuccessful<TestFunction>()
            function.nameBacking.set("rename")
            function.params["a"]?.apply {
                nameBacking.set("Nameable")
                type.set("string")
            }
            function.toFormattedString() `should be equal to` "fun rename(Nameable : string) { }"
        }

        it("should support addition of parameters") {
            val result = TestParser.parse("fun my_function(a : int) { }")
            val function = result.assertElementSuccessful<TestFunction>()
            function.params.apply {
                merge(NamedElementObjectDefinition(NameIndexer("b"), TestParameter.Meta::create)) { type.set("int") }
                merge(NamedElementObjectDefinition(NameIndexer("c"), TestParameter.Meta::create)) { type.set("string") }
            }
            function.toFormattedString() `should be equal to` "fun my_function(a : int, b: int, c: string) { }"
        }

        it("should support addition of parameters that are pseudo elements") {
            val result = TestParser.parse("fun my_function(a : int) { }")
            val function = result.assertElementSuccessful<TestFunction>()
            function.params.addAsTokens("b: string")
            function.toFormattedString() `should be equal to` "fun my_function(a : int, b: string) { }"
        }

        it("should support addition of parameters where none exist") {
            val result = TestParser.parse("fun my_function() { }")
            val function = result.assertElementSuccessful<TestFunction>()
            function.params.apply {
                // Add a parameter named "a" of type int
                add(NamedElementObjectDefinition(NameIndexer("a"), TestParameter.Meta::create)) {
                    type.set("int")
                }
                // rename the first parameter (a) to b
                updateAt(0) { nameBacking.set("b") }
            }
            function.toFormattedString() `should be equal to` "fun my_function(b: int) { }"
        }

        it("should support removal of parameters where one contains") {
            val result = TestParser.parse("fun my_function(b: Int) { }")
            val function = result.assertElementSuccessful<TestFunction>()
            function.params.apply { remove("b") }
            function.toFormattedString() `should be equal to` "fun my_function() { }"
        }

        it("should match single line comments") {
            val result = TestParser.parse("// fun my_function() { }")
            result.assertSuccessful()
            // FIXME: A better test of a comment existing is required
        }

        it("should match multi line comments") {
            val result = TestParser.parse("fun my_function(/*a comment*/) { }")
            val function = result.assertElementSuccessful<TestFunction>()
            // FIXME: because params is a collection property we cannot access "container"
            function.toFormattedString() `should be equal to` "fun my_function(/*a comment*/) { }"
        }
    }
})
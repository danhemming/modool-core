package modool.parser

import modool.core.TestDomContext
import modool.lexer.Lexer
import modool.lexer.rule.unknown.CaptureUnknownTerminalLexerRule
import modool.parser.rule.*
import modool.parser.rule.builder.element
import modool.parser.strategy.ParseStrategy
import modool.parser.strategy.StrictParseStrategy
import modool.dom.lexer.DomLexerContext
import modool.dom.signifier.tag.terminal.DomSeparator
import modool.dom.token.DomTokenReaderDomProviderView

object TestParser {

    object KeywordRules {
        val `fun` = ConstantTerminalParserRule(funKeyword)
    }

    val id = NameParserRule(TestNameTerminal)
    val type = NameParserRule(TestNameTerminal)

    val param = element(TestParameter.Meta) {
        +id.sets(TestParameter.NameMeta)
        +ConstantTerminalParserRule(TestParameter.Terminals.COLON)
        +type.sets(TestParameter.TypeMeta)
    }

    val parameters = ListParserRule(
            open = ConstantTerminalParserRule(TestFunction.ParametersDomProperty.Terminals.OPEN_PAREN),
            separator = ConstantTerminalParserRule(TestFunction.ParametersDomProperty.Terminals.COMMA),
            item = OptionallyParserRule(param),
            close = ConstantTerminalParserRule(TestFunction.ParametersDomProperty.Terminals.CLOSE_PAREN))

    private val body = CompositeParserRule(
            ConstantTerminalParserRule(TestFunction.Terminals.BRACE_OPEN),
            ConstantTerminalParserRule(TestFunction.Terminals.BRACE_CLOSE))

    private val function = element(TestFunction.Meta) {
        +KeywordRules.`fun`
        +id.sets(TestFunction.NameMeta)
        +parameters.sets(TestFunction.ParametersMeta)
        +body
    }

    private val root = CompositeParserRule(UnorderedParserRule(Unlimited(function)), EndOfContentParserRule)

    private fun createLexer() = Lexer.from(CaptureUnknownTerminalLexerRule()) {
        +unicodeWhitespace()
        +funKeyword
        +DomSeparator.Symbol("(")
        +DomSeparator.Symbol(")")
        +DomSeparator.Symbol("{")
        +DomSeparator.Symbol("}")
        +DomSeparator.Symbol(":")
        +DomSeparator.Symbol(",")
        +singleLineComment(terminal(DomSeparator.Symbol("//")))
        +multiLineComment(
                terminal(DomSeparator.Symbol("/*")),
                terminal(DomSeparator.Symbol("*/")))
        +pattern(TestNameTerminal) {
            +choose(char('_'), asciiLetterChar())
            +unlimited(asciiLetterOrDigitChar())
        }
    }

    fun parse(
            content: String,
            strategy: ParseStrategy = StrictParseStrategy): ParseResult {

        val tokenReader = createLexer().tokenise(DomLexerContext.from(content))
        val result = TestDomContext.parse(root, tokenReader, strategy = strategy)
        val domTokenReader = tokenReader.createProviderView(DomTokenReaderDomProviderView::class.java)

        return if (result is ParseResult.Success) {
            ParseResult.CompleteContent(domTokenReader.startToken)
        } else {
            result
        }
    }
}
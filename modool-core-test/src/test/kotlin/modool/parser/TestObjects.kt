package modool.parser

import modool.core.TestDomContext
import modool.lexer.rule.LexerRule
import modool.parser.rule.ParserRulable
import modool.dom.identity.Identifier
import modool.dom.identity.NamePropertyIdentifier
import modool.dom.identity.caseSensitiveIdentityEquals
import modool.dom.meta.DomElementMeta
import modool.dom.meta.DomPropertyMeta
import modool.dom.name.MandatoryRenameable
import modool.dom.node.ConcreteContentBuilder
import modool.dom.node.IdentifiableDomElement
import modool.dom.property.nameProperty
import modool.dom.property.sequence.element.map.DomNamedElementBackedMapProperty
import modool.dom.property.value.DomNameProperty
import modool.dom.signifier.tag.terminal.DomKeyword
import modool.dom.signifier.tag.terminal.DomSeparator

val funKeyword = DomKeyword.Hard("fun")

open class TestFunction internal constructor(
        context: TestDomContext,
        val nameBacking: DomNameProperty<TestDomContext, String>,
        val params: ParametersDomProperty)
    : IdentifiableDomElement<TestDomContext>(Meta, context),
        MandatoryRenameable {

    override val identifier: Identifier = NamePropertyIdentifier(::caseSensitiveIdentityEquals, nameBacking)

    override var name: String
        get() = nameBacking.get()
        set(value) {
            nameBacking.set(value)
        }

    override fun initialiseAbstractContent() {
        adopt(nameBacking)
        adopt(params)
    }

    override fun initialiseConcreteContent(builder: ConcreteContentBuilder) {
        builder.apply {
            +funKeyword
            +nameBacking
            +params
            +Terminals.BRACE_OPEN
            +Terminals.BRACE_CLOSE
        }
    }

    object Terminals {
        val BRACE_OPEN = DomSeparator.Symbol("{")
        val BRACE_CLOSE = DomSeparator.Symbol("}")
    }

    class ParametersDomProperty(
            context: TestDomContext
    ) : DomNamedElementBackedMapProperty<TestDomContext, TestParameter>(
            ParametersMeta,
            context,
            TestParameter::class.java) {

        override fun createParserRule(): ParserRulable {
            return TestParser.parameters
        }

        override fun createLexerRules(): List<LexerRule> {
            return emptyList()
        }

        override fun initialiseConcreteOpen(builder: ConcreteContentBuilder) {
            builder.then(Terminals.OPEN_PAREN)
        }

        override fun initialiseConcreteSeparator(builder: ConcreteContentBuilder) {
            builder.then(Terminals.COMMA)
        }

        override fun initialiseConcreteClose(builder: ConcreteContentBuilder) {
            builder.then(Terminals.CLOSE_PAREN)
        }

        object Terminals {
            val OPEN_PAREN = DomSeparator.Symbol("(")
            val COMMA = DomSeparator.Symbol(",")
            val CLOSE_PAREN = DomSeparator.Symbol(")")
        }
    }

    object Meta : DomElementMeta<TestDomContext, TestFunction> {

        override fun create(context: TestDomContext): TestFunction {
            return TestFunction(
                    context = context,
                    nameBacking = nameProperty(context),
                    params = ParametersDomProperty(TestDomContext))
        }
        override val type = TestFunction::class.java
        override val name = ""
        override val description = ""
    }

    object NameMeta : DomPropertyMeta<TestDomContext, TestFunction, DomNameProperty<TestDomContext, String>> {

        override val name = ""
        override val description = ""

        override fun getProperty(propertyContainer: TestFunction): DomNameProperty<TestDomContext, String> {
            return propertyContainer.nameBacking
        }
    }

    object ParametersMeta
        : DomPropertyMeta<TestDomContext, TestFunction, DomNamedElementBackedMapProperty<TestDomContext, TestParameter>> {

        override val name = ""
        override val description = ""

        override fun getProperty(propertyContainer: TestFunction): DomNamedElementBackedMapProperty<TestDomContext, TestParameter> {
            return propertyContainer.params
        }
    }
}

open class TestParameter internal constructor(
        context: TestDomContext,
        val nameBacking: DomNameProperty<TestDomContext, String>,
        val type: DomNameProperty<TestDomContext, String>)
    : IdentifiableDomElement<TestDomContext>(Meta, context),
        MandatoryRenameable {

    override val identifier: Identifier = NamePropertyIdentifier(::caseSensitiveIdentityEquals, nameBacking)

    override var name: String
        get() = nameBacking.get()
        set(value) {
            nameBacking.set(value)
        }

    override fun initialiseConcreteContent(builder: ConcreteContentBuilder) {
        builder.apply {
            +nameBacking
            +Terminals.COLON
            +type
        }
    }

    object Terminals {
        val COLON = DomSeparator.Symbol(":")
    }

    object Meta : DomElementMeta<TestDomContext, TestParameter> {

        override val type = TestParameter::class.java
        override val name = ""
        override val description = ""

        override fun create(context: TestDomContext): TestParameter {
            return TestParameter(
                    context = context,
                    nameBacking = nameProperty(context),
                    type = nameProperty(context))
        }
    }

    object NameMeta : DomPropertyMeta<TestDomContext, TestParameter, DomNameProperty<TestDomContext, String>> {

        override val name = ""
        override val description = ""

        override fun getProperty(propertyContainer: TestParameter): DomNameProperty<TestDomContext, String> {
            return propertyContainer.nameBacking
        }
    }

    object TypeMeta : DomPropertyMeta<TestDomContext, TestParameter, DomNameProperty<TestDomContext, String>> {
        override val name = ""
        override val description = ""

        override fun getProperty(propertyContainer: TestParameter): DomNameProperty<TestDomContext, String> {
            return propertyContainer.type
        }
    }
}
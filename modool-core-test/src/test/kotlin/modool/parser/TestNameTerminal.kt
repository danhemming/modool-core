package modool.parser

import modool.core.content.signifier.tag.Tag
import modool.core.content.signifier.tag.terminal.Name

object TestNameTerminal : Tag.DynamicTerminalType(Name)
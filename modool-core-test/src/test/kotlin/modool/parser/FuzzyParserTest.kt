package modool.parser

import modool.parser.strategy.FuzzyParseStrategy
import modool.test.parser.assertElementSuccessful
import modool.test.parser.assertFailure
import org.amshove.kluent.`should be equal to`
import org.amshove.kluent.`should be`
import org.amshove.kluent.`should equal`
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it

@Suppress("UNCHECKED_CAST")
class FuzzyParserTest : Spek({

    /**
     * Note we are not tolerant of malformed lists i.e. (,) this is because without these delimiters other correction
     * strategies cause the elements to bleed into one another.  The Fuzzy parser is here mostly to get around partially
     * formed grammars
     */
    describe("A simple function parser") {

        it("should ignore an incorrect token in the function declaration") {
            val result = TestParser.parse("fun my_function wibble(a : int, b: string) { }", FuzzyParseStrategy)
            val function = result.assertElementSuccessful<TestFunction>()
            function.name `should equal` "my_function"
            function.params.count() `should be` 2
            function.params["a"]!!.type.get() `should be equal to` "int"
            function.params["b"]!!.type.get() `should be equal to` "string"
        }

        it("should ignore an incorrect token in the parameter declaration") {
            val result = TestParser.parse("fun my_function(a : int fuck, b: string) { }", FuzzyParseStrategy)
            val function = result.assertElementSuccessful<TestFunction>()
            function.name `should equal` "my_function"
            function.params.count() `should be` 2
            function.params["a"]!!.type.get() `should be equal to` "int"
            function.params["b"]!!.type.get() `should be equal to` "string"
        }

        it("should tolerate a missing token (:) but not skip past the parent anchor (,) to the next match") {
            val result = TestParser.parse("fun my_function(a int, b: string) { }", FuzzyParseStrategy)
            val function = result.assertElementSuccessful<TestFunction>()
            function.name `should equal` "my_function"
            function.params.count() `should be` 2
            function.params["a"]!!.type.get() `should be equal to` "int"
            function.params["b"]!!.type.get() `should be equal to` "string"
        }

        it("should read the first parameter when the list separator is missing (anchors at closed paren)") {
            val result = TestParser.parse("fun my_function(a: int b: string) { }", FuzzyParseStrategy)
            val function = result.assertElementSuccessful<TestFunction>()
            function.name `should equal` "my_function"
            function.params.count() `should be` 1
            function.params["a"]!!.type.get() `should be equal to` "int"
        }

        it("should NOT tolerate a missing list separator when the close bracket is too many tokens away. However it will used balanced matching to complete") {
            val result = TestParser.parse("fun my_function(a: int b: string c: string d: string) { }", FuzzyParseStrategy)
            val function = result.assertElementSuccessful<TestFunction>()
            function.name `should equal` "my_function"
            // Restoring the context (restoreTo) means the parsing of the first parameter is wiped out
            function.params.count() `should be` 0
        }

        it("should NOT tolerate a missing list open") {
            TestParser.parse("fun my_function a: int, b: string) { }", FuzzyParseStrategy).assertFailure()
        }

        it("should NOT tolerate a missing list close") {
            TestParser.parse("fun my_function(a: int b: string { }", FuzzyParseStrategy).assertFailure()
        }

        it("should tolerate a combination of unknown tokens i.e. public and [int]") {
            val result = TestParser.parse("public fun my_function(a: int, b: string[int]) { }", FuzzyParseStrategy)
            val function = result.assertElementSuccessful<TestFunction>()
            function.name `should equal` "my_function"
            function.params.count() `should be` 2
            function.params["a"]!!.type.get() `should be equal to` "int"
            function.params["b"]!!.type.get() `should be equal to` "string"
        }

        it("should NOT tolerate a missing hard keyword") {
            TestParser.parse("my_function() { }", FuzzyParseStrategy).assertFailure()
        }
    }
})
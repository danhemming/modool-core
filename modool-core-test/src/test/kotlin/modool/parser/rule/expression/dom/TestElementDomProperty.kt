package modool.parser.rule.expression.dom

import modool.core.TestDomContext
import modool.core.meta.property.NoPropertyMeta
import modool.dom.node.DomElementBacked
import modool.dom.property.value.DomDynamicElementProperty
import modool.lexer.rule.LexerRule
import modool.parser.rule.ParserRulable

class TestElementDomProperty<T : DomElementBacked<TestDomContext>>(
        context: TestDomContext,
        type: Class<T>)
    : DomDynamicElementProperty<TestDomContext, T>(NoPropertyMeta, context, type) {
    override fun createParserRule(): ParserRulable {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun createLexerRules(): List<LexerRule> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}
package modool.parser.rule.expression.dom

import modool.core.TestDomContext
import modool.dom.meta.DomElementMeta
import modool.dom.meta.DomPropertyMeta
import modool.dom.node.ConcreteContentBuilder
import modool.dom.node.expression.DomBinaryExpression

class TestBinaryDomExpression(
        val lhsBacking: TestElementDomProperty<TestDomExpression>,
        val rhsBacking: TestElementDomProperty<TestDomExpression>
) : TestDomExpression(),
        DomBinaryExpression<TestDomContext, TestType, TestDomExpression> {

    override var lhs: TestDomExpression
        get() = lhsBacking.get()!!
        set(value) {
            lhsBacking.set(value)
        }

    override var rhs: TestDomExpression
        get() = rhsBacking.get()!!
        set(value) {
            rhsBacking.set(value)
        }

    override val type: TestType
        get() = TODO("not implemented") //To change initializer of created properties use File | Settings | File Templates.

    override fun initialiseAbstractContent() {
        adopt(lhsBacking)
        adopt(rhsBacking)
    }

    override fun initialiseConcreteContent(builder: ConcreteContentBuilder) {
        builder.apply {
            +lhsBacking
            +rhsBacking
        }
    }

    object Meta : DomElementMeta<TestDomContext, TestBinaryDomExpression> {

        override val type = TestBinaryDomExpression::class.java
        override val name = ""
        override val description = ""

        override fun create(context: TestDomContext): TestBinaryDomExpression {
            return TestBinaryDomExpression(
                    lhsBacking = TestElementDomProperty(context, TestDomExpression::class.java),
                    rhsBacking = TestElementDomProperty(context, TestDomExpression::class.java))
        }
    }

    object LhsMeta : DomPropertyMeta<TestDomContext, TestBinaryDomExpression, TestElementDomProperty<TestDomExpression>> {

        override val name = ""
        override val description = ""

        override fun getProperty(propertyContainer: TestBinaryDomExpression): TestElementDomProperty<TestDomExpression> {
            return propertyContainer.lhsBacking
        }
    }

    object RhsMeta : DomPropertyMeta<TestDomContext, TestBinaryDomExpression, TestElementDomProperty<TestDomExpression>> {

        override val name = ""
        override val description = ""

        override fun getProperty(propertyContainer: TestBinaryDomExpression): TestElementDomProperty<TestDomExpression> {
            return propertyContainer.rhsBacking
        }
    }
}
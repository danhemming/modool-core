package modool.parser.rule.expression.dom

import modool.core.TestDomContext
import modool.dom.meta.DomElementMeta
import modool.dom.meta.DomPropertyMeta
import modool.dom.node.ConcreteContentBuilder
import modool.dom.node.expression.DomExpression
import modool.dom.property.value.DomNameProperty

class TestVariableDomExpression constructor(
        val name: DomNameProperty<TestDomContext, String>
) : TestDomExpression(),
        DomExpression<TestDomContext, TestType> {

    override val type: TestType
        get() = TODO("not implemented") //To change initializer of created properties use File | Settings | File Templates.

    override fun initialiseConcreteContent(builder: ConcreteContentBuilder) {
        builder.then(name)
    }

    object Meta : DomElementMeta<TestDomContext, TestVariableDomExpression> {

        override val type = TestVariableDomExpression::class.java
        override val name = ""
        override val description = ""

        override fun create(context: TestDomContext): TestVariableDomExpression {
            return TestVariableDomExpression(name = TestNameDomProperty(context))
        }
    }

    object NameMeta : DomPropertyMeta<TestDomContext, TestVariableDomExpression, DomNameProperty<TestDomContext, String>> {

        override val name = ""
        override val description = ""

        override fun getProperty(propertyContainer: TestVariableDomExpression): DomNameProperty<TestDomContext, String> {
            return propertyContainer.name
        }
    }
}
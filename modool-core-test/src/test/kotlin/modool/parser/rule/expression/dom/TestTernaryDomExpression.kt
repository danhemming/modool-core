package modool.parser.rule.expression.dom

import modool.core.TestDomContext
import modool.dom.meta.DomElementMeta
import modool.dom.meta.DomPropertyMeta
import modool.dom.node.ConcreteContentBuilder
import modool.dom.node.expression.DomTernaryExpression

class TestTernaryDomExpression(
        val firstExpressionBacking: TestElementDomProperty<TestDomExpression>,
        val secondExpressionBacking: TestElementDomProperty<TestDomExpression>,
        val thirdExpressionBacking: TestElementDomProperty<TestDomExpression>
) : TestDomExpression(),
        DomTernaryExpression<TestDomContext, TestType, TestDomExpression> {

    override var first: TestDomExpression
        get() = firstExpressionBacking.get()!!
        set(value) {
            firstExpressionBacking.set(value)
        }

    override var second: TestDomExpression
        get() = secondExpressionBacking.get()!!
        set(value) {
            secondExpressionBacking.set(value)
        }

    override var third: TestDomExpression
        get() = thirdExpressionBacking.get()!!
        set(value) {
            thirdExpressionBacking.set(value)
        }

    override val type: TestType
        get() = TODO("not implemented") //To change initializer of created properties use File | Settings | File Templates.

    override fun initialiseAbstractContent() {
        adopt(firstExpressionBacking)
        adopt(secondExpressionBacking)
        adopt(thirdExpressionBacking)
    }

    override fun initialiseConcreteContent(builder: ConcreteContentBuilder) {
        builder.apply {
            +firstExpressionBacking
            +secondExpressionBacking
            +thirdExpressionBacking
        }
    }

    object Meta : DomElementMeta<TestDomContext, TestTernaryDomExpression> {

        override val type = TestTernaryDomExpression::class.java
        override val name = ""
        override val description = ""

        override fun create(context: TestDomContext): TestTernaryDomExpression {
            return TestTernaryDomExpression(
                    firstExpressionBacking = TestElementDomProperty(context, TestDomExpression::class.java),
                    secondExpressionBacking = TestElementDomProperty(context, TestDomExpression::class.java),
                    thirdExpressionBacking = TestElementDomProperty(context, TestDomExpression::class.java))
        }
    }

    object FirstExpressionMeta : DomPropertyMeta<TestDomContext, TestTernaryDomExpression, TestElementDomProperty<TestDomExpression>> {

        override val name = ""
        override val description = ""

        override fun getProperty(propertyContainer: TestTernaryDomExpression): TestElementDomProperty<TestDomExpression> {
            return propertyContainer.firstExpressionBacking
        }
    }

    object SecondExpressionMeta : DomPropertyMeta<TestDomContext, TestTernaryDomExpression, TestElementDomProperty<TestDomExpression>> {

        override val name = ""
        override val description = ""

        override fun getProperty(propertyContainer: TestTernaryDomExpression): TestElementDomProperty<TestDomExpression> {
            return propertyContainer.secondExpressionBacking
        }
    }

    object ThirdExpressionMeta : DomPropertyMeta<TestDomContext, TestTernaryDomExpression, TestElementDomProperty<TestDomExpression>> {

        override val name = ""
        override val description = ""


        override fun getProperty(propertyContainer: TestTernaryDomExpression): TestElementDomProperty<TestDomExpression> {
            return propertyContainer.thirdExpressionBacking
        }
    }
}
package modool.parser.rule.expression.dom

import modool.core.TestDomContext
import modool.dom.meta.DomElementMeta
import modool.dom.meta.DomPropertyMeta
import modool.dom.node.ConcreteContentBuilder

class TestFunctionCallDomExpression(
        val name: TestNameDomProperty,
        val arguments: TestFunctionCallArgumentListDomProperty
) : TestDomExpression() {

    override val type: TestType
        get() = TODO("not implemented") //To change initializer of created properties use File | Settings | File Templates.

    override fun initialiseConcreteContent(builder: ConcreteContentBuilder) {
        builder.apply {
            +name
            +arguments
        }
    }

    object Meta : DomElementMeta<TestDomContext, TestFunctionCallDomExpression> {

        override val type = TestFunctionCallDomExpression::class.java
        override val name = ""
        override val description = ""

        override fun create(context: TestDomContext): TestFunctionCallDomExpression {
            return TestFunctionCallDomExpression(
                    name = TestNameDomProperty(context),
                    arguments = TestFunctionCallArgumentListDomProperty(context))
        }
    }

    object NameMeta
        : DomPropertyMeta<TestDomContext, TestFunctionCallDomExpression, TestNameDomProperty> {

        override val name = ""
        override val description = ""

        override fun getProperty(propertyContainer: TestFunctionCallDomExpression): TestNameDomProperty {
            return propertyContainer.name
        }
    }

    object ArgumentsMeta
        : DomPropertyMeta<TestDomContext, TestFunctionCallDomExpression, TestFunctionCallArgumentListDomProperty> {

        override val name = ""
        override val description = ""

        override fun getProperty(propertyContainer: TestFunctionCallDomExpression): TestFunctionCallArgumentListDomProperty {
            return propertyContainer.arguments
        }
    }
}
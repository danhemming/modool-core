package modool.parser.rule.expression.dom

import modool.core.TestDomContext
import modool.core.meta.property.NoPropertyMeta
import modool.dom.property.value.DomValueProperty
import modool.dom.property.value.DomNameProperty

class TestNameDomProperty(
        context: TestDomContext
) : DomValueProperty<TestDomContext, String>(NoPropertyMeta, context),
        DomNameProperty<TestDomContext, String> {
    override fun setUnstyled(value: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun set(value: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun get(): String {
        return getMandatory()
    }
}
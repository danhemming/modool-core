package modool.parser.rule.expression.dom

import modool.core.TestDomContext
import modool.core.meta.property.NoPropertyMeta
import modool.dom.node.ConcreteContentBuilder
import modool.dom.property.sequence.element.list.DomElementBackedListProperty
import modool.lexer.rule.LexerRule
import modool.parser.rule.ParserRulable
import modool.dom.signifier.tag.terminal.DomSeparator

class TestFunctionCallArgumentListDomProperty(
        context: TestDomContext
) : DomElementBackedListProperty<TestDomContext, TestDomExpression>(NoPropertyMeta, context, TestDomExpression::class.java) {

    override fun createParserRule(): ParserRulable {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun createLexerRules(): List<LexerRule> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun initialiseConcreteOpen(builder: ConcreteContentBuilder) {
        builder.then(Terminals.OPEN_PAREN)
    }

    override fun initialiseConcreteSeparator(builder: ConcreteContentBuilder) {
        builder.then(Terminals.COMMA)
    }

    override fun initialiseConcreteClose(builder: ConcreteContentBuilder) {
        builder.then(Terminals.CLOSE_PAREN)
    }

    object Terminals {
        val OPEN_PAREN = DomSeparator.Symbol("(")
        val COMMA = DomSeparator.Symbol(",")
        val CLOSE_PAREN = DomSeparator.Symbol(")")
    }
}
package modool.parser.rule.expression.dom

import modool.core.TestDomContext
import modool.dom.meta.DomElementMeta
import modool.dom.node.DomElement
import modool.dom.node.expression.DomExpression

open class TestDomExpression
    : DomElement<TestDomContext>(Meta, TestDomContext),
        DomExpression<TestDomContext, TestType> {

    override val type: TestType
        get() = TODO("not implemented") //To change initializer of created properties use File | Settings | File Templates.

    object Meta : DomElementMeta<TestDomContext, TestDomExpression> {

        override val type = TestDomExpression::class.java
        override val name = ""
        override val description = ""

        override fun create(context: TestDomContext): TestDomExpression {
            return TestDomExpression()
        }
    }
}
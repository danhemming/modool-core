package modool.parser.rule.expression.dom

import modool.core.TestDomContext
import modool.dom.meta.DomElementMeta
import modool.dom.meta.DomPropertyMeta
import modool.dom.node.ConcreteContentBuilder
import modool.dom.node.expression.DomUnaryExpression

class TestUnaryPrefixDomExpression(
        val expressionBacking: TestElementDomProperty<TestDomExpression>
) : TestDomExpression(),
        DomUnaryExpression<TestDomContext, TestType, TestDomExpression> {

    override var expression: TestDomExpression
        get() = expressionBacking.get()!!
        set(value) {
            expressionBacking.set(value)
        }

    override val type: TestType
        get() = TODO("not implemented") //To change initializer of created properties use File | Settings | File Templates.

    override fun initialiseAbstractContent() {
        adopt(expressionBacking)
    }

    override fun initialiseConcreteContent(builder: ConcreteContentBuilder) {
        builder.apply {
            +expressionBacking
        }
    }

    object Meta : DomElementMeta<TestDomContext, TestUnaryPrefixDomExpression> {

        override val type = TestUnaryPrefixDomExpression::class.java
        override val name = ""
        override val description = ""

        override fun create(context: TestDomContext): TestUnaryPrefixDomExpression {
            return TestUnaryPrefixDomExpression(expressionBacking = TestElementDomProperty(context, TestDomExpression::class.java))
        }
    }

    object ExpressionMeta
        : DomPropertyMeta<TestDomContext, TestUnaryPrefixDomExpression, TestElementDomProperty<TestDomExpression>> {

        override val name = ""
        override val description = ""

        override fun getProperty(propertyContainer: TestUnaryPrefixDomExpression): TestElementDomProperty<TestDomExpression> {
            return propertyContainer.expressionBacking
        }
    }
}
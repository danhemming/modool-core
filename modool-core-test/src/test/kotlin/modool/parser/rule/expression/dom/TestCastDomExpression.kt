package modool.parser.rule.expression.dom

import modool.core.TestDomContext
import modool.dom.meta.DomElementMeta
import modool.dom.meta.DomPropertyMeta
import modool.dom.node.ConcreteContentBuilder
import modool.dom.property.value.DomNameProperty
import modool.dom.signifier.tag.terminal.DomSeparator

class TestCastDomExpression(
        val typeName: TestNameDomProperty,
        val expression: TestElementDomProperty<TestDomExpression>
) : TestDomExpression() {

    override val type: TestType
        get() = TODO("not implemented") //To change initializer of created properties use File | Settings | File Templates.

    override fun initialiseConcreteContent(builder: ConcreteContentBuilder) {
        builder.apply {
            +Terminals.OPEN_PAREN
            +typeName
            +Terminals.CLOSE_PAREN
            +expression
        }
    }

    object Meta : DomElementMeta<TestDomContext, TestCastDomExpression> {

        override val type = TestCastDomExpression::class.java
        override val name = ""
        override val description = ""

        override fun create(context: TestDomContext): TestCastDomExpression {
            return TestCastDomExpression(
                    typeName = TestNameDomProperty(context),
                    expression = TestElementDomProperty(context, TestDomExpression::class.java))
        }
    }

    object Terminals {
        val OPEN_PAREN = DomSeparator.Symbol("(")
        val CLOSE_PAREN = DomSeparator.Symbol(")")
    }

    object TypeNameMeta : DomPropertyMeta<TestDomContext, TestCastDomExpression, DomNameProperty<TestDomContext, String>> {

        override val name = ""
        override val description = ""

        override fun getProperty(propertyContainer: TestCastDomExpression): DomNameProperty<TestDomContext, String> {
            return propertyContainer.typeName
        }
    }

    object ExpressionMeta : DomPropertyMeta<TestDomContext, TestCastDomExpression, TestElementDomProperty<TestDomExpression>> {

        override val name = ""
        override val description = ""

        override fun getProperty(propertyContainer: TestCastDomExpression): TestElementDomProperty<TestDomExpression> {
            return propertyContainer.expression
        }
    }
}
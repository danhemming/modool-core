package modool.parser.rule.expression

import modool.core.TestDomContext
import modool.core.content.operator.OperatorAssociativity
import modool.lexer.Lexer
import modool.lexer.rule.builder.LexerRuleCollectionFactory
import modool.lexer.rule.unknown.CaptureUnknownTerminalLexerRule
import modool.parser.ParseResult
import modool.parser.TestNameTerminal
import modool.parser.rule.ConstantTerminalParserRule
import modool.parser.rule.ListParserRule
import modool.parser.rule.NameParserRule
import modool.parser.rule.builder.ExpressionParserRuleBuilder
import modool.parser.rule.builder.element
import modool.parser.rule.expression.dom.*
import modool.parser.rule.expression.operator.*
import modool.parser.rule.expression.operator.condition.ExpressionCondition
import modool.parser.rule.expression.operator.condition.ExpressionDisallowCondition
import modool.dom.lexer.DomLexerContext
import modool.dom.signifier.tag.terminal.DomSeparator
import modool.dom.token.DomTokenReaderDomProviderView

object TestExpressionParser {

    private val lexerBuilder = LexerRuleCollectionFactory.create {
        +unicodeWhitespace()
        +pattern(TestNameTerminal) {
            +atLeast(1, asciiLetterChar())
        }
    }

    private fun binaryRule(
            value: String,
            isOverlapped: Boolean = false,
            conditions: List<ExpressionCondition> = emptyList())
            : BinaryOperatorExpressionPrecedenceParserRule {
        val symbol = DomSeparator.Symbol(value)
        lexerBuilder.apply { +symbol }
        return BinaryOperatorExpressionPrecedenceParserRule(
                TestBinaryDomExpression.Meta,
                TestBinaryDomExpression.LhsMeta,
                TestBinaryDomExpression.RhsMeta,
                ConstantTerminalParserRule(symbol),
                isOverlapped,
                conditions)
    }

    private fun binaryRule(
            open: String,
            close: String,
            isOverlapped: Boolean = false,
            conditions: List<ExpressionCondition> = emptyList())
            : BinaryGroupOperatorExpressionPrecedenceParserRule {
        val openSymbol = DomSeparator.Symbol(open)
        val closeSymbol = DomSeparator.Symbol(close)
        lexerBuilder.apply {
            +openSymbol
            +closeSymbol
        }
        return BinaryGroupOperatorExpressionPrecedenceParserRule(
                TestBinaryGroupDomExpression.Meta,
                TestBinaryGroupDomExpression.LhsMeta,
                TestBinaryGroupDomExpression.RhsMeta,
                ConstantTerminalParserRule(openSymbol),
                ConstantTerminalParserRule(closeSymbol),
                isOverlapped,
                conditions)
    }

    private fun ternaryRule(
            first: String,
            second: String,
            isOverlapped: Boolean = false,
            conditions: List<ExpressionCondition> = emptyList())
            : TernaryOperatorExpressionPrecedenceParserRule {
        val firstSymbol = DomSeparator.Symbol(first)
        val secondSymbol = DomSeparator.Symbol(second)
        lexerBuilder.apply {
            +firstSymbol
            +secondSymbol
        }
        return TernaryOperatorExpressionPrecedenceParserRule(
                TestTernaryDomExpression.Meta,
                TestTernaryDomExpression.FirstExpressionMeta,
                TestTernaryDomExpression.SecondExpressionMeta,
                TestTernaryDomExpression.ThirdExpressionMeta,
                ConstantTerminalParserRule(firstSymbol),
                ConstantTerminalParserRule(secondSymbol),
                isOverlapped,
                conditions)
    }

    private fun groupRule(
            open: String,
            close: String,
            isOverlapped: Boolean = false,
            conditions: List<ExpressionCondition> = emptyList())
            : ExpressionGroupOperatorPrecedenceParserRule {
        val openSymbol = DomSeparator.Symbol(open)
        val closeSymbol = DomSeparator.Symbol(close)
        lexerBuilder.apply {
            +openSymbol
            +closeSymbol
        }
        return ExpressionGroupOperatorPrecedenceParserRule(
                TestDomExpressionGroup.Meta,
                TestDomExpressionGroup.ExpressionMeta,
                ConstantTerminalParserRule(openSymbol),
                ConstantTerminalParserRule(closeSymbol),
                isOverlapped,
                conditions)
    }

    private fun unaryPrefixRule(
            value: String,
            isOverlapped: Boolean = false,
            conditions: List<ExpressionCondition> = emptyList())
            : UnaryPrefixOperatorExpressionPrecedenceParserRule {
        val symbol = DomSeparator.Symbol(value)
        lexerBuilder.apply { +symbol }
        return UnaryPrefixOperatorExpressionPrecedenceParserRule(
                TestUnaryPrefixDomExpression.Meta,
                TestUnaryPrefixDomExpression.ExpressionMeta,
                ConstantTerminalParserRule(symbol),
                isOverlapped,
                conditions)
    }

    private fun unaryPostfixRule(
            value: String,
            isOverlapped: Boolean = false,
            conditions: List<ExpressionCondition> = emptyList())
            : UnaryPostfixOperatorExpressionPrecedenceParserRule {
        val symbol = DomSeparator.Symbol(value)
        lexerBuilder.apply { +symbol }
        return UnaryPostfixOperatorExpressionPrecedenceParserRule(
                TestUnaryPostfixDomExpression.Meta,
                TestUnaryPostfixDomExpression.ExpressionMeta,
                ConstantTerminalParserRule(symbol),
                isOverlapped,
                conditions)
    }

    fun tokeniseAndParse(content: String): ParseResult {
        createExpressionParserRule() // ensure the rules are lazy evaluated
        val tokenReader = Lexer.from(
                CaptureUnknownTerminalLexerRule(),
                lexerBuilder.toList())
                .tokenise(DomLexerContext.from(content))
        val parseResult = TestDomContext.parse(createExpressionParserRule(), tokenReader)
        val domTokenReader = tokenReader.createProviderView(DomTokenReaderDomProviderView::class.java)
        return if (parseResult is ParseResult.Success) {
            ParseResult.CompleteContent(domTokenReader.startToken)
        } else {
            parseResult
        }
    }

    private val variableParserRule by lazy {
        element(TestVariableDomExpression.Meta) {
            +NameParserRule(TestNameTerminal).sets(TestVariableDomExpression.NameMeta)
        }
    }

    private val castParserRule by lazy {
        element(TestCastDomExpression.Meta) {
            +ConstantTerminalParserRule(TestCastDomExpression.Terminals.OPEN_PAREN)
            +NameParserRule(TestNameTerminal).sets(TestCastDomExpression.TypeNameMeta)
            +ConstantTerminalParserRule(TestCastDomExpression.Terminals.CLOSE_PAREN)
            +variableParserRule.sets(TestCastDomExpression.ExpressionMeta)
        }
    }

    private val functionCallParserRule by lazy {
        element(TestFunctionCallDomExpression.Meta) {
            +NameParserRule(TestNameTerminal).sets(TestFunctionCallDomExpression.NameMeta)
            +ListParserRule(
                    open = ConstantTerminalParserRule(TestFunctionCallArgumentListDomProperty.Terminals.OPEN_PAREN),
                    item = createExpressionParserRule(),
                    separator = ConstantTerminalParserRule(TestFunctionCallArgumentListDomProperty.Terminals.COMMA),
                    close = ConstantTerminalParserRule(TestFunctionCallArgumentListDomProperty.Terminals.CLOSE_PAREN)).sets(
                    TestFunctionCallDomExpression.ArgumentsMeta)
        }
    }

    private val expressionParserRule by lazy {
        val expressionBuilder = ExpressionParserRuleBuilder(TestDomExpression.Meta)
        expressionBuilder.apply {

            precedence {

                OperatorAssociativity.LEFT_TO_RIGHT {
                    +groupRule("(", ")", isOverlapped = true)
                }

                OperatorAssociativity.LEFT_TO_RIGHT {
                    +ExpressionOperablePrecedenceParserRule(TestFunctionCallDomExpression.Meta, functionCallParserRule)
                }

                OperatorAssociativity.NONE {
                    +unaryPostfixRule("++")
                    +unaryPostfixRule("--")
                }

                OperatorAssociativity.RIGHT_TO_LEFT {
                    +binaryRule("[", "]")
                    +unaryPrefixRule("+")
                    +unaryPrefixRule("-")
                    +unaryPrefixRule("++")
                    +unaryPrefixRule("--")
                }

                OperatorAssociativity.LEFT_TO_RIGHT {
                    +binaryRule("*")
                    +binaryRule("/")
                }

                OperatorAssociativity.LEFT_TO_RIGHT {
                    +binaryRule("+")
                    +binaryRule("-")
                }

                OperatorAssociativity.NONE {
                    +binaryRule(">=")
                    +binaryRule("<=")
                }

                OperatorAssociativity.RIGHT_TO_LEFT {
                    +binaryRule("?:")
                }

                OperatorAssociativity.LEFT_TO_RIGHT {
                    +binaryRule("==")
                    +binaryRule("!=")
                }

                OperatorAssociativity.RIGHT_TO_LEFT {
                    +binaryRule("=")
                    +ternaryRule("?", ":")
                }

                OperatorAssociativity.RIGHT_TO_LEFT {
                    // This is here to represent the sequence operator in Javascript and WILL clash with the function call
                    +binaryRule(",",
                            false,
                            listOf(ExpressionDisallowCondition(TestDomExpression.Meta, TestFunctionCallDomExpression.Meta)))
                }

                OperatorAssociativity.LEFT_TO_RIGHT {
                    +ExpressionOperablePrecedenceParserRule(TestCastDomExpression.Meta, castParserRule)
                    +ExpressionOperablePrecedenceParserRule(TestVariableDomExpression.Meta, variableParserRule)
                }
            }

            terminator {
                +ConstantTerminalParserRule(DomSeparator.Symbol(";"))
            }
        }

        expressionBuilder.createExpressionParserRule()
    }

    private fun createExpressionParserRule(): ExpressionParserRule = expressionParserRule
}
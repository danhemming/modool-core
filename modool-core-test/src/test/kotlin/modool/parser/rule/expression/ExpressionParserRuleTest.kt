package modool.parser.rule.expression

import modool.core.TestDomContext
import modool.dom.node.DomElement
import modool.parser.rule.expression.dom.*
import modool.test.parser.assertElementSuccessful
import modool.test.parser.assertFailure
import modool.test.parser.assertNoEffect
import org.amshove.kluent.`should be instance of`
import org.amshove.kluent.`should equal`
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it

class TestExpressionParserTest : Spek({

    describe("expression parsing") {

        it("should NOT match empty content") {
            TestExpressionParser.tokeniseAndParse("").assertFailure()
        }

        it("should match an empty expression") {
            // Think of a c style for loop for(;;)
            TestExpressionParser.tokeniseAndParse(";").assertNoEffect()
        }

        it("should match a single part simple expression") {
            val result = TestExpressionParser.tokeniseAndParse("a").assertElementSuccessful<DomElement<TestDomContext>>()
            result.toFormattedString() `should equal` "a"
        }

        it("should match a terminated expression") {
            val result = TestExpressionParser.tokeniseAndParse("a; +b").assertElementSuccessful<DomElement<TestDomContext>>()
            result.toFormattedString() `should equal` "a"
            result `should be instance of` TestVariableDomExpression::class.java
        }

        it("should match a simple expression") {
            val result = TestExpressionParser.tokeniseAndParse("a + b").assertElementSuccessful<DomElement<TestDomContext>>()
            result.toFormattedString() `should equal` "a + b"
            val bexpr = result as TestBinaryDomExpression
            bexpr.lhsBacking.get()!!.toFormattedString().trim() `should equal` "a"
            bexpr.rhsBacking.get()!!.toFormattedString().trim() `should equal` "b"
        }

        it("should match a multi-part simple expression") {
            val result = TestExpressionParser.tokeniseAndParse("a + b * c").assertElementSuccessful<DomElement<TestDomContext>>()
            result.toFormattedString() `should equal` "a + b * c"
            val bexpr = result as TestBinaryDomExpression
            bexpr.lhsBacking.get()!!.toFormattedString().trim() `should equal` "a"
            bexpr.rhsBacking.get()!!.toFormattedString().trim() `should equal` "b * c"
        }

        it("should match a grouped expression where the sequence overrides associativity") {
            val result = TestExpressionParser.tokeniseAndParse(" (a + b) * c").assertElementSuccessful<DomElement<TestDomContext>>()
            // TODO seems like expression rule is parsing past ) when doing a grouped expression, ending up at c
            result.toFormattedString() `should equal` " (a + b) * c"
            val bexpr = result as TestBinaryDomExpression
            bexpr.lhsBacking.get()!!.toFormattedString().trim() `should equal` "(a + b)"
            bexpr.rhsBacking.get()!!.toFormattedString().trim() `should equal` "c"
        }

        it("should not match a wrong expression") {
            TestExpressionParser.tokeniseAndParse("a ++ b").assertFailure()
        }

        it("should not match a non-associative operators used sequentially") {
            TestExpressionParser.tokeniseAndParse("a <= b >= c").assertFailure()
        }

        it("should match multiple non-associative operators with an associative operator separating them") {
            val result = TestExpressionParser.tokeniseAndParse("a <= b == b >= c").assertElementSuccessful<DomElement<TestDomContext>>()
            result.toFormattedString() `should equal` "a <= b == b >= c"
            val bexpr = result as TestBinaryDomExpression
            bexpr.lhsBacking.get()!!.toFormattedString().trim() `should equal` "a <= b"
            bexpr.rhsBacking.get()!!.toFormattedString().trim() `should equal` "b >= c"
        }

        it("should distinguish between unary and binary operators") {
            val result = TestExpressionParser.tokeniseAndParse("-a + - + - b").assertElementSuccessful<DomElement<TestDomContext>>()
            result.toFormattedString() `should equal` "-a + - + - b"
            val bexpr = result as TestBinaryDomExpression
            bexpr.lhsBacking.get()!!.toFormattedString().trim() `should equal` "-a"
            bexpr.rhsBacking.get()!!.toFormattedString().trim() `should equal` "- + - b"
        }

        it("should handle postfix and prefix operators") {
            val result = TestExpressionParser.tokeniseAndParse("a ++ == --b").assertElementSuccessful<DomElement<TestDomContext>>()
            result.toFormattedString() `should equal` "a ++ == --b"
            val bexpr = result as TestBinaryDomExpression
            bexpr.lhsBacking.get()!!.toFormattedString().trim() `should equal` "a ++"
            bexpr.rhsBacking.get()!!.toFormattedString().trim() `should equal` "--b"
            bexpr.lhsBacking.get()!! `should be instance of` TestUnaryPostfixDomExpression::class.java
            bexpr.rhsBacking.get()!! `should be instance of` TestUnaryPrefixDomExpression::class.java
        }

        it("should handle a mix of LTR and RTL") {
            val result = TestExpressionParser.tokeniseAndParse("b ?: c + a").assertElementSuccessful<DomElement<TestDomContext>>()
            result.toFormattedString() `should equal` "b ?: c + a"
            val bexpr = result as TestBinaryDomExpression
            bexpr.lhsBacking.get()!!.toFormattedString().trim() `should equal` "b"
            bexpr.rhsBacking.get()!!.toFormattedString().trim() `should equal` "c + a"
            bexpr.lhsBacking.get()!! `should be instance of` TestVariableDomExpression::class.java
            bexpr.rhsBacking.get()!! `should be instance of` TestBinaryDomExpression::class.java
        }

        it("should handle ternary expressions") {
            val result = TestExpressionParser.tokeniseAndParse("b ? c + d : a").assertElementSuccessful<DomElement<TestDomContext>>()
            result.toFormattedString() `should equal` "b ? c + d : a"
            val texpr = result as TestTernaryDomExpression
            texpr.firstExpressionBacking.get()!!.toFormattedString().trim() `should equal` "b"
            texpr.secondExpressionBacking.get()!!.toFormattedString().trim() `should equal` "c + d"
            texpr.thirdExpressionBacking.get()!!.toFormattedString().trim() `should equal` "a"
            texpr.firstExpressionBacking.get()!! `should be instance of` TestVariableDomExpression::class.java
            texpr.secondExpressionBacking.get()!! `should be instance of` TestBinaryDomExpression::class.java
            texpr.thirdExpressionBacking.get()!! `should be instance of` TestVariableDomExpression::class.java
        }

        it("should handle a long sequence of operators") {
            val result = TestExpressionParser.tokeniseAndParse("a + b / c * e + f").assertElementSuccessful<DomElement<TestDomContext>>()
            // ((a + ((b / c) * e)) + f)"
            result.toFormattedString() `should equal` "a + b / c * e + f"
            val bexpr = result as TestBinaryDomExpression
            bexpr.lhsBacking.get()!!.toFormattedString().trim() `should equal` "a + b / c * e"
            bexpr.rhsBacking.get()!!.toFormattedString().trim() `should equal` "f"
            bexpr.lhsBacking.get()!! `should be instance of` TestBinaryDomExpression::class.java
            bexpr.rhsBacking.get()!! `should be instance of` TestVariableDomExpression::class.java
        }

        it("should handle a mix of prefix and postfix operators") {
            val result = TestExpressionParser.tokeniseAndParse("+a + b++").assertElementSuccessful<DomElement<TestDomContext>>()
            result.toFormattedString() `should equal` "+a + b++"
            val bexpr = result as TestBinaryDomExpression
            bexpr.lhsBacking.get()!!.toFormattedString().trim() `should equal` "+a"
            bexpr.rhsBacking.get()!!.toFormattedString().trim() `should equal` "b++"
            bexpr.lhsBacking.get()!! `should be instance of` TestUnaryPrefixDomExpression::class.java
            bexpr.rhsBacking.get()!! `should be instance of` TestUnaryPostfixDomExpression::class.java
        }

        it("should handle simple binary group expressions") {
            val result = TestExpressionParser.tokeniseAndParse("a[b]").assertElementSuccessful<DomElement<TestDomContext>>()
            result.toFormattedString() `should equal` "a[b]"
            val bexpr = result as TestBinaryGroupDomExpression
            bexpr.lhsBacking.get()!!.toFormattedString().trim() `should equal` "a"
            bexpr.rhsBacking.get()!!.toFormattedString().trim() `should equal` "b"
            bexpr.lhsBacking.get()!! `should be instance of` TestVariableDomExpression::class.java
            bexpr.rhsBacking.get()!! `should be instance of` TestVariableDomExpression::class.java
        }

        it("should handle complex binary group expressions") {
            val result = TestExpressionParser.tokeniseAndParse("a[b + c]").assertElementSuccessful<DomElement<TestDomContext>>()
            result.toFormattedString() `should equal` "a[b + c]"
            val bexpr = result as TestBinaryGroupDomExpression
            bexpr.lhsBacking.get()!!.toFormattedString().trim() `should equal` "a"
            bexpr.rhsBacking.get()!!.toFormattedString().trim() `should equal` "b + c"
            bexpr.lhsBacking.get()!! `should be instance of` TestVariableDomExpression::class.java
            bexpr.rhsBacking.get()!! `should be instance of` TestBinaryDomExpression::class.java
        }

        it("should handle multiple binary group expressions") {
            val result = TestExpressionParser.tokeniseAndParse("(a + b) + (c + d)").assertElementSuccessful<DomElement<TestDomContext>>()
            result.toFormattedString() `should equal` "(a + b) + (c + d)"
            val bexpr = result as TestBinaryDomExpression
            bexpr.lhsBacking.get()!!.toFormattedString().trim() `should equal` "(a + b)"
            bexpr.rhsBacking.get()!!.toFormattedString().trim() `should equal` "(c + d)"
            bexpr.lhsBacking.get()!! `should be instance of` TestDomExpressionGroup::class.java
            bexpr.rhsBacking.get()!! `should be instance of` TestDomExpressionGroup::class.java
        }

        it("should handle expressions that reuse operator symbols e.g. () group and cast") {
            val result = TestExpressionParser.tokeniseAndParse("(type)a").assertElementSuccessful<DomElement<TestDomContext>>()
            result.toFormattedString() `should equal` "(type)a"
            val cast = result as TestCastDomExpression
            cast.typeName.get() `should equal` "type"
        }

        it("should handle complex expressions that reuse operator symbols") {
            val result = TestExpressionParser.tokeniseAndParse("a + (type)b").assertElementSuccessful<DomElement<TestDomContext>>()
            result.toFormattedString() `should equal` "a + (type)b"
            val bexpr = result as TestBinaryDomExpression
            bexpr.lhsBacking.get()!!.toFormattedString().trim() `should equal` "a"
            bexpr.rhsBacking.get()!!.toFormattedString().trim() `should equal` "(type)b"
            bexpr.lhsBacking.get()!! `should be instance of` TestVariableDomExpression::class.java
            bexpr.rhsBacking.get()!! `should be instance of` TestCastDomExpression::class.java
        }

        it("should handle function call where a lower precedence sequence operator clashes with the comma arg separator") {
            val result = TestExpressionParser.tokeniseAndParse("fc(a, b)").assertElementSuccessful<DomElement<TestDomContext>>()
            result.toFormattedString() `should equal` "fc(a, b)"
            val functionCall = result as TestFunctionCallDomExpression
            functionCall.name.get() `should equal` "fc"
            functionCall.arguments.count() `should equal` 2
        }

        it("should handle sequence binary expression and not be confused with function arguments") {
            val result = TestExpressionParser.tokeniseAndParse("a, b").assertElementSuccessful<DomElement<TestDomContext>>()
            result.toFormattedString() `should equal` "a, b"
            val bexpr = result as TestBinaryDomExpression
            bexpr.lhsBacking.get()!!.toFormattedString().trim() `should equal` "a"
            bexpr.rhsBacking.get()!!.toFormattedString().trim() `should equal` "b"
            bexpr.lhsBacking.get()!! `should be instance of` TestVariableDomExpression::class.java
            bexpr.rhsBacking.get()!! `should be instance of` TestVariableDomExpression::class.java
        }

        it("should handle function call with a single sequence parameter") {
            val result = TestExpressionParser.tokeniseAndParse("fc((a, b))").assertElementSuccessful<DomElement<TestDomContext>>()
            result.toFormattedString() `should equal` "fc((a, b))"
            val functionCall = result as TestFunctionCallDomExpression
            functionCall.name.get() `should equal` "fc"
            functionCall.arguments.count() `should equal` 1
        }
    }
})
package modool.parser.rule.expression.dom

import modool.dom.type.ModoolPrimitiveType
import modool.dom.identity.Identifier

class TestType(override val identifier: Identifier, override val name: String) : ModoolPrimitiveType("Test")
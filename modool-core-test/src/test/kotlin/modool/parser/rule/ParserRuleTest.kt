package modool.parser.rule

import modool.core.TestDomContext
import modool.core.config.SemanticIndentationStrategy
import modool.core.content.signifier.tag.terminal.Contextual
import modool.core.content.signifier.tag.terminal.Meta
import modool.lexer.Lexer
import modool.lexer.rule.unknown.CaptureUnknownTerminalLexerRule
import modool.parser.ParseResult
import modool.parser.TestNameTerminal
import modool.parser.rule.property.ElementPropertySetterTerminalValueRule
import modool.parser.strategy.StrictParseStrategy
import modool.dom.identity.Identifier
import modool.dom.identity.NamePropertyIdentifier
import modool.dom.identity.caseSensitiveIdentityEquals
import modool.dom.lexer.DomLexerContext
import modool.dom.meta.DomElementMeta
import modool.dom.meta.DomPropertyMeta
import modool.dom.name.MandatoryRenameable
import modool.dom.node.IdentifiableDomElement
import modool.dom.property.nameProperty
import modool.dom.property.value.DomNameProperty
import modool.dom.signifier.tag.terminal.DomKeyword
import modool.dom.signifier.tag.terminal.DomSeparator
import modool.dom.token.DomTokenReaderDomProviderView
import modool.test.parser.assertElementSuccessful
import modool.test.parser.assertFailure
import modool.test.parser.assertNoEffect
import modool.test.parser.assertSuccessful
import org.amshove.kluent.`should equal`
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it

@Suppress("UNCHECKED_CAST")
class ParserRuleTest : Spek({

    describe("keyword") {

        val myKeyword = DomKeyword.Hard("parse")
        val myKeywordRule = ConstantTerminalParserRule(myKeyword)
        val lexer = Lexer.from(CaptureUnknownTerminalLexerRule()) {
            +unicodeWhitespace()
            +myKeyword
        }

        fun tokeniseAndParse(content: String): ParseResult {
            return TestDomContext.parse(myKeywordRule, lexer.tokenise(DomLexerContext.from(content)))
        }

        it("should parse when the single is the same") {
            tokeniseAndParse("parse").assertSuccessful()
        }

        it("should parse when the single is the same and offset from the start") {
            tokeniseAndParse(" parse ").assertSuccessful()
        }

        it("should NOT parse when the single is different") {
            tokeniseAndParse("not").assertFailure()
        }

        it("should parse when the single is the same followed by a breaking character") {
            tokeniseAndParse("parse()").assertSuccessful()
        }

        it("should NOT parse when the keyword has no breaking character following") {
            tokeniseAndParse("matchmatch").assertFailure()
        }
    }

    describe("whitespace") {

        describe("mandatory whitespace") {

            val lexer = Lexer.from(CaptureUnknownTerminalLexerRule()) {
                +unicodeWhitespace()
            }

            fun ParserRulable.tokeniseAndParse(content: String): ParseResult {
                return TestDomContext.parse(this, lexer.tokenise(DomLexerContext.from(content)))
            }

            it("should parse a single space") {
                WhitespaceParserRule.tokeniseAndParse(" ").assertSuccessful()
            }

            it("should parse a single tab") {
                WhitespaceParserRule.tokeniseAndParse("\t").assertSuccessful()
            }

            it("should parse a single newline") {
                WhitespaceParserRule.tokeniseAndParse("\n").assertSuccessful()
            }

            it("should NOT parse non white space") {
                WhitespaceParserRule.tokeniseAndParse("hello").assertFailure()
            }

            it("should parse mixed white space") {
                WhitespaceParserRule.tokeniseAndParse(" \n\t hello").assertSuccessful()
            }
        }
    }

    describe("optional") {

        val testKeyword = DomKeyword.Hard("test")
        val optRule = OptionallyParserRule(ConstantTerminalParserRule(testKeyword))
        val lexer = Lexer.from(CaptureUnknownTerminalLexerRule()) {
            +unicodeWhitespace()
            +testKeyword
        }

        fun tokeniseAndParseAndAssertMoreContentIsAvailable(content: String, moreContent: Boolean): ParseResult {
            val tokenReader = lexer.tokenise(DomLexerContext.from(content))
            val result = TestDomContext.parse(optRule, tokenReader)
            tokenReader.isMoreContentAvailable() `should equal` moreContent
            return result
        }

        it("should parse failure without error") {
            tokeniseAndParseAndAssertMoreContentIsAvailable("fail", true).assertNoEffect()
        }

        it("should parse success") {
            tokeniseAndParseAndAssertMoreContentIsAvailable("test", false).assertSuccessful()
        }
    }

    describe("choice") {

        val test1Keyword = DomKeyword.Hard("test1")
        val test2Keyword = DomKeyword.Hard("test2")
        val choiceRule = PickOneParserRule(ConstantTerminalParserRule(test1Keyword), ConstantTerminalParserRule(test2Keyword))
        val lexer = Lexer.from(CaptureUnknownTerminalLexerRule()) {
            +unicodeWhitespace()
            +test1Keyword
            +test2Keyword
        }

        fun tokeniseAndParseAndAssertMoreContentIsAvailable(content: String, moreContent: Boolean): ParseResult {
            val tokenReader = lexer.tokenise(DomLexerContext.from(content))
            val result = TestDomContext.parse(choiceRule, tokenReader)
            tokenReader.isMoreContentAvailable() `should equal` moreContent
            return result
        }

        it("should parse when one pattern matches") {
            tokeniseAndParseAndAssertMoreContentIsAvailable("test2", false).assertSuccessful()
        }

        it("should fail when no patterns parse") {
            tokeniseAndParseAndAssertMoreContentIsAvailable("test3", true).assertFailure()
        }
    }

    describe("composition") {

        val kw1 = DomKeyword.Hard("kw1")
        val kw2 = DomKeyword.Hard("kw2")
        val terminator = DomSeparator.Symbol(";")

        val lexer = Lexer.from(CaptureUnknownTerminalLexerRule()) {
            +unicodeWhitespace()
            +kw1
            +kw2
            +terminator
            +terminal("/", Meta.LineContinuation)
            +pattern(TestNameTerminal) {
                +choose(char('_'), asciiLetterChar())
                +unlimited(asciiLetterOrDigitChar())
            }
        }

        fun ParserRulable.tokeniseAndParseAndAssertMoreContentIsAvailable(
                content: String,
                moreContent: Boolean): ParseResult {
            val tokenReader = lexer.tokenise(DomLexerContext.from(content))
            val result = TestDomContext.parse(this, tokenReader)
            tokenReader.isMoreContentAvailable() `should equal` moreContent
            val domTokenReader = tokenReader.createProviderView(DomTokenReaderDomProviderView::class.java)
            return if (result is ParseResult.Success) {
                ParseResult.CompleteContent(domTokenReader.startToken)
            } else {
                result
            }
        }

        describe("terminator") {

            val compRule = ElementParserRule(TestObject.Meta) {
                listOf(ConstantTerminalParserRule(kw1),
                        OptionalLeafNodeParserRule(TerminatorParserRule(ConstantTerminalParserRule(terminator))),
                        ConstantTerminalParserRule(kw2))
            }

            it("should terminate early when an optional terminator is encountered") {
                compRule.tokeniseAndParseAndAssertMoreContentIsAvailable("kw1; kw2", true).assertSuccessful()
            }

            it("should terminate normally when an optional terminator is NOT encountered") {
                compRule.tokeniseAndParseAndAssertMoreContentIsAvailable("kw1 kw2", false).assertSuccessful()
            }
        }

        describe("line continuation") {

            val compRule = ElementParserRule(TestObject.Meta) {
                listOf(ConstantTerminalParserRule(kw1),
                        OptionalLeafNodeParserRule(TerminatorParserRule(LineTransitionParserRule)),
                        ConstantTerminalParserRule(kw2))
            }

            it("should match where a newline separates the keywords but only parse kw1") {
                compRule.tokeniseAndParseAndAssertMoreContentIsAvailable("kw1\nkw2", true).assertSuccessful()
            }

            it("should parse where there is no new line") {
                compRule.tokeniseAndParseAndAssertMoreContentIsAvailable("kw1 kw2", false).assertSuccessful()
            }

            it("should match the whole thing where the new line is escaped") {
                compRule.tokeniseAndParseAndAssertMoreContentIsAvailable("kw1 /\n kw2", false).assertSuccessful()
            }
        }

        describe("Two keyword composition") {

            val compRule =
                    ElementParserRule(TestObject.Meta) { listOf(ConstantTerminalParserRule(kw1), ConstantTerminalParserRule(kw2)) }

            it("should parse exactly when each component is matched") {
                compRule.tokeniseAndParseAndAssertMoreContentIsAvailable("kw1 kw2", false).assertSuccessful()
            }

            it("should not parse when only one component is matched") {
                compRule.tokeniseAndParseAndAssertMoreContentIsAvailable("kw1 not kw2", true).assertFailure()
            }
        }

        describe("Keyword identifier composition") {

            val compRule =
                    ElementParserRule(TestObject.Meta) {
                        listOf(ConstantTerminalParserRule(kw1),
                                ElementPropertySetterTerminalValueRule(NameParserRule(TestNameTerminal), TestObject.NameMeta))
                    }

            it("should call a binding function on parse") {
                val match = compRule.tokeniseAndParseAndAssertMoreContentIsAvailable("kw1 myId", false)
                val element = match.assertElementSuccessful<TestObject>()
                element.nameBacking.get() `should equal` "myId"
            }
        }
    }

    describe("Indented grouping") {

        val kw1 = DomKeyword.Hard("kw1")
        val kw2 = DomKeyword.Hard("kw2")

        fun createLexer() = Lexer.from(CaptureUnknownTerminalLexerRule()) {
            +unicodeWhitespace()
            +kw1
            +kw2
            +terminal(":")
            +terminal("=", Contextual.EscapeSemanticIndentation)
            +pattern(TestNameTerminal) {
                +choose(char('_'), asciiLetterChar())
                +unlimited(asciiLetterOrDigitChar())
            }
        }

        fun ParserRulable.tokeniseAndParseAndAssertMoreContentIsAvailable(content: String, moreContent: Boolean): ParseResult {
            val tokenReader = createLexer().tokenise(DomLexerContext.from(content))
            val result = TestDomContext.parse(this, tokenReader, StrictParseStrategy)
            tokenReader.isMoreContentAvailable() `should equal` moreContent
            val domTokenReader = tokenReader.createProviderView(DomTokenReaderDomProviderView::class.java)
            return if (result is ParseResult.Success) {
                ParseResult.CompleteContent(domTokenReader.startToken)
            } else {
                result
            }
        }

        val blockIndent =
                ElementParserRule(TestObject.Meta) {
                    listOf(
                            ConstantTerminalParserRule(kw1),
                            ConstantTerminalParserRule(DomSeparator.Symbol(":")),
                            SemanticallyIndentedUnorderedParserRule(
                                    SemanticIndentationStrategy.Exactly(size = 4, tabCharacterCount = 4),
                                    Exactly(ConstantTerminalParserRule(kw2), count = 2)))
                }

        val blockOrInlineIndent =
                ElementParserRule(TestObject.Meta) {
                    listOf(
                            ConstantTerminalParserRule(kw1),
                            PickOneParserRule(
                                    ConstantTerminalParserRule(DomSeparator.Symbol(":")),
                                    ConstantTerminalParserRule(DomSeparator.Symbol("="))),
                            // Either indented or whitespace separated inline
                            SemanticallyIndentedListParserRule(
                                    SemanticIndentationStrategy.AtLeast(size = 1, tabCharacterCount = 4),
                                    ConstantTerminalParserRule(kw2)))
                }

        it("should parse exactly when indented content is provided") {
            val testContent = """
                kw1 :
                    kw2
                    kw2
            """.trimIndent()
            blockIndent.tokeniseAndParseAndAssertMoreContentIsAvailable(testContent, false).assertSuccessful()
        }

        it("should fail when immediate content provided is not of sufficient indent") {
            val testContent = """
                kw1 :
                 kw2
                    kw2
            """.trimIndent()
            blockIndent.tokeniseAndParseAndAssertMoreContentIsAvailable(testContent, true).assertFailure()
        }

        it("should fail when some following content provided is not of sufficient indent") {
            val testContent = """
                kw1 :
                    kw2
                 kw2
            """.trimIndent()
            blockIndent.tokeniseAndParseAndAssertMoreContentIsAvailable(testContent, true).assertFailure()
        }

        it("should fail when inline content is provided but not supported") {
            blockIndent.tokeniseAndParseAndAssertMoreContentIsAvailable("kw1 : kw2 kw2", true).assertFailure()
        }

        it("should parse exactly when inline content is provided and supported") {
            blockOrInlineIndent.tokeniseAndParseAndAssertMoreContentIsAvailable("kw1 = kw2", false).assertSuccessful()
        }

        it("should parse exactly when indented content is provided and also inline content supported but not used") {
            val testContent = """
                kw1 :
                    kw2
                    kw2
            """.trimIndent()
            blockOrInlineIndent.tokeniseAndParseAndAssertMoreContentIsAvailable(testContent, false).assertSuccessful()
        }

        it("should parse where we have a dynamic but mismatching indent. However, trailing kw2 is not parsed") {
            val testContent = """
                kw1 :
                 kw2
                  kw2
            """.trimIndent()
            blockOrInlineIndent.tokeniseAndParseAndAssertMoreContentIsAvailable(testContent, true).assertSuccessful()
        }
    }

    describe("list") {

        val kw = DomKeyword.Hard("kw")

        val lexer = Lexer.from(CaptureUnknownTerminalLexerRule()) {
            +unicodeWhitespace()
            +kw
            +DomSeparator.Symbol(",")
            +DomSeparator.Symbol("(")
            +DomSeparator.Symbol(")")
            +pattern(TestNameTerminal) {
                +choose(char('_'), asciiLetterChar())
                +unlimited(asciiLetterOrDigitChar())
            }
        }

        fun ParserRulable.tokeniseAndParseAndAssertMoreContentIsAvailable(content: String, moreContent: Boolean): ParseResult {
            val tokenReader = lexer.tokenise(DomLexerContext.from(content))
            val result = TestDomContext.parse(this, tokenReader, StrictParseStrategy)
            tokenReader.isMoreContentAvailable() `should equal` moreContent
            val domTokenReader = tokenReader.createProviderView(DomTokenReaderDomProviderView::class.java)
            return if (result is ParseResult.Success) {
                ParseResult.CompleteContent(domTokenReader.startToken)
            } else {
                result
            }
        }

        describe("Space separated list") {

            val whiteSpaceSeparatedListRule = RepeaterParserRule(
                    ElementParserRule(TestObject.Meta) {
                        listOf(
                                ConstantTerminalParserRule(kw),
                                ElementPropertySetterTerminalValueRule(NameParserRule(TestNameTerminal), TestObject.NameMeta))
                    })

            it("should parse multiple items") {
                whiteSpaceSeparatedListRule.tokeniseAndParseAndAssertMoreContentIsAvailable("kw id1 kw id2 kw id3", false)
                        .assertSuccessful()
            }
        }

        describe("comma separated") {


            it("should parse multiple items separated with a comma") {
                val commaSeparatedListRule = ListParserRule(
                        separator = ConstantTerminalParserRule(DomSeparator.Symbol(",")),
                        item = ElementParserRule(TestObject.Meta) {
                            listOf(
                                    ConstantTerminalParserRule(kw),
                                    ElementPropertySetterTerminalValueRule(NameParserRule(TestNameTerminal), TestObject.NameMeta))
                        })

                val result = commaSeparatedListRule.tokeniseAndParseAndAssertMoreContentIsAvailable("kw id1, kw id2, kw id3", false)
                val list = result.assertSuccessful()
                // TODO fragment now list.countOfElements() `should be` 3
            }

            it("should parse multiple items separated with a comma surrounded with brackets") {
                val comp = ElementParserRule(TestObject.Meta) { listOf(NameParserRule(TestNameTerminal)) }
                val list = ListParserRule(
                        item = comp,
                        open = ConstantTerminalParserRule(DomSeparator.Symbol("(")),
                        separator = ConstantTerminalParserRule(DomSeparator.Symbol(",")),
                        close = ConstantTerminalParserRule(DomSeparator.Symbol(")")))
                val result = list.tokeniseAndParseAndAssertMoreContentIsAvailable("(id1, id2, id3, id4)", false)
                val listElement = result.assertSuccessful()
                // TODO fragment now listElement.countOfElements() `should be` 4
            }

            it("should parse multiple items separated with a comma and a trailing comma when requested") {
                val comp = ElementParserRule(TestObject.Meta) {
                    listOf(ConstantTerminalParserRule(kw), NameParserRule(TestNameTerminal))
                }
                val list = ListParserRule(
                        item = comp,
                        separator = ConstantTerminalParserRule(DomSeparator.Symbol(",")),
                        isTrailingSeparatorAllowed = true)
                list.tokeniseAndParseAndAssertMoreContentIsAvailable("kw id1, kw id2, kw id3,", false).assertSuccessful()
            }

            it("should parse multiple items separated with a comma and a trailing comma but leave the last comma un-parsed") {
                val comp = ElementParserRule(TestObject.Meta) {
                    listOf(ConstantTerminalParserRule(kw),
                            ElementPropertySetterTerminalValueRule(NameParserRule(TestNameTerminal), TestObject.NameMeta))
                }
                val list = ListParserRule(
                        item = comp,
                        separator = ConstantTerminalParserRule(DomSeparator.Symbol(",")),
                        isTrailingSeparatorAllowed = false)
                list.tokeniseAndParseAndAssertMoreContentIsAvailable("kw id1, kw id2, kw id3,", true).assertSuccessful()
            }

            it("should parse multiple items separated with a comma and a trailing comma when mandatory") {
                val comp = ElementParserRule(TestObject.Meta) {
                    listOf(ConstantTerminalParserRule(kw), NameParserRule(TestNameTerminal))
                }
                val list = ListParserRule(
                        item = comp,
                        separator = ConstantTerminalParserRule(DomSeparator.Symbol(",")),
                        isTrailingSeparatorMandatory = true)
                list.tokeniseAndParseAndAssertMoreContentIsAvailable("kw id1, kw id2, kw id3,", false).assertSuccessful()
            }

            it("should NOT parse multiple items separated with a comma but missing a trailing comma when mandatory") {
                val comp = ElementParserRule(TestObject.Meta) {
                    listOf(ConstantTerminalParserRule(kw), NameParserRule(TestNameTerminal))
                }
                val list = ListParserRule(
                        item = comp,
                        separator = ConstantTerminalParserRule(DomSeparator.Symbol(",")),
                        isTrailingSeparatorMandatory = true)
                list.tokeniseAndParseAndAssertMoreContentIsAvailable("kw id1, kw id2, kw id3", true).assertFailure()
            }

            it("should NOT parse multiple items separated with a comma when required brackets are missing") {
                val list = ListParserRule(
                        item = NameParserRule(TestNameTerminal),
                        open = ConstantTerminalParserRule(DomSeparator.Symbol("(")),
                        separator = ConstantTerminalParserRule(DomSeparator.Symbol(",")),
                        close = ConstantTerminalParserRule(DomSeparator.Symbol(")")))
                list.tokeniseAndParseAndAssertMoreContentIsAvailable("id1, id2, id3, id4", true).assertFailure()
            }

            it("should parse multiple items separated with a comma NOT surrounded with brackets but brackets are optional") {
                val comp = ElementParserRule(TestObject.Meta) { listOf(NameParserRule(TestNameTerminal)) }
                val list = ListParserRule(
                        item = OptionallyParserRule(comp),
                        open = OptionalLeafNodeParserRule(ConstantTerminalParserRule(DomSeparator.Symbol("("))),
                        separator = ConstantTerminalParserRule(DomSeparator.Symbol(",")),
                        close = OptionalLeafNodeParserRule(ConstantTerminalParserRule(DomSeparator.Symbol(")"))))
                val listElement = list.tokeniseAndParseAndAssertMoreContentIsAvailable("id1, id2, id3, id4", false).assertSuccessful()
                // TODO fragment now listElement.countOfElements() `should be` 4
            }

            it("should NOT parse items separated with a comma WHEN surrounded with optional but mismatched brackets") {
                val list = ListParserRule(
                        item = NameParserRule(TestNameTerminal),
                        open = OptionalLeafNodeParserRule(ConstantTerminalParserRule(DomSeparator.Symbol("("))),
                        separator = ConstantTerminalParserRule(DomSeparator.Symbol(",")),
                        close = OptionalLeafNodeParserRule(ConstantTerminalParserRule(DomSeparator.Symbol(")"))),
                        isCloseRequiredIfOpened = true)
                list.tokeniseAndParseAndAssertMoreContentIsAvailable("(id1, id2, id3, id4", true).assertFailure()
            }

            it("should parse no items in empty brackets when the item is optional") {
                val list = ListParserRule(
                        item = OptionallyParserRule(NameParserRule(TestNameTerminal)),
                        open = OptionalLeafNodeParserRule(ConstantTerminalParserRule(DomSeparator.Symbol("("))),
                        separator = ConstantTerminalParserRule(DomSeparator.Symbol(",")),
                        close = OptionalLeafNodeParserRule(ConstantTerminalParserRule(DomSeparator.Symbol(")"))),
                        isCloseRequiredIfOpened = true)
                list.tokeniseAndParseAndAssertMoreContentIsAvailable("()", false).assertSuccessful()
            }

            it("should NOT parse empty content if not optional") {
                val list = ListParserRule(
                        item = NameParserRule(TestNameTerminal),
                        separator = ConstantTerminalParserRule(DomSeparator.Symbol(",")))
                list.tokeniseAndParseAndAssertMoreContentIsAvailable("", false).assertFailure()
            }
        }
    }

    describe("Bag Parser") {

        val lexer = Lexer.from(CaptureUnknownTerminalLexerRule()) {
            +unicodeWhitespace()
            +DomKeyword.Hard("kw1")
            +DomKeyword.Hard("kw2")
        }

        fun ParserRulable.tokeniseAndParseAndAssertMoreContentIsAvailable(content: String, moreContent: Boolean): ParseResult {
            val tokenReader = lexer.tokenise(DomLexerContext.from(content))
            val result = TestDomContext.parse(this, tokenReader, StrictParseStrategy)
            tokenReader.isMoreContentAvailable() `should equal` moreContent
            val domTokenReader = tokenReader.createProviderView(DomTokenReaderDomProviderView::class.java)
            return if (result is ParseResult.Success) {
                ParseResult.CompleteContent(domTokenReader.startToken)
            } else {
                result
            }
        }

        val kw1 = ConstantTerminalParserRule(DomKeyword.Hard("kw1"))
        val kw2 = ConstantTerminalParserRule(DomKeyword.Hard("kw2"))

        it("should allow list instances") {
            val diffuse = UnorderedParserRule(childRules = listOf(Unlimited(kw1)), separator = WhitespaceParserRule)
            diffuse.tokeniseAndParseAndAssertMoreContentIsAvailable("", false).assertNoEffect()
            diffuse.tokeniseAndParseAndAssertMoreContentIsAvailable("kw1", false).assertSuccessful()
            diffuse.tokeniseAndParseAndAssertMoreContentIsAvailable("kw1 kw1", false).assertSuccessful()
        }

        it("should limit to zero or one instance") {
            val diffuse = UnorderedParserRule(childRules = listOf(ZeroOrOne(kw1)), separator = WhitespaceParserRule)
            diffuse.tokeniseAndParseAndAssertMoreContentIsAvailable("", false).assertNoEffect()
            diffuse.tokeniseAndParseAndAssertMoreContentIsAvailable("kw1", false).assertSuccessful()
            diffuse.tokeniseAndParseAndAssertMoreContentIsAvailable("kw1 kw1", true).assertFailure()
        }

        it("should limit to one instance") {
            val diffuse = UnorderedParserRule(childRules = listOf(One(kw1)), separator = WhitespaceParserRule)
            diffuse.tokeniseAndParseAndAssertMoreContentIsAvailable("", false).assertFailure()
            diffuse.tokeniseAndParseAndAssertMoreContentIsAvailable("kw1", false).assertSuccessful()
            diffuse.tokeniseAndParseAndAssertMoreContentIsAvailable("kw1 kw1", true).assertFailure()
        }

        it("should limit to exactly 3 instances") {
            val diffuse = UnorderedParserRule(childRules = listOf(Exactly(kw1, 3)), separator = WhitespaceParserRule)
            diffuse.tokeniseAndParseAndAssertMoreContentIsAvailable("", false).assertFailure()
            diffuse.tokeniseAndParseAndAssertMoreContentIsAvailable("kw1", true).assertFailure()
            diffuse.tokeniseAndParseAndAssertMoreContentIsAvailable("kw1 kw1", true).assertFailure()
            diffuse.tokeniseAndParseAndAssertMoreContentIsAvailable("kw1 kw1 kw1", false).assertSuccessful()
            diffuse.tokeniseAndParseAndAssertMoreContentIsAvailable("kw1 kw1 kw1 kw1", true).assertFailure()
        }

        it("should allow childRules in any order") {
            val diffuse = UnorderedParserRule(childRules = listOf(One(kw1), One(kw2)), separator = WhitespaceParserRule)
            diffuse.tokeniseAndParseAndAssertMoreContentIsAvailable("kw1 kw2", false).assertSuccessful()
            diffuse.tokeniseAndParseAndAssertMoreContentIsAvailable("kw2 kw1", false).assertSuccessful()
        }

        it("should allow mixed childRules with container in any order") {
            val diffuse = UnorderedParserRule(childRules = listOf(One(kw1), NoMoreThan(kw2, 2)), separator = WhitespaceParserRule)
            diffuse.tokeniseAndParseAndAssertMoreContentIsAvailable("kw1", false).assertSuccessful()
            diffuse.tokeniseAndParseAndAssertMoreContentIsAvailable("kw2 kw1", false).assertSuccessful()
            diffuse.tokeniseAndParseAndAssertMoreContentIsAvailable("kw2 kw1 kw2", false).assertSuccessful()
            diffuse.tokeniseAndParseAndAssertMoreContentIsAvailable("kw2 kw1 kw2 kw1", true).assertFailure()
            diffuse.tokeniseAndParseAndAssertMoreContentIsAvailable("kw2 kw1 kw2 kw2", true).assertFailure()
        }
    }

})

class TestObject(context: TestDomContext, val nameBacking: DomNameProperty<TestDomContext, String>)
    : IdentifiableDomElement<TestDomContext>(Meta, context),
        MandatoryRenameable {

    override val identifier: Identifier = NamePropertyIdentifier(::caseSensitiveIdentityEquals, nameBacking)

    override var name: String
        get() = nameBacking.get()
        set(value) {
            nameBacking.set(value)
        }

    object Meta : DomElementMeta<TestDomContext, TestObject> {

        override val type = TestObject::class.java
        override val name = ""
        override val description = ""

        override fun create(context: TestDomContext): TestObject {
            return TestObject(
                    context = context,
                    nameBacking = nameProperty(context))
        }
    }

    object NameMeta : DomPropertyMeta<TestDomContext, TestObject, DomNameProperty<TestDomContext, String>> {

        override val name = ""
        override val description = ""

        override fun getProperty(propertyContainer: TestObject): DomNameProperty<TestDomContext, String> {
            return propertyContainer.nameBacking
        }
    }
}
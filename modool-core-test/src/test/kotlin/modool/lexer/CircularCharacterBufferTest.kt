package modool.lexer

import org.amshove.kluent.shouldEqual
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it

class CircularCharacterBufferTest : Spek({
    describe("Character buffer index access") {
        it ("should return values from the first page") {
            val buffer = CircularTextBlock(3)
            buffer.add('a')
            buffer.add('b')
            buffer.add('c')
            buffer[0] shouldEqual 'a'
            buffer[1] shouldEqual 'b'
            buffer[2] shouldEqual 'c'
        }

        it ("should return values from subsequent pages") {
            val buffer = CircularTextBlock(3)
            buffer.add('a')
            buffer.add('b')
            buffer.add('c')
            buffer.add('d')
            buffer[3] shouldEqual 'd'
        }

        it ("should throw an out of bounds exception when indexed past the last entry") {
            val buffer = CircularTextBlock(3)
            var error = false
            try {
                buffer[0]
            } catch (e: IndexOutOfBoundsException) {
                error = true
            }
            error shouldEqual true
        }
    }

    describe("Character buffer substring") {
        it ("should return values from the first page") {
            val buffer = CircularTextBlock(3)
            buffer.add('a')
            buffer.add('b')
            buffer.add('c')
            buffer.substring(0, 2) shouldEqual "ab"
        }

        it ("should return values from the page boundary") {
            val buffer = CircularTextBlock(3)
            buffer.add('a')
            buffer.add('b')
            buffer.add('c')
            buffer.add('d')
            buffer.substring(2, 4) shouldEqual "cd"
        }
    }
})


package modool.lexer

import modool.core.content.signifier.tag.terminal.JvmLiteral
import modool.dom.lexer.DomLexerContext
import modool.dom.signifier.tag.terminal.DomSeparator
import modool.lexer.rule.constant.ConstantTerminalFactoryLexerRule
import modool.lexer.rule.unknown.CaptureFreeFormTextLexerRule
import modool.lexer.rule.unknown.CaptureUnknownTerminalLexerRule
import modool.lexer.rule.unknown.IgnoreUnknownLexerRule
import modool.lexer.rule.whitespace.UnicodeWhitespaceLexerRule
import modool.lexer.strategy.LexerCommitStrategy
import modool.lexer.strategy.ScanUnknownAndSkipToAcceptableLexerRecoveryStrategy
import modool.test.dom.*
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it

class LexerTest : Spek({

    describe("Lexer") {

        val openParen = DomSeparator.Symbol("(")
        val closeParen = DomSeparator.Symbol(")")

        it("Should parse symbols") {
            val lexer = Lexer.from(CaptureUnknownTerminalLexerRule()) {
                +openParen
                +closeParen
            }

            lexer.tokenise(DomLexerContext.from("()"))
                .assertStartOfContent()
                .assertNextContent()
                .assertTerminal("(")
                .assertNextContent()
                .assertTerminal(")")
                .assertNext()
                .assertEndOfContent()
        }

        it("Should parse symbols when duplicated") {
            val lexer = Lexer.from(CaptureUnknownTerminalLexerRule()) {
                +openParen
                +closeParen
            }

            lexer.tokenise(DomLexerContext.from("(("))
                .assertStartOfContent()
                .assertNextContent()
                .assertTerminal("(")
                .assertNextContent()
                .assertTerminal("(")
                .assertNext()
                .assertEndOfContent()
        }

        it("Should parse symbols and leading and following nonsense") {
            val lexer = Lexer.from(CaptureUnknownTerminalLexerRule()) {
                +openParen
                +closeParen
            }

            lexer.tokenise(DomLexerContext.from("wibble()wobble"))
                .assertStartOfContent()
                .assertNextContent()
                .assertUnknown("wibble")
                .assertNextContent()
                .assertTerminal("(")
                .assertNextContent()
                .assertTerminal(")")
                .assertNextContent()
                .assertUnknown("wobble")
                .assertNext()
                .assertEndOfContent()
        }

        it("Should parse symbols and whitespace") {
            val lexer = Lexer.from(CaptureUnknownTerminalLexerRule()) {
                +unicodeWhitespace()
                +openParen
                +closeParen
            }
            lexer.tokenise(DomLexerContext.from("  ( ) "))
                .assertStartOfContent()
                .assertNextContent()
                .assertSpace(2)
                .assertNextContent()
                .assertTerminal("(")
                .assertNextContent()
                .assertSpace(1)
                .assertNextContent()
                .assertTerminal(")")
                .assertNextContent()
                .assertSpace(1)
                .assertNext()
                .assertEndOfContent()
        }

        it("Should parse symbols and optional whitespace in a component") {
            val lexer = Lexer.from(IgnoreUnknownLexerRule) {
                +component {
                    +openParen
                    +whitespace { +unlimited(unicodeWhitespaceChar()) }
                    +closeParen
                }
            }

            lexer.tokenise(DomLexerContext.from("()"))
                .assertStartOfContent()
                .assertNextContent()
                .assertTerminal("(")
                .assertNextContent()
                .assertTerminal(")")
                .assertNext()
                .assertEndOfContent()
        }

        it("should support a simple repeating pattern") {

            val lexer = Lexer.from(CaptureUnknownTerminalLexerRule()) {
                +openParen
                +closeParen
                +pattern(JvmLiteral.Untyped) {
                    +exactly(2, asciiUpperLetterChar())
                    +exactly(1, asciiLowerLetterChar())
                }
            }

            lexer.tokenise(DomLexerContext.from("(ABc)"))
                .assertStartOfContent()
                .assertNextContent()
                .assertTerminal("(")
                .assertNextContent()
                .assertTerminal("ABc", JvmLiteral.Untyped)
                .assertNextContent()
                .assertTerminal(")")
                .assertNext()
                .assertEndOfContent()
        }

        it("should support a simple repeating optional pattern") {

            val lexer = Lexer.from(CaptureUnknownTerminalLexerRule()) {
                +pattern(JvmLiteral.Untyped) {
                    +exactly(2, choose(asciiUpperLetterChar(), digit()))
                }
            }

            lexer.tokenise(DomLexerContext.from("A1B"))
                .assertStartOfContent()
                .assertNextContent()
                .assertTerminal("A1", JvmLiteral.Untyped)
                .assertNextContent()
                .assertUnknown("B")
                .assertNext()
                .assertEndOfContent()
        }

        it("should not match components with all optional rules that result in no content") {
            val lexer = Lexer.from(IgnoreUnknownLexerRule) {
                +pattern(JvmLiteral.Float) {
                    +optional(char('1'))
                    +optional(char('2'))
                }
            }

            lexer.tokenise(DomLexerContext.from("blah"))
                .assertStartOfContent()
                .assertNext()
                .assertEndOfContent()
        }

        it("should support unlimited components") {
            val lexer = Lexer.from(IgnoreUnknownLexerRule) {
                +component {
                    +pattern(JvmLiteral.Float) {
                        +digit()
                        +unlimited(choose(digit(), char('_')))
                        +char('.')
                        +digit()
                        +unlimited(choose(digit(), char('_')))
                    }
                    +pattern { +optional(char('f')) }
                }
            }

            lexer.tokenise(DomLexerContext.from("1.3"))
                .assertStartOfContent()
                .assertNextContent()
                .assertTerminal("1.3", JvmLiteral.Float)
                .assertNext()
                .assertEndOfContent()
        }

        it("should skip an optional element") {

            val lexer = Lexer.from(CaptureUnknownTerminalLexerRule()) {
                +pattern(JvmLiteral.Integer) {
                    +exactly(1, digit())
                    +optional(asciiUpperLetterChar())
                    +exactly(1, digit())
                }
            }

            lexer.tokenise(DomLexerContext.from("11"))
                .assertStartOfContent()
                .assertNextContent()
                .assertTerminal("11", JvmLiteral.Integer)
                .assertNext()
                .assertEndOfContent()
        }

        it("should support escaped char content") {

            val lexer = Lexer.from(CaptureUnknownTerminalLexerRule()) {
                +pattern(JvmLiteral.String) {
                    +char('"')
                    +unlimited(anyChar())
                    +escape(char('"'), '\\')
                }
            }

            lexer.tokenise(DomLexerContext.from("\"begin\\\"mid\tdle\\\"end\""))
                .assertStartOfContent()
                .assertNextContent()
                .assertTerminal("\"begin\\\"mid\tdle\\\"end\"", JvmLiteral.String)
                .assertNext()
                .assertEndOfContent()
        }

        it("should support bounded pattern including escaped char content") {

            val lexer = Lexer.from(CaptureUnknownTerminalLexerRule()) {
                +pattern(JvmLiteral.Untyped) {
                    +char('/')
                    +unlimited(except(unicodeNewLineChar()))
                    +escape(char('/'), '\\')
                    +unlimited(choose(char('g'), char('i'), char('m'), char('s'), char('u'), char('y')))
                }
            }

            lexer.tokenise(DomLexerContext.from("/ab+c/"))
                .assertStartOfContent()
                .assertNextContent()
                .assertTerminal("/ab+c/", JvmLiteral.Untyped)
                .assertNext()
                .assertEndOfContent()

        }
    }

    describe("Tabs") {
        val lexer = Lexer.from(IgnoreUnknownLexerRule) {
            +pattern(JvmLiteral.Untyped) {
                +exactly(5, inlineWhitespaceChar(tabMatchCount = 4))
            }
        }

        it("should match 5 spaces") {
            lexer.tokenise(DomLexerContext.from("     "))
                .assertStartOfContent()
                .assertNextContent()
                .assertTerminal("     ", JvmLiteral.Untyped)
                .assertNext()
                .assertEndOfContent()
        }


        it("should not match a single tab") {
            lexer.tokenise(DomLexerContext.from("\t"))
                .assertStartOfContent()
                .assertNext()
                .assertEndOfContent()
        }

        it("should match tabs as a match count of 4") {
            lexer.tokenise(DomLexerContext.from("\t "))
                .assertStartOfContent()
                .assertNextContent()
                .assertTerminal("\t ", JvmLiteral.Untyped)
                .assertNext()
                .assertEndOfContent()
        }
    }

    describe("Structured whitespace") {

        val lexer = Lexer.from(IgnoreUnknownLexerRule) {
            +whitespace {
                +inlineWhitespaceChar()
                +unicodeNewLineChar()
            }
        }

        it("should match correct sequence") {
            lexer.tokenise(DomLexerContext.from(" \n"))
                .assertStartOfContent()
                .assertNext()
                .assertWhitespace()
                .assertNext()
                .assertEndOfContent()
        }

        it("should ignore incorrect sequence pattern") {
            lexer.tokenise(DomLexerContext.from("\n\n"))
                .assertStartOfContent()
                .assertNext()
                .assertEndOfContent()
        }
    }

    describe("Pattern with look behind") {

        val lexer = Lexer.from(IgnoreUnknownLexerRule) {
            +pattern(JvmLiteral.Untyped) {
                +follows(
                    choose(start(), unicodeNewLineChar()),
                    char('a')
                )
            }
        }

        it("should ignore content that does not follow an end of line") {
            lexer.tokenise(DomLexerContext.from("|a"))
                .assertStartOfContent()
                .assertNext()
                .assertEndOfContent()

        }

        it("should extract content that meets the follows condition (end of line)") {
            lexer.tokenise(DomLexerContext.from("\na"))
                .assertStartOfContent()
                .assertNextContent()
                .assertTerminal("a", JvmLiteral.Untyped)
                .assertNext()
                .assertEndOfContent()
        }
    }

    describe("Pattern with look ahead") {

        val lexer = Lexer.from(IgnoreUnknownLexerRule) {
            +pattern(JvmLiteral.Untyped) {
                +precedes(
                    char('a'),
                    choose(unicodeNewLineChar(), end())
                )
            }
        }

        it("should ignore content that does not precede an end of line") {
            lexer.tokenise(DomLexerContext.from("a|"))
                .assertStartOfContent()
                .assertNext()
                .assertEndOfContent()
        }

        it("should extract content that meets the precedes condition (end of line)") {
            lexer.tokenise(DomLexerContext.from("a\n"))
                .assertStartOfContent()
                .assertNextContent()
                .assertTerminal("a")
                .assertNext()
                .assertEndOfContent()

        }
    }

    describe("Pattern with look ahead and behind") {

        val lexer = Lexer.from(IgnoreUnknownLexerRule) {
            +pattern(JvmLiteral.Untyped) {
                +precedes(char('a'), group(char('b'))) // Capture a with test b
                +follows(char('a'), char('b')) // capture b with test a
            }
        }

        it("should match content bounded by follows and precedes") {
            lexer.tokenise(DomLexerContext.from("ab"))
                .assertStartOfContent()
                .assertNextContent()
                .assertTerminal("ab", JvmLiteral.Untyped)
                .assertNext()
                .assertEndOfContent()
        }
    }

    describe("Repeated Pattern") {

        val lexer = Lexer.from(IgnoreUnknownLexerRule) {
            +unlimited(
                component(
                    pattern(JvmLiteral.Untyped) {
                        +unlimited(char('a'))
                        +char('b')
                    })
            )
        }

        it("should output multiple matches") {
            lexer.tokenise(DomLexerContext.from("abab"))
                .assertStartOfContent()
                .assertNextContent()
                .assertTerminal("ab", JvmLiteral.Untyped)
                .assertNextContent()
                .assertTerminal("ab", JvmLiteral.Untyped)
                .assertNext()
                .assertEndOfContent()
        }
    }

    describe("Text block i.e. unknown content block") {

        val lexer = Lexer.from(
            CaptureFreeFormTextLexerRule(
                UnicodeWhitespaceLexerRule(),
                tags = emptyList(),
                contiguousLineBreakLimit = 2
            )
        ) {
            +pattern(JvmLiteral.Untyped) {
                +asciiUpperLetterChar()
            }
        }

        it("should match an opening whitespace content, with an embedded space and a closing whitespace") {
            lexer.tokenise(DomLexerContext.from(" a a "))
                .assertStartOfContent()
                .assertNextContent()
                .assertSpace(1)
                .assertNextContent()
                .assertTerminal("a a")
                .assertNextContent()
                .assertSpace(1)
                .assertNext()
                .assertEndOfContent()
        }

        it("should match a word") {
            lexer.tokenise(DomLexerContext.from("hello"))
                .assertStartOfContent()
                .assertNextContent()
                .assertTerminal("hello")
                .assertNext()
                .assertEndOfContent()
        }

        it("should match multiple opening whitespace, content with an embedded space and NO closing whitespace") {
            lexer.tokenise(DomLexerContext.from("  a  a"))
                .assertStartOfContent()
                .assertNextContent()
                .assertSpace(2)
                .assertNextContent()
                .assertTerminal("a  a")
                .assertNext()
                .assertEndOfContent()
        }

        it("should match NO opening whitespace, content with an embedded EOLN and NO closing whitespace") {
            lexer.tokenise(DomLexerContext.from("a\na"))
                .assertStartOfContent()
                .assertNextContent()
                .assertTerminal("a\na")
                .assertNext()
                .assertEndOfContent()
        }

        it("should not match a line of greater indentation i.e. creates two separate entries") {
            lexer.tokenise(DomLexerContext.from("a\n a"))
                .assertStartOfContent()
                .assertNextContent()
                .assertTerminal("a")
                .assertNextContent()
                .assertNewLine(1)
                .assertSpace(1)
                .assertNextContent()
                .assertTerminal("a")
                .assertNext()
                .assertEndOfContent()
        }

        it("Should split on line breaks that break the contiguousLineBreakLimit") {
            lexer.tokenise(DomLexerContext.from("a\n\na"))
                .assertStartOfContent()
                .assertNextContent()
                .assertTerminal("a")
                .assertNextContent()
                .assertNewLine(2)
                .assertNextContent()
                .assertTerminal("a")
                .assertNext()
                .assertEndOfContent()
        }

        it("should NOT over match i.e. do not return unknown with the pattern match") {
            lexer.tokenise(DomLexerContext.from(" A\nA "))
                .assertStartOfContent()
                .assertNextContent()
                .assertSpace(1)
                .assertNextContent()
                .assertTerminal("A", JvmLiteral.Untyped)
                .assertNextContent()
                .assertNewLine(1)
                .assertNextContent()
                .assertTerminal("A", JvmLiteral.Untyped)
                .assertNextContent()
                .assertSpace(1)
                .assertNext()
                .assertEndOfContent()
        }
    }

    describe("Text block with clashing leading white space rule") {

        val lexer = Lexer.from(
            CaptureFreeFormTextLexerRule(
                UnicodeWhitespaceLexerRule(),
                tags = listOf(JvmLiteral.String),
                contiguousLineBreakLimit = 2
            )
        ) {

            +component {
                +unicodeWhitespace()
                +pattern(JvmLiteral.Untyped) {
                    +asciiUpperLetterChar()
                }
            }
        }

        it("should continue to match free form text after the second space even though the component rule clashes") {
            lexer.tokenise(DomLexerContext.from(" a b "))
                .assertStartOfContent()
                .assertNextContent()
                .assertSpace(1)
                .assertNextContent()
                .assertTerminal("a b", JvmLiteral.String)
                .assertNextContent()
                .assertSpace(1)
                .assertNext()
                .assertEndOfContent()
        }
    }

    describe("Text block transitions") {

        val unknown = CaptureFreeFormTextLexerRule(
            UnicodeWhitespaceLexerRule(),
            tags = emptyList(),
            contiguousLineBreakLimit = 2
        )

        val lexer = Lexer.from(
            ScanUnknownAndSkipToAcceptableLexerRecoveryStrategy(next = null),
            LexerCommitStrategy.Default,
            unknown
        ) {

            +pattern(JvmLiteral.Untyped) {
                +char('*')
            }
            // This grabs focus before * can match so a recovery is required
            +component {
                +unicodeWhitespace()
                +pattern(JvmLiteral.Untyped) {
                    +asciiUpperLetterChar()
                }
            }
        }

        it("should transition to and from patterns and free text") {
            lexer.tokenise(DomLexerContext.from("hello *there fancy* modooler"))
                .assertStartOfContent()
                .assertNextContent()
                .assertTerminal("hello")
                .assertNextContent()
                .assertSpace(1)
                .assertNextContent()
                .assertTerminal("*", JvmLiteral.Untyped)
                .assertNextContent()
                .assertTerminal("there fancy")
                .assertNextContent()
                .assertTerminal("*", JvmLiteral.Untyped)
                .assertNextContent()
                .assertSpace(1)
                .assertNextContent()
                .assertTerminal("modooler")
                .assertNext()
                .assertEndOfContent()
        }
    }

    describe("Choice in a pattern") {

        val lexer = Lexer.from(CaptureUnknownTerminalLexerRule()) {
            +pattern(JvmLiteral.Untyped) {
                +choose(asciiUpperLetterChar(), digit())
            }
        }

        it("should match the first choice in a pattern") {
            lexer.tokenise(DomLexerContext.from("A"))
                .assertStartOfContent()
                .assertNextContent()
                .assertTerminal("A", JvmLiteral.Untyped)
                .assertNext()
                .assertEndOfContent()
        }

        it("should return unknown content tokens for something not identified by the pattern") {
            lexer.tokenise(DomLexerContext.from("a"))
                .assertStartOfContent()
                .assertNextContent()
                .assertUnknown("a")
                .assertNext()
                .assertEndOfContent()
        }

        it("should match the alternative choice in an optional pattern") {
            lexer.tokenise(DomLexerContext.from("4"))
                .assertStartOfContent()
                .assertNextContent()
                .assertTerminal("4", JvmLiteral.Untyped)
                .assertNext()
                .assertEndOfContent()
        }
    }

    describe("Negative choice in a pattern") {

        val lexer = Lexer.from(CaptureUnknownTerminalLexerRule()) {
            +pattern(JvmLiteral.Untyped) {
                +atLeast(1, except(digit(), asciiUpperLetterChar()))
            }
        }

        it("should break on capital letters") {
            lexer.tokenise(DomLexerContext.from("startAend"))
                .assertStartOfContent()
                .assertNextContent()
                .assertTerminal("start", JvmLiteral.Untyped)
                .assertNextContent()
                .assertUnknown("A")
                .assertNextContent()
                .assertTerminal("end", JvmLiteral.Untyped)
                .assertNext()
                .assertEndOfContent()
        }

        it("should break on capital or digits letters") {
            lexer.tokenise(DomLexerContext.from("startAmiddle3end"))
                .assertStartOfContent()
                .assertNextContent()
                .assertTerminal("start", JvmLiteral.Untyped)
                .assertNextContent()
                .assertUnknown("A")
                .assertNextContent()
                .assertTerminal("middle", JvmLiteral.Untyped)
                .assertNextContent()
                .assertUnknown("3")
                .assertNextContent()
                .assertTerminal("end", JvmLiteral.Untyped)
                .assertNext()
                .assertEndOfContent()
        }
    }

    describe("Pattern with overlapping choices (both start with b)") {
        val lexer = Lexer.from(IgnoreUnknownLexerRule) {
            +pattern(JvmLiteral.Untyped) {
                +choose(constant("boo"), constant("baa"))
            }
        }

        it("should match the first choice") {
            lexer.tokenise(DomLexerContext.from("boo"))
                .assertStartOfContent()
                .assertNextContent()
                .assertTerminal("boo", JvmLiteral.Untyped)
                .assertNext()
                .assertEndOfContent()
        }

        it("should match the second choice") {
            lexer.tokenise(DomLexerContext.from("baa"))
                .assertStartOfContent()
                .assertNextContent()
                .assertTerminal("baa", JvmLiteral.Untyped)
                .assertNext()
                .assertEndOfContent()
        }
    }

    describe("pattern with greedy matching") {
        val lexer = Lexer.from(IgnoreUnknownLexerRule) {
            +pattern(JvmLiteral.Long) {
                +greedy(unlimited(constant("foo")))
            }
            +pattern(JvmLiteral.Char) {
                +optional(anyChar())
            }
        }

        it("should match two foos and not pass control to second pattern after first foo") {
            lexer.tokenise(DomLexerContext.from("foofoo!"))
                .assertStartOfContent()
                .assertNextContent()
                .assertTerminal("foofoo", JvmLiteral.Long)
                .assertNextContent()
                .assertTerminal("!", JvmLiteral.Char)
                .assertNext()
                .assertEndOfContent()
        }
    }

    describe("yield") {
        val lexer = Lexer.from(
            ScanUnknownAndSkipToAcceptableLexerRecoveryStrategy(null),
            LexerCommitStrategy.Default,
            CaptureUnknownTerminalLexerRule()) {
            +yieldTo(
                "other",
                pattern(JvmLiteral.Untyped) {
                    +char('x')
                    +exactly(2, anyChar())
                    +char('x')
                })
            +addRule("other", constant("yy"))
        }

        it("should yield to rule matching smaller (inner) yy pattern") {
            lexer.tokenise(DomLexerContext.from("xyyx"))
                .assertStartOfContent()
                .assertNextContent()
                .assertUnknown("x")
                .assertNextContent()
                .assertTerminal("yy")
                .assertNext()
                .assertUnknown("x")
                .assertNext()
                .assertEndOfContent()
        }

        it("should NOT yield to rule matching smaller (inner) yy pattern because it cannot match") {
            lexer.tokenise(DomLexerContext.from("xzzx"))
                .assertStartOfContent()
                .assertNextContent()
                .assertTerminal("xzzx", JvmLiteral.Untyped)
                .assertNext()
                .assertEndOfContent()
        }
    }

    describe("Identifier") {
        val lexer = Lexer.from(IgnoreUnknownLexerRule) {
            +pattern(JvmLiteral.Untyped) {
                +choose(asciiLetterChar(), char('_'))
                +unlimited(asciiLetterOrDigitChar())
            }
            +constant(":")
        }

        it("should not over match") {
            lexer.tokenise(DomLexerContext.from("name:"))
                .assertStartOfContent()
                .assertNextContent()
                .assertTerminal("name", JvmLiteral.Untyped)
                .assertNextContent()
                .assertTerminal(":")
                .assertNext()
                .assertEndOfContent()
        }
    }

    describe("component emitting multiple tokens") {
        val lexer = Lexer.from(CaptureUnknownTerminalLexerRule()) {
            +component {
                +pattern(JvmLiteral.Float) {
                    +atLeast(1, digit())
                    +char('.')
                    +atLeast(1, digit())
                }
                +pattern(JvmLiteral.Untyped) {
                    +optional(char('f'))
                }
            }
        }

        it("should support a component returning multiple tokens") {
            lexer.tokenise(DomLexerContext.from("10.23f"))
                .assertStartOfContent()
                .assertNextContent()
                .assertTerminal("10.23", JvmLiteral.Float)
                .assertNextContent()
                .assertTerminal("f", JvmLiteral.Untyped)
                .assertNext()
                .assertEndOfContent()
        }

        it("should match when one token is optional") {
            lexer.tokenise(DomLexerContext.from("10.23"))
                .assertStartOfContent()
                .assertNextContent()
                .assertTerminal("10.23", JvmLiteral.Float)
                .assertNext()
                .assertEndOfContent()
        }
    }

    describe("should support complex pattern with nested repeats") {
        val lexer = Lexer.from(CaptureUnknownTerminalLexerRule()) {
            +pattern(JvmLiteral.Integer) {
                +choose(
                    char('0'),
                    group(
                        charRange('1', '9'),
                        unlimited(digit()),
                        optional(group(char('.'), atLeast(1, digit()))),
                        optional(
                            group(
                                choose(char('e'), char('E')),
                                optional(choose(char('+'), char('-'))),
                                atLeast(1, digit())
                            )
                        )
                    )
                )
            }
        }

        it("should match when multiple matches for nested repeat i.e. 236") {
            lexer.tokenise(DomLexerContext.from("110.236"))
                .assertStartOfContent()
                .assertNextContent()
                .assertTerminal("110.236", JvmLiteral.Integer)
                .assertNext()
                .assertEndOfContent()
        }

        it("should match when single match for nested repeat i.e. 2") {
            lexer.tokenise(DomLexerContext.from("10.2a"))
                .assertStartOfContent()
                .assertNextContent()
                .assertTerminal("10.2", JvmLiteral.Integer)
                .assertNextContent()
                .assertUnknown("a")
                .assertNext()
                .assertEndOfContent()
        }

        it("should match when multiple matches for skipped repeat") {
            lexer.tokenise(DomLexerContext.from("1e-3"))
                .assertStartOfContent()
                .assertNextContent()
                .assertTerminal("1e-3", JvmLiteral.Integer)
                .assertNext()
                .assertEndOfContent()
        }

        it("should match when multiple matches for sequential nested repeat i.e. 23 and e-3") {
            lexer.tokenise(DomLexerContext.from("123.45e-3"))
                .assertStartOfContent()
                .assertNextContent()
                .assertTerminal("123.45e-3", JvmLiteral.Integer)
                .assertNext()
                .assertEndOfContent()
        }
    }

    describe("Match until") {
        val lexer = Lexer.from(CaptureUnknownTerminalLexerRule()) {
            +captureUntil(
                tags = listOf(JvmLiteral.String),
                until = arrayOf('#'),
                escape = '\\'
            )
        }

        it("should match content with an escaped #") {
            lexer.tokenise(DomLexerContext.from("value\\#some more#"))
                .assertStartOfContent()
                .assertNextContent()
                .assertTerminal("value\\#some more", JvmLiteral.String)
                .assertNextContent()
                .assertUnknown("#")
                .assertNext()
                .assertEndOfContent()
        }
    }

    describe("Match from to") {

        it("should support multiline comment extraction") {
            val lexer = Lexer.from(CaptureUnknownTerminalLexerRule()) {
                +captureFromTo(
                    tags = listOf(JvmLiteral.Untyped),
                    from = ConstantTerminalFactoryLexerRule(DomSeparator.Symbol("/*")),
                    to = ConstantTerminalFactoryLexerRule(DomSeparator.Symbol("*/"))
                )
            }

            lexer.tokenise(DomLexerContext.from("/* A nice comment */"))
                .assertStartOfContent()
                .assertNextContent()
                .assertTerminal("/*")
                .assertNextContent()
                .assertTerminal(" A nice comment ", JvmLiteral.Untyped)
                .assertNextContent()
                .assertTerminal("*/")
                .assertNext()
                .assertEndOfContent()
        }

        it("should support content with an escape character") {
            val lexer = Lexer.from(CaptureUnknownTerminalLexerRule()) {
                +captureFromTo(
                    tags = listOf(JvmLiteral.Untyped),
                    from = ConstantTerminalFactoryLexerRule(DomSeparator.Symbol("\"")),
                    to = ConstantTerminalFactoryLexerRule(DomSeparator.Symbol("\"")),
                    escape = '\\'
                )
            }

            lexer.tokenise(DomLexerContext.from("\"Some \\\"quoted\\\" text\""))
                .assertStartOfContent()
                .assertNextContent()
                .assertTerminal("\"")
                .assertNextContent()
                .assertTerminal("Some \\\"quoted\\\" text")
                .assertNextContent()
                .assertTerminal("\"")
                .assertTerminal("\"")
                .assertNext()
                .assertEndOfContent()
        }

        it("should support content with an escape character prior to the end") {
            val lexer = Lexer.from(CaptureUnknownTerminalLexerRule()) {
                +captureFromTo(
                    tags = listOf(JvmLiteral.Untyped),
                    from = ConstantTerminalFactoryLexerRule(DomSeparator.Symbol("\"")),
                    to = ConstantTerminalFactoryLexerRule(DomSeparator.Symbol("\"")),
                    escape = '\\'
                )
            }

            lexer.tokenise(DomLexerContext.from("\"Some \\\"quoted\\\"\""))
                .assertStartOfContent()
                .assertNextContent()
                .assertTerminal("\"")
                .assertNextContent()
                .assertTerminal("Some \\\"quoted\\\"")
                .assertNextContent()
                .assertTerminal("\"")
                .assertNext()
                .assertEndOfContent()
        }
    }

    describe("Match from Until") {

        val lexer = Lexer.from(CaptureUnknownTerminalLexerRule()) {
            +captureFromUntil(
                tags = listOf(JvmLiteral.Untyped),
                from = ConstantTerminalFactoryLexerRule(DomSeparator.Symbol("//")),
                until = constant("#"),
                escape = '\\'
            )
        }

        it("should support terminator based extraction") {
            lexer.tokenise(DomLexerContext.from("// Hash will terminate but not be included #"))
                .assertStartOfContent()
                .assertNextContent()
                .assertTerminal("//")
                .assertNextContent()
                .assertTerminal(" Hash will terminate but not be included ")
                .assertNextContent()
                .assertUnknown("#")
                .assertNext()
                .assertEndOfContent()
        }

        it("should support escaped terminator") {
            lexer.tokenise(DomLexerContext.from("// Hash will terminate (not his one\\#) but not be included #"))
                .assertStartOfContent()
                .assertNextContent()
                .assertTerminal("//")
                .assertNextContent()
                .assertTerminal(" Hash will terminate (not his one\\#) but not be included ")
                .assertNextContent()
                .assertUnknown("#")
                .assertNext()
                .assertEndOfContent()
        }
    }

    describe("Mixed content") {
        val lexer = Lexer.from(CaptureUnknownTerminalLexerRule()) {
            +captureFromTo(
                tags = listOf(JvmLiteral.String),
                from = terminal(DomSeparator.Symbol("\"")),
                to = terminal(DomSeparator.Symbol("\""))
            )
            // This is a token in it's own right but also used in string above
            +DomSeparator.Symbol("\"")
            +pattern {
                +atLeast(1, asciiLetterChar())
            }
        }

        it("Should match the string over quotes and identifiers") {
            lexer.tokenise(DomLexerContext.from("\"hello\""))
                .assertStartOfContent()
                .assertNextContent()
                .assertTerminal("\"")
                .assertNextContent()
                .assertTerminal("hello", JvmLiteral.String)
                .assertNextContent()
                .assertTerminal("\"")
                .assertNext()
                .assertEndOfContent()
        }
    }

    describe("Model expression strings") {

        val lexer = Lexer.from(CaptureUnknownTerminalLexerRule()) {
            +component {
                +DomSeparator.Symbol("\"")
                +unlimited(
                    choose(
                        component {
                            +DomSeparator.Symbol("\$")
                            +DomSeparator.Symbol("{")
                            // TODO: Embedded parser but for now ...
                            +pattern(JvmLiteral.Untyped) { +atLeast(1, anyChar()) }
                            +DomSeparator.Symbol("}")
                        },
                        captureUntil(
                            tags = listOf(JvmLiteral.String),
                            until = arrayOf('"', '$'),
                            escape = '\\'
                        )
                    )
                )
                +DomSeparator.Symbol("\"")
            }
        }

        it("should support a simple string") {
            lexer.tokenise(DomLexerContext.from("\"hello\""))
                .assertStartOfContent()
                .assertNextContent()
                .assertTerminal("\"")
                .assertNextContent()
                .assertTerminal("hello", JvmLiteral.String)
                .assertNextContent()
                .assertTerminal("\"")
                .assertNext()
                .assertEndOfContent()
        }

        it("should support an embedded expression") {
            lexer.tokenise(DomLexerContext.from("\"\${expr}\""))
                .assertStartOfContent()
                .assertNextContent()
                .assertTerminal("\"")
                .assertNextContent()
                .assertTerminal("$")
                .assertNextContent()
                .assertTerminal("{")
                .assertNextContent()
                .assertTerminal("expr")
                .assertNextContent()
                .assertTerminal("}")
                .assertNextContent()
                .assertTerminal("\"")
                .assertNext()
                .assertEndOfContent()
        }

        it("should support mixed expression and string") {
            lexer.tokenise(DomLexerContext.from("\"a \${expr} b\""))
                .assertStartOfContent()
                .assertNextContent()
                .assertTerminal("\"")
                .assertNextContent()
                .assertTerminal("a ", JvmLiteral.String)
                .assertNextContent()
                .assertTerminal("$")
                .assertNextContent()
                .assertTerminal("{")
                .assertNextContent()
                .assertTerminal("expr")
                .assertNextContent()
                .assertTerminal("}")
                .assertNextContent()
                .assertTerminal(" b", JvmLiteral.String)
                .assertNextContent()
                .assertTerminal("\"")
                .assertNext()
                .assertEndOfContent()
        }

        it("should support multiple expressions") {
            lexer.tokenise(DomLexerContext.from("\"\${e1} c \${e2}\""))
                .assertStartOfContent()
                .assertNextContent()
                .assertTerminal("\"")
                .assertNextContent()
                .assertTerminal("$")
                .assertNextContent()
                .assertTerminal("{")
                .assertNextContent()
                .assertTerminal("e1")
                .assertNextContent()
                .assertTerminal("}")
                .assertNextContent()
                .assertTerminal(" c ", JvmLiteral.String)
                .assertNextContent()
                .assertTerminal("$")
                .assertNextContent()
                .assertTerminal("{")
                .assertNextContent()
                .assertTerminal("e2")
                .assertNextContent()
                .assertTerminal("}")
                .assertNextContent()
                .assertTerminal("\"")
                .assertNext()
                .assertEndOfContent()
        }
    }

    describe("Symmetrical matching with the stack") {

        val lexer = Lexer.from(IgnoreUnknownLexerRule) {
            +component {
                +setMatch("value1", pattern(JvmLiteral.Untyped) {
                    +atLeast(1, anyChar())
                })
                +constant("X")
            }
            +component {
                +matchAndClear("value1", JvmLiteral.Untyped)
            }
        }

        it("should match with symmetry") {
            lexer.tokenise(DomLexerContext.from("abXab"))
                .assertStartOfContent()
                .assertNextContent()
                .assertTerminal("ab")
                .assertNextContent()
                .assertTerminal("X")
                .assertNextContent()
                .assertTerminal("ab")
                .assertNext()
                .assertEndOfContent()
        }
    }

    describe("complex patterns") {
        val lexer = Lexer.from(IgnoreUnknownLexerRule) {
            +eager(
                pattern(JvmLiteral.Long) {
                    +choose(
                        char('0'),
                        group(
                            charRange('1', '9'),
                            unlimited(digit()),
                            optional(group(char('.'), atLeast(1, digit()))),
                            optional(
                                group(
                                    choose(char('e'), char('E')),
                                    optional(choose(char('+'), char('-'))),
                                    atLeast(1, digit())
                                )
                            )
                        )
                    )
                })
        }

        it("should work") {
            lexer.tokenise(DomLexerContext.from("123e+4"))
                .assertStartOfContent()
                .assertNextContent()
                .assertTerminal("123e+4")
                .assertNext()
                .assertEndOfContent()
        }
    }

    describe("scopes") {
        val lexer = Lexer.from(
            CaptureFreeFormTextLexerRule(
                UnicodeWhitespaceLexerRule(),
                tags = emptyList(),
                contiguousLineBreakLimit = 2
            )
        ) {
            +scope(
                CaptureFreeFormTextLexerRule(
                    UnicodeWhitespaceLexerRule(),
                    tags = emptyList(),
                    contiguousLineBreakLimit = 2
                ),
                ConstantTerminalFactoryLexerRule(DomSeparator.Symbol("*"))
            )
            +scope(
                CaptureFreeFormTextLexerRule(
                    UnicodeWhitespaceLexerRule(),
                    tags = emptyList(),
                    contiguousLineBreakLimit = 2
                ),
                ConstantTerminalFactoryLexerRule(DomSeparator.Symbol("/"))
            )
        }

        it("should use scopes to capture free text at the appropriate level") {
            lexer.tokenise(DomLexerContext.from("b *d* a"))
                .assertStartOfContent()
                .assertNextContent()
                .assertTerminal("b")
                .assertNextContent()
                .assertSpace(1)
                .assertNextContent()
                .assertTerminal("*")
                .assertNextContent()
                .assertTerminal("d")
                .assertNextContent()
                .assertTerminal("*")
                .assertNextContent()
                .assertSpace(1)
                .assertNextContent()
                .assertTerminal("a")
                .assertNext()
                .assertEndOfContent()
        }

        it("should distinguish between scopes") {
            lexer.tokenise(DomLexerContext.from("/d/"))
                .assertStartOfContent()
                .assertNextContent()
                .assertTerminal("/")
                .assertNextContent()
                .assertTerminal("d")
                .assertNextContent()
                .assertTerminal("/")
                .assertNext()
                .assertEndOfContent()
        }

        it("should capture free text at the root because there is no match on symbols") {
            lexer.tokenise(DomLexerContext.from("b +d+ a"))
                .assertStartOfContent()
                .assertNextContent()
                .assertTerminal("b +d+ a")
                .assertNext()
                .assertEndOfContent()
        }
    }
})
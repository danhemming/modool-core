package modool.lexer

import modool.dom.lexer.DomLexerContext
import modool.dom.node.text.DomSymbol
import modool.lexer.rule.unknown.CaptureUnknownTerminalLexerRule
import modool.lexer.rule.unknown.IgnoreUnknownLexerRule
import modool.dom.signifier.tag.terminal.DomSeparator
import modool.test.dom.*
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it

class LexerCommentTest : Spek({
    describe("Single line comments") {
        val lexer = Lexer.from(IgnoreUnknownLexerRule) {
            +singleLineComment(terminal(DomSymbol.DOUBLE_FORWARD_SLASH))
        }

        it("should parse comments into a special section") {
            lexer.tokenise(DomLexerContext.from("// hello"))
                    .assertStartOfContent()
                    .assertNext()
                    .assertStartOfComment()
                    .assertNext()
                    .assertTerminal("//")
                    .assertNext()
                    .assertTerminal(" hello")
                    .assertNext()
                    .assertEndOfComment()
                    .assertNext()
                    .assertEndOfContent()
        }

        it("should parse empty comment") {
            lexer.tokenise(DomLexerContext.from("//"))
                    .assertStartOfContent()
                    .assertNext()
                    .assertStartOfComment()
                    .assertNext()
                    .assertTerminal("//")
                    .assertNext()
                    .assertEndOfComment()
                    .assertNext()
                    .assertEndOfContent()

        }
    }

    describe("Multi line comments") {
        val lexer = Lexer.from(CaptureUnknownTerminalLexerRule()) {
            +unicodeWhitespace()
            +multiLineComment(terminal(DomSeparator.Symbol("/*")), terminal(DomSeparator.Symbol("*/")))
        }

        it("should parse comments into a special section") {
            lexer.tokenise(DomLexerContext.from("/* hello */"))
                    .assertStartOfContent()
                    .assertNext()
                    .assertStartOfComment()
                    .assertNext()
                    .assertTerminal("/*")
                    .assertNext()
                    .assertTerminal(" hello ")
                    .assertNext()
                    .assertTerminal("*/")
                    .assertNext()
                    .assertEndOfComment()
                    .assertNext()
                    .assertEndOfContent()
        }

        it("should parse empty comment") {
            lexer.tokenise(DomLexerContext.from("/**/"))
                    .assertStartOfContent()
                    .assertNext()
                    .assertStartOfComment()
                    .assertNext()
                    .assertTerminal("/*")
                    .assertNext()
                    .assertTerminal("*/")
                    .assertNext()
                    .assertEndOfComment()
                    .assertNext()
                    .assertEndOfContent()

        }

        it("should parse comments into a special section and be terminated correctly") {
            lexer.tokenise(DomLexerContext.from("/* hello */ other"))
                    .assertStartOfContent()
                    .assertNext()
                    .assertStartOfComment()
                    .assertNext()
                    .assertTerminal("/*")
                    .assertNext()
                    .assertTerminal(" hello ")
                    .assertNext()
                    .assertTerminal("*/")
                    .assertNext()
                    .assertEndOfComment()
                    .assertNext()
                    .assertSpace(1)
                    .assertNext()
                    .assertUnknown("other")
                    .assertNext()
                    .assertEndOfContent()
        }
    }
})


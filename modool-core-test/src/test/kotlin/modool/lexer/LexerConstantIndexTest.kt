package modool.lexer

import modool.dom.lexer.DomLexerContext
import modool.lexer.rule.constant.ConstantIndex
import modool.lexer.rule.unknown.CaptureUnknownTerminalLexerRule
import modool.dom.signifier.tag.terminal.DomKeyword
import modool.dom.signifier.tag.terminal.DomSeparator
import modool.test.dom.*
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it

object TestConstantIndex : ConstantIndex() {

    override fun initialiseEntries() {
        add(DomSeparator.Symbol("(("))
        add(DomSeparator.Symbol("("))
        add(DomSeparator.Symbol(")"))
        add(DomKeyword.Hard("test", isCaseSensitive = false))
        add(DomKeyword.Hard("\u5555"))
    }
}

class LexerConstantIndexTest : Spek({

    describe("Lexer constant index") {

        val lexer = Lexer.from(CaptureUnknownTerminalLexerRule()) {
            +TestConstantIndex
        }

        it("Should parse ascii symbols") {

            lexer.tokenise(DomLexerContext.from("(test)"))
                    .assertStartOfContent()
                    .assertNextContent()
                    .assertTerminal("(")
                    .assertNextContent()
                    .assertTerminal("test")
                    .assertNextContent()
                    .assertTerminal(")")
                    .assertNext()
                    .assertEndOfContent()
        }

        it("Should parse ascii symbols case insensitively") {

            lexer.tokenise(DomLexerContext.from("TeSt"))
                .assertStartOfContent()
                .assertNextContent()
                .assertTerminal("TeSt")
                .assertNext()
                .assertEndOfContent()
        }

        it("Should parse non-ascii symbols") {

            lexer.tokenise(DomLexerContext.from("(\u5555)"))
                    .assertStartOfContent()
                    .assertNextContent()
                    .assertTerminal("(")
                    .assertNextContent()
                    .assertTerminal("\u5555")
                    .assertNextContent()
                    .assertTerminal(")")
                    .assertNext()
                    .assertEndOfContent()
        }
    }
})
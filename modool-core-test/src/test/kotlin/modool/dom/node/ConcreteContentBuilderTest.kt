package modool.dom.node

import modool.dom.node.text.factory.CommonDynamicTerminals
import modool.dom.token.DomElementStartToken
import modool.core.content.signifier.tag.Tag
import modool.dom.token.DomContentStartToken
import modool.dom.token.DomToken
import modool.dom.token.DomTokenContainer
import modool.core.content.token.TokenRangeOption
import modool.test.dom.assertJoinToEnd
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it

class ConcreteContentBuilderTest : Spek({

    describe("should add content appropriately") {

        it("should not add content when it is already defined at that position") {
            val cnt = TestTokenContainer()
            cnt.addTokenAtEnd(CommonDynamicTerminals.unknown("A"))
            cnt.addTokenAtEnd(CommonDynamicTerminals.unknown("B"))
            cnt.addTokenAtEnd(CommonDynamicTerminals.unknown("C"))

            val builder = DefaultConcreteContentBuilder(cnt)
            builder.then(CommonDynamicTerminals.unknown("A"))

            cnt.assertTextJoinToEnd("A,B,C")
        }

        it("should add content at the start when no matching has taken place") {
            val cnt = TestTokenContainer()
            cnt.addTokenAtEnd(CommonDynamicTerminals.unknown("A"))
            cnt.addTokenAtEnd(CommonDynamicTerminals.unknown("B"))
            cnt.addTokenAtEnd(CommonDynamicTerminals.unknown("C"))

            val builder = DefaultConcreteContentBuilder(cnt)
            builder.then(CommonDynamicTerminals.unknown("D"))

            cnt.assertTextJoinToEnd("D,A,B,C")
        }

        it("should progress as it matches") {
            val cnt = TestTokenContainer()
            cnt.addTokenAtEnd(CommonDynamicTerminals.unknown("A"))
            cnt.addTokenAtEnd(CommonDynamicTerminals.unknown("B"))
            cnt.addTokenAtEnd(CommonDynamicTerminals.unknown("C"))

            val builder = DefaultConcreteContentBuilder(cnt)
            builder.then(CommonDynamicTerminals.unknown("A"))
            builder.then(CommonDynamicTerminals.unknown("1"))

            cnt.assertTextJoinToEnd("A,1,B,C")
        }

        it("should skip over unrecognised content to match where it can") {
            val cnt = TestTokenContainer()
            cnt.addTokenAtEnd(CommonDynamicTerminals.unknown("A"))
            cnt.addTokenAtEnd(CommonDynamicTerminals.unknown("B"))
            cnt.addTokenAtEnd(CommonDynamicTerminals.unknown("C"))

            val builder = DefaultConcreteContentBuilder(cnt)
            builder.then(CommonDynamicTerminals.unknown("B"))
            builder.then(CommonDynamicTerminals.unknown("1"))

            cnt.assertTextJoinToEnd("A,B,1,C")
        }

        it("should skip white space when adding") {
            val cnt = TestTokenContainer()
            cnt.addTokenAtEnd(CommonDynamicTerminals.unknown("A"))
            cnt.addTokenAtEnd(space())
            cnt.addTokenAtEnd(CommonDynamicTerminals.unknown("B"))
            cnt.addTokenAtEnd(space())
            cnt.addTokenAtEnd(CommonDynamicTerminals.unknown("C"))

            val builder = DefaultConcreteContentBuilder(cnt)
            builder.then(CommonDynamicTerminals.unknown("A"))
            builder.then(CommonDynamicTerminals.unknown("D"))

            cnt.assertTextJoinToEnd("A, ,D,B, ,C")
        }
    }

    describe("sections") {
        it("should handle sections correctly when added twice") {
            val cnt = TestTokenContainer()
            val item = TestElementSection()

            val builder = DefaultConcreteContentBuilder(cnt)
            builder.then(item)
            builder.then(item)
        }
    }
})

fun space(): DomWhitespaceNode {
    val result = DomWhitespaceNode()
    result.addTrailingSpace()
    return result
}

class TestElementSection : DomElementStartToken()

/**
 * Implement just enough of this for the requirements of the ConcreteContentBuilder
 */
class TestTokenContainer: DomTokenContainer {

    private val startToken = DomContentStartToken()

    fun assertTextJoinToEnd(value: String) {
        val test = StringBuilder()
        var firstNode : DomNode? = null

        asRecursiveTokenSequence().forEach {
            if (it is DomNode) {
                if (firstNode == null) {
                    firstNode = it
                }

                if (test.isNotEmpty()) {
                    test.append(",")
                }
                test.append(it.toFormattedString())
            }
        }

        firstNode?.assertJoinToEnd(value)
    }

    override fun tag(tag: Tag): Boolean {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun untag(tag: Tag): Boolean {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun isEmpty(): Boolean {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun isNotEmpty(): Boolean {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun contains(token: DomToken): Boolean {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun remove(token: DomToken): Boolean {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun asLocalTokenSequence(): Sequence<DomToken> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun asRecursiveTokenSequence(): Sequence<DomToken> = sequence {
        var candidate = startToken.next

        while (candidate != null) {

            if (candidate != startToken.end) {
                yield(candidate!!)
            }

            candidate = candidate.next
        }
    }

    override fun clear() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun addTokenAtStart(token: DomToken) {
        startToken.insertExtractedChainNext(token)
    }

    override fun addTokenAtEnd(token: DomToken) {
        startToken.end.insertExtractedChainPrior(token)
    }

    override fun expandRightToInclude(token: DomToken) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun wrap(startToken: DomToken, endToken: DomToken, option: TokenRangeOption) {
        TODO("Not yet implemented")
    }

    override fun expandLeftToInclude(token: DomToken) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}

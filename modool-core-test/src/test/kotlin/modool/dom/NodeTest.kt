package modool.dom

import modool.dom.node.DomElement
import modool.core.TestDomContext
import modool.core.meta.element.NoElementMeta
import org.amshove.kluent.`should be`
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it
import org.jetbrains.spek.api.dsl.on

object Example: Spek({
    describe("dom ancestry") {

        on("successful removeFromParent") {

            val child = object: DomElement<DomContext>(NoElementMeta, TestDomContext) { }
            val parent = object: DomElement<DomContext>(NoElementMeta, TestDomContext) { }

            child.attachTo(parent)
            child.detachFromParent()

            it("should no longer have a parent") {
                child.getParentOrNull() `should be` null
            }
        }
    }
})